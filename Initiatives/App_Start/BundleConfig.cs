﻿using System.Web;
using System.Web.Optimization;

namespace Initiatives
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/JSGlobalCompulsory").Include(
                        "~/Scripts/Compulsory/jquery.js",
                        "~/Scripts/Compulsory/jquery-migrate.js",
                        "~/Scripts/Compulsory/jquery.unobtrusive*",
                        "~/Scripts/Compulsory/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/Compulsory/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/Compulsory/jquery.validate.js",
                        "~/Scripts/Compulsory/jquery.validate.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/JSImplementingPlugins").Include(
                "~/Scripts/Implementation/jquery.dataTables.js",
                "~/Scripts/Implementation/moment.js",
                "~/Scripts/Implementation/datetime-moment.js",
                "~/Scripts/Implementation/dataTables.responsive.js",
                "~/Scripts/Implementation/dataTables.bootstrap.js",
                "~/Scripts/Implementation/dataTables.checkboxes.js",
                "~/Scripts/Implementation/waypoints.js",
                "~/Scripts/Implementation/dataTables.buttons.js",
                "~/Scripts/Implementation/buttons.flash.js",
                "~/Scripts/Implementation/jszip.js",
                "~/Scripts/Implementation/pdfmake.js",
                "~/Scripts/Implementation/vfs_fonts.js",
                "~/Scripts/Implementation/buttons.html5.js",
                "~/Scripts/Implementation/buttons.print.js",
                "~/Scripts/Implementation/jquery.counterup.js",
                "~/Scripts/Implementation/circles.js",
                "~/Scripts/Implementation/circles-master.js",
                "~/Scripts/Implementation/back-to-top.js",
                //"~/Scripts/Implementation/canvasjs.js",
                "~/Scripts/Implementation/smoothScroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/JSCustom").Include(
                "~/Scripts/Page/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/JSPages").Include(
                "~/Scripts/Page/main.js",
                //"~/Scripts/Page/chart.js",
                "~/Scripts/Page/viewOppo.js",
                "~/Scripts/Page/viewPO.js",
                "~/Scripts/Page/viewIO.js",
                "~/Scripts/Page/viewPR.js",
                "~/Scripts/Page/viewDO.js",
                "~/Scripts/Page/viewContract.js",
                "~/Scripts/Page/createOppo.js",
                "~/Scripts/Page/createPO.js",
                "~/Scripts/Page/createIO.js",
                "~/Scripts/Page/createPR.js",
                "~/Scripts/Page/inventory.js",
                "~/Scripts/Page/createDO.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Implementation/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/themes/ui-lightness/css").Include(
                                        "~/Content/themes/ui-lightness/jquery.ui.all.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}