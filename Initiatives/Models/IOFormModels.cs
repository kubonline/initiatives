﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Initiatives.Models
{
    
    public class IOFormModels
    {
        public List<IOFormList> IOFormListObj { get; set; }
        public string PONo { get; set; }
        public string RefNo { get; set; }
        public string IssueDate { get; set; }
        public string ToDept { get; set; }
        public string FromDept { get; set; }
        public string ExchangeName { get; set; }
        public string Justification { get; set; }
        public string ContractNo { get; set; }
        public string RequiredDate { get; set; }
        public string IssuedBy { get; set; }
        public class IOFormList
        {            
            public string MaterialNo { get; set; }
            public string ItemDescriptions { get; set; }
            public int ItemQuantity { get; set; }
            public string Remarks { get; set; }
            public string Justification { get; set; }
            public string ContractNo { get; set; }
            public string ExchangeName { get; set; }
        }
    }
}