﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Initiatives.Models
{
    public class DOFormModels
    {
        public List<DOFormList> DOFormListObj { get; set; }
        public string ToCust { get; set; }
        public string IssueDate { get; set; }
        public string TelephoneNo { get; set; }
        public string Region { get; set; }
        public string TrackingNo { get; set; }
        public string ContractNo { get; set; }
        public string PONo { get; set; }
        public string ContractName { get; set; }
        public string ContactPerson { get; set; }
        public string DONo { get; set; }
        public class DOFormList
        {
            public string MaterialNo { get; set; }
            public string Description { get; set; }
            public int DemandQty { get; set; }
            public int DeliveredQty { get; set; }
            public int DeliveryQty { get; set; }
            public int OutstandingQty { get; set; }
            public string ContractNo { get; set; }
            public string ContractName { get; set; }
            public string Address { get; set; }
            public string Region { get; set; }
            public string ExchangeName { get; set; }
        }
    }
}