//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Initiatives.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Activity
    {
        public System.Guid uuid { get; set; }
        public int activityId { get; set; }
        public Nullable<System.DateTime> activityDate { get; set; }
        public Nullable<int> custId { get; set; }
        public string project { get; set; }
        public string report { get; set; }
        public string actionPlan { get; set; }
        public string remark { get; set; }
        public Nullable<int> createById { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
    }
}
