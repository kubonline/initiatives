//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Initiatives.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompetitorStrength
    {
        public CompetitorStrength()
        {
            this.Opportunities = new HashSet<Opportunity>();
            this.OpportunityCompetitors = new HashSet<OpportunityCompetitor>();
        }
    
        public System.Guid uuid { get; set; }
        public string strengthId { get; set; }
        public string strengthLevel { get; set; }
    
        public virtual ICollection<Opportunity> Opportunities { get; set; }
        public virtual ICollection<OpportunityCompetitor> OpportunityCompetitors { get; set; }
    }
}
