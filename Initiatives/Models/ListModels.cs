﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Initiatives.Models
{
    public class UserListModel
    {
        public int userId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string SuperiorName { get; set; }
        public string Role { get; set; }
        public string TelephoneNo { get; set; }
        public string Address { get; set; }
        public int? ExtensionNo { get; set; }
    }
    public class CustomerListModel
    {
        public int custId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string CustRegNo { get; set; }
        public string Type { get; set; }
        public string HomeURL { get; set; }
        public string Address { get; set; }
        public int? TelephoneNo { get; set; }
    }
    public class PartNoListModel
    {
        public int partNo { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public string UoM { get; set; }
        public decimal UnitPrice { get; set; }
        public string OppoId { get; set; }
        public string ContName { get; set; }
    }
    public class ActivityListTable
    {

    }
    public class OppoListTable
    {
        public int Id { get; set; }
        public string Empty { get; set; }
        public string Abbreviation { get; set; }
        public string OpportunityName { get; set; }
        public string OppoType { get; set; }
        public string Description { get; set; }
        public string AccManagerName { get; set; }
        public string OpportunityValue { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Submission { get; set; }
        public string TargetAward { get; set; }
        public string Status { get; set; }
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
    }
    public class TechProviderList
    {
        public string TechName { get; set; }
        public string TechProduct { get; set; }
        public string appointmentId { get; set; }
        public string appointmentName { get; set; }
        public string TechSolution { get; set; }
    }   
    public class AppointmentList
    {
        public string appointmentId { get; set; }
        public string appointmentName { get; set; }
    }
    public class CompetitorList
    {
        public string CompetitorName { get; set; }
        public string CompetitorProduct { get; set; }
        public string CompetitorSolution { get; set; }
        public string CompStrengthId { get; set; }
        public string CompStrength { get; set; }
    }
    public class ContractListTable
    {
        public int Id { get; set; }
        public int? SupplierId { get; set; }
        public string Empty { get; set; }
        public string Customer { get; set; }
        public string ContractName { get; set; }
        public string ContractType { get; set; }
        public string ContractNo { get; set; }
        public string Description { get; set; }
        public string AssignTo { get; set; }
        public string ContractValue { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
    }
    public class POTrackingInfo
    {
        public string ProcessFlowName { get; set; }
        public DateTime? StartFlowDate { get; set; }
        public DateTime? EndFlowDate { get; set; }
        public string Remark { get; set; }
    }
    public class POExpectedList
    {
        public string Year { get; set; }
        public string TotalPrice { get; set; }
        public string TotalGST { get; set; }
        public string ExpectedAmount { get; set; }
    }
    public class POListTable
    {
        public int? POId { get; set; }
        public int? ContId { get; set; }
        public string Empty { get; set; }
        public string PONo { get; set; }
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? POIssueDate { get; set; }
        public decimal? TotalAmount { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
    }
    public class POItemsTable
    {
        public int ItemsId { get; set; }
        public int POId { get; set; }
        public int PartNo { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DeliveryDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DeliveredDate { get; set; }
        public int DeliveredQuantity { get; set; }
        public int OutstandingQuantity { get; set; }
        public int DeliveryQuantity { get; set; }
        public int RequestedQuantity { get; set; }
        public DateTime? IssueDate { get; set; }
        public int Quantity { get; set; }
        public string UoM { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string IONo { get; set; }
        public string DONo { get; set; }
        public string PONo { get; set; }
        public string PRNo { get; set; }
        public string POStatus { get; set; }
    }
    public class IOListTable
    {
        public int OppContId { get; set; }
        public int IOId { get; set; }

        [Required]
        public string IONo { get; set; }

        public string PONo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOIssueDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOCompleteDate { get; set; }
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public bool? IOUpdate { get; set; }
    }
    public class PRListTable
    {
        public int PRId { get; set; }

        [Required]
        public string PRNo { get; set; }
        public int OppContId { get; set; }
        public string PONo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PRIssueDate { get; set; }
        public string PurchaseType { get; set; }
        public string Justification { get; set; }
        public int? VendorId { get; set; }
        public string VendorName { get; set; }
        public string VendorPIC { get; set; }
        public string VendorContNo { get; set; }
        public string VendorEmail { get; set; }
        public int? ReqCompId { get; set; }
        public int? ReqId { get; set; }
        public string BudgetDescription { get; set; }
        public decimal BudgetedAmount { get; set; }
        public decimal UtilizedToDate { get; set; }
        public decimal BudgetBalance { get; set; }
        public decimal AmountRequired { get; set; }
        public string Remarks { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalGST { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PRCompleteDate { get; set; }
        public string LastActivity { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastActivityDate { get; set; }
        public bool? PRUpdate { get; set; }
    }
    public class DOListTable
    {
        public int OppContId { get; set; }
        public int DOId { get; set; }
        public int CustId { get; set; }
        public int? BranchId { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string DONo { get; set; }
        public int? DeliveryQuantity { get; set; }
        public string PONo { get; set; }

        [StringLength(50, ErrorMessage = "* Field cannot exceed {1} characters max.")]
        public string TelNo { get; set; }
        public string Region { get; set; }

        [StringLength(50, ErrorMessage = "* Field cannot exceed {1} characters max.")]
        public string TrackingNo { get; set; }

        [StringLength(50, ErrorMessage = "* Field cannot exceed {1} characters max.")]
        public string ContactPerson { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DOIssueDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DOCompleteDate { get; set; } 
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public bool? DOUpdate { get; set; }
        public bool SendEmail { get; set; }
    }
    public class IOItemsTable
    {
        public int ItemsId { get; set; }
        public int POId { get; set; }
        public string PONo { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public int DemandQuantity { get; set; }
        public int OutstandingQuantity { get; set; }
        public int RequestedQuantity { get; set; }
    }
    public class PRItemsTable
    {
        public int ItemsId { get; set; }
        public int POId { get; set; }
        public string PONo { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RequiredDate { get; set; }
        public int DemandQuantity { get; set; }
        public int RequestedQuantity { get; set; }
        public int OutstandingQuantity { get; set; }

        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        //[Range(0, 9999999999999999.99)]
        public decimal? UnitPrice { get; set; }

        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        //[Range(0, 9999999999999999.99)]
        public decimal? GST { get; set; }

        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        //[Range(0, 9999999999999999.99)]
        public decimal? TotalAmount { get; set; }
    }
    public class DOItemsTable
    {
        public int ItemsId { get; set; }
        public int POId { get; set; }
        public string PONo { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public int DemandQuantity { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int DeliveryQuantity { get; set; }
        public int DeliveredQuantity { get; set; }
        public int OutstandingQuantity { get; set; }
    }
    public class POReportListTable
    {
        public string MaterialNo { get; set; }
        //public string BranchAddress { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? POIssueDate { get; set; }
        public string PONo { get; set; }
        public string UnitPrice { get; set; }
        public string TotalPrice { get; set; }
        public string TotalGST { get; set; }
        public string TotalPOAmount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DeliveryDate { get; set; }
        public string IONo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOIssuedate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PRIssuedate { get; set; }
        public string PRNo { get; set; }
        public string CustName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DOIssueDate { get; set; }
        public string DONo { get; set; }
        public int? DeliveryQuantity { get; set; }
        public int? DeliveredQuantity { get; set; }
        public string Status { get; set; }
    }
    public class InventoryDetailInfo
    {
        [Required]
        public int PartNo { get; set; }
        public string MaterialCode {get;set;}
        public string MaterialDesc { get; set; }
        public int AvailableBalance { get; set; }
        public int BalanceReserved { get; set; }

        [Required]
        public DateTime ReceivedDate { get; set; }

        [Required]
        public int QuantityReceived { get; set; }
        public string UOM { get; set; }
        public decimal? UnitPrice { get; set; }
        public string ItemType { get; set; }

        [Required]
        public int ItemTypeId { get; set; }

        [Required]
        public string Supplier { get; set; }
        public string PurchasePONo { get; set; }
        public int AvailableQuantity { get; set; }

        [Required]
        public int RequestQuantity { get; set; }

        [Required]
        public int ProjectId { get; set; }
        public string Project { get; set; }

        [Required]
        public int POId { get; set; }
        public string ProjectPONo { get; set; }
        public string RequestBy { get; set; }
        public int RequestById { get; set; }

        [Required]
        public DateTime OutDate { get; set; }

        [Required]
        public int DOId { get; set; }
        public string DeliveryOrderNo { get; set; }
        public string Status { get; set; }

        [Required]
        public int StatusId { get; set; }
        public string Remark { get; set; }
    }
    public class TNIListTable
    {
        public int TnIId { get; set; }
        public string TnINo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string TestDate { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? StartTime { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? EndTime { get; set; }
        public string Description { get; set; }
        public string Venue { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AcceptanceDate { get; set; }
        public int OppContId { get; set; }
        public string Status { get; set; }
        public string LastActivity { get; set; }
        public DateTime? LastActivityDate { get; set; }
    }
    public class FilePODetailInfo
    {
        //public System.Guid uuid { get; set; }
        public int fileEntryId { get; set; }
        public int? opportunityId { get; set; }
        public int? contractId { get; set; }
        public int? uploadUserId { get; set; }
        public string uploadUserName { get; set; }
        public DateTime? uploadDate { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public byte[] File { get; set; }
    }
    public class QuestionAnswerModels
    {
        public int ChatId { get; set; }
        public int OpportunityId { get; set; }
        public int ContractId { get; set; }
        public string ContractName { get; set; }
        public string FromUserName { get; set; }
        public DateTime? CreateDate { get; set; }
        //public int? FromUserId { get; set; }            
        //public int ToUserID { get; set; }
        public string ToUserName { get; set; }
        public string Content { get; set; }
        public string QNAtype { get; set; }
    }
    public class TeamMember
    {
        public int? OpportunityId { get; set; }
        public int? ContractId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Remark { get; set; }
        public bool canView { get; set; }
        public bool canEdit { get; set; }
    }
    public class NotiMemberList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}