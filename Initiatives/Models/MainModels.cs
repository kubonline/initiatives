﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Initiatives.Models
{
    public class MainModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int? CompanyId { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public int OppContId { get; set; }
        public int POId {get;set;}
        public int IOId { get; set; }
        public int PRId { get; set; }
        public int DOId { get; set; }
        public int CreateByUserId { get; set; }
        public bool SaveSuccess { get; set; }   
        public bool UpdatePO { get; set; }
        public bool CreatePO { get; set; }
        public bool Status { get; set; }
        public int DepDivId { get; set; }

        [StringLength(100, ErrorMessage = "* Field cannot exceed {1} characters max.")]
        public string FileDescription { get; set; }
        public HttpPostedFileBase file { get; set; }
        public HttpPostedFileBase IntCheckList { get; set; }
        public HttpPostedFileBase InviLetter { get; set; }
        public LoginModel LoginObject { get; set; }
        public POExpectedField POExpectedObj { get; set; }
        public PODetailList POListDetail { get; set; }
        public POItemsTable POItemsObject { get; set; }
        public IOListTable IODetailObject { get; set; }
        public PRListTable PRDetailObject { get; set; }
        //public POItemsTable PRItemsObject { get; set; }
        public DOListTable DODetailObject { get; set; }
        public InventoryDetailInfo InvInOutObject { get; set; }
        //public TNIListTable TNIDetailObject { get; set; }
        public EditPermission objCanEdit { get; set; }
        public ActivityList ActivityDetail { get; set; }
        public OppoDetailList OppoListDetail { get; set; }
        public TechProviderList TechProviderObject { get; set; }
        public CompetitorList CompetitorObject { get; set; }
        public PartNoListModel PartNoObject { get; set; }
        public List<TechProviderList> TechProviderDetail { get; set; }
        public List<AppointmentList> AppointmentNameList { get; set; }
        public List<CompetitorList> CompetitorDetail { get; set; }
        public SWOTAnalysisList SWOTAnalysisDetail { get; set; }
        public RiskReviewList RiskReviewDetail { get; set; }
        public TeamPermission ViewEdit { get; set; }
        public List<UserListModel> UserListObject { get; set; }
        public List<CustomerListModel> CustomerListObject { get; set; }
        public List<PartNoListModel> PartNoListObject { get; set; }
        public List<ActivityListTable> ActivityListObject { get; set; }
        public List<OppoListTable> OppoListObject { get; set; }
        public List<ContractListTable> ContListObject { get; set; }
        public List<POTrackingInfo> POTrackingTable { get; set; }              
        public List<POExpectedList> POExpectedListObject { get; set; }       
        public List<POListTable> POListObject { get; set; }
        public List<POItemsTable> POItemListObject { get; set; }
        public List<POReportListTable> POReportListObject { get; set; }
        public List<TNIListTable> TNIListObject { get; set; }
        public List<IOListTable> IOListObject { get; set; }
        public List<IOItemsTable> IOItemListObject { get; set; }
        public List<PRListTable> PRListObject { get; set; }
        public List<PRItemsTable> PRItemListObject { get; set; }
        public List<DOListTable> DOListObject { get; set; }
        public List<DOItemsTable> DOItemListObject { get; set; }
        public List<InventoryDetailInfo> InventoryListTable { get; set; }
        public List<InventoryDetailInfo> InvInListTable { get; set; }
        public List<InventoryDetailInfo> InvOutListTable { get; set; }
        public List<FilePODetailInfo> ContDocListObject { get; set; }
        public List<FilePODetailInfo> IODocListObject { get; set; }
        public List<FilePODetailInfo> PRDocListObject { get; set; }
        public List<FilePODetailInfo> PODocListObject { get; set; }
        public List<FilePODetailInfo> DODocListObject { get; set; }
        public List<FilePODetailInfo> FileArchiveModel { get; set; }
        public List<QuestionAnswerModels> MessageListModel { get; set; }
        public List<TeamMember> TeamMemberList { get; set; }

        //public PPList PPDetail { get; set; }
        //public FPList FPDetail { get; set; }
        //public CEOOfficeList CEOOfficeDetail { get; set; }
        //public PO2SupplierList PO2SupplierDetail { get; set; }
        ////public DeliveryInfoList DeliveryInfoDetail { get; set; }
        //public InvFromVendorList InvFromVendorDetail { get; set; }
        //public InvToCustList InvToCustDetail { get; set; }
    }
}
