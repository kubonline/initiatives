﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Initiatives.Models
{
    public class PRFormModels
    {
        public List<PRFormList> PRFormListObj { get; set; }
        public string PRNo { get; set; }
        public string PRIssueDate { get; set; }       
        public string Justification { get; set; }
        public string Vendor { get; set; }
        public string ContactPIC { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string ReqCompany { get; set; }
        public string ReqName { get; set; }
        public string Designation { get; set; }
        public string DivDept { get; set; }
        public string PurchaseType { get; set; }
        public string BudgetDescription { get; set; }
        public string BudgetedAmount { get; set; }
        public string UtilizedToDate { get; set; }
        public string AmountRequired { get; set; }
        public string BudgetBalance { get; set; }
        public string Remarks { get; set; }
        public string TotalPrice { get; set; }
        public class PRFormList
        {
            public string CompanyName { get; set; }
            public string FullName { get; set; }
            public string Designation { get; set; }
            public string DivDept { get; set; }

            [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime DateRequired { get; set; }
            public string Description { get; set; }
            public string Remarks { get; set; }
            public string ItemCode { get; set; }
            public string PartNo { get; set; }
            public string PORefNo { get; set; }
            public int Quantity { get; set; }
            public string UM { get; set; }

            [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
            public decimal? UnitPrice { get; set; }
            public decimal? Total { get; set; }
            public decimal? GST { get; set; }           
            public decimal? Amount { get; set; }
        }
    }
}