﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Initiatives.Models
{
    public class LoginModel
    {
        [Required]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
    public class UserList
    {
        [Required]
        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 3)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 5)]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 3)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 5)]
        public string JobTitle { get; set; }
        public int? OneLevelSuperiorId { get; set; }
        public string TelephoneNo { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 10)]
        public string Address { get; set; }
        public int? ExtensionNo { get; set; }
        public string RoleId { get; set; }
        public int RoleIds { get; set; }
        public string Role { get; set; }
        public bool SaveSuccess { get; set; }
        public bool ReadOnly { get; set; }
        public bool Status { get; set; }
        public string UserLoginRole { get; set; }
    }
    public class CustomerList
    {
        [Required]
        [StringLength(150, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 4)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 2)]
        public string Abbreviation { get; set; }

        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 5)]
        public string RegistrationNo { get; set; }

        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 3)]
        public string CustomerType { get; set; }

        [StringLength(75, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 5)]
        public string CustomerURL { get; set; }

        [StringLength(150, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 10)]
        public string Address { get; set; }

        [Range(0, 9999999999999, ErrorMessage = "Value must be between 0 and 250")]
        public int? TelephoneNo { get; set; }
        public bool SaveSuccess { get; set; }
        public bool ReadOnly { get; set; }
    }
    public class PartNoList
    {
        [Required]
        public string MaterialNo { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string Description { get; set; }

        [StringLength(10, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 2)]
        public string UoM { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [StringLength(75, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 5)]
        public string ContName { get; set; }
        public bool ReadOnly { get; set; }
    }
    public class ActivityList
    {
        public DateTime ActivityDate { get; set; }
        public int CustId { get; set; }
        public string CustName { get; set; }
        public string Project { get; set; }
        public string Report { get; set; }
        public string ActionPlan { get; set; }
        public string Remark { get; set; }
    }
    public class OppoDetailList
    {
        public int CustId { get; set; }
        public string CustName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string OpportunityName { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 10)]
        public string Description { get; set; }
        public int AccManagerId { get; set; }
        public string AccManagerName { get; set; }
        //public decimal BudgetaryRevenue { get; set; }
        public bool? ApprovalId { get; set; }
        public string ApprovalbyLOB { get; set; }
        public string LOBApproval { get; set; }
        public string WinningChancesId { get; set; }
        public string WinningChances { get; set; }
        public string OppoTypeId { get; set; }
        public string OppoType { get; set; }
        public string CatId { get; set; }
        public string OppoCat { get; set; }

        [Required]
        public decimal OpportunityValue { get; set; }
        public int? TargetGP { get; set; }
        public string MajorProjectRisk { get; set; }
        public string SimilarProject { get; set; }
        public string ApprovingAuthority { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.DateTime)]
        public DateTime? Submission { get; set; }

        [StringLength(50, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string TargetAward { get; set; }                
        public string StatusId { get; set; }
        public string Status { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string ContractNo { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public string CauseOfLost { get; set; }
    }
    public class SWOTAnalysisList
    {
        public string Strength { get; set; }
        public string Weakness { get; set; }
        public string Opportunity { get; set; }
        public string Threat { get; set; }
    }
    public class RiskReviewList
    {
        public string Role { get; set; }
        public bool? SecA_Q1 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q1Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q1CeoComment { get; set; }
        public bool? SecA_Q2 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q2Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q2CeoComment { get; set; }
        public bool? SecA_Q3 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q3Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q3CeoComment { get; set; }
        public bool? SecA_Q4 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q4Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q4CeoComment { get; set; }
        public bool? SecA_Q5 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q5Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q5CeoComment { get; set; }
        public bool? SecA_Q6 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q6Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q6CeoComment { get; set; }
        public bool? SecA_Q7 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q7Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q7CeoComment { get; set; }
        public bool? SecA_Q8 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q8Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q8CeoComment { get; set; }
        public bool? SecA_Q9 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q9Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q9CeoComment { get; set; }
        public bool? SecA_Q10 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q10Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q10CeoComment { get; set; }
        public bool? SecA_Q11 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q11Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q11CeoComment { get; set; }
        public bool? SecA_Q12 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q12Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q12CeoComment { get; set; }
        public bool? SecA_Q13 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q13Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q13CeoComment { get; set; }
        public bool? SecA_Q14 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q14Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q14CeoComment { get; set; }
        public bool? SecA_Q15 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q15Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q15CeoComment { get; set; }
        public bool? SecA_Q16 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q16Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q16CeoComment { get; set; }
        public bool? SecA_Q17 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q17Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q17CeoComment { get; set; }
        public bool? SecA_Q18 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q18Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q18CeoComment { get; set; }
        public bool? SecA_Q19 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q19Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q19CeoComment { get; set; }
        public bool? SecA_Q20 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q20Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q20CeoComment { get; set; }
        public bool? SecA_Q21 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q21Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q21CeoComment { get; set; }
        public bool? SecA_Q22 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q22Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q22CeoComment { get; set; }
        public bool? SecA_Q23 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q23Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecA_Q23CeoComment { get; set; }
        public bool? SecB_Q1 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q1Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q1CeoComment { get; set; }
        public bool? SecB_Q2 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q2Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q2CeoComment { get; set; }
        public bool? SecB_Q3 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q3Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q3CeoComment { get; set; }
        public bool? SecB_Q4 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q4Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q4CeoComment { get; set; }
        public bool? SecB_Q5 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q5Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q5CeoComment { get; set; }
        public bool? SecB_Q6 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q6Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q6CeoComment { get; set; }
        public bool? SecB_Q7 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q7Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q7CeoComment { get; set; }
        public bool? SecB_Q8 { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q8Remark { get; set; }

        [StringLength(100, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 6)]
        public string SecB_Q8CeoComment { get; set; }

        [StringLength(300, ErrorMessage = "* Must be at least {2} characters long and {1} characters max.", MinimumLength = 10)]
        public string SecB_Q9Networking { get; set; }
        public int? SecB_Q9StaffId { get; set; }
        public string SecB_Q9StaffName { get; set; }
        public int? SecB_Q9CustId { get; set; }
        public string SecB_Q9CustName { get; set; }
    }
    public class PODetailList
    {
        [Required]
        public string Details_PONo { get; set; }

        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Details_POIssueDate { get; set; }
        public string Details_projectDescription { get; set; }
        public decimal Details_totalPrice { get; set; }
        public decimal Details_totalGST { get; set; }
        public string Status { get; set; }
        public int OppoId { get; set; }
        public bool? Details_Update { get; set; }
        public bool SaveSuccess { get; set; }
        public bool SendEmail { get; set; }
    }
    public class IOInfoList
    {
        //[Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOInfo_POReceivedDate { get; set; }

        //[Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOInfo_IOIssueDate { get; set; }
        public int? IOInfo_RefNo { get; set; }
        public int? IOInfo_PreparedById { get; set; }
        public int? IOInfo_ApprovedById { get; set; }
        public string IOInfo_VendorName { get; set; }

        //[Required]
        public string IOInfo_Remark { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? IOInfo_CompleteDate { get; set; }
        public bool IOInfo_Update { get; set; }
    }
    public class PRInfoList
    {
        //[Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PRInfo_IOReceivedDate { get; set; }
        public string PRInfo_PRNo { get; set; }
        public int? PRInfo_PreparedById { get; set; }
        public int? PRInfo_ApprovedById { get; set; }

        //[Required]
        public string PRInfo_Remark { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PRInfo_CompleteDate { get; set; }
        public bool PRInfo_Update { get; set; }
    }
    public class POExpectedField
    {
        public string SelectedYear { get; set; }

        [Required]
        public decimal ExpectedPO { get; set; }
    }
    public class EditPermission
    {
        public bool canEdits { get; set; }
        public bool? canUpload { get; set; }
        public bool? canDelete { get; set; }
    }
    public class TeamPermission
    {
        public bool canView { get; set; }
        public bool canEdit { get; set; }
    }

    //public class PPList
    //{
    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? PP_DocReceivedDate { get; set; }

    //    //[Required]
    //    public string PP_TypeOfDoc { get; set; }

    //    //[Required]
    //    public string PP_PONo { get; set; }

    //    //[Required]
    //    public string PP_Remark { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? PP_CompleteDate { get; set; }
    //    public bool PP_Update { get; set; }
    //}
    //public class FPList
    //{
    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FP_DocReceivedDate { get; set; }

    //    //[Required]
    //    public string FP_TypeOfDoc { get; set; }

    //    //[Required]
    //    public string FP_Remark { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FP_CompleteDate { get; set; }
    //    public bool FP_Update { get; set; }
    //}
    //public class CEOOfficeList
    //{
    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? CEOO_DocReceivedDate { get; set; }

    //    //[Required]
    //    public string CEOO_TypeOfDoc { get; set; }

    //    //[Required]
    //    public string CEOO_Remark { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? CEOO_CompleteDate { get; set; }
    //    public bool CEOO_Update { get; set; }
    //}
    //public class PO2SupplierList
    //{
    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? VS_DocReceivedDate { get; set; }

    //    //[Required]
    //    public string VS_TypeOfDoc { get; set; }

    //    //[Required]
    //    public string VS_Remarks { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? VS_POIssueDate { get; set; }
    //    public string VS_PONo { get; set; }
    //    public decimal? VS_POAmount { get; set; }
    //    public decimal? VS_TotalGST { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? VS_AwardDate { get; set; }
    //    public string VS_CompanyName { get; set; }
    //    public string VS_ContactPerson { get; set; }
    //    public string VS_TelephoneNo { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? VS_CompleteDate { get; set; }
    //    public bool VS_Update { get; set; }
    //}

    //public class DeliveryInfoList
    //{
    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? DOInfo_DocReceivedDate { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? DOInfo_ApprovedDate { get; set; }

    //    //[Required]
    //    public string DOInfo_DONo { get; set; }

    //    //[Required]
    //    public string DOInfo_Remark { get; set; }
    //    public string DOInfo_EI_Name { get; set; }
    //    public string DOInfo_EI_Position { get; set; }
    //    public string DOInfo_SI_Name { get; set; }
    //    public string DOInfo_SI_Position { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? DOInfo_CompleteDate { get; set; }
    //    public bool DOInfo_Update { get; set; }
    //}
    //public class InvFromVendorList
    //{
    //    public int? ModifyById { get; set; }
    //    public int ModifyDate { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_ReceivedDate { get; set; }

    //    //[Required]
    //    public string FromVendor_InvoiceNo { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_InvoiceDate { get; set; }
    //    public decimal FromVendor_Amount { get; set; }

    //    //[Required]
    //    public string FromVendor_Remark { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_PaymentDate { get; set; }
    //    public string FromVendor_ChequeNo { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_ChequeDate { get; set; }
    //    public decimal? FromVendor_Percentage { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_PreparedDate { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? FromVendor_ApprovedDate { get; set; }
    //    public bool FromVendor_Update { get; set; }
    //}
    //public class InvToCustList
    //{
    //    public int? ModifyById { get; set; }
    //    public int ModifyDate { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? ToCust_PreparedDate { get; set; }

    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? ToCust_CompleteDate { get; set; }

    //    //[Required]
    //    public string ToCust_Remark { get; set; }

    //    //[Required]
    //    [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? ToCust_InvoiceDate { get; set; }

    //    //[Required]
    //    public string ToCust_InvoiceNo { get; set; }
    //    public decimal ToCust_Amount { get; set; }
    //    public decimal? ToCust_Percentage { get; set; }
    //    public bool ToCust_Update { get; set; }
    //}
}
