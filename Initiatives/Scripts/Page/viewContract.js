﻿$(document).ready(function () {
    $(document).on("click", "#SubmitContPermission", function (e) {
        e.preventDefault();
        var fd = new FormData();
        var data = memberTable.$(".permission");
        $.each(data, function (i, input) {
            fd.append("TeamMemberList[" + i + "].UserId", $(input).find(".hide")[0].textContent);
            fd.append("TeamMemberList[" + i + "].Remark", $(input).find("input[name='Remark']").val());
            fd.append("TeamMemberList[" + i + "].canView", $(input).find("input[name='canView']")[0].checked);
            fd.append("TeamMemberList[" + i + "].canEdit", $(input).find("input[name='canEdit']")[0].checked);
        });
        $.ajax({
            type: "POST",
            url: UrlTeamPermission,
            data: fd,
            contentType: false,
            processData: false,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlTeamMember,
                    dataType: "json",
                    data: { OppContId: $("#ContractIds")[0].textContent },
                    cache: false,
                    success: function (data) {
                        $("#teammember-1").html(data);
                        $(".PermissionBorder").css({
                            "border-bottom-style": "solid",
                            "border-bottom-color": "#ddd",
                            "border-bottom-width": "1px"
                        });
                    }
                });
                alert("Update Permission Success!");
            }
        });
    });

    $(document).on("click", "#SubmitContNewMember", function (e) {
        e.preventDefault();
        var uid = $("#TeamMemberList option:selected").val();
        $.ajax({
            type: "POST",
            url: UrlAddTeamMember,
            data: { UserId: uid, OppContId: $("#ContractIds")[0].textContent },
            dataType: "json",
            cache: false,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlTeamMember,
                    data: { OppContId: $("#ContractIds")[0].textContent },
                    cache: false,
                    success: function (data) {
                        $("#teammember-1").html(data);
                        $(".PermissionBorder").css({
                            "border-bottom-style": "solid",
                            "border-bottom-color": "#ddd",
                            "border-bottom-width": "1px"
                        });
                        memberTable = $('#newMember').DataTable({
                            bAutoWidth: false,
                            bSort: false,
                            responsive: {
                                breakpoints: [
                                    { name: 'desktop', width: 1024 },
                                    { name: 'tablet', width: 768 },
                                    { name: 'phone', width: 480 }
                                ]
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: UrlQuestionAnswer,
                            data: { OppContId: $("#ContractIds")[0].textContent },
                            cache: false,
                            success: function (data) {
                                $("#qna-1").html(data);
                            }
                        });
                    }
                });
                alert("Add New Member Success!");
            }
        });
    });

    $(document).on("click", "#SubmitContRemoveMember", function (e) {
        e.preventDefault();
        var uid = $("#TeamMemberRemoveList option:selected").val();
        $.ajax({
            type: "POST",
            url: UrlRemoveTeamMember,
            data: { UserId: uid, OppContId: $("#ContractIds")[0].textContent },
            dataType: "json",
            cache: false,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlTeamMember,
                    data: { OppContId: $("#ContractIds")[0].textContent },
                    cache: false,
                    success: function (data) {
                        $("#teammember-1").html(data);
                        $(".PermissionBorder").css({
                            "border-bottom-style": "solid",
                            "border-bottom-color": "#ddd",
                            "border-bottom-width": "1px"
                        });
                        memberTable = $('#newMember').DataTable({
                            bAutoWidth: false,
                            bSort: false,
                            responsive: {
                                breakpoints: [
                                    { name: 'desktop', width: 1024 },
                                    { name: 'tablet', width: 768 },
                                    { name: 'phone', width: 480 }
                                ]
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: UrlQuestionAnswer,
                            data: { OppContId: $("#ContractIds")[0].textContent },
                            cache: false,
                            success: function (data) {
                                $("#qna-1").html(data);
                            }
                        });
                    }
                });
                alert("Remove Team Member Success!");
            }
        });
    });

    memberTable = $('#newMember').DataTable({
        bAutoWidth: false,
        bSort: false,
        paging: true,
        destroy: true,
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    $('#PurchaseOrder tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    POListTable = $('#PurchaseOrder').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                if (full.Status === "Red Alert") {
                    return '<a class="infoButton" id="POButton" title="One of the process have been delayed! Click here for more info" href="#"><img src="' + UrlRedalert + '" /></a>';
                } else if (full.Status === "Yellow Alert") {
                    return '<a class="infoButton" id="POButton" title="Some process are close to due date! Click here for more info" href="#"><img src="' + UrlYellowalert + '" /></a>';
                } else {
                    return '<a class="infoButton" id="POButton" title="All process are on track. Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
                }
            }
        }],
        aaSorting: [9, "desc"],
        initComplete: function () {
            var r = $('#PurchaseOrder tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#PurchaseOrder thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    POListTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#PurchaseOrder tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $("#PurchaseOrder tbody, #DOTable tbody, #IOTable tbody, #PRTable tbody").on("click", ".infoButton", function (e) {
        e.preventDefault(); var data;
        if ($(this)[0].id === "POButton") {
            data = POListTable.row($(this).parents('tr')).data();
            if (data[0] !== null) {
                var POId = data[0];
                $.ajax({
                    method: 'POST',
                    cache: false,
                    url: UrlPOTabs,
                    data: { 'POId': POId },
                    dataType: "json",
                    success: function (response) {
                        if (response.success)
                            window.location = response.url;
                    }
                });
            } else { alert("Please contact administrator for supports"); }
        } else if ($(this)[0].id === "IOButton") {
            data = IOTable.row($(this).parents('tr')).data();
            if (data[0] !== null) {
                var IOId = data[0];
                $.ajax({
                    method: 'POST',
                    cache: false,
                    url: UrlIOTabs,
                    data: { 'IOId': IOId },
                    dataType: "json",
                    success: function (response) {
                        if (response.success)
                            window.location = response.url;
                    }
                });
            } else { alert("Please contact administrator for supports"); }
        } else if ($(this)[0].id === "PRButton") {
            data = PRTable.row($(this).parents('tr')).data();
            if (data[0] !== null) {
                var PRId = data[0];
                $.ajax({
                    method: 'POST',
                    cache: false,
                    url: UrlPRTabs,
                    data: { 'PRId': PRId },
                    dataType: "json",
                    success: function (response) {
                        if (response.success)
                            window.location = response.url;
                    }
                });
            } else { alert("Please contact administrator for supports"); }
        } else if ($(this)[0].id === "DOButton") {
            data = DOTable.row($(this).parents('tr')).data();
            if (data[0] !== null) {
                var DOId = data[0];
                $.ajax({
                    method: 'POST',
                    cache: false,
                    url: UrlDOTabs,
                    data: { 'DOId': DOId },
                    dataType: "json",
                    success: function (response) {
                        if (response.success)
                            window.location = response.url;
                    }
                });
            } else { alert("Please contact administrator for supports"); }
        }
    });

    $('#bAddPO, #bAddIO, #bAddPR, #bAddDO').one('click', function (e) {
        e.preventDefault();
        if ($(this)[0].id === "bAddPO") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: UrlNewPO,
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
            $(this).prop('disabled', true);
        } else if ($(this)[0].id === "bAddIO") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: UrlNewIO,
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
            $(this).prop('disabled', true);
        } else if ($(this)[0].id === "bAddPR") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: UrlNewPR,
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
            $(this).prop('disabled', true);
        } else if ($(this)[0].id === "bAddDO") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: UrlNewDO,
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
            $(this).prop('disabled', true);
        }
    });

    POExpected = $('#POETable').DataTable({
        //data: aaDataPOE,
        bAutoWidth: false,
        destroy: true,
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    $(document).on("click", "#SubmitPOExpected", function (e) {
        e.preventDefault();
        var fd = new FormData();
        //fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#POExpected").serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.ajax({
            method: 'POST',
            url: UrlPOExpected,
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $('#poexpected-1').html("");
                    $('#poexpected-1').html(response.view);
                    alert("Save sucess");
                    $.ajax({
                        type: "POST",
                        url: UrlQuestionAnswer,
                        data: { OppContId: $("#ContractIds")[0].textContent },
                        cache: false,
                        success: function (data) {
                            $("#qna-1").html(data);
                        }
                    });
                } else if (response.success === false && response.exception === true) {
                    alert("Exception occured. Please contact admin");
                } else {
                    $('#poexpected-1').html("");
                    $('#poexpected-1').html(response.view);
                }
                POExpected = $('#POETable').DataTable({
                    destroy: true,
                    bAutoWidth: false,
                    responsive: {
                        breakpoints: [
                            { name: 'desktop', width: 1024 },
                            { name: 'tablet', width: 768 },
                            { name: 'phone', width: 480 }
                        ]
                    }
                });
            }
        });
    });

    $('#IOTable tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    IOTable = $('#IOTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                url = UrlGreenalert;
                return '<a class="infoButton" id="IOButton" title="All process are on track. Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        aaSorting: [6, "desc"],
        initComplete: function () {
            var r = $('#IOTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#IOTable thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    IOTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#IOTable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $('#PRTable tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    PRTable = $('#PRTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                url = UrlGreenalert;
                return '<a class="infoButton" id="PRButton" title="All process are on track. Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        aaSorting: [9, "desc"],
        initComplete: function () {
            var r = $('#PRTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#PRTable thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    PRTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#PRTable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $('#DOTable tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    DOTable = $('#DOTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                url = UrlGreenalert;
                return '<a class="infoButton" id="DOButton" title="All process are on track. Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        aaSorting: [7, "desc"],
        initComplete: function () {
            var r = $('#DOTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#DOTable thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    DOTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#DOTable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    //$('#POReportTable tfoot th').each(function () {
    //    if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
    //        var title = $(this).text();
    //        $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
    //    }
    //});

    POReportTable = $('#POReportTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B><"btn-FloatRight"f>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [4, "desc"],
        //initComplete: function () {
        //    var r = $('#POReportTable tfoot tr');
        //    r.find('th').each(function () {
        //        $(this).css('padding', 8);
        //    });
        //    $('#POReportTable thead').append(r);
        //    $('#search_0').css('text-align', 'center');
        //},
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    //POReportTable.columns().every(function () {
    //    var that = this;
    //    $('input', this.footer()).on('keyup change', function () {
    //        if (that.search() !== this.value) {
    //            that
    //                .search(this.value)
    //                .draw();
    //        }
    //    });
    //});
});