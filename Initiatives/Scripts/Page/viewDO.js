﻿$(document).ready(function () {
    DOdocUploader();

    $(document).on("click", "#VerifyDODetails", function (e) {
        e.preventDefault();
        $("#VDOD_DONo").val($("#DOD_DONo").val());
        $("#VDOD_BranchId").val($("#DOD_BranchAddress option:selected").text());
        $("#VDOD_IssueDate").val($("#DOD_IssueDate").val());
        $("#VDOD_TelNo").val($("#DOD_TelNo").val());
        $("#VDOD_TrackingNo").val($("#DOD_TrackingNo").val());
        $("#VDOD_ContactPerson").val($("#DOD_ContactPerson").val());
        $("#VDOD_FileDescription").val($("#DOD_FileDescription").val());
        $("#VDOD_File").val($("#DOD_Files").val());
        $("#DODetailsModal").modal("show");
    });

    $(document).on("click", "#SaveDOtoPDF", function () {
        window.location.href = UrlDOForm;
        return false;
    });

    $(document).on("click", "#SubmitDODetails", function (e) {
        e.preventDefault();
        var partialForm = "DODetails"; var modalInfo = "DODetailsModal";
        var modalLoading = "DODetailsLoading"; var fd = new FormData();

        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $("#" + modalInfo).modal("hide");
        $("#" + modalLoading).modal("show");
        $.ajax({
            url: UrlDODetails,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.success) {                   
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlDOUpdates,
                        data: {
                            DOId: $(result.view).find("#DOIds")[0].textContent
                        },
                        success: function (data) {
                            $("#DOUpdates-1").html(data);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlDODocManagement,
                        data: {
                            DOId: $(result.view).find("#DOIds")[0].textContent
                        },
                        success: function (data) {
                            $("#DODocManagement-1").html(data);
                            DOdocTable = $('#DODocList').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                            arcTable = $('.archive').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                        }
                    });
                    alert("Save Success!");
                }
                $('.firstTab').html(result.view);
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
                DOItemsTable = $('#DOItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });
            }
        });
    });

    $(document).on("click", "#UploadDOFile", function (e) {
        e.preventDefault();
        var fd = new FormData();
        fd.append("file", $('#DOUpdates').find('[name="file"]')[0].files[0]);
        fd.append("Description", $("#FileDesc").val());
        fd.append("UserId", $("#UserIds")[0].textContent);
        fd.append("DOId", $("#DOIds")[0].textContent);
        $.ajax({
            url: UrlDODocUpload,
            type: 'POST',
            cache: false,
            beforeSend: function () {
                $("#uploadModal").modal("show");
            },
            xhr: function () { // Custom XMLHttpRequest
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $('#uploadProgress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                        $('#uploadProgress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                    }
                }, false);
                return xhr;
            },
            success: function (result) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlDOUpdates,
                    data: {
                        DOId: $("#DOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#DOUpdates-1").html(data);
                    }
                });
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlDODocManagement,
                    data: {
                        DOId: $("#DOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#DODocManagement-1").html(data);
                        DOdocTable = $('#DODocument').DataTable({
                            bAutoWidth: false,
                            order: [
                                [4, "desc"]
                            ],
                            responsive: {
                                breakpoints: [{
                                    name: 'desktop',
                                    width: 1024
                                },
                                {
                                    name: 'tablet',
                                    width: 768
                                },
                                {
                                    name: 'phone',
                                    width: 480
                                }
                                ]
                            }
                        });
                    }
                });
                $('body').removeClass('modal-open');
                alert("Upload Success!");
            },
            error: function () { },
            data: fd,
            contentType: false,
            processData: false
        });
    });

    function DOdocUploader() {
        DOItemsTable = $('#DOItemsTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        DOdocTable = $('#DODocument').DataTable({
            bAutoWidth: false,
            destroy: true,
            order: [
                [4, "desc"]
            ],
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
    }

    $(document).on("click", ".ArchiveFile", function (e) {
        e.preventDefault();
        var selectedId = $(this)[0].id;
        $.ajax({
            type: "POST",
            cache: false,
            url: UrlFileArchive,
            data: {
                selectedId: selectedId
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlDODocManagement,
                    cache: false,
                    data: {
                        DOId: $("#DOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#DODocManagement-1").html(data);
                        POdocUploader();
                        alert("Archiving Success!");
                    }
                });
            }
        });
    });

    //QuestionAnswer.cshtml script
    $(document).on("click", "#DOSendMessage", function (e) {
        e.preventDefault();
        if ($("#MessageContent").val().trim() !== "") {
            var uid = $("#UserIds")[0].textContent;
            var doid = $("#DOIds")[0].textContent;
            var content = $("#MessageContent").val();
            var selectedUserId = [];
            $("#DOselMultiple :selected").each(function (i, selected) {
                selectedUserId[i] = $(selected).val();
            });
            if (selectedUserId.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: UrlDOSendMessage,
                    data: {
                        UserId: uid,
                        DOId: doid,
                        Content: content,
                        selectedUserId: selectedUserId
                    },
                    dataType: "json",
                    traditional: true,
                    cache: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (newdata) {
                        $.ajax({
                            type: "POST",
                            url: UrlDOUpdates,
                            data: {
                                DOId: $("#DOIds")[0].textContent
                            },
                            cache: false,
                            success: function (data) {
                                $("#DOUpdates-1").html(data);
                                $('body').removeClass('modal-open');
                                alert("Send Message Success!");
                            }
                        });
                    }
                });
            } else {
                alert("Please select the recipient");
                $(".responsive").modal("toggle");
                $('body').removeClass('modal-open');
            }
        } else {
            alert("Do not leave the message empty");
            $(".responsive").modal("toggle");
            $('body').removeClass('modal-open');
        }
    });
});