﻿var POTable; var rows_selected = [];

function updateDataTableSelectAllCtrl(POTable) {
    var $table = POTable.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function getTables() {
    $('#POTable tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    POTable = $('#POTable').DataTable({
        serverSide: false,
        autoWidth: false,
        paging: true,
        deferRender: true,
        columnDefs: [{
            targets: 0,
            searchable: false,
            orderable: false,
            width: '1%',
            className: 'dt-body-center',
            render: function (data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }],
        select: {
            style: 'multi'
        },
        rowCallback: function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        },
        order: [6, 'desc'],
        initComplete: function () {
            var r = $('#POTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#POTable thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        destroy: true,
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    POTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#POTable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $('#POTable tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        // Get row data
        var data = POTable.row($row).data();
        // Get row ID
        var rowId = data[0];
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);
            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(POTable);
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#POTable').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', POTable.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#POTable tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#POTable tbody input[type="checkbox"]:checked').trigger('click');
        }
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    POTable.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(POTable);
    });
}

$(document).ready(function () {
    getTables();

    $(document).on('click', '#submitDOPO', function (e) {
        e.preventDefault();
        if (rows_selected.length === 0) {
            alert("Please select PO No. first!");
            return false;
        }
        // Iterate over all selected checkboxes
        var fd = new FormData();
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        $.ajax({
            url: UrlGetMaterialNo,
            type: "POST",
            contentType: false,
            processData: false,
            data: fd
        })
            .success(function (data) {
                $("#DOItemsTableDiv").html(data);
                DOItemsTable = $('#DOItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    paging: false,
                    searching: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });                
            });
    });

    $(document).on('click', '#DOItemsTable tbody .RemoveDOItem', function (e) {
        e.preventDefault();
        DOItemsTable.row($(this).parents('tr')).remove().draw(false);
    });

    // Automatically add a first row of data
    //$('#addRow').click();

    $(document).on("click", "#CreateDO", function (e) {
        e.preventDefault();
        var partialForm = "CreateNewDO";
        var fd = new FormData(); var i = 0;
        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {            
            if (input.name === "DODetailObject.SendEmail" && input.value === "on") {
                fd.append(input.name, true);
            } else if (input.name === "DODetailObject.SendEmail" && input.value === "off") {
                fd.append(input.name, false);
            } else {
                fd.append(input.name, input.value);
            }
        });
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        var data = DOItemsTable.$(".ItemList");
        $.each(data, function (i, input) {
            fd.append("DOItemListObject[" + i + "].POId", $(input).find(".POId")[0].textContent);
            fd.append("DOItemListObject[" + i + "].ItemsId", $(input).find(".ItemsId")[0].textContent);
            fd.append("DOItemListObject[" + i + "].DeliveryQuantity", $(input).find("input[id='DOD_DeliveryQuantity']").val());
            fd.append("DOItemListObject[" + i + "].DeliveredQuantity", $(input).find(".delivered")[0].textContent);
            fd.append("DOItemListObject[" + i + "].OutstandingQuantity", $(input).find(".outstanding")[0].textContent);
        });
        $("#CreatePOLoading").modal("show");
        $.ajax({
            url: UrlCreateDO,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function () {
                $("#CreateDOLoading").modal("show");
            },
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    alert("Create DO success");
                    window.location = UrlViewContract + "#delivery-1";
                } else if (result.success === false && result.exception === true) {
                    alert("Exception occured. Please contact admin");
                } else if (result.success === false && result.haveDONo === true) {
                    alert("Create DO failed! Reason: Maybe the DONo already registered");
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitDOPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === true) {
                    alert(result.ErrorMessage);
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitDOPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === false) {
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitDOPO").trigger("click");
                    });                   
                }
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
    });
});