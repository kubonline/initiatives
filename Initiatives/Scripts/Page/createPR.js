﻿$(document).ready(function () {
    $(document).on('click', '#submitPRPO', function (e) {
        e.preventDefault();
        if (rows_selected.length === 0) {
            alert("Please select PO No. first!");
            return false;
        }
        // Iterate over all selected checkboxes
        var fd = new FormData();
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        $.ajax({
            url: UrlGetMaterialNoPR,
            type: "POST",
            contentType: false,
            processData: false,
            data: fd
        })
            .success(function (data) {
                $("#PRItemsTableDiv").html(data);
                PRItemsTable = $('#PRItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    paging: false,
                    searching: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            });
    });

    $(document).on('click', '#PRItemsTable tbody .RemovePRItem', function (e) {
        e.preventDefault();
        PRItemsTable.row($(this).parents('tr')).remove().draw(false);
    });

    $(document).on("change", ".UnitPrice", function () {
        var RequestedQuantity = $(this).parent().parent().parent().find("input[id='PRD_RequestedQuantity']").val();
        var TotalPrice = $(this).val() * RequestedQuantity;
        $(this).parent().parent().parent().find("input[id='PRD_TotalPrice']").val(TotalPrice);
    });

    $(document).on("change", ".RequestedQuantity", function () {
        var UnitPrice = $(this).parent().parent().parent().find("input[id='PRD_UnitPrice']").val();
        var TotalPrice = $(this).val() * UnitPrice;
        $(this).parent().parent().parent().find("input[id='PRD_TotalPrice']").val(TotalPrice);
    });

    $(document).on("click", "#CreatePR", function (e) {
        e.preventDefault();
        var partialForm = "CreateNewPR";
        var fd = new FormData(); var i = 0;
        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        var data = PRItemsTable.$(".ItemList");
        $.each(data, function (i, input) {
            fd.append("PRItemListObject[" + i + "].POId", $(input).find(".POId")[0].textContent);
            fd.append("PRItemListObject[" + i + "].ItemsId", $(input).find(".ItemsId")[0].textContent);
            fd.append("PRItemListObject[" + i + "].Remark", $("#PRD_Remark4Desc" + i).val());
            fd.append("PRItemListObject[" + i + "].RequiredDate", $(input).find("input[id='PRD_RequiredDate" + i + "']").val());
            fd.append("PRItemListObject[" + i + "].RequestedQuantity", $(input).find("input[id='PRD_RequestedQuantity']").val());
            fd.append("PRItemListObject[" + i + "].OutstandingQuantity", $(input).find(".outstanding")[0].textContent);
            fd.append("PRItemListObject[" + i + "].UnitPrice", $(input).find("input[id='PRD_UnitPrice']").val());
            fd.append("PRItemListObject[" + i + "].TotalAmount", $(input).find("input[id='PRD_TotalPrice']").val());
        });
        $("#CreatePOLoading").modal("show");
        $.ajax({
            url: UrlCreatePR,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function () {
                $("#CreatePRLoading").modal("show");
            },
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    alert("Create PR success");
                    window.location = UrlViewContract + "#pr-1";
                } else if (result.success === false && result.exception === true) {
                    alert("Exception occured. Please contact admin");
                } else if (result.success === false && result.havePRNo === true) {
                    alert("Create PR failed! Reason: Maybe the PRNo already registered");
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitPRPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === true) {
                    alert(result.ErrorMessage);
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitPRPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === false) {
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitPRPO").trigger("click");
                    });
                }
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
    });
});