﻿
$(document).ready(function () {    
    InventorySummaryTable = $('#InventorySummaryTable').DataTable({
        serverSide: false,
        //dom: 'frtiS',
        processing: true,
        paging: true,
        deferRender: true,
        stateSave: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [1, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    InventoryInTable = $('#InvInTable').DataTable({
        serverSide: false,
        //dom: 'frtiS',
        processing: true,
        paging: true,
        deferRender: true,
        stateSave: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [1, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    InventoryOutTable = $('#InvOutTable').DataTable({
        serverSide: false,
        //dom: 'frtiS',
        processing: true,
        paging: true,
        deferRender: true,
        stateSave: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [1, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    $(document).on("change", "#NewReqMaterialCode, #NewStockMaterialCode", function () {
        $.ajax({
            url: UrlPartNoInfo,
            method: 'GET',
            data: {
                PartNo: $(this).val()
            },
            success: function (resp) {
                $("#MaterialDescription").val(resp.desc);
                $("#AvailableQuantity").val(resp.availableQty);
            }
        });
    });

    $(document).on("change", "#Project", function () {
        $.ajax({
            url: UrlProjectInfo,
            method: 'GET',
            data: {
                ProjectId: $(this).val()
            },
            success: function (resp) {
                $("#ProjectPONo").html(resp.selectListPONo);
                $("#ProjectPONo").prop("disabled", false);
            }
        });
    });

    $(document).on("click", "#btn-BackDo, #btn-BackAddNewStock", function (e) {
        e.preventDefault();
        location.href = UrlContractList + "#Inventory-1";
    });

    $("#btn-SubmitNewReq").click(function (e) {
        e.preventDefault();
        var fd = new FormData(); var other_data;
        other_data = $("#AddNewReqForm").serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        fd.append("AvailableQuantity", $("#AvailableQuantity").val());
        $.ajax({
            url: UrlSubmitNewReq,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function (resp) {
                if (resp.success) {
                    alert("The new Inventory Item has been requested");
                    location.href = UrlContractList + "#Inventory-1";
                }
                else if (resp.success === false && resp.exception === false) {
                    alert("Validation error");
                    $.each(resp.data, function (key, input) {
                        //$('input[name="' + input.name + '"]').val(input.value);
                        $('span[data-valmsg-for="' + input.key + '"]').text(input.errors[0]);
                    });
                }
                else if (resp.success === false && resp.exception === true) {
                    alert(resp.message);
                    location.href = UrlContractList + "#Inventory-1";
                }
            }
        });
    });

    $("#btn-SubmitNewStock").click(function (e) {
        e.preventDefault();
        var fd = new FormData(); var other_data;
        other_data = $("#AddNewStockForm").serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.ajax({
            url: UrlSubmitNewStock,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function (resp) {
                if (resp.success) {
                    alert("The new Inventory Item has been saved");
                    location.href = UrlContractList + "#Inventory-1";
                }
                else if (resp.success === false && resp.exception === false) {
                    alert("Validation error");
                    $.each(resp.data, function (key, input) {
                        //$('input[name="' + input.name + '"]').val(input.value);
                        $('span[data-valmsg-for="' + input.key + '"]').text(input.errors[0]);
                    });
                }
                else if (resp.success === false && resp.exception === true) {
                    alert(resp.message);
                    location.href = UrlContractList + "#Inventory-1";
                }
            }
        });

    });

    $(document).on("click", "#clickHere", function () {
        data = InventorySummaryTable.row($(this).first('tr')).data();
        var PartNo = data[0];
        $.ajax({
            url: UrlCheckInvDetails,
            type: 'POST',
            data: { 'PartNo': PartNo },
            cache: false,
            dataType: "json",
            success: function (resp) {
                if (resp.success) {
                    window.location.href = URLInvInfo;
                    return false;
                }
            }
        });
        
    });
});