﻿$(document).ready(function () {
    docUploader();

    function docUploader() {
        OppodocTable = $('#OppoDocList').DataTable({
            bAutoWidth: false,
            destroy: true,
            order: [
                [4, "desc"]
            ],
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        TechProviderTable = $('#TechProviderTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        CompetitorTable = $('#CompetitorTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        SWOTAnalysisTable = $('#SWOTAnalysisTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        arcTable = $('.archive').DataTable({
            bAutoWidth: false,
            destroy: true,
            order: [
                [4, "desc"]
            ],
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        $(".PermissionBorder").css({
            "border-bottom-style": "solid",
            "border-bottom-color": "#ddd",
            "border-bottom-width": "1px"
        });
    }

    $(document).on("click", "#SubmitOppoDetails", function (e) {
        e.preventDefault();
        var partialForm; var form;
        if (this.className === "SubmitOppoDetails" || this.id === "SubmitOppoDetails") {
            partialForm = "PertinentInfo"; form = "info-1";
        }
        var fd = new FormData();
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.ajax({
            url: UrlOppoDetails,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                if (result.success === true) {                   
                    $.ajax({
                        type: "POST",
                        url: UrlQuestionAnswer,
                        data: { OppContId: $("#OppoIds")[0].textContent },
                        cache: false,
                        success: function (data) {
                            $("#QuestionAnswer-1").html(data);
                        }
                    });
                    alert("Save Success!");
                } else {
                    alert("Save Unsuccessful!");
                }
                $('#PertinentInfo-1').html(result.view);
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
        return false;
    });

    $(document).on('click', '#TechProviderTable tbody .RemoveTechProviderItem', function (e) {
        e.preventDefault();
        $('#TechProviderTable').DataTable().row($(this).parents('tr')).remove().draw(false);
    });

    $(document).on('click', '#CompetitorTable tbody .RemoveCompetitorItem', function (e) {
        e.preventDefault();
        $('#CompetitorTable').DataTable().row($(this).parents('tr')).remove().draw(false);
    });

    var j = 1; var k = 1;

    $('#AddTechProvider').on('click', function () {
        $('#TechProviderTable').DataTable().row.add([
            '<input type="text" id="Techname' + j + '" class="Techname form-control form-control-doc all" name="Name" />',
            '<input type="text" id="TechProduct' + j + '" class="TechProduct form-control form-control-doc all" name="Product" />',
            '<input type="text" id="TechSolution' + j + '" class="TechSolution form-control form-control-doc all" name="Solution" />',
            '<label class="select"><select class="form-control-static input-validation-error" data-val="true" data-val-number="The field appointmentId must be a number." data-val-required="The appointmentId field is required." id="Appointment' + j + '" name="TechProviderObject.appointmentId"><option value="AP01">Quotation</option><option value="AP02" >Tender</option><option value="AP03">Direct Nego</option></select></label>',
            '<button type="submit" class="RemoveTechProviderItem btn-u btn-u-primary">Delete</button>'
        ]).draw(false);

        j++;
    });

    $('#AddCompetitorTable').on('click', function () {
        $('#CompetitorTable').DataTable().row.add([
            '<input type="text" id="Competitorname' + k + '" class="Competitorname form-control form-control-doc all" name="Name" />',
            '<input type="text" id="CompetitorProduct' + k + '" class="CompetitorProduct form-control form-control-doc all" name="Product" />',
            '<input type="text" id="CompetitorSolution' + k + '" class="CompetitorSolution form-control form-control-doc all" name="Solution" />',
            '<label class="select"><select class="form-control-static" data-val="true" data-val-number="The field CompStrengthId must be a number." data-val-required="The CompStrengthId field is required." id="Strength' + k + '" name="CompetitorObject.CompStrengthId"><option value="S001">High</option><option value="S002">Medium</option><option value="S003">Low</option></select></label>',
            '<button type="submit" class="RemoveCompetitorItem btn-u btn-u-primary">Delete</button>'
        ]).draw(false);

        k++;
    });

    $(document).on("click", "#SubmitInitReport, #UpdateInitReport", function (e) {
        e.preventDefault();
        var fd = new FormData();
        var k = 0; var j = 0;
        var other_data = $("#InitiationReport").serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        var dataTech = TechProviderTable.$('input,select');
        var dataComp = CompetitorTable.$("input,select");
        var dataSWOT = SWOTAnalysisTable.$("textarea");
        $.each(dataTech, function (i, input) {
            if (input.name === "Name") {
                fd.append("TechProviderDetail[" + k + "].TechName", input.value);
            }
            if (input.name === "Product") {
                fd.append("TechProviderDetail[" + k + "].TechProduct", input.value);
            }
            if (input.name === "Solution") {
                fd.append("TechProviderDetail[" + k + "].TechSolution", input.value);
            }
            if (input.name === "TechProviderObject.appointmentId") {
                fd.append("TechProviderDetail[" + k + "].appointmentId", input.value.trim());
                k++;
            }
        });
        $.each(dataComp, function (i, input) {
            if (input.name === "Name") {
                fd.append("CompetitorDetail[" + j + "].CompetitorName", input.value);
            }
            if (input.name === "Product") {
                fd.append("CompetitorDetail[" + j + "].CompetitorProduct", input.value);
            }
            if (input.name === "Solution") {
                fd.append("CompetitorDetail[" + j + "].CompetitorSolution", input.value);
            }
            if (input.name === "CompetitorObject.CompStrengthId") {
                fd.append("CompetitorDetail[" + j + "].CompStrengthId", input.value.trim());
                j++;
            }
        });
        $.each(dataSWOT, function (i, input) {
            if (input.parentNode.id === "Strength") {
                fd.append("SWOTAnalysisDetail.Strength", input.value);
            }
            if (input.parentNode.id === "Weakness") {
                fd.append("SWOTAnalysisDetail.Weakness", input.value);
            }
            if (input.parentNode.id === "Opportunity") {
                fd.append("SWOTAnalysisDetail.Opportunity", input.value);
            }
            if (input.parentNode.id === "Threat") {
                fd.append("SWOTAnalysisDetail.Threat", input.value);
            }
        });
        if (this.id === "SubmitInitReport") {
            $.ajax({
                url: UrlCreateInitReport,
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    if (result.success === true) {
                        $('#initiation-1').html(result.view);
                        alert("Save Success!");
                    } else {
                        alert("Save Unsuccessful!");
                    }
                }
            });
        } else if (this.id === "UpdateInitReport") {
            $.ajax({
                url: UrlInitReport,
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    if (result.success === true) {
                        $('#initiation-1').html(result.view);
                        alert("Save Success!");
                    } else {
                        alert("Save Unsuccessful!");
                    }
                }
            });
        }
        
        return false;
    });

    $(document).on("click", "#SubmitOppoPermission", function (e) {
        e.preventDefault();
        var fd = new FormData();
        var data = memberTable.$(".permission");
        $.each(data, function (i, input) {
            fd.append("TeamMemberList[" + i + "].UserId", $(input).find(".hide")[0].textContent);
            fd.append("TeamMemberList[" + i + "].Remark", $(input).find("input[name='Remark']").val());
            fd.append("TeamMemberList[" + i + "].canView", $(input).find("input[name='canView']")[0].checked);
            fd.append("TeamMemberList[" + i + "].canEdit", $(input).find("input[name='canEdit']")[0].checked);
        });
        $.ajax({
            type: "POST",
            url: UrlTeamPermission,
            data: fd,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                //$.ajax({
                //    type: "POST",
                //    url: UrlTeamMember,
                //    dataType: "json",
                //    data: { OppContId: $("#OppoIds")[0].textContent },
                //    cache: false,
                //    success: function (data) {
                //        $("#teammember-1").html(data);
                //        $(".PermissionBorder").css({
                //            "border-bottom-style": "solid",
                //            "border-bottom-color": "#ddd",
                //            "border-bottom-width": "1px"
                //        });
                //    }
                //});
                if (result.success === true) {
                    $('#teammember-1').html(result.view);
                    alert("Update Permission Success!");
                } else {
                    alert("Save Unsuccessful!");
                }   
                memberTable = $('#newMember').DataTable({
                    bAutoWidth: false,
                    bSort: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [
                            { name: 'desktop', width: 1024 },
                            { name: 'tablet', width: 768 },
                            { name: 'phone', width: 480 }
                        ]
                    }
                });
                $(".PermissionBorder").css({
                            "border-bottom-style": "solid",
                            "border-bottom-color": "#ddd",
                            "border-bottom-width": "1px"
                        });
            }
        });
    });

    $(document).on("click", "#SubmitOppoNewMember", function (e) {
        e.preventDefault();
        var uid = $("#TeamMemberList option:selected").val();
        $.ajax({
            type: "POST",
            url: UrlAddTeamMember,
            data: { UserId: uid, OppContId: $("#OppoIds")[0].textContent },
            dataType: "json",
            cache: false,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlTeamMember,
                    data: { OppContId: $("#OppoIds")[0].textContent },
                    cache: false,
                    success: function (data) {
                        $("#teammember-1").html(data);
                        $(".PermissionBorder").css({
                            "border-bottom-style": "solid",
                            "border-bottom-color": "#ddd",
                            "border-bottom-width": "1px"
                        });
                        memberTable = $('#newMember').DataTable({
                            bAutoWidth: false,
                            bSort: false,
                            responsive: {
                                breakpoints: [
                                    { name: 'desktop', width: 1024 },
                                    { name: 'tablet', width: 768 },
                                    { name: 'phone', width: 480 }
                                ]
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: UrlQuestionAnswer,
                            data: { OppContId: $("#OppoIds")[0].textContent },
                            cache: false,
                            success: function (data) {
                                $("#QuestionAnswer-1").html(data);
                            }
                        });
                    }
                });
                alert("Add New Member Success!");
            }
        });
    });

    memberTable = $('#newMember').DataTable({
        bAutoWidth: false,
        bSort: false,
        destroy: true,
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    $(document).on("click", "#UploadOppoFile", function (e) {
        e.preventDefault();
        var fd = new FormData();
        fd.append("file", $('#QuestionAnswer').find('[name="file"]')[0].files[0]);
        fd.append("Description", $("#FileDesc").val());
        fd.append("UserId", $("#UserIds")[0].textContent);
        fd.append("OppContId", $("#OppoIds")[0].textContent);
        $.ajax({
            url: UrlOppoDocUpload,
            type: 'POST',
            cache: false,
            beforeSend: function () {
                $("#uploadModal").modal("show");
            },
            xhr: function () { // Custom XMLHttpRequest
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $('#uploadProgress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                        $('#uploadProgress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                    }
                }, false);
                return xhr;
            },
            success: function (result) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlQuestionAnswer,
                    data: {
                        OppContId: $("#OppoIds")[0].textContent
                    },
                    success: function (data) {
                        $("#QuestionAnswer-1").html(data);
                    }
                });
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlOppoDocManagement,
                    data: {
                        OppContId: $("#OppoIds")[0].textContent
                    },
                    success: function (data) {
                        $("#docmanagement-1").html(data);
                        docUploader();
                    }
                });
                $('body').removeClass('modal-open');
                alert("Upload Success!");
            },
            error: function () { },
            data: fd,
            contentType: false,
            processData: false
        });
    });

    $(document).on("click", ".ArchiveFile", function (e) {
        e.preventDefault();
        var selectedId = $(this)[0].id;
        $.ajax({
            type: "POST",
            cache: false,
            url: UrlOppoFileArchive,
            data: {
                selectedId: selectedId
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlOppoDocManagement,
                    cache: false,
                    data: {
                        OppContId: $("#OppoIds")[0].textContent
                    },
                    success: function (data) {
                        $("#DocManagement-1").html(data);
                        docUploader();
                        alert("Archiving Success!");
                    }
                });
            }
        });
    });

    $(document).on("click", "#OppoSendMessage", function (e) {
        e.preventDefault();
        if ($("#MessageContent").val().trim() !== "") {
            var uid = $("#UserIds")[0].textContent;
            var cid = $("#OppoIds")[0].textContent;
            var content = $("#MessageContent").val();
            var selectedUserId = [];
            $("#OpposelMultiple :selected").each(function (i, selected) {
                selectedUserId[i] = $(selected).val();
            });
            if (selectedUserId.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: UrlOppoSendMessage,
                    data: {
                        UserId: uid,
                        OppContId: cid,
                        Content: content,
                        selectedUserId: selectedUserId
                    },
                    dataType: "json",
                    traditional: true,
                    cache: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (newdata) {
                        $.ajax({
                            type: "POST",
                            url: UrlQuestionAnswer,
                            data: {
                                OppContId: $("#OppoIds")[0].textContent
                            },
                            cache: false,
                            success: function (data) {
                                $("#QuestionAnswer-1").html(data);
                                $('body').removeClass('modal-open');
                                alert("Send Message Success!");
                            }
                        });
                    }
                });
            } else {
                alert("Please select the recipient");
                $(".responsive").modal("toggle");
                $('body').removeClass('modal-open');
            }
        } else {
            alert("Do not leave the message empty");
            $(".responsive").modal("toggle");
            $('body').removeClass('modal-open');
        }
    });
});