﻿$(document).ready(function () {
    PRdocUploader();

    $(document).on("click", "#VerifyPRDetails", function (e) {
        e.preventDefault();
        $("#VPRD_PRNo").val($("#PRD_PRNo").val());
        $("#VPRD_IssueDate").val($("#PRD_IssueDate").val());
        $("#VPRD_VendorCompany").val($("#PRD_VendorCompany option:selected").text());
        $("#VPRD_VendorPIC").val($("#PRD_ContactPIC").val());
        $("#VPRD_VendorContNo").val($("#PRD_VendorContactNo").val());
        $("#VPRD_VendorEmail").val($("#PRD_VendorEmail").val());
        $("#VPRD_RequestorCompany").val($("#PRD_RequestorCompany option:selected").text());
        $("#VPRD_RequestorName").val($("#PRD_RequestorName option:selected").text());
        $("#VPRD_TotalPrice").val($("#PRD_TotalPrice").val());
        $("#VPRD_PurchaseType").val($("#PRD_PurchaseType").val());
        $("#VPRD_BudgetDescription").val($("#PRD_BudgetDescription").val());
        $("#VPRD_BudgetedAmount").val($("#PRD_BudgetedAmount").val());       
        $("#VPRD_UtilizedToDate").val($("#PRD_UtilizedToDate").val());
        $("#VPRD_AmountRequired").val($("#PRD_AmountRequired").val());
        $("#VPRD_BudgetBalance").val($("#PRD_BudgetBalance").val());
        $("#VPRD_Remarks").val($("#PRD_Remarks").val());
        $("#VPRD_Justification").val($("#PRD_Justification").val());
        $("#VPRD_FileDescription").val($("#PRD_FileDescription").val());
        $("#VPRD_File").val($("#PRD_Files").val());
        $("#PRDetailsModal").modal("show");
    });

    $(document).on("click", "#SavePRtoPDF", function () {
        window.location.href = UrlPRForm;
        return false;
    });

    $(document).on("click", "#SubmitPRDetails", function (e) {
        e.preventDefault();
        var partialForm = "PRDetails"; var modalInfo = "PRDetailsModal";
        var modalLoading = "PRDetailsLoading"; var fd = new FormData();

        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $("#" + modalInfo).modal("hide");
        $("#" + modalLoading).modal("show");
        $.ajax({
            url: UrlPRDetails,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.success) {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlPRUpdates,
                        data: {
                            PRId: $(result.view).find("#PRIds")[0].textContent
                        },
                        success: function (data) {
                            $("#PRUpdates-1").html(data);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlPRDocManagement,
                        data: {
                            PRId: $(result.view).find("#PRIds")[0].textContent
                        },
                        success: function (data) {
                            $("#PRDocManagement-1").html(data);
                            PRdocTable = $('#PRDocList').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                            arcTable = $('.archive').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                        }
                    });
                    alert("Save Success!");
                }
                $('.firstTab').html(result.view);
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
                PRItemsTable = $('#PRItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });
            }
        });
    });

    $(document).on("click", "#UploadPRFile", function (e) {
        e.preventDefault();
        var fd = new FormData();
        fd.append("file", $('#PRUpdates').find('[name="file"]')[0].files[0]);
        fd.append("Description", $("#FileDesc").val());
        fd.append("UserId", $("#UserIds")[0].textContent);
        fd.append("PRId", $("#PRIds")[0].textContent);
        $.ajax({
            url: UrlPRDocUpload,
            type: 'POST',
            cache: false,
            beforeSend: function () {
                $("#uploadModal").modal("show");
            },
            xhr: function () { // Custom XMLHttpRequest
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $('#uploadProgress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                        $('#uploadProgress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                    }
                }, false);
                return xhr;
            },
            success: function (result) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlPRUpdates,
                    data: {
                        PRId: $("#PRIds")[0].textContent
                    },
                    success: function (data) {
                        $("#PRUpdates-1").html(data);
                    }
                });
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlPRDocManagement,
                    data: {
                        PRId: $("#PRIds")[0].textContent
                    },
                    success: function (data) {
                        $("#PRDocManagement-1").html(data);
                        PRdocTable = $('#PRDocument').DataTable({
                            bAutoWidth: false,
                            order: [
                                [4, "desc"]
                            ],
                            responsive: {
                                breakpoints: [{
                                    name: 'desktop',
                                    width: 1024
                                },
                                {
                                    name: 'tablet',
                                    width: 768
                                },
                                {
                                    name: 'phone',
                                    width: 480
                                }
                                ]
                            }
                        });
                    }
                });
                $('body').removeClass('modal-open');
                alert("Upload Success!");
            },
            error: function () { },
            data: fd,
            contentType: false,
            processData: false
        });
    });

    function PRdocUploader() {
        PRItemsTable = $('#PRItemsTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        PRdocTable = $('#PRDocument').DataTable({
            bAutoWidth: false,
            destroy: true,
            order: [
                [4, "desc"]
            ],
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
    }

    $(document).on("click", ".ArchiveFile", function (e) {
        e.preventDefault();
        var selectedId = $(this)[0].id;
        $.ajax({
            type: "POST",
            cache: false,
            url: UrlFileArchive,
            data: {
                selectedId: selectedId
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlPRDocManagement,
                    cache: false,
                    data: {
                        PRId: $("#PRIds")[0].textContent
                    },
                    success: function (data) {
                        $("#PRDocManagement-1").html(data);
                        PRdocUploader();
                        alert("Archiving Success!");
                    }
                });
            }
        });
    });
    //QuestionAnswer.cshtml script
    $(document).on("click", "#PRSendMessage", function (e) {
        e.preventDefault();
        if ($("#MessageContent").val().trim() !== "") {
            var uid = $("#UserIds")[0].textContent;
            var prid = $("#PRIds")[0].textContent;
            var content = $("#MessageContent").val();
            var selectedUserId = [];
            $("#PRselMultiple :selected").each(function (i, selected) {
                selectedUserId[i] = $(selected).val();
            });
            if (selectedUserId.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: PRSendMessage,
                    data: {
                        UserId: uid,
                        PRId: prid,
                        Content: content,
                        selectedUserId: selectedUserId
                    },
                    dataType: "json",
                    traditional: true,
                    cache: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (newdata) {
                        $.ajax({
                            type: "POST",
                            url: UrlPRUpdates,
                            data: {
                                PRId: $("#PRIds")[0].textContent
                            },
                            cache: false,
                            success: function (data) {
                                $("#PRUpdates-1").html(data);
                                $('body').removeClass('modal-open');
                                alert("Send Message Success!");
                            }
                        });
                    }
                });
            } else {
                alert("Please select the recipient");
                $(".responsive").modal("toggle");
                $('body').removeClass('modal-open');
            }
        } else {
            alert("Do not leave the message empty");
            $(".responsive").modal("toggle");
            $('body').removeClass('modal-open');
        }
    });
});