﻿$(document).ready(function () {
    //$.getJSON("/Dashboard/GetData/", function (data) {
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
            text: "Revenues by Segment",
            fontWeight: "normal",
            fontSize: 20
        },
        legend: {
            fontSize: 14,
            fontFamily: "Helvetica"
        },
        axisY: {
            title: "RM mil",
            valueFormatString: "#,###,,.##M",
        },
        data: [
            {
                type: "stackedColumn",
                yValueFormatString: "#,###,,.##M",
                legendText: "Previous Project",
                showInLegend: "false",
                dataPoints: [
                    { y: 85900000, label: "2017" },
                    { y: 108200000, label: "2018" },
                    { y: 125900000, label: "2019" },
                ],

                //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
                //dataPoints: @Html.Raw(ViewBag.DataPoints2),
            }, {
                type: "stackedColumn",
                legendText: "AFC",
                yValueFormatString: "#,###,,.##M",
                showInLegend: "true",
                dataPoints: [
                    { y: 5800000, label: "2017" },
                    { y: 13500000, label: "2018" },
                    { y: 9100000, label: "2019" },
                ],

                //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
                //dataPoints: @Html.Raw(ViewBag.DataPoints2),
            },
        {
            type: "stackedColumn",
            legendText: "Telco Towers",
            yValueFormatString: "#,###,,.##M",
            showInLegend: "true",
            dataPoints: [
                { y: 5500000, label: "2017" },
                { y: 1700000, label: "2018" },
                { y: 2200000, label: "2019" },
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints1),
        }, {
            type: "stackedColumn",
            legendText: "Others",
            showInLegend: "true",
            indexLabel: "#total",
            yValueFormatString: "#,###,,.##M",
            indexLabelFormatString: "#0.#,.",
            indexLabelPlacement: "outside",
            dataPoints: [
                { y: 2200000, label: "2017" },
                { y: 3200000, label: "2018" },
                { y: 3200000, label: "2019" },
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints2),
        }
        ]
    });
    var chart2 = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        title: {
            text: "Secured vs Potential Projects",
            fontWeight: "normal",
            fontSize: 20
        },
        axisY: {
            title: "RM mil",
            valueFormatString: "#,###,,.##M",
        },
        legend: {
            fontSize: 14,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "stackedColumn",
            legendText: "Secured",
            yValueFormatString: "#,###,,.##M",
            showInLegend: "true",
            dataPoints: [
                { y: 28100000, label: "2017" },
                { y: 32400000, label: "2018" },
                { y: 31700000, label: "2019" },
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints1),
        }, {
            type: "stackedColumn",
            legendText: "Potential",
            showInLegend: "true",
            indexLabel: "#total",
            yValueFormatString: "#,###,,.##M",
            indexLabelFormatString: "#,###,,.##M",
            indexLabelPlacement: "outside",
            dataPoints: [
                { y: 71300000, label: "2017" },
                { y: 94300000, label: "2018" },
                { y: 108700000, label: "2019" },
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints2),
        }
        ]
    });
    //var chart = new CanvasJS.Chart("chartContainer",
    //    {
    //        title: {
    //            text: "Tender Participation 2015 & 2016"
    //        },
    //        data: [
    //      {
    //          type: "stackedColumn",
    //          dataPoints: [
    //          { y: 547010000, label: "Opportunity", toolTipContent: "{label}: {y}" },
    //          { y: 428030000, label: "Quoted", indexLabel: "Tender", indexLabelFontColor: "black", toolTipContent: "{indexLabel}: {y}" },
    //          { y: 156950000, label: "Forecast", toolTipContent: "{label}: {y}" },
    //          { y: 161930000, label: "Result", indexLabel: "Yes", indexLabelFontColor: "black", toolTipContent: "{indexLabel}: {y}" }
    //          ]
    //      }, {
    //          type: "stackedColumn",
    //          dataPoints: [
    //         { y: 0, label: "Opportunity", toolTipContent: "{label}: {y}" },
    //         { y: 55980000, label: "Quoted", indexLabel: "Direct Nego", indexLabelFontColor: "black", toolTipContent: "{indexLabel}: {y}" },
    //         { y: 0, label: "Forecast", toolTipContent: "{label}: {y}" },
    //         { y: 171490000, label: "Result", indexLabel: "Pending", indexLabelFontColor: "black", toolTipContent: "{indexLabel}: {y}" }
    //          ]
    //      }, {
    //          type: "stackedColumn",
    //          dataPoints: [
    //         { y: 0, label: "Opportunity", toolTipContent: "{label}: {y}" },
    //         { y: 0, label: "Quoted", toolTipContent: "{label}: {y}" },
    //         { y: 0, label: "Forecast", toolTipContent: "{label}: {y}" },
    //         { y: 213600000, label: "Result", indexLabel: "No", indexLabelFontColor: "black", toolTipContent: "{indexLabel}: {y}" }
    //          ]
    //      }
    //        ]
    //    });
    //});
    //debugger;
    var chart3 = new CanvasJS.Chart("chartContainer3", {
        title: {
            text: "Total Revenue by Projects (2017) = RM99.4M",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        //theme: "theme2",
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{y}%",
            dataPoints: [
                { y: 25.25, legendText: "Secured Project (RM)", label: "25.1M" },
                { y: 74.75, legendText: "Unsecured Project (RM)", label: "74.3M" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    var chart4 = new CanvasJS.Chart("chartContainer4", {
        title: {
            text: "Total Opportunities Target (2017) for Telco & Celco Equipment",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{legendText} {y}%",
            dataPoints: [
                { y: 90, legendText: "Opportunity bidding", label: "9 Entries" },
                { y: 10, legendText: "Opportunity Won", label: "1 Entries" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    var chart5 = new CanvasJS.Chart("chartContainer5", {
        title: {
            text: "Total Opportunities Target (2017) for Transportation",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{legendText} {y}%",
            dataPoints: [
                { y: 75, legendText: "Opportunity bidding", label: "3 Entries" },
                { y: 25, legendText: "Opportunity Won", label: "1 Entries" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    var chart6 = new CanvasJS.Chart("chartContainer6", {
        title: {
            text: "Total Opportunities Target (2017) for Telco Tower (MCMC)",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{legendText} {y}%",
            dataPoints: [
                { y: 75, legendText: "Opportunity bidding", label: "3 Entries" },
                { y: 25, legendText: "Opportunity Won", label: "1 Entries" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    var chart7 = new CanvasJS.Chart("chartContainer7", {
        title: {
            text: "Total Opportunities Target (2017) for Telco Tower (Build & Lease)",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{legendText} {y}%",
            dataPoints: [
                { y: 75, legendText: "Opportunity bidding", label: "3 Entries" },
                { y: 25, legendText: "Opportunity Won", label: "1 Entries" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    var chart8 = new CanvasJS.Chart("chartContainer8", {
        title: {
            text: "Total Opportunities Target (2017) for IoT & Managed Services",
            fontWeight: "normal",
            fontSize: 20
        },
        animationEnabled: true,
        legend: {
            fontSize: 16,
            fontFamily: "Helvetica"
        },
        data: [
        {
            type: "doughnut",
            indexLabelFontFamily: "Garamond",
            indexLabelFontSize: 15,
            indexLabel: "{label}",
            startAngle: -20,
            showInLegend: true,
            toolTipContent: "{legendText} {y}%",
            dataPoints: [
                { y: 75, legendText: "Opportunity bidding", label: "3 Entries" },
                { y: 25, legendText: "Opportunity Won", label: "1 Entries" }
            ],

            //You can add dynamic data from the controller as shown below. Check the controller and uncomment the line which generates dataPoints.
            //dataPoints: @Html.Raw(ViewBag.DataPoints),
        }
        ]
    });
    chart.render();
    chart2.render();
    chart3.render();
    chart4.render();
    chart5.render();
    chart6.render();
    chart7.render();
    chart8.render();
});