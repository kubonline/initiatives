﻿$(document).on("click", "#SubmitOppoDetails", function (e) {
    e.preventDefault();
    var partialForm; var form;
    if (this.className === "SubmitOppoDetails" || this.id === "SubmitOppoDetails") {
        partialForm = "PertinentInfo"; form = "info-1";
    }
    var fd = new FormData();
    var other_data = $("#" + partialForm).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    $.ajax({
        url: UrlOppoDetails,
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (result) {
            if (result.success === true) {
                $('#PertinentInfo-1').html(result.view);
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
                alert("Save Success!");
            } else {
                alert("Save Unsuccessful!");
            }
        }
    });
    return false;
});

if ($(this)[0].pathname == "/Initiatives/Home/NewPassword") {
    if ($(this)[0].pathname == "/Home/NewPassword") {
        var data = custTable.row($(this).parents('tr')).data();
        var cId = data[0];
        //debugger;
        $.ajax({
            method: 'POST',
            cache: false,
            url: $("#actionNewPassowrd").attr('href'),
            data: { 'cId': cId },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    //debugger;
                    window.location = response.url;
                };
            }
        });
    }
}
    var OppoTable = $('#Opportunity').DataTable({
        serverSide: false,
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
            }
        }],
        aaSorting: [12, "desc"],
        initComplete: function () {
            var r = $('#Opportunity tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#Opportunity thead').append(r);
            $('#search_0').css('text-align', 'center');
            $("#Opportunity").removeClass("hidden");
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    $('#Opportunity tbody').on('dblclick', 'tr td:nth-child(1),tr td:nth-child(2)', function (e) {
        e.preventDefault();
        var data = table.row($(this).parents('tr')).data();
        clickInfo(data);
    });

    OppoTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

                $('#TnITable tfoot th').each(function () {
        if ($(this)[0].textContent != "" && $(this)[0].textContent != "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    TnITable = $('#TnITable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                url = UrlGreenalert;
                return '<a class="infoButton" title="All process are on track. Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        //aaSorting: [8, "desc"],
        initComplete: function () {
            var r = $('#TnITable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#TnITable thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    TnITable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#TnITable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $("#TnITable tbody").on("click", ".infoButton", function (e) {
        e.preventDefault();
        if ($(this)[0].pathname == UrlTnIList) {
            var data = POListTable.row($(this).parents('tr')).data();
            if (data[0] != null) {
                var POId = data[0]
                $.ajax({
                    method: 'POST',
                    cache: false,
                    url: $("#actionPO").attr('href'),
                    data: { 'POId': POId },
                    dataType: "json",
                    success: function (response) {
                        if (response.success) {
                            window.location = response.url;
                        };
                    }
                });
            } else { alert("Please contact administrator for supports") }
        }
    });