﻿function POdocUploader() {
    POItemsTable = $('#POItemsTable').DataTable({
        bAutoWidth: false,
        ordering: false,
        paging: false,
        searching: false,
        destroy: true,
        columnDefs: [
            { width: '10%', targets: [1, 3, 4, 5, 6, 7] }
            , { width: '40%', targets: [2] }
            ],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });
    POdocTable = $('#PODocList').DataTable({
        bAutoWidth: false,
        destroy: true,
        order: [
            [4, "desc"]
        ],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });
}

function POdocUploader2() {
    POItemsTable = $('#POItemsTable').DataTable({
        bAutoWidth: false,
        ordering: false,
        paging: false,
        searching: false,
        destroy: true,
        columnDefs: [
            { width: '10%', targets: [0, 2, 3, 4, 5, 6] }
            , { width: '5%', targets: [7] }
            , { width: '35%', targets: [1] }
        ],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });
    POdocTable = $('#PODocList').DataTable({
        bAutoWidth: false,
        destroy: true,
        order: [
            [4, "desc"]
        ],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });
}

$(document).ready(function () {   
    var form_id = $(this)[0].forms[0].id;
    if (form_id != "CreateNewPO") {
        POdocUploader();
    } else {
        POdocUploader2();
    }
    
    $(document).on("click", "#VerifyPODetails", function (e) {
        e.preventDefault();
        if (this.id === "VerifyPODetails") {
            $("#VPOD_PONo").val($("#POD_PONo").val());
            $("#VPOD_IssueDate").val($("#POD_IssueDate").val());
            $("#VPOD_Description").val($("#POD_Description").val());
            $("#VPOD_TotalPrice").val($("#POD_TotalPrice").val());
            $("#VPOD_TotalGST").val($("#POD_TotalGST").val());
            $("#VPOD_FileDescription").val($("#POD_FileDescription").val());
            $("#VPOD_File").val($("#POD_Files").val());
            $("#PODetailsModal").modal("show");
        }
    });
    
    $(document).on("click", ".SubmitPOItemDetails", function (e) {
        e.preventDefault();
        var fd = new FormData(); var i = 0;
        var data = POItemsTable.$("tr");
        if ($(this)[0].id === "UpdatePO") {
            fd.append("UpdatePO", true);
            $.each(data, function (i, input) {
                fd.append("POItemListObject[" + i + "].ItemsId", $(input).find("input[name='ItemsId']").val());
                fd.append("POItemListObject[" + i + "].PartNo", $(input).find("select[name='item.PartNo']").val());
                fd.append("POItemListObject[" + i + "].MaterialNo", $(input).find(".MaterialNo option:selected").text());
                fd.append("POItemListObject[" + i + "].Description", $(input).find("input[name='Description']").val());
                fd.append("POItemListObject[" + i + "].DeliveryDate", $(input).find(".delivered").val());
                fd.append("POItemListObject[" + i + "].Quantity", $(input).find("input[name='Quantity']").val());
                fd.append("POItemListObject[" + i + "].UoM", $(input).find("input[name='UoM']").val());
                fd.append("POItemListObject[" + i + "].UnitPrice", $(input).find("input[name='UnitPrice']").val());
                fd.append("POItemListObject[" + i + "].TotalPrice", $(input).find("input[name='TotalPrice']").val());
            });
        } else if ($(this)[0].id === "CreatePO") {
            fd.append("CreatePO", true);
            $.each(data, function (i, input) {
                fd.append("POItemListObject[" + i + "].PartNo", $(input).find(".MaterialNo").val());
                fd.append("POItemListObject[" + i + "].MaterialNo", $(input).find(".MaterialNo option:selected").text());
                fd.append("POItemListObject[" + i + "].Description", $(input).find("input[name='Description']").val());
                fd.append("POItemListObject[" + i + "].DeliveryDate", $(input).find(".delivered").val());
                fd.append("POItemListObject[" + i + "].Quantity", $(input).find("input[name='Quantity']").val());
                fd.append("POItemListObject[" + i + "].UoM", $(input).find("input[name='UoM']").val());
                fd.append("POItemListObject[" + i + "].UnitPrice", $(input).find("input[name='UnitPrice']").val());
                fd.append("POItemListObject[" + i + "].TotalPrice", $(input).find("input[name='TotalPrice']").val());
            });
        }
        
        $("#PODetailsLoading").modal("show");
        $.ajax({
            url: UrlPOItemDetails,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.success) {
                    alert("Create PO Item success");
                    window.location = UrlViewPO;
                } else {
                    $('.wrapper').html("");
                    $('.wrapper').html(result.view);
                }
            }
        });
    });

    $(document).on("click", "#SubmitPODetails", function (e) {
        e.preventDefault();
        var partialForm; var form; var url; var modalInfo; var modalLoading;
        if (this.className === "SubmitPODetails" || this.id === "SubmitPODetails") {
            partialForm = "PODetails"; form = "PODetails-1"; modalInfo = "PODetailsModal"; modalLoading = "PODetailsLoading";
            url = UrlPODetails;
        }
        var fd = new FormData();
        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $("#" + modalInfo).modal("hide");
        $("#" + modalLoading).modal("show");
        $.ajax({
            url: url,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function (result) {
                var saveSuccess = $('#' + form).html(result).find('[id="POSS"]')[0].textContent;
                if (saveSuccess === "True") {
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: url,
                        success: function (data) {
                            $("#" + form).html(data);
                            POItemsTable = $('#POItemsTable').DataTable({
                                bAutoWidth: false,
                                ordering: false,
                                destroy: true,
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                        }
                    });
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlPOQuestionAnswer,
                        data: {
                            POId: $("#POIds")[0].textContent
                        },
                        success: function (data) {
                            $("#POQuestionAnswer-1").html(data);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlPODocManagement,
                        data: {
                            POId: $("#POIds")[0].textContent
                        },
                        success: function (data) {
                            $("#PODocManagement-1").html(data);
                            POdocTable = $('#PODocList').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                        }
                    });
                    $('#' + form).html();
                    $('body').removeClass('modal-open');
                    alert("Save Success!");
                } else {
                    $('body').removeClass('modal-open');
                    $('#' + form).html(result);
                }
                $.validator.addMethod('date', function (value, element) {
                    if (this.optional(element)) {
                        return true;
                    }
                    var ok = true;
                    try {
                        $.datepicker.parseDate('dd/mm/yy', value);
                    } catch (err) {
                        ok = false;
                    }
                    return ok;
                });
                $(".PermissionBorder").css({
                    "border-bottom-style": "solid",
                    "border-bottom-color": "#ddd",
                    "border-bottom-width": "1px"
                });
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
        return false;
    });

    //$(document).on("click", "#UploadPOFile", function (e) {
    //    e.preventDefault();
    //    var fd = new FormData();
    //    fd.append("file", $('#POQuestionAnswer').find('[name="file"]')[0].files[0]);
    //    fd.append("Description", $("#FileDesc").val());
    //    fd.append("UserId", $("#UserIds")[0].textContent);
    //    fd.append("POId", $("#POIds")[0].textContent);
    //    $.ajax({
    //        url: UrlPODocUpload,
    //        type: 'POST',
    //        cache: false,
    //        beforeSend: function () {
    //            $("#uploadModal").modal("show");
    //        },
    //        xhr: function () { // Custom XMLHttpRequest
    //            var xhr = new window.XMLHttpRequest();
    //            //Upload progress
    //            xhr.upload.addEventListener("progress", function (evt) {
    //                if (evt.lengthComputable) {
    //                    var percentComplete = evt.loaded / evt.total;
    //                    //Do something with upload progress
    //                    console.log(percentComplete);
    //                    $('#uploadProgress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
    //                    $('#uploadProgress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
    //                }
    //            }, false);
    //            return xhr;
    //        },
    //        success: function (result) {
    //            $.ajax({
    //                type: "POST",
    //                cache: false,
    //                url: UrlPOQuestionAnswer,
    //                data: {
    //                    POId: $("#POIds")[0].textContent
    //                },
    //                success: function (data) {
    //                    $("#POQuestionAnswer-1").html(data);
    //                }
    //            });
    //            $.ajax({
    //                type: "POST",
    //                cache: false,
    //                url: UrlPODocManagement,
    //                data: {
    //                    POId: $("#POIds")[0].textContent
    //                },
    //                success: function (data) {
    //                    $("#PODocManagement-1").html(data);
    //                    POdocTable = $('#PODocList').DataTable({
    //                        bAutoWidth: false,
    //                        order: [
    //                            [4, "desc"]
    //                        ],
    //                        responsive: {
    //                            breakpoints: [{
    //                                name: 'desktop',
    //                                width: 1024
    //                            },
    //                            {
    //                                name: 'tablet',
    //                                width: 768
    //                            },
    //                            {
    //                                name: 'phone',
    //                                width: 480
    //                            }
    //                            ]
    //                        }
    //                    });
    //                }
    //            });
    //            $('body').removeClass('modal-open');
    //            alert("Upload Success!");
    //        },
    //        error: function () { },
    //        data: fd,
    //        contentType: false,
    //        processData: false
    //    });
    //});

    $(document).on("click", ".ArchiveFile", function (e) {
        e.preventDefault();
        var selectedId = $(this)[0].id;
        $.ajax({
            type: "POST",
            cache: false,
            url: UrlPOFileArchive,
            data: {
                selectedId: selectedId
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlPODocManagement,
                    cache: false,
                    data: {
                        POId: $("#POIds")[0].textContent
                    },
                    success: function (data) {
                        $("#PODocManagement-1").html(data);
                        POdocUploader();
                        alert("Archiving Success!");
                    }
                });
            }
        });
    });
    //QuestionAnswer.cshtml script
    $(document).on("click", "#POSendMessage", function (e) {
        e.preventDefault();
        if ($("#MessageContent").val().trim() !== "") {
            var uid = $("#UserIds")[0].textContent;
            var cid = $("#POIds")[0].textContent;
            var content = $("#MessageContent").val();
            var selectedUserId = [];
            $("#POselMultiple :selected").each(function (i, selected) {
                selectedUserId[i] = $(selected).val();
            });
            if (selectedUserId.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: UrlPOSendMessage,
                    data: {
                        UserId: uid,
                        POId: cid,
                        Content: content,
                        selectedUserId: selectedUserId
                    },
                    dataType: "json",
                    traditional: true,
                    cache: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (newdata) {
                        $.ajax({
                            type: "POST",
                            url: UrlPOQuestionAnswer,
                            data: {
                                POId: $("#POIds")[0].textContent
                            },
                            cache: false,
                            success: function (data) {
                                $("#POQuestionAnswer-1").html(data);
                                $('body').removeClass('modal-open');
                                alert("Send Message Success!");
                            }
                        });
                    }
                });
            } else {
                alert("Please select the recipient");
                $(".responsive").modal("toggle");
                $('body').removeClass('modal-open');
            }
        } else {
            alert("Do not leave the message empty");
            $(".responsive").modal("toggle");
            $('body').removeClass('modal-open');
        }
    });

    IOListTable = $("#IOListTable").DataTable({
        bAutoWidth: false,
        order: [
            [0, "asc"]
        ],
        paging: false,
        searching: false,
        destroy: true,
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });

    PRListTable = $("#PRListTable").DataTable({
        bAutoWidth: false,
        order: [
            [0, "asc"]
        ],
        paging: false,
        searching: false,
        destroy: true,
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });

    DeliveryListTable = $("#DeliveryListTable").DataTable({
        bAutoWidth: false,
        order: [
            [0, "asc"]
        ],
        paging: false,
        searching: false,
        destroy: true,
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });
});