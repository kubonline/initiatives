﻿$(document).ready(function () {
    var j = 2;

    $(document).on("click", ".AddPOItem", function (e) {
        e.preventDefault();
        var html = "";
        $.ajax({
            url: UrlMaterialNoList,
            method: 'GET',
            data: { iteration: j },
            success: function (resp) {
                html = resp.html;
                POItemsTable.row.add([
                    '<label class="select">' + html + '</label>',
                    '<input type="text" id="Description' + j + '" class="form-control custom-filter all" name="Description" readonly />',
                    '<label class="input"><i class="icon-append fa fa-calendar" ></i ><input type="text" id="DeliveryDate' + j + '" class="delivered datepicker" name="DeliveryDate" /></label>',
                    '<input type="text" id="Quantity' + j + '" class="Quantity form-control custom-filter all" name="Quantity" />',
                    '<input type="text" id="UoM' + j + '" class="form-control custom-filter all" name="UoM" readonly />',
                    '<input type="text" id="UnitPrice' + j + '" class="UnitPrice form-control custom-filter all" name="UnitPrice" />',
                    '<input type="text" id="TotalPrice' + j + '" class="form-control custom-filter all" name="TotalPrice" readonly />',
                    '<button type="submit" type="button" class="RemovePOItem btn-u btn-u-primary">Delete</button>',
                    ''
                ]).draw(false);
                j++;
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });       
    });

    $(document).on("change", ".MaterialNo", function () {
        //e.preventDefault();
        var selectlistid;
        if ($(this)[0].id.toString().length === 12) {
            selectlistid = $(this)[0].id.substring(10, 12);
        } else if ($(this)[0].id.toString().length === 11){
            selectlistid = $(this)[0].id.substring(10, 11);
        } else if ($(this)[0].id.toString().length === 13) {
            selectlistid = $(this)[0].id.substring(10, 13);
        }       
        var partno = $(this).val();
        var html = "";
        $.ajax({
            url: UrlGetMaterialNoInfo,
            method: 'GET',
            data: { partno: partno },
            success: function (resp) {
                var Description = resp.materialNoInfo.Description;
                var UoM = resp.materialNoInfo.UoM;
                $("#Description" + selectlistid).val(Description);
                $("#UoM" + selectlistid).val(UoM);
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });       
    });

    $(document).on("change", ".UnitPrice", function () {
        var RequestedQuantity = $(this).parent().parent().find("input[name='Quantity']").val();
        var TotalPrice = $(this).val() * RequestedQuantity;
        $(this).parent().parent().find("input[name='TotalPrice']").val(TotalPrice);
    });

    $(document).on("change", ".Quantity", function () {
        var UnitPrice = $(this).parent().parent().find("input[name='UnitPrice']").val();
        var TotalPrice = $(this).val() * UnitPrice;
        $(this).parent().parent().find("input[name='TotalPrice']").val(TotalPrice);
    });

    $(document).on('click', '#POItemsTable tbody .RemovePOItem', function (e) {
        e.preventDefault();
        POItemsTable.row($(this).parents('tr')).remove().draw(false);
    });

    //$(document).on('click', '#RemovePOItem', function (e) {
    //    e.preventDefault();
    //    if ($(this).closest('tr')[0].id !== "") {
    //        $(this).closest('tr').remove();
    //    }
    //});
    $(document).on("click", "#CreatePO", function (e) {
        e.preventDefault();
        var partialForm = "CreateNewPO"; var form = "CreateNewPO-1";
        var fd = new FormData(); var i = 0;
        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        fd.append("CreatePO", true);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            if (input.name === "POListDetail.SendEmail" && input.value === "on") {
                fd.append(input.name, true);
            } else if (input.name === "POListDetail.SendEmail" && input.value === "off") {
                fd.append(input.name, false);
            } else {
                fd.append(input.name, input.value);
            }
        });
        var data = POItemsTable.$("tr");
        $.each(data, function (i, input) {
            fd.append("POItemListObject[" + i + "].PartNo", $(input).find(".MaterialNo").val());
            fd.append("POItemListObject[" + i + "].MaterialNo", $(input).find(".MaterialNo option:selected").text());
            fd.append("POItemListObject[" + i + "].Description", $(input).find("input[name='Description']").val());
            fd.append("POItemListObject[" + i + "].DeliveryDate", $(input).find(".delivered").val());
            fd.append("POItemListObject[" + i + "].Quantity", $(input).find("input[name='Quantity']").val());
            fd.append("POItemListObject[" + i + "].UoM", $(input).find("input[name='UoM']").val());
            fd.append("POItemListObject[" + i + "].UnitPrice", $(input).find("input[name='UnitPrice']").val());
            fd.append("POItemListObject[" + i + "].TotalPrice", $(input).find("input[name='TotalPrice']").val());
        });
        $("#CreatePOLoading").modal("show");
        $.ajax({
            url: UrlCreatePO,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: function () { $("#POuploadModal").modal("show"); },
            success: function (result) {
                if (result.success) {
                    alert("Create PO success");
                    window.location = UrlViewContract + "#po-1";
                } else if (result.success === false && result.exception === true) {
                    alert("Exception occured. Please contact admin");
                    $('.wrapper').html("");
                    $('.wrapper').html(result.view);
                } else if (result.success === false && result.havePONo === true) {
                    alert("Create PO failed! Reason: Maybe the PONo already registered");
                    $('.wrapper').html("");
                    $('.wrapper').html(result.view);
                    //$('body').removeClass('modal-open');
                    //$('#' + form).html(result);                         
                } else {
                    //$('.wrapper').html("");
                    $('.wrapper').html(result.view);
                }
                POdocUploader();
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
    });

    //$(document).on("click", "#CreatePO-All", function (e) {
    //    e.preventDefault();
    //    var partialForm = "CreateAllPO";
    //    var fd = new FormData(); var i = 0;
    //    fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
    //    var other_data = $("#" + partialForm).serializeArray();
    //    $.each(other_data, function (key, input) {
    //        fd.append(input.name, input.value);
    //        if (input.name === "MaterialNo") {
    //            fd.append("POItemListObject[" + i + "].MaterialNo", input.value);
    //        }
    //        if (input.name === "Description") {
    //            fd.append("POItemListObject[" + i + "].Description", input.value);
    //        }
    //        if (input.name === "DeliveryDate") {
    //            fd.append("POItemListObject[" + i + "].DeliveryDate", input.value);
    //        }
    //        if (input.name === "Quantity") {
    //            fd.append("POItemListObject[" + i + "].Quantity", input.value);
    //        }
    //        if (input.name === "UoM") {
    //            fd.append("POItemListObject[" + i + "].UoM", input.value);
    //        }
    //        if (input.name === "UnitPrice") {
    //            fd.append("POItemListObject[" + i + "].UnitPrice", input.value);
    //        }
    //        if (input.name === "TotalPrice") {
    //            fd.append("POItemListObject[" + i + "].TotalPrice", input.value);
    //            i++;
    //        }
    //    });
    //    $("#CreatePOLoading").modal("show");
    //    $.ajax({
    //        url: UrlCreatePO,
    //        type: 'POST',
    //        data: fd,
    //        contentType: false,
    //        processData: false,
    //        cache: false,
    //        dataType: "json",
    //        beforeSend: function () { $("#POuploadModal").modal("show"); },
    //        success: function (result) {
    //            if (result.success) {
    //                alert("Create PO success");
    //                window.location = UrlViewContract + "#po-1";
    //            } else if (result.success === false && result.exception === true) {
    //                alert("Exception occured. Please contact admin");
    //            } else if (result.success === false && result.havePONo === true) {
    //                alert("Create PO failed! Reason: Maybe the PONo already registered");
    //                $('.wrapper').html("");
    //                $('.wrapper').html(result.view);
    //                //$('body').removeClass('modal-open');
    //                //$('#' + form).html(result);
    //            } else {
    //                //$('.wrapper').html("");
    //                $('.wrapper').html(result.view);
    //            }
    //            $(".datepicker").datepicker({
    //                dateFormat: 'dd/mm/yy',
    //                prevText: '<i class="fa fa-angle-left"></i>',
    //                nextText: '<i class="fa fa-angle-right"></i>'
    //            });
    //        }
    //    });
    //});
});