﻿$(document).ready(function () {
    var model = $("#Opportunity").data('model');
    var modelContract = $("#Contract").data('model');
    var modelPO = $("#PurchaseOrder").data('model');
    var modelUser = $("#User").data('model');
    var modelCust = $("#Customer").data('model');
    //debugger;
    if (model != undefined) { var aaData = JSON.parse(model.aaData); }  
    if (modelContract != undefined) { var aaDataContract = JSON.parse(modelContract.aaData); }
    if (modelPO != undefined) { var aaDataPO = JSON.parse(modelPO.aaData); }
    if (modelUser != undefined) { var aaDataUser = JSON.parse(modelUser.aaData); }
    if (modelCust != undefined) { var aaDataCust = JSON.parse(modelCust.aaData); }

    $('#Opportunity tfoot th, #Contract tfoot th, #PurchaseOrder tfoot th, #User tfoot th, #Customer tfoot th').each(function () {
        //debugger;
        if ($(this)[0].textContent != "" && $(this)[0].textContent != "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }        
    });

    var table = $('#Opportunity').DataTable({
        data: aaData,
        bAutoWidth: false,
        aoColumnDefs: [ {
            aTargets: [ 1 ],
            mRender: function ( data, type, full ) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="'+ url + '" /></a>';
            }
        } ],
        initComplete: function () {
            var r = $('#Opportunity tfoot tr');
            r.find('th').each(function () {
                //debugger;
                $(this).css('padding', 8);           
            });
            $('#Opportunity thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    var contractTable = $('#Contract').DataTable({
        data: aaDataContract,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        initComplete: function () {
            var r = $('#Contract tfoot tr');
            r.find('th').each(function () {
                //debugger;
                $(this).css('padding', 8);
            });
            $('#Contract thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    var POTable = $('#PurchaseOrder').DataTable({
        data: aaDataPO,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        initComplete: function () {
            var r = $('#PurchaseOrder tfoot tr');
            r.find('th').each(function () {
                //debugger;
                $(this).css('padding', 8);
            });
            $('#PurchaseOrder thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    var userTable = $('#User').DataTable({
        data: aaDataUser,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        initComplete: function () {
            var r = $('#User tfoot tr');
            r.find('th').each(function () {
                //debugger;
                $(this).css('padding', 8);
            });
            $('#User thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        aaSorting: [[4, 'asc']],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    var custTable = $('#Customer').DataTable({
        data: aaDataCust,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + url + '" /></a>';
            }
        }],
        initComplete: function () {
            var r = $('#Customer tfoot tr');
            r.find('th').each(function () {
                //debugger;
                $(this).css('padding', 8);
            });
            $('#Customer thead').append(r);
            $('#search_0').css('text-align', 'center');
        },
        aaSorting: [[2, 'asc']],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    contractTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    POTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    userTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    custTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#Opportunity tbody, #Contract tbody, #PurchaseOrder tbody, #User tbody, #Customer tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $("#Opportunity tbody, #Contract tbody, #PurchaseOrder tbody, #User tbody, #Customer tbody").on("click", ".infoButton", function (e) {
        e.preventDefault();
        //debugger; 
        //if ($(this)[0].pathname == "/Initiatives/OppoList/OpportunityList") {
        if ($(this)[0].pathname == "/OppoList/OpportunityList") {
            var data = table.row($(this).parents('tr')).data();
            var oId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#action").attr('href'),
                data: { 'oId': oId },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });

        }
        //if ($(this)[0].pathname == "/Initiatives/ConPOList/ContractList") {
        //debugger;
        if ($(this)[0].pathname == "/ConPOList/ContractList") {
            var data = contractTable.row($(this).parents('tr')).data();
            var cId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionContract").attr('href'),
                data: { 'cId': cId },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });

        }
        //if ($(this)[0].pathname == "/Initiatives/ConPOList/POList") {       
        if ($(this)[0].pathname == "/ConPOList/ViewPOList") {
            var data = POTable.row($(this).parents('tr')).data();
            var POId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionPO").attr('href'),
                data: { 'POId': POId },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });

        }
        //if ($(this)[0].pathname == "/Initiatives/Home/UserList") {
        if ($(this)[0].pathname == "/Home/UserList") {
            var data = userTable.row($(this).parents('tr')).data();
            var uId = data[0];
            //debugger;
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionUser").attr('href'),
                data: { 'uId': uId },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }
        //if ($(this)[0].pathname == "/Initiatives/Home/CustomerList") {
        if ($(this)[0].pathname == "/Home/CustomerList") {
            var data = custTable.row($(this).parents('tr')).data();
            var cId = data[0];
            //debugger;
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionCustomer").attr('href'),
                data: { 'cId': cId },
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }
        //if ($(this)[0].pathname == "/Initiatives/Home/NewPassword") {
        //if ($(this)[0].pathname == "/Home/NewPassword") {
        //    var data = custTable.row($(this).parents('tr')).data();
        //    var cId = data[0];
        //    //debugger;
        //    $.ajax({
        //        method: 'POST',
        //        cache: false,
        //        url: $("#actionNewPassowrd").attr('href'),
        //        data: { 'cId': cId },
        //        dataType: "json",
        //        success: function (response) {
        //            if (response.success) {
        //                //debugger;
        //                window.location = response.url;
        //            };
        //        }
        //    });
        //}
    })
    //$('#Opportunity tbody').on('dblclick', 'tr td:nth-child(1),tr td:nth-child(2)', function (e) {
    //    e.preventDefault();
    //    var data = table.row($(this).parents('tr')).data();
    //    clickInfo(data);
    //});

    $('#bAddOpportunity, #bAddContract, #bAddPO, #bAddUser, #bAddCustomer').on('click', function (e) {
        e.preventDefault();
        //debugger;
        if ($(this)[0].id == "bAddOpportunity") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#aCreateOpportunity").attr('href'),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }

        if ($(this)[0].id == "bAddContract") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#aCreateContract").attr('href'),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }

        if ($(this)[0].id == "bAddPO") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#aCreatePO").attr('href'),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }

        if ($(this)[0].id == "bAddUser") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#aCreateUser").attr('href'),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }

        if ($(this)[0].id == "bAddCustomer") {
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#aCreateCustomer").attr('href'),
                dataType: "json",
                success: function (response) {
                    if (response.success) {
                        //debugger;
                        window.location = response.url;
                    };
                }
            });
        }
    });

});