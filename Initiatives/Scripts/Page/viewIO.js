﻿$(document).ready(function () {
    IOdocUploader();

    $(document).on("click", "#VerifyIODetails", function (e) {
        e.preventDefault();
        $("#VIOD_IONo").val($("#IOD_IONo").val());
        $("#VIOD_IssueDate").val($("#IOD_IssueDate").val());
        $("#VIOD_FileDescription").val($("#IOD_FileDescription").val());
        $("#VIOD_File").val($("#IOD_Files").val());
        $("#IODetailsModal").modal("show");
    });

    $(document).on("click", "#SaveIOtoPDF", function () {
        window.location.href = UrlIOForm;
        return false;
    });

    $(document).on("click", "#SubmitIODetails", function (e) {
        e.preventDefault();
        var partialForm = "IODetails"; var modalInfo = "IODetailsModal";
        var modalLoading = "IODetailsLoading"; var fd = new FormData();

        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $("#" + modalInfo).modal("hide");
        $("#" + modalLoading).modal("show");
        $.ajax({
            url: UrlIODetails,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.success) {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlIOUpdates,
                        data: {
                            IOId: $(result.view).find("#IOIds")[0].textContent
                        },
                        success: function (data) {
                            $("#IOUpdates-1").html(data);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: UrlIODocManagement,
                        data: {
                            IOId: $(result.view).find("#IOIds")[0].textContent
                        },
                        success: function (data) {
                            $("#IODocManagement-1").html(data);
                            IOdocTable = $('#IODocList').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                            arcTable = $('.archive').DataTable({
                                bAutoWidth: false,
                                destroy: true,
                                order: [
                                    [4, "desc"]
                                ],
                                responsive: {
                                    breakpoints: [{
                                        name: 'desktop',
                                        width: 1024
                                    },
                                    {
                                        name: 'tablet',
                                        width: 768
                                    },
                                    {
                                        name: 'phone',
                                        width: 480
                                    }
                                    ]
                                }
                            });
                        }
                    });
                    alert("Save Success!");
                }
                $('.firstTab').html(result.view);
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
                IOItemsTable = $('#IOItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });
            }
        });
    });

    $(document).on("click", "#UploadIOFile", function (e) {
        e.preventDefault();
        var fd = new FormData();
        fd.append("file", $('#IOUpdates').find('[name="file"]')[0].files[0]);
        fd.append("Description", $("#FileDesc").val());
        fd.append("UserId", $("#UserIds")[0].textContent);
        fd.append("IOId", $("#IOIds")[0].textContent);
        $.ajax({
            url: UrlIODocUpload,
            type: 'POST',
            cache: false,
            beforeSend: function () {
                $("#uploadModal").modal("show");
            },
            xhr: function () { // Custom XMLHttpRequest
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $('#uploadProgress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                        $('#uploadProgress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                    }
                }, false);
                return xhr;
            },
            success: function (result) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlIOUpdates,
                    data: {
                        IOId: $("#IOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#IOUpdates-1").html(data);
                    }
                });
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: UrlIODocManagement,
                    data: {
                        IOId: $("#IOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#IODocManagement-1").html(data);
                        IOdocTable = $('#IODocument').DataTable({
                            bAutoWidth: false,
                            order: [
                                [4, "desc"]
                            ],
                            responsive: {
                                breakpoints: [{
                                    name: 'desktop',
                                    width: 1024
                                },
                                {
                                    name: 'tablet',
                                    width: 768
                                },
                                {
                                    name: 'phone',
                                    width: 480
                                }
                                ]
                            }
                        });
                    }
                });
                $('body').removeClass('modal-open');
                alert("Upload Success!");
            },
            error: function () { },
            data: fd,
            contentType: false,
            processData: false
        });
    });

    function IOdocUploader() {
        IOItemsTable = $('#IOItemsTable').DataTable({
            bAutoWidth: false,
            ordering: false,
            paging: false,
            searching: false,
            destroy: true,
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        IOdocTable = $('#IODocument').DataTable({
            bAutoWidth: false,
            destroy: true,
            order: [
                [4, "desc"]
            ],
            responsive: {
                breakpoints: [{
                    name: 'desktop',
                    width: 1024
                },
                {
                    name: 'tablet',
                    width: 768
                },
                {
                    name: 'phone',
                    width: 480
                }
                ]
            }
        });
        IOFormTable = $('#IOFormTable').DataTable({
            bAutoWidth: false,
            destroy: true           
        });
    }

    $(document).on("click", ".ArchiveFile", function (e) {
        e.preventDefault();
        var selectedId = $(this)[0].id;
        $.ajax({
            type: "POST",
            cache: false,
            url: UrlFileArchive,
            data: {
                selectedId: selectedId
            },
            dataType: "json",
            traditional: true,
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: UrlIODocManagement,
                    cache: false,
                    data: {
                        IOId: $("#IOIds")[0].textContent
                    },
                    success: function (data) {
                        $("#IODocManagement-1").html(data);
                        IOdocUploader();
                        alert("Archiving Success!");
                    }
                });
            }
        });
    });

    //QuestionAnswer.cshtml script
    $(document).on("click", "#IOSendMessage", function (e) {
        e.preventDefault();
        if ($("#MessageContent").val().trim() !== "") {
            var uid = $("#UserIds")[0].textContent;
            var ioid = $("#IOIds")[0].textContent;
            var content = $("#MessageContent").val();
            var selectedUserId = [];
            $("#IOselMultiple :selected").each(function (i, selected) {
                selectedUserId[i] = $(selected).val();
            });
            if (selectedUserId.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: UrlIOSendMessage,
                    data: {
                        UserId: uid,
                        IOId: ioid,
                        Content: content,
                        selectedUserId: selectedUserId
                    },
                    dataType: "json",
                    traditional: true,
                    cache: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                console.log(percentComplete);
                                $('.progress > .progress-bar').attr("style", "width: " + percentComplete * 99 + "%");
                                $('.progress > .progress-bar')[0].textContent = "" + percentComplete * 99 + "% Complete ";
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (newdata) {
                        $.ajax({
                            type: "POST",
                            url: UrlIOUpdates,
                            data: {
                                IOId: $("#IOIds")[0].textContent
                            },
                            cache: false,
                            success: function (data) {
                                $("#IOUpdates-1").html(data);
                                $('body').removeClass('modal-open');
                                alert("Send Message Success!");
                            }
                        });
                    }
                });
            } else {
                alert("Please select the recipient");
                $(".responsive").modal("toggle");
                $('body').removeClass('modal-open');
            }
        } else {
            alert("Do not leave the message empty");
            $(".responsive").modal("toggle");
            $('body').removeClass('modal-open');
        }
    });
});