﻿$(document).ready(function () {
     $(document).on('click', '#submitIOPO', function (e) {
        e.preventDefault();
        if (rows_selected.length === 0) {
            alert("Please select PO No. first!");
            return false;
        }
        // Iterate over all selected checkboxes
        var fd = new FormData();
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        $.ajax({
            url: UrlGetMaterialNoIO,
            type: "POST",
            contentType: false,
            processData: false,
            data: fd
        })
            .success(function (data) {
                $("#IOItemsTableDiv").html(data);
                IOItemsTable = $('#IOItemsTable').DataTable({
                    bAutoWidth: false,
                    ordering: false,
                    paging: false,
                    searching: false,
                    destroy: true,
                    responsive: {
                        breakpoints: [{
                            name: 'desktop',
                            width: 1024
                        },
                        {
                            name: 'tablet',
                            width: 768
                        },
                        {
                            name: 'phone',
                            width: 480
                        }
                        ]
                    }
                });
            });
    });

    $(document).on('click', '#IOItemsTable tbody .RemoveIOItem', function (e) {
        e.preventDefault();
        IOItemsTable.row($(this).parents('tr')).remove().draw(false);
    });

    $(document).on("click", "#CreateIO", function (e) {
        e.preventDefault();
        var partialForm = "CreateNewIO";
        var fd = new FormData(); var i = 0;
        fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#" + partialForm).serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.each(rows_selected, function (i, rowId) {
            fd.append("POListObject[" + i + "].POId", rowId);
        });
        var data = IOItemsTable.$(".ItemList");
        $.each(data, function (i, input) {
            fd.append("IOItemListObject[" + i + "].POId", $(input).find(".POId")[0].textContent);
            fd.append("IOItemListObject[" + i + "].ItemsId", $(input).find(".ItemsId")[0].textContent);
            fd.append("IOItemListObject[" + i + "].RequestedQuantity", $(input).find("input[id='IOD_RequestedQuantity']").val());
            fd.append("IOItemListObject[" + i + "].OutstandingQuantity", $(input).find(".outstanding")[0].textContent);
        });
        $("#CreatePOLoading").modal("show");
        $.ajax({
            url: UrlCreateIO,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function () {
                $("#CreateIOLoading").modal("show");
            },
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    alert("Create IO success");
                    window.location = UrlViewContract + "#io-1";
                } else if (result.success === false && result.exception === true) {
                    alert("Exception occured. Please contact admin");
                } else if (result.success === false && result.haveIONo === true) {
                    alert("Create IO failed! Reason: Maybe the IONo already registered");
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitIOPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === true) {
                    alert(result.ErrorMessage);
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitIOPO").trigger("click");
                    });
                } else if (result.success === false && result.haveQuantity === false) {
                    $('.wrapper').html(result.view);
                    $.ajax({
                        url: UrlGetPOList,
                        type: "GET",
                        cache: false
                    }).success(function (data) {
                        $("#POTable").html(data);
                        getTables();
                        $("#submitIOPO").trigger("click");
                    });
                }
                $('body').removeClass('modal-open');
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
    });
});