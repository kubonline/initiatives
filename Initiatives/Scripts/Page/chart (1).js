﻿$(document).ready(function () {
    //$.getJSON("/Dashboard/GetData/", function (data) {
    var chart = new CanvasJS.Chart("chartContainer",
    {
        title: {
            text: "Tender Participation 2015 & 2016",
            fontFamily: "arial black",
            fontColor: "#695A42"

        },
        animationEnabled: true,
        toolTip: {
            shared: true,
            content: function (e) {
                var str = '';
                var total = 0;
                var str3;
                var str2;
                for (var i = 0; i < e.entries.length; i++) {
                    var str1 = "<span style= 'color:" + e.entries[i].dataSeries.color + "'> " + e.entries[i].dataSeries.name + "</span>: $<strong>" + e.entries[i].dataPoint.y + "</strong>  bn<br/>";
                    total = e.entries[i].dataPoint.y + total;
                    str = str.concat(str1);
                }
                str2 = "<span style = 'color:DodgerBlue; '><strong>" + (e.entries[0].dataPoint.x).getFullYear() + "</strong></span><br/>";
                total = Math.round(total * 100) / 100
                str3 = "<span style = 'color:Tomato '>Total:</span><strong> $" + total + "</strong> bn<br/>";

                return (str2.concat(str)).concat(str3);
            }
        },
        axisY: {
            valueFormatString: "$#0bn",
            interval: 10,
            gridColor: "#B6B1A8",
            tickColor: "#B6B1A8",
            interlacedColor: "rgba(182,177,168,0.2)"

        },
        axisX: {
            interval: 1,
            intervalType: "year"
        },
        data: [
        {
            type: "stackedColumn",
            showInLegend: true,
            color: "#696661",
            name: "Q1",
            dataPoints: [
            { y: 2.25, x: new Date(2006, 0) },
            { y: 3.66, x: new Date(2007, 0) },
            { y: 5.18, x: new Date(2008, 0) },
            { y: 5.50, x: new Date(2009, 0) },
            { y: 6.75, x: new Date(2010, 0) },
            { y: 8.57, x: new Date(2011, 0) },
            { y: 10.64, x: new Date(2012, 0) }
            ]
        },
       {
           type: "stackedColumn",
           showInLegend: true,
           name: "Q2",
           color: "#EDCA93",
           dataPoints: [
           { y: 2.45, x: new Date(2006, 0) },
           { y: 3.87, x: new Date(2007, 0) },
           { y: 5.36, x: new Date(2008, 0) },
           { y: 5.52, x: new Date(2009, 0) },
           { y: 6.82, x: new Date(2010, 0) },
           { y: 9.02, x: new Date(2011, 0) },
           { y: 11.80, x: new Date(2012, 0) }
           ]
       },
       {
           type: "stackedColumn",
           showInLegend: true,
           name: "Q3",
           color: "#695A42",
           dataPoints: [
           { y: 2.68, x: new Date(2006, 0) },
           { y: 4.23, x: new Date(2007, 0) },
           { y: 5.54, x: new Date(2008, 0) },
           { y: 5.94, x: new Date(2009, 0) },
           { y: 7.28, x: new Date(2010, 0) },
           { y: 9.72, x: new Date(2011, 0) },
           { y: 13.30, x: new Date(2012, 0) }
           ]
       },
       {
           type: "stackedColumn",
           showInLegend: true,
           name: "Q4",
           color: "#B6B1A8",
           dataPoints: [
           { y: 3.20, x: new Date(2006, 0) },
           { y: 4.82, x: new Date(2007, 0) },
           { y: 5.70, x: new Date(2008, 0) },
           { y: 6.67, x: new Date(2009, 0) },
           { y: 8.44, x: new Date(2010, 0) },
           { y: 10.58, x: new Date(2011, 0) },
           { y: 14.41, x: new Date(2012, 0) }
           ]
       }]
    });
    //});
    //debugger;
    var chart2 = new CanvasJS.Chart("chartContainer2", {

        title: {
            text: "Amount Project Awarded by Quarterly 2015"
        },
        data: [//array of dataSeries              
          { //dataSeries object

              /*** Change type "column" to "bar", "area", "line" or "pie"***/
              type: "column",
              dataPoints: [
              { label: "Q1", y: 0 },
              { label: "Q2", y: 0 },
              { label: "Q3", y: 0 },
              { label: "Q4", y: 43163718 }
              ]
          }
        ]
    });

    chart.render();
    chart2.render();
});