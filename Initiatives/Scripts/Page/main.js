﻿$(document).ready(function () {
    var custTable; var userTable; var partNoTable; var contractTable; var POListTable; var POExpected; var TnITable; var DOTable; var IOTable; var PRTable;
    var memberTable; var OppodocTable; var POdocTable; var IOdocTable; var DOdocTable; var arcTable; var POItemsTable; var i; var newRowNum; var IOListTable;
    var PRListTable; var DeliveryListTable; var POReportTable; var IOItemsTable; var PRItemsTable; var DOItemsTable; var IOFormTable; var TechProviderTable;
    var CompetitorTable; var SWOTAnalysisTable; var SalesActivitiesTable; var AMActivitiesTable;

    //tab url + hash controller
    $(function () {
        var hash = window.location.hash;
        if (hash) {
            $('a[href="' + hash + '"]').parent('li').addClass('active');
            $(hash).addClass('active');
        } else {
            $('.firstTab').addClass('active');
            $('.tab-v2 ul li:first').addClass('active');
        }
    });

    $.fn.dataTable.moment('DD/MM/YYYY');
    $.fn.dataTable.moment('DD/MM/YYYY HH:mm:ss');

    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });

    $.validator.addMethod('date',
        function (value, element) {
            if (this.optional(element)) {
                return true;
            }

            var ok = true;
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            }
            catch (err) {
                ok = false;
            }
            return ok;
        });

    $('#Contract tfoot th, #User tfoot th, #CustomerTable tfoot th, #PartNoTable tfoot th').each(function () {
        if ($(this)[0].textContent !== "" && $(this)[0].textContent !== "#") {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control custom-filter" placeholder="Search" />');
        }
    });

    OppoTable = $('#OppoTable').DataTable({
        serverSide: false,
        //dom: 'frtiS',
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
            }
        }],
        aaSorting: [9, "desc"],
        initComplete: function () {
            //var r = $('#OppoTable tfoot tr');
            //r.find('th').each(function () {
            //    $(this).css('padding', 8);
            //});
            //$('#OppoTable thead').append(r);
            //$('#search_0').css('text-align', 'center');
            $("#OppoTable").removeClass("hidden");
        },
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        },
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            total = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            pageTotal = api
                .column(5, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(5).footer()).html(
                pageTotal + ' (' + total + ' total)'
            );
        }
    });

    contractTable = $('#Contract').DataTable({
        serverSide: false,
        //dom: 'frtiS',
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
            }
        }],
        //aaSorting: [12, "desc"],
        ordering: false,
        initComplete: function () {
            //var r = $('#Contract tfoot tr');
            //r.find('th').each(function () {
            //    $(this).css('padding', 8);
            //});
            //$('#Contract thead').append(r);
            //$('#search_0').css('text-align', 'center');
            $("#Contract").removeClass("hidden");
        },
        destroy: true,
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    //jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    //    'dateNonStandard-asc': function (a, b) {
    //        var x = Date.parse(a);
    //        var y = Date.parse(b);
    //        if (x == y) { return 0; }
    //        if (isNaN(x) || x < y) { return 1; }
    //        if (isNaN(y) || x > y) { return -1; }
    //    },
    //    'dateNonStandard-desc': function (a, b) {
    //        var x = Date.parse(a);
    //        var y = Date.parse(b);
    //        if (x == y) { return 0; }
    //        if (isNaN(y) || x < y) { return -1; }
    //        if (isNaN(x) || x > y) { return 1; }
    //    }
    //});
 
    SalesActivitiesTable = $('#SalesActivitiesTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B><"btn-FloatRight"f>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [1, "desc"],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });

    AMActivitiesTable = $('#AMActivitiesTable').DataTable({
        dom: '<"btn-FloatLeft"l><"btn-FloatRight"B><"btn-FloatRight"f>tip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        processing: true,
        paging: true,
        deferRender: true,
        bAutoWidth: false,
        destroy: true,
        aaSorting: [1, "desc"],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });

    $(document).on('click', '#SubmitMyActivity', function (e) {
        e.preventDefault();
        var fd = new FormData(); var i = 0;
        //fd.append("file", $("#" + partialForm).find('[name="file"]')[0].files[0]);
        var other_data = $("#MyActivity").serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.ajax({
            url: UrlDashboard,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    alert("Activity submission success");
                    window.location = UrlViewContract + "#delivery-1";
                } else {
                    $('#MyActivity').html(result.view);
                }
                $(".datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    nextText: '<i class="fa fa-angle-right"></i>'
                });
            }
        });
    });

    arcTable = $('.archive').DataTable({
        bAutoWidth: false,
        destroy: true,
        order: [
            [4, "desc"]
        ],
        responsive: {
            breakpoints: [{
                name: 'desktop',
                width: 1024
            },
            {
                name: 'tablet',
                width: 768
            },
            {
                name: 'phone',
                width: 480
            }
            ]
        }
    });

    userTable = $('#User').DataTable({
        bAutoWidth: false,
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
            }
        }],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete: function () {
            var r = $('#User tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#User thead').append(r);
            $('#search_0').css('text-align', 'center');
            $("#User").removeClass("hidden");
        },
        aaSorting: [3, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    custTable = $('#CustomerTable').DataTable({
        bAutoWidth: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        aoColumnDefs: [{
            aTargets: [1],
            mRender: function (data, type, full) {
                return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
            }
        }],
        initComplete: function () {
            var r = $('#CustomerTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#CustomerTable thead').append(r);
            $('#search_0').css('text-align', 'center');
            $("#CustomerTable").removeClass("hidden");
        },
        aaSorting: [3, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    partNoTable = $('#PartNoTable').DataTable({
        bAutoWidth: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        //aoColumnDefs: [{
        //    aTargets: [1],
        //    mRender: function (data, type, full) {
        //        return '<a class="infoButton" title="Click here for more info" href="#"><img src="' + UrlGreenalert + '" /></a>';
        //    }
        //}],
        initComplete: function () {
            var r = $('#PartNoTable tfoot tr');
            r.find('th').each(function () {
                $(this).css('padding', 8);
            });
            $('#PartNoTable thead').append(r);
            $('#search_0').css('text-align', 'center');
            $("#PartNoTable").removeClass("hidden");
        },
        aaSorting: [1, "asc"],
        responsive: {
            breakpoints: [
                { name: 'desktop', width: 1024 },
                { name: 'tablet', width: 768 },
                { name: 'phone', width: 480 }
            ]
        }
    });

    //contractTable.columns().every(function () {
    //    var that = this;
    //    $('input', this.footer()).on('keyup change', function () {
    //        if (that.search() !== this.value) {
    //            that
    //                .search(this.value)
    //                .draw();
    //        }
    //    });
    //});

    userTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    custTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    partNoTable.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });

    $('#OppoTable tbody, #Contract tbody, #User tbody, #CustomerTable tbody, #PartNoTable tbody').on('mouseover', 'tr td:nth-child(1)', function () {
        $(this).css('cursor', 'pointer');
    });

    $("#OppoTable tbody, #Contract tbody, #User tbody, #CustomerTable tbody, #PartNoTable tbody").on("click", ".infoButton", function (e) {
        e.preventDefault(); var data; var cId;
        if ($(this)[0].pathname === UrlOpportunityList) {            
            data = OppoTable.row($(this).parents('tr')).data();
            var oId = data[0];
            window.location.href = UrlViewInfo + "?OppContId=" + oId;
            return false;
        }
        if ($(this)[0].pathname === UrlContractList) {
            data = contractTable.row($(this).parents('tr')).data();
            cId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionContract").attr('href'),
                data: { 'cId': cId },
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
        }
        if ($(this)[0].pathname === UrlUserList) {
            data = userTable.row($(this).parents('tr')).data();
            var uId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionUser").attr('href'),
                data: { 'uId': uId },
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
        }
        if ($(this)[0].pathname === UrlCustomerList) {
            data = custTable.row($(this).parents('tr')).data();
            cId = data[0];
            $.ajax({
                method: 'POST',
                cache: false,
                url: $("#actionCustomer").attr('href'),
                data: { 'cId': cId },
                dataType: "json",
                success: function (response) {
                    if (response.success)
                        window.location = response.url;
                }
            });
        }
        //if ($(this)[0].pathname === UrlPartNoDetails) {
        //    data = custTable.row($(this).parents('tr')).data();
        //    cId = data[0];
        //    window.location.href = UrlPartNoDetails + "?cId=" + cId;
        //    return false;
        //}
    });

    $(document).on("click", ".closeButton", function (e) {
        $(".responsive").modal("toggle");
        $('body').removeClass('modal-open');
    });
});