var CirclesMaster = function () {

    return {

        //Circles Master v1
        initCirclesMaster1: function () {
        	//Circles 1
		    Circles.create({
		        id:         'circle-1',
		        percentage: 75,
		        radius:     75,
		        width:      8,
		        number:     75,
		        text:       'M',
		        colors:     ['#eee', '#72c02c'],
		        duration:   2000
		    })

        	//Circles 2
		    Circles.create({
		        id:         'circle-2',
		        percentage: 74,
		        radius:     80,
		        width:      8,
		        number:     30,
		        text: 'M',
		        colors:     ['#eee', '#72c02c'],
		        duration:   2000
		    })

        	//Circles 3
		    Circles.create({
		        id:         'circle-3',
		        percentage: 65,
		        radius:     80,
		        width:      8,
		        number:     20,
		        text: 'M',
		        colors:     ['#eee', '#72c02c'],
		        duration:   2000
		    })

		    Circles.create({
		        id: 'circle-4',
		        percentage: 65,
		        radius: 80,
		        width: 8,
		        number: 10,
		        text: 'M',
		        colors: ['#eee', '#72c02c'],
		        duration: 2000
		    })

		    Circles.create({
		        id: 'circle-5',
		        percentage: 65,
		        radius: 80,
		        width: 8,
		        number: 10,
		        text: 'M',
		        colors: ['#eee', '#72c02c'],
		        duration: 2000
		    })

		    Circles.create({
		        id: 'circle-6',
		        percentage: 65,
		        radius: 80,
		        width: 8,
		        number: 5,
		        text: 'M',
		        colors: ['#eee', '#72c02c'],
		        duration: 2000
		    })

        },
        
        //Circles Master v2
        initCirclesMaster2: function () {
		    var colors = [
		        ['#D3B6C6', '#9B6BCC'], ['#C9FF97', '#72c02c'], ['#BEE3F7', '#3498DB'], ['#FFC2BB', '#E74C3C']
		    ];

		    for (var i = 1; i <= 6; i++) {
		        var child = document.getElementById('circles-' + i),
		            percentage = 45 + (i * 9);
		            
		        Circles.create({
		            id:         child.id,
		            percentage: percentage,
		            radius:     70,
		            width:      2,
		            number:     percentage / 1,
		            text:       'M',
		            colors:     colors[i - 1],
		            duration:   2000,
		        });
		    }	    
        }

    };
    
}();