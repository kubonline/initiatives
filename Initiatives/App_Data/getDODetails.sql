SELECT DISTINCT o.Details_PONo,
                m.materialno,
                m.description,
				n.outstandingQuantity,
                n.deliveryQuantity,
				n.deliveredQuantity						   
FROM   po_item m
       LEFT JOIN do_items n
              ON ( m.itemsid = n.itemsid )
       LEFT JOIN purchaseorder o
              ON ( m.poid = o.poid )
	   LEFT JOIN DeliveryOrder p
			  ON ( n.DOId = p.doid )
WHERE p.doid = 18