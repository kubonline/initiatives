SELECT [Process Flow Name],
       details_poissuedate        AS 'Start Flow Date',
       createdate                 AS 'End Flow Date',
       details_projectdescription AS 'Remark'
FROM   (SELECT 'PO to IO Process Flow' AS [Process Flow Name],
               [details_poissuedate],
               [createdate],
               [details_projectdescription],
               [poid]
        FROM   purchaseorder
        UNION ALL
        SELECT 'IO to PR Process Flow' AS [Process Flow Name],
               [ioinfo_poreceiveddate],
               [ioinfo_completedate],
               [ioinfo_remark],
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'PR to Procurement Process Flow' AS [Process Flow Name],
               [PRInfo_IOReceivedDate],
               [PRInfo_CompleteDate],
               [PRInfo_Remark],
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'Procurement to Finance Process Flow' AS [Process Flow Name],
               [PP_DocReceivedDate],
               [PP_CompleteDate],
               [PP_Remark],
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'Finance to CEO Process Flow' AS [Process Flow Name],
               [FP_DocReceivedDate],
               [FP_CompleteDate],
               FP_Remark,
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'CEO to Supplier Process Flow' AS [Process Flow Name],
               [CEOO_DocReceivedDate],
               [CEOO_CompleteDate],
               CEOO_Remark,
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'Supplier to Delivery Process Flow' AS [Process Flow Name],
               [VS_DocReceivedDate],
               [VS_CompleteDate],
               VS_Remarks,
               [poid]
        FROM   purchaseorder
		UNION ALL
        SELECT 'Delivery to Invoice From Supplier Process Flow' AS [Process Flow Name],
               [DOInfo_DocReceivedDate],
               [DOInfo_CompleteDate],
               DOInfo_Remark,
               [poid]
        FROM   purchaseorder
		) t1
WHERE  t1.poid = 2946  