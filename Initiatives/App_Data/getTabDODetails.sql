SELECT o.DONo,
				--m.itemsid,
                m.materialno,
                m.description,
                m.deliverydate,
                --m.outstandingquantity + m.deliveredquantity AS 'Quantity',
				n.deliveredQuantity,
				n.outstandingQuantity,
                m.uom,
                m.unitprice,
                m.totalprice,
				o.deliveredDate				
        --        Stuff((SELECT DISTINCT ',' + p.DONo
        --               FROM   PurchaseOrder m
        --                      LEFT JOIN po_item n
        --                             ON ( m.POId = n.POId )
        --                      LEFT JOIN DO_Items o
        --                             ON ( n.itemsid = o.itemsid )
								--	 LEFT JOIN DeliveryOrder p
								--	 ON ( o.DOId = p.DOId)
					   --WHERE m.POId = 3446
					   --AND o.itemsid = n.itemsid
        --               FOR xml path ('')), 1, 1, '')        AS [DONo]					   
FROM   po_item m
       LEFT JOIN do_items n
              ON ( m.itemsid = n.itemsid )
       LEFT JOIN deliveryorder o
              ON ( n.doid = o.doid )
WHERE  m.poid = 8656  