SELECT [POId]
      ,[opportunityId]
      ,[createByUserId]
      ,[createDate]
      ,[Details_PONo]
      ,[Details_POIssueDate]
      ,[PP_DocReceivedDate]
      ,[PP_CompleteDate]
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [PP_Update] = 1
  ORDER BY [PP_CompleteDate] DESC

  SELECT COUNT(*)
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [PP_Update] = 1