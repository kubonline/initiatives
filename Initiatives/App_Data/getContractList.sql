 SELECT DISTINCT m.[contractid],
                m.[name],
				p.[abbreviation] AS CustName,
                m.[contracttitle],
                m.[contractno],
                m.[type],
                n.[username] AS assignTo,
                m.[contractvalue],
                m.[remark],
                m.[startdate],
                m.[enddate],
                m.[active],
                o.[canview]
FROM   [dbo].[contracts] m
       LEFT JOIN [user] n
              ON ( m.assigntoid = n.userid )
       LEFT JOIN [users_opportunity] o
              ON ( m.contractid = o.contractid )
	   LEFT JOIN [Customer] p
              ON ( m.custid = p.custid )
WHERE  m.active = 1
       AND o.canview = 1
       AND n.username = 'saiful'