SELECT DISTINCT
	m.[opportunityid],
	'' AS #,
	o.[abbreviation] AS CustName,
	m.[name],
	s.[type],
	m.[description],
	n.[firstName] AS assignTo,
	m.[value],
	m.[proposalduedate],
	m.[targetaward],
	r.[status],
	(SELECT TOP 1
		v.username + ' - ' + w.[content]
	FROM [notification] w
	LEFT JOIN [user] v
		ON (w.fromuserid = v.userid)
	WHERE w.opportunityid = m.[opportunityNo]
	ORDER BY chatId DESC)
	AS [LastActivity],
	(SELECT TOP 1
		x.createdate
	FROM [notification] x
	LEFT JOIN [user] y
		ON (x.fromuserid = y.userid)
	WHERE x.opportunityid = m.[opportunityNo]
	ORDER BY createDate DESC)
	AS [LastActivityDate],
	m.[active],
	p.[canview]
FROM Opportunity m
LEFT JOIN [user] n
	ON (m.assigntoid = n.userid)
LEFT JOIN [customer] o
	ON (m.custid = o.custid)
LEFT JOIN [users_opportunity] p
	ON (m.opportunityid = p.opportunityid)
LEFT JOIN [OpportunityCategory] q
	ON (m.catId = q.catId)
LEFT JOIN [OpportunityStatus] r
	ON (m.statusId = r.statusId)
LEFT JOIN [OpportunityType] s
	ON (m.typeId = s.typeId)
LEFT JOIN [user] t
	ON (p.userid = t.userid)
WHERE m.active = 1
AND p.canview = 1
--AND q.username = 'arman'
ORDER BY [LastActivityDate] DESC