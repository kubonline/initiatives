  /****** Script for SelectTopNRows command from SSMS  ******/
 SELECT DISTINCT m.[opportunityid],
                m.[name],
                o.[name]                             AS CompanyName,
                n.[username],
                Stuff((SELECT ',' + p.[username]
                       FROM   [user] p
                              LEFT JOIN users_opportunity q
                                     ON ( p.userid = q.userid )
                       WHERE  p.status = 1
                              AND q.opportunityid = m.opportunityid
                       FOR xml path ('')), 1, 1, '') AS [TeamMemberName],
                (SELECT TOP 1 t.username + ' - ' + u.[content]
                 FROM   [notification] u
                        LEFT JOIN [user] t
                               ON ( u.fromuserid = t.userid )
                 WHERE  u.opportunityid = m.opportunityid
                 ORDER  BY chatid DESC)              AS [LastActivity],
                (SELECT TOP 1 u.createdate
                 FROM   [notification] u
                        LEFT JOIN [user] t
                               ON ( u.fromuserid = t.userid )
                 WHERE  u.opportunityid = m.opportunityid
                 ORDER  BY chatid DESC)              AS [LastActivityDate]
FROM   [dbo].[opportunity] m
LEFT JOIN [PurchaseOrder]} p
ON ( m.opportunityid = r.opportunityid )
       LEFT JOIN [user] n
              ON ( m.createbyuserid = n.userid )
       LEFT JOIN [customer] o
              ON ( m.custid = o.custid )
       LEFT JOIN [users_opportunity] r
              ON ( m.opportunityid = r.opportunityid )
       LEFT JOIN [user] s
              ON ( r.userid = s.userid )
WHERE  m.active = 1
       AND r.canview = 1
       AND s.username = 'm.faizal'  