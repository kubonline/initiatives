/****** Script for looking user login date n last date  ******/
SELECT [screenName]
      ,[loginDate]
      ,[lastLoginDate]
      ,[lastLoginIP]
      ,[lastFailedLoginDate]
      ,[failedLoginAttempts]
      ,[lockout]
      ,[lockoutDate]
      ,[agreedToTermsOfUse]
      ,[emailAddressVerified]
      ,[status]
  FROM [Liferay].[dbo].[User_]
  order by loginDate desc