/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [POId]
      ,[opportunityId]
      ,[createByUserId]
      ,[createDate]
      ,[Details_PONo]
      ,[Details_POIssueDate]
      ,[IOInfo_POReceivedDate]
      ,[IOInfo_IOIssueDate]
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE IOInfo_Update = 1
  ORDER BY [IOInfo_IOIssueDate] DESC

  SELECT COUNT(*)
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE IOInfo_Update = 1