USE [master]
GO
/****** Object:  Database [OpportunityManagement]    Script Date: 19/11/2015 12:10:31 AM ******/
CREATE DATABASE [OpportunityManagement]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OpportunityManagement', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\OpportunityManagement.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'OpportunityManagement_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\OpportunityManagement_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [OpportunityManagement] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OpportunityManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OpportunityManagement] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OpportunityManagement] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OpportunityManagement] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OpportunityManagement] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OpportunityManagement] SET ARITHABORT OFF 
GO
ALTER DATABASE [OpportunityManagement] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [OpportunityManagement] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OpportunityManagement] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OpportunityManagement] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OpportunityManagement] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OpportunityManagement] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OpportunityManagement] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OpportunityManagement] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OpportunityManagement] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OpportunityManagement] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OpportunityManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OpportunityManagement] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OpportunityManagement] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OpportunityManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OpportunityManagement] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OpportunityManagement] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OpportunityManagement] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OpportunityManagement] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [OpportunityManagement] SET  MULTI_USER 
GO
ALTER DATABASE [OpportunityManagement] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OpportunityManagement] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OpportunityManagement] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OpportunityManagement] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [OpportunityManagement] SET DELAYED_DURABILITY = DISABLED 
GO
USE [OpportunityManagement]
GO
/****** Object:  Table [dbo].[Chat_Entry]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chat_Entry](
	[entryId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[createDate] [datetime] NULL,
	[fromUserId] [int] NULL,
	[toUserId] [int] NULL,
	[content] [nvarchar](1000) NULL,
	[flag] [int] NULL,
 CONSTRAINT [PK_Chat_Entry] PRIMARY KEY CLUSTERED 
(
	[entryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[custId] [uniqueidentifier] ROWGUIDCOL  NULL CONSTRAINT [DF_Customer_custId]  DEFAULT (newid()),
	[name] [nvarchar](150) NULL,
	[custRegNo] [varchar](50) NULL,
	[type] [varchar](50) NULL,
	[homeURL] [nvarchar](75) NULL,
	[address] [nvarchar](150) NULL,
	[telephoneNo] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FileEntry]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileEntry](
	[uuid] [uniqueidentifier] ROWGUIDCOL  NULL,
	[fileEntryId] [int] NOT NULL,
	[groupId] [int] NULL,
	[companyId] [int] NULL,
	[userId] [int] NULL,
	[userName] [nvarchar](75) NULL,
	[createDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[repositoryId] [int] NULL,
	[folderId] [int] NULL,
	[treePath] [nvarchar](2000) NULL,
	[name] [nvarchar](255) NULL,
	[extension] [nvarchar](75) NULL,
	[mimeType] [nvarchar](75) NULL,
	[title] [nvarchar](255) NULL,
	[size] [bigint] NULL,
	[readCount] [int] NULL,
 CONSTRAINT [PK_FileEntry] PRIMARY KEY CLUSTERED 
(
	[fileEntryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Folder]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[uuid] [uniqueidentifier] NULL,
	[folderId] [int] NOT NULL,
	[opportunityId] [int] NULL,
	[companyId] [int] NULL,
	[userId] [int] NULL,
	[userName] [nvarchar](75) NULL,
	[createDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[repositoryId] [int] NULL,
	[parentFolderId] [int] NULL,
	[treePath] [nvarchar](2000) NULL,
	[name] [nvarchar](100) NULL,
	[lastPostDate] [datetime] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[folderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Opportunities]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opportunities](
	[uuid] [uniqueidentifier] ROWGUIDCOL  NULL,
	[opportunityId] [int] NOT NULL,
	[custId] [int] NULL,
	[creatorUserId] [varchar](50) NULL,
	[name] [nvarchar](150) NULL,
	[type] [varchar](50) NULL,
	[source] [nvarchar](150) NULL,
	[creditTerm] [int] NULL,
	[creditLimit] [int] NULL,
	[industry] [varchar](50) NULL,
	[region] [nvarchar](150) NULL,
	[cusRequirementShort] [nvarchar](150) NULL,
	[cusRequirementDetail] [nvarchar](2000) NULL,
	[remark] [nvarchar](2000) NULL,
	[budgetaryRevenue] [bigint] NULL,
	[proposalDueDate] [datetime] NULL,
	[proposalIntDate] [datetime] NULL,
	[proposalRevenue] [bigint] NULL,
	[salesOrderRevenue] [bigint] NULL,
	[proposedGP] [bigint] NULL,
	[salesOrder] [bigint] NULL,
	[active] [bit] NULL,
 CONSTRAINT [PK_Opportunities] PRIMARY KEY CLUSTERED 
(
	[opportunityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Repository]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repository](
	[uuid] [uniqueidentifier] ROWGUIDCOL  NULL,
	[repositoryId] [int] NOT NULL,
	[opportunityId] [int] NULL,
	[companyId] [int] NULL,
	[userId] [int] NULL,
	[userName] [nvarchar](75) NULL,
	[createDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[name] [nvarchar](75) NULL,
	[folderId] [int] NULL,
 CONSTRAINT [PK_Repository] PRIMARY KEY CLUSTERED 
(
	[repositoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[uuid] [uniqueidentifier] ROWGUIDCOL  NULL,
	[roleId] [int] NOT NULL,
	[companyId] [int] NULL,
	[userId] [int] NULL,
	[userName] [nvarchar](75) NULL,
	[createDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[name] [varchar](75) NULL,
	[title] [varchar](2000) NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[uuid] [uniqueidentifier] ROWGUIDCOL  NULL CONSTRAINT [DF_User_uuid]  DEFAULT (newid()),
	[userId] [varchar](50) NOT NULL,
	[companyId] [int] NULL,
	[createDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[password] [nvarchar](75) NULL,
	[passwordEncrypted] [bit] NULL,
	[passwordReset] [bit] NULL,
	[passwordModifiedDate] [datetime] NULL,
	[reminderQueryQuestion] [varchar](75) NULL,
	[reminderQueryAnswer] [varchar](75) NULL,
	[screenName] [nvarchar](75) NULL,
	[emailAddress] [nvarchar](75) NULL,
	[firstName] [nvarchar](75) NULL,
	[lastName] [nvarchar](75) NULL,
	[jobTitle] [varchar](100) NULL,
	[loginDate] [datetime] NULL,
	[loginIP] [nvarchar](75) NULL,
	[lastLoginDate] [datetime] NULL,
	[lastLoginIP] [nvarchar](75) NULL,
	[lastFailedLoginDate] [datetime] NULL,
	[failedLoginAttempts] [int] NULL,
	[lockout] [bit] NULL,
	[lockoutDate] [datetime] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserOpportunityRole]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserOpportunityRole](
	[userId] [varchar](50) NOT NULL,
	[opportunityId] [int] NOT NULL,
	[roleId] [int] NOT NULL,
	[canView] [bit] NULL,
	[canRename] [bit] NULL,
	[canUpload] [bit] NULL,
	[canEdit] [bit] NULL,
 CONSTRAINT [PK_UserOpportunityRole] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC,
	[userId] ASC,
	[opportunityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users_Opportunity]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users_Opportunity](
	[opportunityId] [int] NOT NULL,
	[userId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Users_Opportunity] PRIMARY KEY CLUSTERED 
(
	[opportunityId] ASC,
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users_Roles]    Script Date: 19/11/2015 12:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users_Roles](
	[roleId] [int] NOT NULL,
	[userId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Users_Roles] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC,
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Chat_Entry] ADD  CONSTRAINT [DF_Chat_Entry_entryId]  DEFAULT (newid()) FOR [entryId]
GO
ALTER TABLE [dbo].[FileEntry] ADD  CONSTRAINT [DF_FileEntry_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [dbo].[Opportunities] ADD  CONSTRAINT [DF_Opportunities_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [dbo].[Repository] ADD  CONSTRAINT [DF_Repository_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_uuid]  DEFAULT (newid()) FOR [uuid]
GO
USE [master]
GO
ALTER DATABASE [OpportunityManagement] SET  READ_WRITE 
GO
