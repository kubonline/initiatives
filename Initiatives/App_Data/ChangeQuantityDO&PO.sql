/****** Script for SelectTopNRows command from SSMS  ******/
SELECT m.[POId],
m.[Details_PONo],
m.[opportunityId],
m.StatusId,
p.[status],
n.itemsId,
n.partNo,
n.outstandingQuantity,
n.deliveryQuantity,
n.deliveredQuantity,
o.DOId,
o.outstandingQuantity,
o.deliveryQuantity,
o.deliveredQuantity
  FROM [OpportunityManagement].[dbo].[PurchaseOrder] m
LEFT JOIN [PO_Item] n ON (m.POId = n.POId)
LEFT JOIN [DO_Items] o ON (n.itemsId = o.itemsId)
LEFT JOIN [POStatus] p ON (m.StatusId = p.StatusId)
  WHERE m.Details_PONo = '4901495240'

  UPDATE [PO_Item]
  SET outstandingQuantity = 100, deliveredQuantity = 400
  WHERE POId = 28186

  UPDATE [DO_Items]
  SET outstandingQuantity = 100, deliveryQuantity = 400
  WHERE DOId = 14598