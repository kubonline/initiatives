SELECT DISTINCT p.[doid],
                m.[opportunityid],
                q.[dono],
                q.[deliverydate],
                q.[doissuedate],
                q.[delivereddate],
                Stuff((SELECT DISTINCT ',' + r.[details_pono]
                       FROM   [purchaseorder] r
                              LEFT JOIN [po_item] s
                                     ON ( r.poid = s.poid )
                              LEFT JOIN [do_items] t
                                     ON ( s.itemsid = t.itemsid )
                              LEFT JOIN [deliveryorder] u
                                     ON ( t.doid = u.doid )
                       WHERE  m.active = 1
                              AND t.doid = p.doid
                              --AND u.DOId IS NOT NULL
                              AND m.supplierid = 81
                       FOR xml path ('')), 1, 1, '') AS [details_pono],
                (SELECT TOP 1 q.username + ' - ' + r.[content]
                 FROM   [notification] r
                        LEFT JOIN [user] q
                               ON ( r.fromuserid = q.userid )
                 WHERE  r.doid = p.doid
                 ORDER  BY chatid DESC)              AS [LastActivity],
                (SELECT TOP 1 r.createdate
                 FROM   [notification] r
                        LEFT JOIN [user] q
                               ON ( r.fromuserid = q.userid )
                 WHERE  r.doid = p.doid
                 ORDER  BY chatid DESC)              AS [LastActivityDate]
FROM   [dbo].[opportunity] m
       LEFT JOIN [purchaseorder] n
              ON ( m.opportunityid = n.opportunityid )
       LEFT JOIN [po_item] o
              ON ( n.poid = o.poid )
       LEFT JOIN [do_items] p
              ON ( o.itemsid = p.itemsid )
       LEFT JOIN [deliveryorder] q
              ON ( p.doid = q.doid )
WHERE  m.active = 1
       AND p.doid IS NOT NULL
       AND m.supplierid = 81
--AND m.opportunityid = 794
ORDER  BY lastactivitydate DESC  