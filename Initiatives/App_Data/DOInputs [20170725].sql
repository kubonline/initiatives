SELECT [POId]
      ,[opportunityId]
	  ,[Details_PONo]
      ,[DOInfo_DocReceivedDate]
      ,[DOInfo_PreparedById]
      ,[DOInfo_ApprovedById]
      ,[DOInfo_ApprovedDate]
      ,[DOInfo_DONo]
      ,[DOInfo_Remark]
      ,[DOInfo_EI_Name]
      ,[DOInfo_EI_Position]
      ,[DOInfo_SI_Name]
      ,[DOInfo_SI_Position]
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [DOInfo_Update] = 1
  ORDER BY [DOInfo_CompleteDate] DESC

  SELECT COUNT(*)
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [DOInfo_Update] = 1