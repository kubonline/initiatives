/****** Script for SelectTopNRows command from SSMS  ******/
SELECT m.[opportunityId]
      ,m.[userId]
	  ,n.[userName]
      ,m.[canView]
      ,m.[canEdit]
      ,m.[canUpload]
      ,m.[canDelete]
  FROM [Users_Opportunity] m
  LEFT JOIN [User] n on (m.userId = n.userId)
  WHERE m.opportunityId = 14