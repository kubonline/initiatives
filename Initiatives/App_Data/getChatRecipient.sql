SELECT m.userid,
       m.username
FROM   [user] m
       LEFT JOIN users_opportunity n
              ON ( m.userid = n.userid )
WHERE  m.status = 1
       AND n.opportunityid = 14
