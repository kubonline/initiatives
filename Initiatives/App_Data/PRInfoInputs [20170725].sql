SELECT [POId]
      ,[opportunityId]
      ,[createByUserId]
      ,[createDate]
      ,[Details_PONo]
      ,[Details_POIssueDate]
      ,[PRInfo_IOReceivedDate]
      ,[PRInfo_CompleteDate]
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [PRInfo_Update] = 1
  ORDER BY [PRInfo_CompleteDate] DESC

  SELECT COUNT(*)
  FROM [OpportunityManagement].[dbo].[PurchaseOrder]
  WHERE [PRInfo_Update] = 1