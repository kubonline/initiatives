/****** Script for SelectTopNRows command from SSMS  ******/
SELECT m.[opportunityId]
      ,m.[name]	  
      ,m.[type]
      ,m.[source]
      ,m.[variationOrder]
      ,o.[name] AS CompanyName
      ,m.[creditTerm]
      ,m.[creditLimit]
      ,m.[outstandingInvoice]
      ,n.[username]
      ,m.[industry]
      ,m.[region]
      ,m.[cusRequirementShort]
      ,m.[cusRequirementDetail]
	  ,m.[coreSolution]
	  ,m.[expectedClosingDate]
      ,m.[remark]
      ,m.[budgetaryRevenue]
      ,m.[proposalDueDate]
      ,m.[proposalIntDate]
      ,m.[proposalRevenue]
      ,m.[salesOrderRevenue]
      ,m.[proposedGP]
      ,m.[salesOrderGP]
      ,m.[active]
  FROM [dbo].[Opportunity] m
  LEFT JOIN [user] n
              ON ( m.createbyuserid = n.userid )
       LEFT JOIN [customer] o
              ON ( m.custid = o.custid )
  --WHERE opportunityId = @opportunityId
  WHERE opportunityId = '4'