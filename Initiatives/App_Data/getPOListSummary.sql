  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT n.[poid],
	   --'' AS #,
	   m.[opportunityId],
       n.[details_pono],
       n.[details_poissuedate],
       n.[Details_totalPrice] + n.[Details_totalGST] AS 'Total Amount',
       r.[status],
       o.[username] AS [Created By],
       (SELECT TOP 1 q.username + ' - ' + p.[content]
        FROM   [notification] p
               LEFT JOIN [user] q
                      ON ( p.fromuserid = q.userid )
        WHERE  p.poid = n.poid
        ORDER  BY chatid DESC) AS [LastActivity],
       (SELECT TOP 1 p.createdate
        FROM   [notification] p
               LEFT JOIN [user] q
                      ON ( p.fromuserid = q.userid )
        WHERE  p.poid = n.poid
        ORDER  BY chatid DESC) AS [LastActivityDate]
FROM   [dbo].[opportunity] m
       LEFT JOIN [purchaseorder] n
              ON ( m.opportunityid = n.opportunityid )
       LEFT JOIN [user] o
              ON ( n.createbyuserid = o.userid )
			  LEFT JOIN [POStatus] r
				ON ( n.StatusId = r.statusId )
WHERE  m.active = 1
       AND m.opportunityid = 794
ORDER  BY lastactivitydate DESC