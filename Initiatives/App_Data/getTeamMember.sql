/****** Script for SelectTopNRows command from SSMS  ******/
SELECT m.[userId]
	  ,m.[userName]
	  ,n.opportunityId
      ,m.[status]
  FROM [User] m
  LEFT JOIN Users_Opportunity n ON (m.userId = n.userId)
  WHERE m.status = 1 and (n.opportunityId <> 4 or n.opportunityId is NULL) and m.userId <> 12