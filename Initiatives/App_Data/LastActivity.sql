SELECT u.opportunityid,
       t.username + ' - ' + u.[content] AS [LastActivity],
       u.createdate
FROM   [notification] u
       LEFT JOIN [user] t
              ON ( u.fromuserid = t.userid )
--WHERE u.opportunityId = m.opportunityId
ORDER  BY chatid DESC