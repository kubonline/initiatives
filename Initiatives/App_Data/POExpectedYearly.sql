declare @startDate datetime;
declare @endDate datetime;
declare @contractId int;
 
-- Edit here
set @startDate = '2016-02-07';
set @endDate = '2017-04-01';
set @contractId = 824;
 
WITH CTE AS
(
    SELECT @startDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(YEAR, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(YEAR, 1, cte_start_date) <= @endDate    
)
 
SELECT
    un_months.Year_Month,
    FORMAT((CASE WHEN ISNULL(Total, -1) = -1 THEN 0 ELSE Total END),'#,0.00') as TotalPrice,
	FORMAT((CASE WHEN ISNULL(TotalGST, -1) = -1 THEN 0 ELSE TotalGST END),'#,0.00') as TotalGST,
		FORMAT(m.expectedAmount,'#,0.00') AS POExpectedAmount
FROM
    (
        SELECT
            (
                CONVERT(VARCHAR(4), DATEPART(YEAR, t01.Details_POIssueDate))
            ) AS estimated_month,
			SUM(t01.Details_totalPrice) AS Total,
            SUM(t01.Details_totalGST) AS TotalGST
        FROM
            [PurchaseOrder] t01
            JOIN [Opportunity] t20
                ON t01.opportunityId = t20.opportunityId			
        WHERE
            t01.Details_POIssueDate BETWEEN @startDate AND @endDate AND
            t01.Details_totalGST >= 0 AND
			t01.opportunityId = @contractId
        GROUP BY
            (
                CONVERT(VARCHAR(4), DATEPART(YEAR, t01.Details_POIssueDate))
            )
    ) un_totals
    FULL OUTER JOIN (
        SELECT
            (
                CONVERT(VARCHAR(4), DATEPART(YEAR, cte_start_date))
            ) as year_month
        FROM
            CTE
    ) un_months ON un_totals.estimated_month = un_months.year_month
	LEFT JOIN [POExpected] m ON (un_months.year_month = m.yearMonth AND m.opportunityId = @contractId)
ORDER BY
    un_months.year_month ASC