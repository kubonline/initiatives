/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT [chatid],
                [opportunityid],
                [fromusername],
                [createdate],
                [content],
                Stuff((SELECT ',' + [tousername]
                       FROM   messagelist
                       WHERE  [chatid] = a.[chatid]
                       FOR xml path ('')), 1, 1, '') AS [ToUserName]
FROM   messagelist AS a 
--WHERE opportunityid = 14