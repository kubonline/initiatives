/****** Script for SelectTopNRows command from SSMS  ******/
SELECT m.[userid],
       m.[companyid],
       m.[username],
       m.[password],
       m.[passwordencrypted],
       m.[passwordreset],
       m.[passwordmodifieddate],
       m.[reminderqueryquestion],
       m.[reminderqueryanswer],
       m.[emailaddress],
       m.[failedloginattempts],
       m.[lockout],
       o.[name] AS role,
       o.canviewalloppo,
       o.canadduser,
       o.canaddcustomer,
       m.[status]
FROM   [dbo].[user] m
       LEFT JOIN users_roles n
              ON ( m.userid = n.userid )
       LEFT JOIN [role] o
              ON ( n.roleid = o.roleid )
WHERE  username = @username
       AND password = @password