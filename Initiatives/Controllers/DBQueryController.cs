﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;

namespace Initiatives.Controllers
{
    public class DBQueryController : Controller
    {
        private static OpportunityManagementEntities db = new OpportunityManagementEntities();
        public static SqlConnection OpenDBConnection()
        {
            SqlConnection conn = null;
            try
            {

                conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString);               
                //conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ADConnectionString"].ConnectionString);
                conn.Open();
                //Debug.Print("Connected")
            }
            catch
            {
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }

                conn = null;
                Debug.Print("Failed");
            }
            return conn;
        }
        public static void CloseDBConnection(SqlConnection conn)
        {
            if ((conn != null))
            {
                conn.Close();
            }
            //Debug.Print("Connection Closed")
        }
        public static void saveUsername(string username, string password)
        {
            try
            {
                User objUserDetaildb = db.Users.First(m => m.userName == username);
                objUserDetaildb.password = password;
                db.SaveChanges();              
            }
            catch
            {
                User _objCreateUser = new User
                {
                    uuid = Guid.NewGuid(),
                    createDate = DateTime.Now,
                    userName = username,
                    password = password,
                    //_objCreateUser.createByUserId = Int32.Parse(Session["UserId"].ToString());
                    companyId = 81,
                    //_objCreateUser.emailAddress = UserDetailList.EmailAddress;
                    //_objCreateUser.firstName = UserDetailList.FirstName;
                    //_objCreateUser.lastName = UserDetailList.LastName;
                    //_objCreateUser.jobTitle = UserDetailList.JobTitle;
                    //_objCreateUser.oneLevelSuperiorId = UserDetailList.OneLevelSuperiorId;
                    //_objCreateUser.telephoneNo = UserDetailList.TelephoneNo;
                    //_objCreateUser.address = UserDetailList.Address;
                    //_objCreateUser.extensionNo = UserDetailList.ExtensionNo;
                    passwordReset = false,
                    lockout = false,
                    status = true
                };

                db.Users.Add(_objCreateUser);
                db.SaveChanges();

                Users_Roles _objCreateUserRole = new Users_Roles
                {
                    uuid = Guid.NewGuid(),
                    userId = _objCreateUser.userId,
                    roleId = "R01"
                };
                db.Users_Roles.Add(_objCreateUserRole);
                db.SaveChanges();               
            }                                                      
        }
        public static List<OppoListTable> getOpportunityInfo(string username, string roleid)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("OpportunityList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "OpportunityList");
                conn = OpenDBConnection();
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                string sql = "";
                if (roleid != "R02")
                {
                    sql = " SELECT DISTINCT " +
                            " m.[opportunityid], " +
                            " '' AS #, " +
                            " o.[abbreviation] AS CustName, " +
                            " m.[name], " +
                            " s.[type], " +
                            " m.[description], " +
                            " n.[firstName] AS assignTo, " +
                            //" FORMAT(m.[value],'#,0.00') AS value, " +
                            " m.[value], " +
                            " m.[proposalduedate], " +
                            " m.[targetaward], " +
                            " r.[status], " +
                            " (SELECT TOP 1 " +
                                " v.username + ' - ' + w.[content] " +
                            " FROM [notification] w " +
                            " LEFT JOIN [user] v " +
                                " ON (w.fromuserid = v.userid) " +
                            " WHERE w.opportunityid = m.[opportunityId] " +
                            " ORDER BY chatid DESC) " +
                            " AS [LastActivity], " +
                            " (SELECT TOP 1 " +
                                " x.createdate " +
                            " FROM [notification] x " +
                            " LEFT JOIN [user] y " +
                                " ON (x.fromuserid = y.userid) " +
                            " WHERE x.opportunityid = m.[opportunityId] " +
                            " ORDER BY createDate DESC) " +
                            " AS [LastActivityDate] " +
                        " FROM Opportunity m " +
                        " LEFT JOIN [user] n " +
                            " ON (m.assigntoid = n.userid) " +
                        " LEFT JOIN [customer] o " +
                            " ON (m.custid = o.custid) " +
                        " LEFT JOIN [users_opportunity] p " +
                            " ON (m.opportunityid = p.opportunityid) " +
                        " LEFT JOIN [OpportunityCategory] q " +
                            " ON (m.catId = q.catId) " +
                        " LEFT JOIN [OpportunityStatus] r " +
                            " ON (m.statusId = r.statusId) " +
                        " LEFT JOIN [OpportunityType] s " +
                            " ON (m.typeId = s.typeId) " +
                        " LEFT JOIN [user] t " +
                            " ON (p.userid = t.userid) " +
                        " WHERE m.active = 1 " +
                        " AND p.canview = 1 " +
                        " AND m.statusId <> 'OS007' " +
                        " AND t.username = @username " +
                        " ORDER BY [LastActivityDate] DESC ";
                    //"AND q.username = 'saiful' ";
                }
                else
                {
                    sql = " SELECT DISTINCT " +
                            " m.[opportunityid], " +
                            " '' AS #, " +
                            " o.[abbreviation] AS CustName, " +
                            " m.[name], " +
                            " s.[type], " +
                            " m.[description], " +
                            " n.[firstName] AS assignTo, " +
                            //" FORMAT(m.[value],'#,0.00') AS value, " +
                            " m.[value], " +
                            " m.[proposalduedate], " +
                            " m.[targetaward], " +
                            " r.[status], " +
                            " (SELECT TOP 1 " +
                                " v.username + ' - ' + w.[content] " +
                            " FROM [notification] w " +
                            " LEFT JOIN [user] v " +
                                " ON (w.fromuserid = v.userid) " +
                            " WHERE w.opportunityid = m.[opportunityId] " +
                            " ORDER BY chatid DESC) " +
                            " AS [LastActivity], " +
                            " (SELECT TOP 1 " +
                                " x.createdate " +
                            " FROM [notification] x " +
                            " LEFT JOIN [user] y " +
                                " ON (x.fromuserid = y.userid) " +
                            " WHERE x.opportunityid = m.[opportunityId] " +
                            " ORDER BY createDate DESC) " +
                            " AS [LastActivityDate] " +
                        " FROM Opportunity m " +
                        " LEFT JOIN [user] n " +
                            " ON (m.assigntoid = n.userid) " +
                        " LEFT JOIN [customer] o " +
                            " ON (m.custid = o.custid) " +
                        " LEFT JOIN [users_opportunity] p " +
                            " ON (m.opportunityid = p.opportunityid) " +
                        " LEFT JOIN [OpportunityCategory] q " +
                            " ON (m.catId = q.catId) " +
                        " LEFT JOIN [OpportunityStatus] r " +
                            " ON (m.statusId = r.statusId) " +
                        " LEFT JOIN [OpportunityType] s " +
                            " ON (m.typeId = s.typeId) " +
                        " LEFT JOIN [user] t " +
                            " ON (p.userid = t.userid) " +
                        " WHERE m.active = 1 " +
                        " AND p.canview = 1 " +
                        " AND m.statusId <> 'OS007' " +
                        " ORDER BY [LastActivityDate] DESC ";
                }

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@username", SqlDbType.VarChar);
                SQLcmd.Parameters["@username"].Value = username;
                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

            var OpportunityList = returnDS.Tables[0].AsEnumerable().Select(
            dataRow => new OppoListTable
            {
                //Uid = uid,
                Id = dataRow.Field<int>("opportunityid"),
                Empty = dataRow.Field<string>("#"),
                Abbreviation = dataRow.Field<string>("CustName"),
                OpportunityName = dataRow.Field<string>("name"),
                TargetAward = dataRow.Field<string>("targetaward"),
                OppoType = dataRow.Field<string>("type"),
                Description = dataRow.Field<string>("description"),
                AccManagerName = dataRow.Field<string>("assignTo"),
                OpportunityValue = String.Format("{0:n}", dataRow.Field<decimal>("value")),
                Submission = dataRow.Field<DateTime?>("proposalduedate"),                
                Status = dataRow.Field<string>("status"),
                LastActivity = dataRow.Field<string>("LastActivity"),
                LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
            }).ToList();

            return OpportunityList;
        }
        public static List<ContractListTable> getContractInfo(int? CompanyId, string username, string roleid)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("ContractInfo");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "ContractInfo");
                conn = OpenDBConnection();
                string sql = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                if (roleid != "R02")
                {
                    sql = " SELECT DISTINCT " +
                            " m.[opportunityid], " +
                            " '' AS #, " +
                            " o.[abbreviation] AS CustName, " +
                            " m.[name], " +
                            " s.[type], " +
                            " m.[contractNo], " +
                            " m.[description], " +
                            " n.[firstName] AS assignTo, " +
                            " m.[value], " +
                            " m.[startDate], " +
                            " m.[EndDate], " +
                            " (SELECT TOP 1 " +
                                " v.username + ' - ' + w.[content] " +
                            " FROM [notification] w " +
                            " LEFT JOIN [user] v " +
                                " ON (w.fromuserid = v.userid) " +
                            " WHERE w.opportunityid = m.[opportunityId] " +
                            " ORDER BY chatid DESC) " +
                            " AS [LastActivity], " +
                            " (SELECT TOP 1 " +
                                " x.createdate " +
                            " FROM [notification] x " +
                            " LEFT JOIN [user] y " +
                                " ON (x.fromuserid = y.userid) " +
                            " WHERE x.opportunityid = m.[opportunityId] " +
                            " ORDER BY createDate DESC) " +
                            " AS [LastActivityDate] " +
                        " FROM Opportunity m " +
                        " LEFT JOIN [user] n " +
                            " ON (m.assigntoid = n.userid) " +
                        " LEFT JOIN [customer] o " +
                            " ON (m.custid = o.custid) " +
                        " LEFT JOIN [users_opportunity] p " +
                            " ON (m.opportunityid = p.opportunityid) " +
                        " LEFT JOIN [OpportunityCategory] q " +
                            " ON (m.catId = q.catId) " +
                        " LEFT JOIN [OpportunityStatus] r " +
                            " ON (m.statusId = r.statusId) " +
                        " LEFT JOIN [OpportunityType] s " +
                            " ON (m.typeId = s.typeId) " +
                        " LEFT JOIN [user] t " +
                            " ON (p.userid = t.userid) " +
                        " WHERE m.active = 1 " +
                        " AND p.canview = 1 " +
                        " AND m.supplierId = @companyId " +
                        " AND m.statusId = 'OS007' " +
                        " AND t.username = @username " +
                        " ORDER BY [LastActivityDate] DESC ";
                }
                else
                {
                    sql = " SELECT DISTINCT " +
                            " m.[opportunityid], " +
                            " '' AS #, " +
                            " o.[abbreviation] AS CustName, " +
                            " m.[name], " +
                            " s.[type], " +
                            " m.[contractNo], " +
                            " m.[description], " +
                            " n.[firstName] AS assignTo, " +
                            " m.[value], " +
                            " m.[startDate], " +
                            " m.[EndDate], " +
                            " (SELECT TOP 1 " +
                                " v.username + ' - ' + w.[content] " +
                            " FROM [notification] w " +
                            " LEFT JOIN [user] v " +
                                " ON (w.fromuserid = v.userid) " +
                            " WHERE w.opportunityid = m.[opportunityId] " +
                            " ORDER BY chatid DESC) " +
                            " AS [LastActivity], " +
                            " (SELECT TOP 1 " +
                                " x.createdate " +
                            " FROM [notification] x " +
                            " LEFT JOIN [user] y " +
                                " ON (x.fromuserid = y.userid) " +
                            " WHERE x.opportunityid = m.[opportunityId] " +
                            " ORDER BY createDate DESC) " +
                            " AS [LastActivityDate] " +
                        " FROM Opportunity m " +
                        " LEFT JOIN [user] n " +
                            " ON (m.assigntoid = n.userid) " +
                        " LEFT JOIN [customer] o " +
                            " ON (m.custid = o.custid) " +
                        " LEFT JOIN [users_opportunity] p " +
                            " ON (m.opportunityid = p.opportunityid) " +
                        " LEFT JOIN [OpportunityCategory] q " +
                            " ON (m.catId = q.catId) " +
                        " LEFT JOIN [OpportunityStatus] r " +
                            " ON (m.statusId = r.statusId) " +
                        " LEFT JOIN [OpportunityType] s " +
                            " ON (m.typeId = s.typeId) " +
                        " LEFT JOIN [user] t " +
                            " ON (p.userid = t.userid) " +
                        " WHERE m.active = 1 " +
                        " AND p.canview = 1 " +
                        " AND m.supplierId = @companyId " +
                        " AND m.statusId = 'OS007' " +
                        " ORDER BY [LastActivityDate] DESC ";
                }

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@username", SqlDbType.VarChar);
                SQLcmd.Parameters.Add("@companyId", SqlDbType.Int);
                SQLcmd.Parameters["@username"].Value = username;
                SQLcmd.Parameters["@companyId"].Value = CompanyId;

                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

            var ContractList = returnDS.Tables[0].AsEnumerable().Select(
            dataRow => new ContractListTable
            {
                //Uid = uid,
                Id = dataRow.Field<int>("opportunityid"),
                Empty = dataRow.Field<string>("#"),
                Customer = dataRow.Field<string>("CustName"),
                ContractName = dataRow.Field<string>("name"),
                ContractType = dataRow.Field<string>("type"),
                ContractNo = dataRow.Field<string>("contractNo"),
                Description = dataRow.Field<string>("description"),
                AssignTo = dataRow.Field<string>("assignTo"),
                ContractValue = String.Format("{0:n}", dataRow.Field<decimal>("value")),
                StartDate = dataRow.Field<DateTime>("startDate"),
                EndDate = dataRow.Field<DateTime>("EndDate"),
                LastActivity = dataRow.Field<string>("LastActivity"),
                LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
            }).ToList();

            return ContractList;
        }
        public static List<POListTable> getPOList(int userid, string roleid, int ContractId, int? SupplierId, string DO)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("POList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "POList");
                conn = OpenDBConnection();
                string sql = ""; string sql1 = ""; string sql2 = ""; string sql3 = ""; string sql4 = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                if (ContractId != 0)
                {                   
                    sql2 = "AND m.opportunityid = @contractId ";
                }
                if (roleid != "R02")
                {
                    sql1 = "AND s.userid = @userId ";
                    sql3 = "LEFT JOIN [users_opportunity] s ON ( m.opportunityid =  s.opportunityid ) ";
                }
                if (DO == "DO")
                {
                    sql4 = "AND n.statusID != 'POS008' ";
                } else if (DO == "IO")
                {
                    sql4 = "AND n.statusID != 'POS008' AND n.[IOInfo_Update] = 0 ";
                }
                else if (DO == "PR")
                {
                    sql4 = "AND n.[PRInfo_Update] = 0 ";
                }
                sql = "SELECT n.[poid], " +
                           //"'' AS #, " +
                           "m.[opportunityId], " +
                           "n.[details_pono], " +
                           "n.[Details_projectDescription], " +
                           "n.[details_poissuedate], " +
                           "n.[details_totalprice] + n.[details_totalGST] AS 'Total Amount', " +
                           "r.[status], " +
                           "o.[username] AS [Created By], " +
                           "(SELECT TOP 1 q.username + ' - ' + p.[content] " +
                            "FROM   [notification] p " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( p.fromuserid = q.userid ) " +
                            "WHERE  p.poid = n.poid " +
                            "ORDER  BY chatid DESC) AS [LastActivity], " +
                           "(SELECT TOP 1 p.createdate " +
                            "FROM   [notification] p " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( p.fromuserid = q.userid ) " +
                            "WHERE  p.poid = n.poid " +
                            "ORDER  BY chatid DESC) AS [LastActivityDate] " +
                    "FROM   [dbo].[opportunity] m " +
                           "LEFT JOIN [purchaseorder] n " +
                                  "ON ( m.opportunityid = n.opportunityid ) " +
                           "LEFT JOIN [user] o " +
                                  "ON ( n.createbyuserid = o.userid ) " +
                           "LEFT JOIN [POStatus] r " +
                                  "ON ( n.StatusId = r.statusId ) " +
                           sql3 +
                    "WHERE  m.active = 1 " +
                           "AND m.supplierId = @supplierId " +
                           sql1 +
                           sql2 +
                           sql4 +
                    "ORDER  BY lastactivitydate DESC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@contractId", SqlDbType.VarChar);
                SQLcmd.Parameters["@contractId"].Value = ContractId;
                SQLcmd.Parameters.Add("@supplierId", SqlDbType.Int);
                SQLcmd.Parameters["@supplierId"].Value = SupplierId;
                SQLcmd.Parameters.Add("@userId", SqlDbType.Int);
                SQLcmd.Parameters["@userId"].Value = userid;

                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

            List<POListTable> POList = new List<POListTable>();
            if (returnDS.Tables[0].Rows.Count != 0)
            {
                POList = returnDS.Tables[0].AsEnumerable().Select(
                dataRow => new POListTable
                {
                    //Uid = uid,
                    POId = dataRow.Field<int?>("poid"),
                    ContId = dataRow.Field<int?>("opportunityId"),
                    //Empty = dataRow.Field<string>("#"),
                    PONo = dataRow.Field<string>("details_pono"),
                    Description = dataRow.Field<string>("Details_projectDescription"),                    
                    POIssueDate = dataRow.Field<DateTime?>("details_poissuedate"),
                    TotalAmount = dataRow.Field<decimal?>("Total Amount"),
                    Status = dataRow.Field<string>("status"),
                    CreatedBy = dataRow.Field<string>("Created By"),
                    LastActivity = dataRow.Field<string>("LastActivity"),
                    LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
                }).DefaultIfEmpty().ToList();
            }
            
            return POList;
        }        
        public static List<POExpectedList> getPOExpected(DateTime startDate, DateTime endDate, int ContractId)
        {
            SqlConnection conn = null;
            DataTable returnDS = new DataTable();
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "POList");
                conn = OpenDBConnection();
                string sql = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                sql = "WITH CTE AS " +
                        "( " +
                            "SELECT @startDate AS cte_start_date " +
                            "UNION ALL " +
                            "SELECT DATEADD(YEAR, 1, cte_start_date) " +
                            "FROM CTE " +
                            "WHERE DATEADD(YEAR, 1, cte_start_date) <= @endDate " +
                        ") " +
                        " " +
                        "SELECT " +
                            "un_months.Year_Month, " +
                            "FORMAT((CASE WHEN ISNULL(Total, -1) = -1 THEN 0 ELSE Total END),'#,0.00') as TotalPrice, " +
                            "FORMAT((CASE WHEN ISNULL(TotalGST, -1) = -1 THEN 0 ELSE TotalGST END),'#,0.00') as TotalGST, " +
                                "FORMAT(m.expectedAmount,'#,0.00') AS POExpectedAmount " +
                        "FROM " +
                            "( " +
                                "SELECT " +
                                    "( " +
                                        "CONVERT(VARCHAR(4), DATEPART(YEAR, t01.Details_POIssueDate)) " +
                                    ") AS estimated_month, " +
                                    "SUM(t01.Details_totalPrice) AS Total, " +
                                    "SUM(t01.Details_totalGST) AS TotalGST " +
                                "FROM " +
                                    "[PurchaseOrder] t01 " +
                                    "JOIN [Opportunity] t20 " +
                                        "ON t01.opportunityId = t20.opportunityId " +
                                "WHERE " +
                                    "t01.Details_POIssueDate BETWEEN @startDate AND @endDate AND " +
                                    "t01.Details_totalGST >= 0 AND " +
                                    "t01.opportunityId = @contractId " +
                                "GROUP BY " +
                                    "( " +
                                        "CONVERT(VARCHAR(4), DATEPART(YEAR, t01.Details_POIssueDate)) " +
                                    ") " +
                            ") un_totals " +
                            "FULL OUTER JOIN ( " +
                                "SELECT " +
                                    "( " +
                                        "CONVERT(VARCHAR(4), DATEPART(YEAR, cte_start_date)) " +
                                    ") as year_month " +
                                "FROM " +
                                    "CTE " +
                            ") un_months ON un_totals.estimated_month = un_months.year_month " +
                            "LEFT JOIN [POExpected] m ON (un_months.year_month = m.yearMonth AND m.opportunityId = @contractId) " +
                        "ORDER BY " +
                            "un_months.year_month ASC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;
                DateTime newEndDate = endDate.AddDays(1);

                SQLcmd.Parameters.Add("@startDate", SqlDbType.DateTime);
                SQLcmd.Parameters.Add("@endDate", SqlDbType.DateTime);
                SQLcmd.Parameters.Add("@contractId", SqlDbType.Int);
                SQLcmd.Parameters["@startDate"].Value = startDate;
                SQLcmd.Parameters["@endDate"].Value = newEndDate;
                SQLcmd.Parameters["@contractId"].Value = ContractId;

                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

            var empList = returnDS.AsEnumerable().Select(
                            dataRow => new POExpectedList
                            {
                                Year = dataRow.Field<String>("Year_Month"),
                                TotalPrice = dataRow.Field<String>("TotalPrice"),
                                TotalGST = dataRow.Field<String>("TotalGST"),
                                ExpectedAmount = dataRow.Field<String>("POExpectedAmount")
                            }).DefaultIfEmpty().ToList();
            return empList;
        }
        public static List<IOListTable> getIOList(string details, int userid, string roleid, int ContractId, int? SupplierId, int IOId)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("IOList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "IOList");
                conn = OpenDBConnection();
                string sql = ""; string sql1 = ""; string sql2 = ""; string sql3 = "";
                string sql4 = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                if (ContractId != 0)
                {
                    sql2 = "AND m.opportunityid = @contractId ";
                }
                if (roleid != "R02")
                {
                    sql1 = "AND s.userid = @userId ";
                    sql3 = "LEFT JOIN [users_opportunity] s ON ( m.opportunityid =  s.opportunityid ) ";
                }
                if (details == "IODetails")
                {
                    sql4 = "AND q.IOId = @IOId ";
                }
                sql = "SELECT DISTINCT p.[ioid], " +
                           //"'' AS #, " +
                           "m.[opportunityId], " +
                           "q.[iono], " +
                           "q.[ioissuedate], " +
                           "Stuff((SELECT DISTINCT ',' + r.[details_pono] " +
                                "FROM   [purchaseorder] r " +
                                    "LEFT JOIN [po_item] s " +
                                        "ON ( r.poid = s.poid ) " +
                                    "LEFT JOIN [io_items] t " +
                                        "ON ( s.itemsid = t.itemsid )" +
                                    "LEFT JOIN [internalorder] u " +
                                        "ON ( t.ioid = u.ioid ) " +
                                "WHERE  m.active = 1 " +
                                    "AND t.ioid = p.ioid " +
                                    "AND m.supplierid = @supplierId " +
                                "FOR xml path ('')), 1, 1, '') AS [details_pono], " +
                           "(SELECT TOP 1 q.username + ' - ' + r.[content] " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.IOId = p.IOId " +
                            "ORDER  BY chatid DESC) AS [LastActivity], " +
                           "(SELECT TOP 1 r.createdate " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.IOId = p.IOId " +
                            "ORDER  BY chatid DESC) AS [LastActivityDate] " +
                    "FROM   [dbo].[opportunity] m " +
                           "LEFT JOIN [purchaseorder] n " +
                                  "ON ( m.opportunityid = n.opportunityid ) " +
                           "LEFT JOIN [po_item] o " +
                                  "ON ( n.poid = o.poid ) " +
                           "LEFT JOIN [IO_Items] p " +
                                  "ON ( o.itemsId = p.itemsId ) " +
                           "LEFT JOIN [internalorder] q " +
                                  "ON ( p.ioid = q.ioid ) " +
                           sql3 +
                    "WHERE  m.active = 1 " +
                           "AND p.ioid is not null " +
                           "AND m.supplierId = @supplierId " +
                           sql1 +
                           sql2 +
                           sql4 +
                    "ORDER  BY lastactivitydate DESC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@contractId", SqlDbType.VarChar);
                SQLcmd.Parameters["@contractId"].Value = ContractId;
                SQLcmd.Parameters.Add("@supplierId", SqlDbType.Int);
                SQLcmd.Parameters["@supplierId"].Value = SupplierId;
                SQLcmd.Parameters.Add("@userId", SqlDbType.Int);
                SQLcmd.Parameters["@userId"].Value = userid;
                SQLcmd.Parameters.Add("@IOId", SqlDbType.Int);
                SQLcmd.Parameters["@IOId"].Value = IOId;

                SQLDataADP.Fill(returnDS);

                List<IOListTable> IOList = new List<IOListTable>();
                if (returnDS.Tables[0].Rows.Count != 0)
                //if (returnDS.Tables[0].Rows[0][0] != "")
                {
                    IOList = returnDS.Tables[0].AsEnumerable().Select(
                    dataRow => new IOListTable
                    {
                        //Uid = uid,
                        IOId = dataRow.Field<int>("ioid"),
                        OppContId = dataRow.Field<int>("opportunityId"),
                        IONo = dataRow.Field<string>("iono"),
                        IOIssueDate = dataRow.Field<DateTime?>("ioissuedate"),
                        PONo = dataRow.Field<string>("details_pono"),
                        LastActivity = dataRow.Field<string>("LastActivity"),
                        LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
                    }).DefaultIfEmpty().ToList();
                }
                return IOList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseDBConnection(conn);
            }
        }
        public static List<PRListTable> getPRList(string details, int userid, string roleid, int ContractId, int? SupplierId, int PRId)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("PRList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "PRList");
                conn = OpenDBConnection();
                string sql = ""; string sql1 = ""; string sql2 = ""; string sql3 = "";
                string sql4 = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                if (ContractId != 0)
                {
                    sql2 = "AND m.opportunityid = @contractId ";
                }
                if (roleid != "R02")
                {
                    sql1 = "AND s.userid = @userId ";
                    sql3 = "LEFT JOIN [users_opportunity] s ON ( m.opportunityid =  s.opportunityid ) ";
                }
                if (details == "PRDetails")
                {
                    sql4 = "AND q.PRId = @PRId ";
                }
                sql = "SELECT DISTINCT p.[prid], " +
                           //"'' AS #, " +
                           "m.[opportunityId], " +
                           "q.[prno], " +
                           "q.[prissuedate], " +
                           "q.[PurchaseType], " +
                           "q.[Justification], " +
                           "q.[VendorPIC], " +
                           "q.[VendorContNo], " +
                           "q.[VendorEmail], " +
                           "q.[BudgetDescription], " +
                           "q.[BudgetedAmount], " +
                           "q.[UtilizedToDate], " +
                           "q.[BudgetBalance], " +
                           "q.[AmountRequired], " +
                           "q.[Remarks], " +
                           "q.[TotalPrice], " +
                           "Stuff((SELECT DISTINCT ',' + r.[details_pono] " +
                                "FROM   [purchaseorder] r " +
                                    "LEFT JOIN [po_item] s " +
                                        "ON ( r.poid = s.poid ) " +
                                    "LEFT JOIN [pr_items] t " +
                                        "ON ( s.itemsid = t.itemsid )" +
                                    "LEFT JOIN [purchaserequisition] u " +
                                        "ON ( t.prid = u.prid ) " +
                                "WHERE  m.active = 1 " +
                                    "AND t.prid = p.prid " +
                                    "AND m.supplierid = @supplierId " +
                                "FOR xml path ('')), 1, 1, '') AS [details_pono], " +
                           "(SELECT TOP 1 q.username + ' - ' + r.[content] " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.PRId = p.PRId " +
                            "ORDER  BY chatid DESC) AS [LastActivity], " +
                           "(SELECT TOP 1 r.createdate " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.PRId = p.PRId " +
                            "ORDER  BY chatid DESC) AS [LastActivityDate] " +
                    "FROM   [dbo].[opportunity] m " +
                           "LEFT JOIN [purchaseorder] n " +
                                  "ON ( m.opportunityid = n.opportunityid ) " +
                           "LEFT JOIN [po_item] o " +
                                  "ON ( n.poid = o.poid ) " +
                           "LEFT JOIN [PR_Items] p " +
                                  "ON ( o.itemsId = p.itemsId ) " +
                           "LEFT JOIN [purchaserequisition] q " +
                                  "ON ( p.PRId = q.PRId ) " +
                           "LEFT JOIN [customer] v " +
                                  "ON ( m.custId = v.custId ) " +
                           sql3 +
                    "WHERE  m.active = 1 " +
                           "AND p.PRId is not null " +
                           "AND m.supplierId = @supplierId " +
                           sql1 +
                           sql2 +
                           sql4 +
                    "ORDER  BY lastactivitydate DESC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@contractId", SqlDbType.VarChar);
                SQLcmd.Parameters["@contractId"].Value = ContractId;
                SQLcmd.Parameters.Add("@supplierId", SqlDbType.Int);
                SQLcmd.Parameters["@supplierId"].Value = SupplierId;
                SQLcmd.Parameters.Add("@userId", SqlDbType.Int);
                SQLcmd.Parameters["@userId"].Value = userid;
                SQLcmd.Parameters.Add("@PRId", SqlDbType.Int);
                SQLcmd.Parameters["@PRId"].Value = PRId;

                SQLDataADP.Fill(returnDS);

                List<PRListTable> PRList = new List<PRListTable>();
                if (returnDS.Tables[0].Rows.Count != 0)
                //if (returnDS.Tables[0].Rows[0][0] != "")
                {
                    PRList = returnDS.Tables[0].AsEnumerable().Select(
                    dataRow => new PRListTable
                    {
                        //Uid = uid,
                        PRId = dataRow.Field<int>("prid"),
                        OppContId = dataRow.Field<int>("opportunityId"),
                        PRNo = dataRow.Field<string>("prno"),
                        PRIssueDate = dataRow.Field<DateTime?>("prissuedate"),
                        PurchaseType = dataRow.Field<string>("PurchaseType"),
                        Justification = dataRow.Field<string>("Justification"),
                        VendorPIC = dataRow.Field<string>("VendorPIC"),
                        VendorContNo = dataRow.Field<string>("VendorContNo"),
                        VendorEmail = dataRow.Field<string>("VendorEmail"),
                        BudgetDescription = dataRow.Field<string>("BudgetDescription"),
                        BudgetedAmount = dataRow.Field<decimal>("budgetedamount"),
                        UtilizedToDate = dataRow.Field<decimal>("UtilizedToDate"),
                        BudgetBalance = dataRow.Field<decimal>("budgetbalance"),
                        AmountRequired = dataRow.Field<decimal>("AmountRequired"),
                        Remarks = dataRow.Field<string>("Remarks"),
                        TotalPrice = dataRow.Field<decimal>("totalprice"),
                        PONo = dataRow.Field<string>("details_pono"),
                        LastActivity = dataRow.Field<string>("LastActivity"),
                        LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
                    }).DefaultIfEmpty().ToList();
                }
                return PRList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseDBConnection(conn);
            }
        }
        public static List<DOListTable> getDOList(string details, int userid, string roleid, int ContractId, int? SupplierId, int DOId)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("DOList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "DOList");
                conn = OpenDBConnection();
                string sql = ""; string sql1 = ""; string sql2 = ""; string sql3 = "";
                string sql4 = "";
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                if (ContractId != 0)
                {
                    sql2 = "AND m.opportunityid = @contractId ";
                }
                if (roleid != "R02")
                {
                    sql1 = "AND s.userid = @userId ";
                    sql3 = "LEFT JOIN [users_opportunity] s ON ( m.opportunityid =  s.opportunityid ) ";
                }
                if (details == "DODetails")
                {
                    sql4 = "AND q.DOId = @DOId ";
                }
                sql = "SELECT DISTINCT p.[doid], " +
                           //"'' AS #, " +
                           "m.[opportunityId], " +
                           "q.[dono], " +
                           "q.[doissuedate], " +
                            "(SELECT SUM([deliveryQuantity]) " +
                            "FROM   [DO_Items] " +
                            "WHERE  DOId = p.DOId) AS [deliveryQuantity], " +
                           "q.[branchid], " +
                           "q.[telno], " +
                           "q.[trackingno], " +
                           "q.[contactperson], " +
                           "Stuff((SELECT DISTINCT ',' + r.[details_pono] " +
                                "FROM   [purchaseorder] r " +
                                    "LEFT JOIN [po_item] s " +
                                        "ON ( r.poid = s.poid ) " +
                                    "LEFT JOIN [do_items] t " +
                                        "ON ( s.itemsid = t.itemsid )" +
                                    "LEFT JOIN [deliveryorder] u " +
                                        "ON ( t.doid = u.doid ) " +
                                "WHERE  m.active = 1 " +
                                    "AND t.doid = p.doid " +
                                    "AND m.supplierid = @supplierId " +
                                "FOR xml path ('')), 1, 1, '') AS [details_pono], " +
                           "(SELECT TOP 1 q.username + ' - ' + r.[content] " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.DOId = p.DOId " +
                            "ORDER  BY chatid DESC) AS [LastActivity], " +
                           "(SELECT TOP 1 r.createdate " +
                            "FROM   [notification] r " +
                                   "LEFT JOIN [user] q " +
                                          "ON ( r.fromuserid = q.userid ) " +
                            "WHERE  r.DOId = p.DOId " +
                            "ORDER  BY chatid DESC) AS [LastActivityDate] " +
                    "FROM   [dbo].[opportunity] m " +
                           "LEFT JOIN [purchaseorder] n " +
                                  "ON ( m.opportunityid = n.opportunityid ) " +
                           "LEFT JOIN [po_item] o " +
                                  "ON ( n.poid = o.poid ) " +
                           "LEFT JOIN [DO_Items] p " +
                                  "ON ( o.itemsId = p.itemsId ) " +
                           "LEFT JOIN [deliveryorder] q " +
                                  "ON ( p.doid = q.doid ) " +
                           sql3 +
                    "WHERE  m.active = 1 " +
                           "AND p.doid is not null " +
                           "AND m.supplierId = @supplierId " +
                           sql1 +
                           sql2 +
                           sql4 +
                    "ORDER  BY lastactivitydate DESC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@contractId", SqlDbType.VarChar);
                SQLcmd.Parameters["@contractId"].Value = ContractId;
                SQLcmd.Parameters.Add("@supplierId", SqlDbType.Int);
                SQLcmd.Parameters["@supplierId"].Value = SupplierId;
                SQLcmd.Parameters.Add("@userId", SqlDbType.Int);
                SQLcmd.Parameters["@userId"].Value = userid;
                SQLcmd.Parameters.Add("@DOId", SqlDbType.Int);
                SQLcmd.Parameters["@DOId"].Value = DOId;

                SQLDataADP.Fill(returnDS);

                List<DOListTable> DOList = new List<DOListTable>();
                if (returnDS.Tables[0].Rows.Count != 0)
                //if (returnDS.Tables[0].Rows[0][0] != "")
                {
                    DOList = returnDS.Tables[0].AsEnumerable().Select(
                    dataRow => new DOListTable
                    {
                        //Uid = uid,
                        DOId = dataRow.Field<int>("doid"),
                        OppContId = dataRow.Field<int>("opportunityId"),
                        DONo = dataRow.Field<string>("dono"),
                        BranchId = dataRow.Field<int>("branchid"),
                        DOIssueDate = dataRow.Field<DateTime?>("doissuedate"),
                        DeliveryQuantity = dataRow.Field<int?>("deliveryQuantity"),
                        TelNo = dataRow.Field<string>("telno"),
                        TrackingNo = dataRow.Field<string>("trackingno"),
                        ContactPerson = dataRow.Field<string>("contactperson"),
                        PONo = dataRow.Field<string>("details_pono"),
                        LastActivity = dataRow.Field<string>("LastActivity"),
                        LastActivityDate = dataRow.Field<DateTime?>("LastActivityDate")
                    }).DefaultIfEmpty().ToList();
                }
                return DOList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseDBConnection(conn);
            }
        }
        //public static List<POTrackingInfo> getPOTracking(int POId)
        //{
        //    SqlConnection conn = null;
        //    DataTable returnDS = new DataTable();
        //    string info = "";
        //    try
        //    {
        //        SqlDataAdapter SQLDataADP = new SqlDataAdapter();
        //        SQLDataADP.TableMappings.Add("Table", "POList");
        //        conn = OpenDBConnection();
        //        string sql = "";
        //        conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
        //        {
        //            info = e.Message.ToString();
        //        };
        //        sql = "SELECT [Process Flow Name], " +
        //                       "details_poissuedate        AS 'Start Flow Date', " +
        //                       "createdate                 AS 'End Flow Date', " +
        //                       "details_projectdescription AS 'Remark' " +
        //                "FROM   (SELECT 'PO to IO Process Flow' AS [Process Flow Name], " +
        //                               "[details_poissuedate], " +
        //                               "[createdate], " +
        //                               "[details_projectdescription], " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'IO to PR Process Flow' AS [Process Flow Name], " +
        //                               "[ioinfo_poreceiveddate], " +
        //                               "[ioinfo_completedate], " +
        //                               "[ioinfo_remark], " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'PR to Procurement Process Flow' AS [Process Flow Name], " +
        //                               "[PRInfo_IOReceivedDate], " +
        //                               "[PRInfo_CompleteDate], " +
        //                               "[PRInfo_Remark], " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'Procurement to Finance Process Flow' AS [Process Flow Name], " +
        //                               "[PP_DocReceivedDate], " +
        //                               "[PP_CompleteDate], " +
        //                               "[PP_Remark], " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'Finance to CEO Process Flow' AS [Process Flow Name], " +
        //                               "[FP_DocReceivedDate], " +
        //                               "[FP_CompleteDate], " +
        //                               "FP_Remark, " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'CEO to Supplier Process Flow' AS [Process Flow Name], " +
        //                               "[CEOO_DocReceivedDate], " +
        //                               "[CEOO_CompleteDate], " +
        //                               "CEOO_Remark, " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'Supplier to Delivery Process Flow' AS [Process Flow Name], " +
        //                               "[VS_DocReceivedDate], " +
        //                               "[VS_CompleteDate], " +
        //                               "VS_Remarks, " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        "UNION ALL " +
        //                        "SELECT 'Delivery to Invoice From Supplier Process Flow' AS [Process Flow Name], " +
        //                               "[DOInfo_DocReceivedDate], " +
        //                               "[DOInfo_CompleteDate], " +
        //                               "DOInfo_Remark, " +
        //                               "[poid] " +
        //                        "FROM   purchaseorder " +
        //                        ") t1 " +
        //                "WHERE  t1.poid = @POId ";

        //        SqlCommand SQLcmd = new SqlCommand(sql, conn)
        //        {
        //            CommandType = CommandType.Text
        //        };
        //        SQLDataADP.SelectCommand = SQLcmd;

        //        SQLcmd.Parameters.Add("@POId", SqlDbType.Int);
        //        SQLcmd.Parameters["@POId"].Value = POId;

        //        SQLDataADP.Fill(returnDS);
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.Print("Report - " + ex.Message);
        //    }
        //    finally
        //    {
        //        CloseDBConnection(conn);
        //    }

        //    var POTrackingTable = returnDS.AsEnumerable().Select(
        //    dataRow => new POTrackingInfo
        //    {
        //        ProcessFlowName = dataRow.Field<string>("Process Flow Name"),
        //        StartFlowDate = dataRow.Field<DateTime?>("Start Flow Date"),
        //        EndFlowDate = dataRow.Field<DateTime?>("End Flow Date"),
        //        Remark = dataRow.Field<string>("Remark")
        //    }).ToList();

        //    return POTrackingTable;
        //}
        public static List<POReportListTable> getPOReportList(int OppoId, int POId)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("POReportList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "POReportList");
                conn = OpenDBConnection();
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                string sql = "";

                    sql = "SELECT o.materialno, " +
                               //"r.branchaddress, " +
                               "o.[description], " +
                               "n.deliveredquantity + n.deliveryquantity " +
                               "+ n.outstandingquantity                   AS [quantity], " +
                               "m.details_poissuedate, " +
                               "m.details_pono, " +
                               "n.unitprice, " +
                               "n.totalprice, " +
                               "m.details_totalgst, " +
                               "m.details_totalprice + m.details_totalgst AS 'TotalPOAmount', " +
                               "n.deliverydate, " +
                               "t.iono, " +
                               "t.ioissuedate, " +
                               "v.prissuedate, " +
                               "v.prno, " +
                               "w.NAME, " +
                               "q.doissuedate, " +
                               "q.dono, " +
                               "p.deliveryQuantity, " +
                               "p.deliveredQuantity, " +
                               "x.[status] " +
                        "FROM   purchaseorder m " +
                               "LEFT JOIN po_item n " +
                                      "ON ( m.poid = n.poid ) " +
                               "LEFT JOIN partno_items o " +
                                      "ON ( n.partno = o.partno ) " +
                               "LEFT JOIN do_items p " +
                                      "ON ( n.itemsid = p.itemsid " +
                                           " ) " +
                               "LEFT JOIN deliveryorder q " +
                                      "ON ( p.doid = q.doid ) " +
                               //"LEFT JOIN custbranch r " +
                               //       "ON ( q.branchid = r.branchid ) " +
                               "LEFT JOIN io_items s " +
                                      "ON ( n.itemsid = s.itemsid " +
                                           " ) " +
                               "LEFT JOIN internalorder t " +
                                      "ON ( s.ioid = t.ioid ) " +
                               "LEFT JOIN pr_items u " +
                                      "ON ( n.itemsid = u.itemsid " +
                                           " ) " +
                               "LEFT JOIN purchaserequisition v " +
                                      "ON ( u.prid = v.prid ) " +
                               "LEFT JOIN customer w " +
                                      "ON ( v.vendorid = w.custid ) " +
                               "LEFT JOIN postatus x " +
                                      "ON ( m.statusid = x.statusid ) " +
                               "LEFT JOIN opportunity y " +
                                      "ON ( m.opportunityid = y.opportunityid ) " +
                        "WHERE  y.opportunityId = @oppoId " +
                        "ORDER  BY details_pono ASC ";

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@oppoId", SqlDbType.Int);
                SQLcmd.Parameters["@oppoId"].Value = OppoId;
                SQLcmd.Parameters.Add("@POId", SqlDbType.Int);
                SQLcmd.Parameters["@POId"].Value = POId;
                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

            var POReportingList = returnDS.Tables[0].AsEnumerable().Select(
            dataRow => new POReportListTable
            {
                MaterialNo = dataRow.Field<string>("materialno"),
                //BranchAddress = dataRow.Field<string>("branchaddress"),
                Description = dataRow.Field<string>("description"),
                Quantity = dataRow.Field<int?>("quantity"),
                POIssueDate = dataRow.Field<DateTime?>("details_poissuedate"),
                PONo = dataRow.Field<string>("details_pono"),
                UnitPrice = String.Format("{0:n}", dataRow.Field<decimal?>("unitprice")),
                TotalPrice = String.Format("{0:n}", dataRow.Field<decimal?>("totalprice")),
                TotalGST = String.Format("{0:n}", dataRow.Field<decimal>("details_totalgst")),
                TotalPOAmount = String.Format("{0:n}", dataRow.Field<decimal>("TotalPOAmount")),
                DeliveryDate = dataRow.Field<DateTime?>("deliverydate"),
                IONo = dataRow.Field<string>("iono"),
                IOIssuedate = dataRow.Field<DateTime?>("ioissuedate"),
                PRIssuedate = dataRow.Field<DateTime?>("prissuedate"),
                PRNo = dataRow.Field<string>("prno"),
                CustName = dataRow.Field<string>("NAME"),
                DOIssueDate = dataRow.Field<DateTime?>("doissuedate"),
                DONo = dataRow.Field<string>("dono"),
                DeliveryQuantity = dataRow.Field<int?>("deliveryQuantity"),
                DeliveredQuantity = dataRow.Field<int?>("deliveredQuantity"),
                Status = dataRow.Field<string>("status")
            }).ToList();

            return POReportingList;
        }
        public static List<QuestionAnswerModels> getMessageList(int caseid, int POId, int IOId, int PRId, int DOId, int uid)
        {
            SqlConnection conn = null;
            DataSet returnDS = new DataSet("MessageList");
            string info = "";
            try
            {
                SqlDataAdapter SQLDataADP = new SqlDataAdapter();
                SQLDataADP.TableMappings.Add("Table", "MessageList");
                conn = OpenDBConnection();
                conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
                {
                    info = e.Message.ToString();
                };
                string sql = "";
                if (POId != 0 && IOId == 0 && PRId == 0 && DOId == 0)
                {
                    sql = "SELECT DISTINCT [opportunityid], " +
                                            "[fromusername], " +
                                            "[createdate], " +
                                            "[name], " +
                                            "[content], " +
                                            "[QNAtype], " +
                                            "Stuff((SELECT ',' + [tousername] " +
                                                   "FROM   messagelist " +
                                                   "WHERE  [chatid] = a.[chatid] " +
                                                   "FOR xml path ('')), 1, 1, '') AS [ToUserName] " +
                            "FROM   messagelist AS a " +
                            "WHERE opportunityid = @oppoId AND POId = @POId AND IOId IS NULL AND PRId IS NULL AND DOId IS NULL " +
                            "ORDER by createdate DESC ";       
                }
                else if (IOId != 0 && POId == 0)
                {
                    sql = "SELECT DISTINCT [opportunityid], " +
                                            "[fromusername], " +
                                            "[createdate], " +
                                            "[name], " +
                                            "[content], " +
                                            "[QNAtype], " +
                                            "Stuff((SELECT ',' + [tousername] " +
                                                   "FROM   messagelist " +
                                                   "WHERE  [chatid] = a.[chatid] " +
                                                   "FOR xml path ('')), 1, 1, '') AS [ToUserName] " +
                            "FROM   messagelist AS a " +
                            "WHERE opportunityid = @oppoId AND IOId = @IOId " +
                            "ORDER by createdate DESC ";
                }
                else if (PRId != 0 && POId == 0)
                {
                    sql = "SELECT DISTINCT [opportunityid], " +
                                            "[fromusername], " +
                                            "[createdate], " +
                                            "[name], " +
                                            "[content], " +
                                            "[QNAtype], " +
                                            "Stuff((SELECT ',' + [tousername] " +
                                                   "FROM   messagelist " +
                                                   "WHERE  [chatid] = a.[chatid] " +
                                                   "FOR xml path ('')), 1, 1, '') AS [ToUserName] " +
                            "FROM   messagelist AS a " +
                            "WHERE opportunityid = @oppoId AND PRId = @PRId " +
                            "ORDER by createdate DESC ";
                }
                else if (DOId != 0 && POId == 0)
                {
                    sql = "SELECT DISTINCT [opportunityid], " +
                                            "[fromusername], " +
                                            "[createdate], " +
                                            "[name], " +
                                            "[content], " +
                                            "[QNAtype], " +
                                            "Stuff((SELECT ',' + [tousername] " +
                                                   "FROM   messagelist " +
                                                   "WHERE  [chatid] = a.[chatid] " +
                                                   "FOR xml path ('')), 1, 1, '') AS [ToUserName] " +
                            "FROM   messagelist AS a " +
                            "WHERE opportunityid = @oppoId AND DOId = @DOId " +
                            "ORDER by createdate DESC ";       
                } else
                {
                    sql = "SELECT DISTINCT [opportunityid], " +
                                            "[fromusername], " +
                                            "[createdate], " +
                                            "[name], " +
                                            "[content], " +
                                            "[QNAtype], " +
                                            "Stuff((SELECT ',' + [tousername] " +
                                                   "FROM   messagelist " +
                                                   "WHERE  [chatid] = a.[chatid] " +
                                                   "FOR xml path ('')), 1, 1, '') AS [ToUserName] " +
                            "FROM   messagelist AS a " +
                            "WHERE opportunityid = @oppoId AND POId IS NULL AND DOId IS NULL AND IOId IS NULL " +
                            "ORDER by createdate DESC ";         
                }

                SqlCommand SQLcmd = new SqlCommand(sql, conn)
                {
                    CommandType = CommandType.Text
                };
                SQLDataADP.SelectCommand = SQLcmd;

                SQLcmd.Parameters.Add("@oppoId", SqlDbType.Int);
                SQLcmd.Parameters["@oppoId"].Value = caseid;
                SQLcmd.Parameters.Add("@POId", SqlDbType.Int);
                SQLcmd.Parameters["@POId"].Value = POId;
                SQLcmd.Parameters.Add("@IOId", SqlDbType.Int);
                SQLcmd.Parameters["@IOId"].Value = IOId;
                SQLcmd.Parameters.Add("@PRId", SqlDbType.Int);
                SQLcmd.Parameters["@PRId"].Value = PRId;
                SQLcmd.Parameters.Add("@DOId", SqlDbType.Int);
                SQLcmd.Parameters["@DOId"].Value = DOId;

                SQLDataADP.Fill(returnDS);
            }
            catch (Exception ex)
            {
                Debug.Print("Report - " + ex.Message);
            }
            finally
            {
                CloseDBConnection(conn);
            }

                var MessageList = returnDS.Tables[0].AsEnumerable().Select(
                dataRow => new QuestionAnswerModels
                {
                    //Uid = uid,
                    //ChatId = dataRow.Field<int>("chatid"),
                    OpportunityId = dataRow.Field<int>("opportunityid"),
                    FromUserName = dataRow.Field<string>("fromusername"),
                    CreateDate = dataRow.Field<DateTime>("createdate"),
                    ContractName = dataRow.Field<string>("name"),
                    ToUserName = dataRow.Field<string>("ToUserName"),
                    Content = dataRow.Field<string>("content"),
                    QNAtype = dataRow.Field<string>("QNAtype")
                }).ToList();

                return MessageList;
        }
        public static List<NotiMemberList> getNotiMemberList(int OCId)
        {
            var NotiMemberList1 = (from m in db.Users
                                  join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                  from y in gy.DefaultIfEmpty()
                                   where m.status == true && y.opportunityId == OCId
                                   //where m.status == true && m.userId == 2 && y.opportunityId == OCId
                                   select new NotiMemberList()
                                  {
                                      Id = m.userId,
                                      Name = m.firstName + " " + m.lastName
                                  }).AsEnumerable().OrderBy(c => c.Name).ToList();

            return NotiMemberList1;
        }

        // public static DataSet getOpportunityInfo(string username, string roleid)
        // {
        //     SqlConnection conn = null;
        //     DataSet returnDS = new DataSet("OpportunityInfo");
        //     string info = "";           
        //     try
        //     {
        //         SqlDataAdapter SQLDataADP = new SqlDataAdapter();
        //         SQLDataADP.TableMappings.Add("Table", "OpportunityInfo");
        //         conn = OpenDBConnection();
        //         string sql = "";
        //         conn.InfoMessage += (object obj, SqlInfoMessageEventArgs e) =>
        //         {
        //             info = e.Message.ToString();
        //         };
        //         if (roleid != "R02")
        //         {
        //             sql = " SELECT DISTINCT " +
        //                     " m.[opportunityid], " +
        //                     " '' AS #, " +
        //                     " o.[abbreviation] AS CustName, " +
        //                     " m.[name], " +
        //                     " s.[type], " +
        //                     " m.[contractNo], " +
        //                     " n.[firstName] AS assignTo, " +
        //                     " FORMAT(m.[value],'#,0.00'), " +
        //                     " m.[proposalduedate], " +
        //                     " m.[targetaward], " +
        //                     " r.[status], " +
        //                     " (SELECT TOP 1 " +
        //                         " v.username + ' - ' + w.[content] " +
        //                     " FROM [notification] w " +
        //                     " LEFT JOIN [user] v " +
        //                         " ON (w.fromuserid = v.userid) " +
        //                     " WHERE w.opportunityid = m.[opportunityId] " +
        //                     " ORDER BY chatid DESC) " +
        //                     " AS [LastActivity], " +
        //                     " (SELECT TOP 1 " +
        //                         " x.createdate " +
        //                     " FROM [notification] x " +
        //                     " LEFT JOIN [user] y " +
        //                         " ON (x.fromuserid = y.userid) " +
        //                     " WHERE x.opportunityid = m.[opportunityId] " +
        //                     " ORDER BY createDate DESC) " +
        //                     " AS [LastActivityDate], " +
        //                     " m.[active], " +
        //                     " p.[canview] " +
        //                 " FROM Opportunity m " +
        //                 " LEFT JOIN [user] n " +
        //                     " ON (m.assigntoid = n.userid) " +
        //                 " LEFT JOIN [customer] o " +
        //                     " ON (m.custid = o.custid) " +
        //                 " LEFT JOIN [users_opportunity] p " +
        //                     " ON (m.opportunityid = p.opportunityid) " +
        //                 " LEFT JOIN [OpportunityCategory] q " +
        //                     " ON (m.catId = q.catId) " +
        //                 " LEFT JOIN [OpportunityStatus] r " +
        //                     " ON (m.statusId = r.statusId) " +
        //                 " LEFT JOIN [OpportunityType] s " +
        //                     " ON (m.typeId = s.typeId) " +
        //                 " LEFT JOIN [user] t " +
        //                     " ON (p.userid = t.userid) " +
        //                 " WHERE m.active = 1 " +
        //                 " AND p.canview = 1 " +
        //                 " AND m.statusId <> 'OS007' " +
        //                 " AND t.username = @username " +
        //                 " ORDER BY [LastActivityDate] DESC ";
        ////"AND q.username = 'saiful' ";
        //         } else {
        //             sql = " SELECT DISTINCT " +
        //                     " m.[opportunityid], " +
        //                     " '' AS #, " +
        //                     " o.[abbreviation] AS CustName, " +
        //                     " m.[name], " +
        //                     " s.[type], " +
        //                     " m.[contractNo], " +
        //                     " n.[firstName] AS assignTo, " +
        //                     " FORMAT(m.[value],'#,0.00'), " +
        //                     " m.[proposalduedate], " +
        //                     " m.[targetaward], " +
        //                     " r.[status], " +
        //                     " (SELECT TOP 1 " +
        //                         " v.username + ' - ' + w.[content] " +
        //                     " FROM [notification] w " +
        //                     " LEFT JOIN [user] v " +
        //                         " ON (w.fromuserid = v.userid) " +
        //                     " WHERE w.opportunityid = m.[opportunityId] " +
        //                     " ORDER BY chatid DESC) " +
        //                     " AS [LastActivity], " +
        //                     " (SELECT TOP 1 " +
        //                         " x.createdate " +
        //                     " FROM [notification] x " +
        //                     " LEFT JOIN [user] y " +
        //                         " ON (x.fromuserid = y.userid) " +
        //                     " WHERE x.opportunityid = m.[opportunityId] " +
        //                     " ORDER BY createDate DESC) " +
        //                     " AS [LastActivityDate], " +
        //                     " m.[active], " +
        //                     " p.[canview] " +
        //                 " FROM Opportunity m " +
        //                 " LEFT JOIN [user] n " +
        //                     " ON (m.assigntoid = n.userid) " +
        //                 " LEFT JOIN [customer] o " +
        //                     " ON (m.custid = o.custid) " +
        //                 " LEFT JOIN [users_opportunity] p " +
        //                     " ON (m.opportunityid = p.opportunityid) " +
        //                 " LEFT JOIN [OpportunityCategory] q " +
        //                     " ON (m.catId = q.catId) " +
        //                 " LEFT JOIN [OpportunityStatus] r " +
        //                     " ON (m.statusId = r.statusId) " +
        //                 " LEFT JOIN [OpportunityType] s " +
        //                     " ON (m.typeId = s.typeId) " +
        //                 " LEFT JOIN [user] t " +
        //                     " ON (p.userid = t.userid) " +
        //                 " WHERE m.active = 1 " +
        //                 " AND p.canview = 1 " +
        //                 " AND m.statusId <> 'OS007' " +
        //                 " ORDER BY [LastActivityDate] DESC ";
        //         }
        //         //"U.counter, C.custname, C.logo, G.keyid, U.position, '' AS 'homebase', U.lock_ctfm " +
        //         //"FROM   users U, customers C, gekmlkey G WHERE  ( U.custid = C.custid ) " +
        //         //"AND ( U.userid = G.userid ) AND U.userid = @username AND U.password = @password " +
        //         //"AND U.usertype <> 'mobileuser' AND U.usertype <> 'driver' ";

        //         SqlCommand SQLcmd = new SqlCommand(sql, conn);
        //         SQLcmd.CommandType = CommandType.Text;
        //         SQLDataADP.SelectCommand = SQLcmd;

        //         SQLcmd.Parameters.Add("@username", SqlDbType.VarChar);
        //         SQLcmd.Parameters["@username"].Value = username;

        //         SQLDataADP.Fill(returnDS);
        //     }
        //     catch (Exception ex)
        //     {
        //         Debug.Print("Report - " + ex.Message);
        //     }
        //     finally
        //     {
        //         CloseDBConnection(conn);
        //     }

        //     return returnDS;
        // }
    }
}
