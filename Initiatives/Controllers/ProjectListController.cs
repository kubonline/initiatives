﻿public ActionResult PertinentInfo(string OppContId)
{
    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
    {
        int OpportunityId = Int32.Parse(OppContId); int UserId = Int32.Parse(Session["UserId"].ToString());
        MainModel MM = new MainModel
        {
            OppContId = OpportunityId,
            UserId = Int32.Parse(Session["UserId"].ToString()),
            OppoListDetail = (from m in db.Opportunities
                              join n in db.Users on m.createByUserId equals n.userId into s
                              join o in db.Customers on m.custId equals o.custId into t
                              join p in db.OpportunityCategories on m.catId equals p.catId into u
                              join q in db.OpportunityStatus on m.statusId equals q.statusId into v
                              join r in db.OpportunityTypes on m.typeId equals r.typeId into w
                              from x in s.DefaultIfEmpty()
                              from y in t.DefaultIfEmpty()
                              from z in u.DefaultIfEmpty()
                              from aa in v.DefaultIfEmpty()
                              from ab in w.DefaultIfEmpty()
                              where m.opportunityId == OpportunityId
                              select new OppoDetailList()
                              {
                                  CustId = m.custId,
                                  CustName = (y == null ? String.Empty : y.name + " (" + y.abbreviation + ")"),
                                  OppoTypeId = m.typeId,
                                  OpportunityName = m.name,
                                  Description = m.description,
                                  AccManagerId = m.assignToId,
                                  AccManagerName = (x == null ? String.Empty : x.firstName + " " + x.lastName),
                                  ApprovalId = m.approvalId,
                                  WinningChancesId = m.chanceId,
                                  OppoType = (ab == null ? String.Empty : ab.type + " (" + y.abbreviation + ")"),
                                  CatId = m.catId,
                                  OppoCat = (z == null ? String.Empty : z.category),
                                  OpportunityValue = m.value.Value,
                                  TargetGP = m.targetGP,
                                  MajorProjectRisk = m.majorRisk,
                                  SimilarProject = m.similarity,
                                  ApprovingAuthority = m.approvingAuthority,
                                  //ContractNo = m.contractNo,
                                  Submission = m.proposalDueDate.Value,
                                  TargetAward = m.targetAward,
                                  StatusId = m.statusId,
                                  Status = (aa == null ? String.Empty : aa.status),
                                  CauseOfLost = m.causeOfLost
                                  //BudgetaryRevenue = m.budgetaryRevenue.Value
                              }).FirstOrDefault(),
            RoleId = Session["RoleId"].ToString(),
            objCanEdit = (from m in db.Users_Opportunity
                          where m.opportunityId == OpportunityId && m.userId == UserId
                          select new EditPermission()
                          {
                              canEdits = m.canEdit
                          }).FirstOrDefault(),
            //MessageListModel = DBQueryController.getMessageList(OpportunityId, 0, 0, 0, 0, userid)
        };

        //var NotiMemberList = (from m in db.Users
        //                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
        //                      from x in gy.DefaultIfEmpty()
        //                      where m.status == true && x.opportunityId == OpportunityId
        //                      select new NotiMemberList()
        //                      {
        //                          Id = m.userId,
        //                          Name = m.firstName + " " + m.lastName
        //                      }).ToList().OrderBy(c => c.Name);

        //ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

        var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
        var UserQuery = (from m in db.Users
                         join n in db.Users_Roles on m.userId equals n.userId into o
                         from p in o.DefaultIfEmpty()
                         where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                         select new OppoDetailList()
                         {
                             AccManagerId = p.userId,
                             AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                         }).OrderBy(c => c.AccManagerName).AsEnumerable();
        var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
        var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
        var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);

        ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", MM.OppoListDetail.CustId);
        ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName", MM.OppoListDetail.AccManagerId);
        ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type", MM.OppoListDetail.OppoTypeId);
        ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category", MM.OppoListDetail.CatId);
        ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status", MM.OppoListDetail.StatusId);
        ViewBag.LOBApproval = new SelectList(
                                    new List<OppoDetailList>
                                    {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                    }, "ApprovalId", "LOBApproval", MM.OppoListDetail.ApprovalId);
        ViewBag.WinningChances = new SelectList(
                                    new List<OppoDetailList>
                                    {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = "S001      "},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = "S002      "},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = "S003      "},
                                    }, "WinningChancesId", "WinningChances", MM.OppoListDetail.WinningChancesId);
        //return View("PertinentInfo",OppoDetailList);
        //return JavaScript("windows.location = '" + Url.Action("PertinentInfo", "MainTabs") + "'");
        return PartialView(MM);
    }
    else
    {
        return Redirect("~/Home/Index");
    }
}

public ActionResult PertinentInfo(MainModel x)
{
    if (!ModelState.IsValid)
    {
        return new JsonResult
        {
            Data = new
            {
                success = false,
                view = this.RenderPartialView("PertinentInfo", x)
            },
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
        };
    }
    int caseId = x.OppContId;
    int userid = x.UserId;
    Opportunity objOppoDetaildb = db.Opportunities.First(m => m.opportunityId == caseId);

    if (objOppoDetaildb.custId != x.OppoListDetail.CustId)
    {
        Notification _objCustomerChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString())
        };
        Customer _objCustomerdb = db.Customers.First(m => m.custId == objOppoDetaildb.custId);
        Customer objCustomerdb = db.Customers.First(m => m.custId == x.OppoListDetail.CustId);
        _objCustomerChange.content = "change customer selection from " + _objCustomerdb.name + " to " + objCustomerdb.name;
        _objCustomerChange.flag = 0;
        db.Notifications.Add(_objCustomerChange);
        objOppoDetaildb.custId = x.OppoListDetail.CustId;
    }
    if (objOppoDetaildb.typeId != x.OppoListDetail.OppoTypeId)
    {
        Notification _objTypeChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString())
        };
        OpportunityType _objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == objOppoDetaildb.typeId);
        OpportunityType objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == x.OppoListDetail.OppoTypeId);
        _objTypeChange.content = "change opportunity type selection from " + _objOppoTypedb.type + " to " + objOppoTypedb.type;
        _objTypeChange.flag = 0;
        db.Notifications.Add(_objTypeChange);
        objOppoDetaildb.typeId = x.OppoListDetail.OppoTypeId;
    }
    if (objOppoDetaildb.catId != x.OppoListDetail.CatId)
    {
        Notification _objCatChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString())
        };
        OpportunityCategory _objOppoCatdb = db.OpportunityCategories.First(m => m.catId == objOppoDetaildb.catId);
        OpportunityCategory objOppoCatdb = db.OpportunityCategories.First(m => m.catId == x.OppoListDetail.CatId);
        _objCatChange.content = "change opportunity category selection from " + _objOppoCatdb.category + " to " + objOppoCatdb.category;
        _objCatChange.flag = 0;
        db.Notifications.Add(_objCatChange);
        objOppoDetaildb.catId = x.OppoListDetail.CatId;
    }
    if (objOppoDetaildb.name != x.OppoListDetail.OpportunityName)
    {
        Notification _objNameChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            content = "change opportunity name from " + objOppoDetaildb.name + " to " + x.OppoListDetail.OpportunityName,
            flag = 0
        };
        db.Notifications.Add(_objNameChange);
        objOppoDetaildb.name = x.OppoListDetail.OpportunityName;
    }
    if (objOppoDetaildb.description != x.OppoListDetail.Description)
    {
        Notification _objDescChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            content = "change the opportunity description to " + x.OppoListDetail.Description
        };
        ;
        _objDescChange.flag = 0;
        db.Notifications.Add(_objDescChange);
        objOppoDetaildb.description = x.OppoListDetail.Description;
    }
    if (objOppoDetaildb.assignToId != x.OppoListDetail.AccManagerId)
    {
        Notification _objAssignToChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString())
        };
        User _objAssignTodb = db.Users.First(m => m.userId == objOppoDetaildb.assignToId);
        User objAssignTodb = db.Users.First(m => m.userId == x.OppoListDetail.AccManagerId);
        _objAssignToChange.content = "change opportunity assigned to selection from " + _objAssignTodb.firstName + " "
        + _objAssignTodb.lastName + " to " + objAssignTodb.firstName + " " + objAssignTodb.lastName;
        _objAssignToChange.flag = 0;
        db.Notifications.Add(_objAssignToChange);
        objOppoDetaildb.assignToId = x.OppoListDetail.AccManagerId;
    }
    if (objOppoDetaildb.value != x.OppoListDetail.OpportunityValue)
    {
        Notification _objValueChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            flag = 0,
            content = "change the opportunity value from " + objOppoDetaildb.value + " to " + x.OppoListDetail.OpportunityValue
        };
        db.Notifications.Add(_objValueChange);
        objOppoDetaildb.value = x.OppoListDetail.OpportunityValue;
    }
    //if (objOppoDetaildb.budgetaryRevenue != x.OppoListDetail.BudgetaryRevenue)
    //{
    //    Notification _objBudgetaryRevenueChange = new Notification
    //    {
    //        uuid = Guid.NewGuid(),
    //        opportunityId = Int32.Parse(Session["OppoId"].ToString()),
    //        createDate = DateTime.Now,
    //        fromUserId = Int32.Parse(Session["UserId"].ToString()),
    //        flag = 0,
    //        content = "change the opportunity budgetary revenue from " + objOppoDetaildb.budgetaryRevenue + " to " + x.OppoListDetail.BudgetaryRevenue
    //    };
    //    db.Notifications.Add(_objBudgetaryRevenueChange);
    //    objOppoDetaildb.budgetaryRevenue = x.OppoListDetail.BudgetaryRevenue;
    //}
    if (objOppoDetaildb.contractNo != x.OppoListDetail.ContractNo)
    {
        Notification _objcontractNoChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            flag = 0,
            content = "change the opportunity no. from " + objOppoDetaildb.contractNo + " to " + x.OppoListDetail.ContractNo
        };
        db.Notifications.Add(_objcontractNoChange);
        objOppoDetaildb.contractNo = x.OppoListDetail.ContractNo;
    }
    if (objOppoDetaildb.proposalDueDate != x.OppoListDetail.Submission)
    {
        Notification _objSubmissionDateChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            flag = 0,
            content = "change the opportunity submission date from " + objOppoDetaildb.proposalDueDate + " to " + x.OppoListDetail.Submission
        };
        db.Notifications.Add(_objSubmissionDateChange);
        objOppoDetaildb.proposalDueDate = x.OppoListDetail.Submission;
    }
    if (objOppoDetaildb.targetAward != x.OppoListDetail.TargetAward)
    {
        Notification _objTargetAwardChange = new Notification
        {
            uuid = Guid.NewGuid(),
            opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            createDate = DateTime.Now,
            fromUserId = Int32.Parse(Session["UserId"].ToString()),
            flag = 0,
            content = "change the opportunity target award from " + objOppoDetaildb.targetAward + " to " + x.OppoListDetail.TargetAward
        };
        db.Notifications.Add(_objTargetAwardChange);
        objOppoDetaildb.targetAward = x.OppoListDetail.TargetAward;
    }
    //if (objOppoDetaildb.statusId != x.OppoListDetail.StatusId)
    //{
    //    Notification _obStatusChange = new Notification
    //    {
    //        uuid = Guid.NewGuid(),
    //        opportunityId = Int32.Parse(Session["OppoId"].ToString()),
    //        createDate = DateTime.Now,
    //        fromUserId = Int32.Parse(Session["UserId"].ToString())
    //    };
    //    OpportunityStatu _objStatusdb = db.OpportunityStatus.First(m => m.statusId == objOppoDetaildb.statusId);
    //    _obStatusChange.content = "change opportunity status selection from " + _objStatusdb.status + " to " + objStatusdb.status;
    //    _obStatusChange.flag = 0;
    //    db.Notifications.Add(_obStatusChange);
    //    objOppoDetaildb.statusId = x.OppoListDetail.StatusId;
    //}
    objOppoDetaildb.contractNo = x.OppoListDetail.ContractNo;
    db.SaveChanges();

    x.objCanEdit = (from m in db.Users_Opportunity
                    where m.opportunityId == x.OppContId && m.userId == userid
                    select new EditPermission()
                    {
                        canEdits = m.canEdit
                    }).FirstOrDefault();
    db.SaveChanges();
    var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
    var UserQuery = (from m in db.Users
                     join n in db.Users_Roles on m.userId equals n.userId into o
                     from p in o.DefaultIfEmpty()
                     where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                     select new OppoDetailList()
                     {
                         AccManagerId = p.userId,
                         AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                     }).OrderBy(c => c.AccManagerName).AsEnumerable();
    var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
    var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
    var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);
    ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", x.OppoListDetail.CustId);
    ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName", x.OppoListDetail.AccManagerId);
    ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type", x.OppoListDetail.OppoTypeId);
    ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category", x.OppoListDetail.CatId);
    ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status", x.OppoListDetail.StatusId);
    ViewBag.LOBApproval = new SelectList(
                                    new List<OppoDetailList>
                                    {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                    }, "ApprovalId", "LOBApproval");
    ViewBag.WinningChances = new SelectList(
                                new List<OppoDetailList>
                                {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = 3},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = 2},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = 1},
                                }, "WinningChancesId", "WinningChances");

    return new JsonResult
    {
        Data = new
        {
            success = true,
            view = this.RenderPartialView("PertinentInfo", x)
        },
        JsonRequestBehavior = JsonRequestBehavior.AllowGet
    };
}

public ActionResult CreateDO(MainModel x)
{
    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
    {
        if (Session["ContractId"] == null)
        {
            Session["ContractId"] = x.OppContId;
        }
        //if (x.file == null)
        //{
        //    ModelState.AddModelError("file", "Please input the attachment before proceed.");
        //}
        var CustBranchQuery = db.CustBranches.Select(c => new { BranchId = c.branchId, branchAddress = c.branchAddress })
            .OrderBy(c => c.BranchId);

        bool haveQuantity = false; string ErrorMessage = "";
        foreach (var value in x.DOItemListObject)
        {
            if (value.DeliveryQuantity < 1)
            {
                haveQuantity = true;
                ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot be 0 or less in DO Item";
                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        havePONo = false,
                        haveDONo = false,
                        haveQuantity,
                        ErrorMessage,
                        exception = false,
                        view = this.RenderPartialView("CreateDO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            if (value.DeliveryQuantity > value.OutstandingQuantity)
            {
                haveQuantity = true;
                ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot exceed Outstanding Quantity in DO Item";
                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        havePONo = false,
                        haveDONo = false,
                        haveQuantity,
                        ErrorMessage,
                        exception = false,
                        view = this.RenderPartialView("CreateDO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
        if (!ModelState.IsValid)
        {
            int DeliveryQuantity = 0;
            foreach (var value in ModelState)
            {
                if (value.Key.Contains("DeliveryQuantity"))
                {
                    DeliveryQuantity = Int32.Parse(value.Value.Value.AttemptedValue);
                    if (DeliveryQuantity < 1)
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot be 0 or less in DO Item";
                    }
                }
                if (value.Key.Contains("OutstandingQuantity"))
                {
                    if (DeliveryQuantity > Int32.Parse(value.Value.Value.AttemptedValue))
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot exceed Outstanding Quantity in DO Item";
                    }
                }
            }
            return new JsonResult
            {
                Data = new
                {
                    success = false,
                    havePONo = false,
                    haveDONo = false,
                    haveQuantity,
                    ErrorMessage,
                    exception = false,
                    view = this.RenderPartialView("CreateDO", x)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        var getDONo = db.DeliveryOrders.SingleOrDefault(y => y.DONo == x.DODetailObject.DONo);
        if (getDONo != null)
        {
            ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

            return new JsonResult
            {
                Data = new
                {
                    success = false,
                    havePONo = false,
                    haveDONo = true,
                    haveQuantity,
                    exception = false,
                    view = this.RenderPartialView("CreateDO", x)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        DeliveryOrder _objCreateDO = new DeliveryOrder
        {
            uuid = Guid.NewGuid(),
            PreparedById = Int32.Parse(Session["UserId"].ToString()),
            CompleteDate = DateTime.Now,
            DONo = x.DODetailObject.DONo,
            BranchId = x.DODetailObject.BranchId,
            DOIssueDate = x.DODetailObject.DOIssueDate,
            TelNo = x.DODetailObject.TelNo,
            TrackingNo = x.DODetailObject.TrackingNo,
            ContactPerson = x.DODetailObject.ContactPerson,
            Update = true
        };
        db.DeliveryOrders.Add(_objCreateDO);
        db.SaveChanges();

        foreach (var value in x.DOItemListObject)
        {
            PO_Item _objPOItem = db.PO_Item.First(m => m.itemsId == value.ItemsId);
            _objPOItem.deliveredQuantity = value.DeliveryQuantity + _objPOItem.deliveredQuantity;
            _objPOItem.outstandingQuantity = value.OutstandingQuantity - value.DeliveryQuantity;
            _objPOItem.deliveryQuantity = 0;
            db.SaveChanges();

            DO_Items _objDOItem = new DO_Items
            {
                uuid = Guid.NewGuid(),
                DOId = _objCreateDO.DOId,
                itemsId = value.ItemsId
            };
            var checkItems = (from m in db.DO_Items
                              where m.itemsId == value.ItemsId
                              select new
                              {
                                  DOId = m.DOId,
                                  DeliveredQuantity = m.deliveryQuantity
                              }).OrderByDescending(m => m.DOId).FirstOrDefault();
            if (checkItems == null)
            {
                _objDOItem.deliveryQuantity = value.DeliveryQuantity;
                _objDOItem.deliveredQuantity = 0;
                _objDOItem.outstandingQuantity = value.OutstandingQuantity - value.DeliveryQuantity;
            }
            else
            {
                _objDOItem.deliveryQuantity = value.DeliveryQuantity;
                _objDOItem.deliveredQuantity = checkItems.DeliveredQuantity;
                _objDOItem.outstandingQuantity = _objPOItem.outstandingQuantity;
            }

            db.DO_Items.Add(_objDOItem);
            db.SaveChanges();
        }

        foreach (var value in x.POListObject)
        {
            int TotalOutstandingQuantity = 0; int TotalDeliveredQuantity = 0;
            var _objCreatePOItemList = (from m in db.PurchaseOrders
                                        join n in db.PO_Item on m.POId equals n.POId
                                        where m.POId == value.POId
                                        select new POItemsTable()
                                        {
                                            OutstandingQuantity = n.outstandingQuantity,
                                            DeliveredQuantity = n.deliveredQuantity,
                                        }).ToList();

            foreach (var values in _objCreatePOItemList)
            {
                TotalOutstandingQuantity = TotalOutstandingQuantity + values.OutstandingQuantity;
                TotalDeliveredQuantity = TotalDeliveredQuantity + values.DeliveredQuantity;
            }
            PurchaseOrder _objPO = db.PurchaseOrders.First(m => m.POId == value.POId);
            if (TotalOutstandingQuantity == 0)
            {
                _objPO.StatusId = "POS008";
            }
            else if (TotalOutstandingQuantity > 0)
            {
                _objPO.StatusId = "POS007";
            }
            db.SaveChanges();
        }
        int DOId = _objCreateDO.DOId;
        int cId = Int32.Parse(Session["ContractId"].ToString());
        int fromUserID = _objCreateDO.PreparedById;
        var myEmail = db.Users.SingleOrDefault(y => y.userId == fromUserID);

        if (x.file != null)
        {
            FileUpload fileUploadModel = new FileUpload();
            DateTime createDate = DateTime.Now;
            string filename1 = x.file.FileName.Replace("\\", ",");
            string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            string fullPath = x.file.FileName;
            string extension = Path.GetExtension(fullPath);
            byte[] uploadFile = new byte[x.file.InputStream.Length];

            fileUploadModel.uuid = Guid.NewGuid();
            fileUploadModel.opportunityId = x.OppContId;
            fileUploadModel.DOId = _objCreateDO.DOId;
            fileUploadModel.uploadUserId = Int32.Parse(Session["UserId"].ToString());
            fileUploadModel.uploadDate = createDate;
            fileUploadModel.FullPath = fullPath;
            fileUploadModel.FileName = filename;
            fileUploadModel.description = x.FileDescription;
            fileUploadModel.Extension = extension;
            fileUploadModel.archivedFlag = false;
            fileUploadModel.DODetailInfo = true;
            fileUploadModel.notPO = false;
            x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
            fileUploadModel.File = uploadFile;
            db.FileUploads.Add(fileUploadModel);
            db.SaveChanges();

            foreach (var value in x.DOItemListObject)
            {
                Notification _objSendLog = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = value.POId,
                    DOId = _objCreateDO.DOId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0
                };

                var _objCreatePOItem = (from m in db.PurchaseOrders
                                        join n in db.PO_Item on m.POId equals n.POId
                                        join o in db.DO_Items on n.itemsId equals o.itemsId
                                        join p in db.DeliveryOrders on o.DOId equals p.DOId
                                        where m.POId == value.POId && p.DOId == DOId
                                        select new POItemsTable()
                                        {
                                            PONo = m.Details_PONo,
                                            DONo = p.DONo
                                        }).FirstOrDefault();
                //PO_Item _objPOItem = db.PO_Item.First(m => m.itemsId == value.ItemsId);
                _objSendLog.content = "create delivery order no. " + _objCreatePOItem.DONo + " for PO No. " + _objCreatePOItem.PONo;
                db.Notifications.Add(_objSendLog);

                POs_FileUpload fileUploadModel2 = new POs_FileUpload
                {
                    uuid = Guid.NewGuid(),
                    fileEntryId = fileUploadModel.fileEntryId,
                    POId = value.POId
                };
                db.POs_FileUpload.Add(fileUploadModel2);
                db.SaveChanges();
            }

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = x.OppContId,
                DOId = DOId,
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "uploaded " + filename,
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);
            db.SaveChanges();
        }

        try
        {
            List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
            NotiMemberList = DBQueryController.getNotiMemberList(cId);
            string Content = "new Delivery Order";

            Notification _objSendDOMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = x.OppContId,
                DOId = DOId,
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "Send email(s) updates for " + Content,
                QNAtype = "Message",
                flag = 0
            };
            db.Notifications.Add(_objSendDOMessage);
            db.SaveChanges();

            foreach (var model in NotiMemberList)
            {
                var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
                var DOInfo = (from m in db.DeliveryOrders
                              join n in db.DO_Items on m.DOId equals n.DOId
                              join o in db.PO_Item on n.itemsId equals o.itemsId
                              join p in db.PurchaseOrders on o.POId equals p.POId
                              join q in db.Opportunities on p.opportunityId equals q.opportunityId
                              join r in db.Users on m.PreparedById equals r.userId
                              join s in db.Customers on q.custId equals s.custId
                              where m.DOId == DOId
                              select new MailModel()
                              {
                                  CustName = s.name + " (" + s.abbreviation + ")",
                                  ContractName = q.name,
                                  Description = q.description
                              }).FirstOrDefault();
                x.RoleId = Session["RoleId"].ToString();
                x.UserId = Int32.Parse(Session["UserId"].ToString());
                x.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                x.DODetailObject = DBQueryController.getDOList("DODetails", x.UserId, "R02", x.OppContId, x.CompanyId, DOId).FirstOrDefault();

                string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/DOEmailTemplate.cshtml"));
                DOInfo.Content = myEmail.userName + " has register new Delivery Order. Please refer the details as below: ";
                DOInfo.FromName = myEmail.emailAddress;
                DOInfo.DOFlow = "DO Details";
                DOInfo.DONo = x.DODetailObject.DONo;
                DOInfo.PONo = x.DODetailObject.PONo;
                DOInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/DOList/ViewDO") + "?cId=" + cId + "&DOId=" + DOId);
                var result =
                        Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "DODetailsTemplateKey", null, DOInfo);

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("muzhafar@kubtel.com")
                };
                mail.To.Add(new MailAddress(UserEmail.emailAddress));
                mail.Subject = "OCM: Updates on new " + DOInfo.DONo + " for PO Number " + DOInfo.PONo;
                mail.Body = result;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient
                {
                    Host = "outlook.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential("muzhafar@kubtel.com", "welcome123$")
                };
                smtp.Send(mail);

                NotiGroup _objSendToUserId = new NotiGroup
                {
                    uuid = Guid.NewGuid(),
                    chatId = _objSendDOMessage.chatId,
                    toUserId = model.Id
                };
                db.NotiGroups.Add(_objSendToUserId);
                db.SaveChanges();
            }
        }
        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        {
            Exception raise = dbEx;
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    string message = string.Format("{0}:{1}",
                        validationErrors.Entry.Entity.ToString(),
                        validationError.ErrorMessage);
                    // raise a new exception nesting
                    // the current instance as InnerException
                    raise = new InvalidOperationException(message, raise);
                }
            }
            throw raise;
        }

        x.OppContId = cId;
        ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

        return new JsonResult
        {
            Data = new
            {
                success = true,
                haveDONo = false,
                exception = false,
                view = this.RenderPartialView("CreateDO", x)
            },
            JsonRequestBehavior = JsonRequestBehavior.AllowGet
        };
    }
    else
    {
        return Redirect("~/Home/Index");
    }
}

#region PODetails
[Authorize]
        public ActionResult PODetails()
        {
            if (User.Identity.IsAuthenticated)
            {
                int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
                OppoDetailListModel PODetailList = new OppoDetailListModel();
                PODetailList.OppoId = Int32.Parse(Session["ContractId"].ToString());
                PODetailList.POId = Int32.Parse(Session["POId"].ToString()); ;
                PODetailList.CreateByUserId = userid;
                PODetailList.POListDetail = (from m in db.PurchaseOrders
                                             join n in db.Users on m.createByUserId equals n.userId into s
                                             from x in s.DefaultIfEmpty()
                                             where m.POId == POId
                                             select new PODetailList()
                                               {
                                                   Details_PONo = m.Details_PONo,
                                                   Details_POIssueDate = m.Details_POIssueDate,
                                                   Details_projectDescription = m.Details_projectDescription,
                                                   Details_quantity = m.Details_quantity,
                                                   Details_pricePerUnit = m.Details_pricePerUnit,
                                                   Details_totalPrice = m.Details_totalPrice,
                                                   Details_totalGST = m.Details_totalGST,
                                                   Details_deliveryDate = m.Details_deliveryDate,
                                                   Details_materialNo = m.Details_materialNo,
                                                   Details_region = m.Details_region,
                                                   Details_trackingNo = m.Details_trackingNo,
                                                   Status = m.Status
                                               }).FirstOrDefault();
                PODetailList.RoleId = Session["RoleId"].ToString().Trim();
                PODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PODetailList.OppoId && m.userId == userid
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(PODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult PODetails(OppoDetailListModel x)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("PODetails", x);
            }
            int PoId = x.POId;
            int userid = Int32.Parse(Session["UserId"].ToString());
            PurchaseOrder objPODetaildb = db.PurchaseOrders.First(m => m.POId == PoId);

            if (objPODetaildb.Details_PONo != x.POListDetail.Details_PONo)
            {
                Notification _objDetails_PONo = new Notification();
                _objDetails_PONo.uuid = Guid.NewGuid();
                _objDetails_PONo.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_PONo.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_PONo.createDate = DateTime.Now;
                _objDetails_PONo.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_PONo.content = objPODetaildb.Details_PONo == null ? "change PO Number to " + x.POListDetail.Details_PONo :
                    "change PO Number from " + objPODetaildb.Details_PONo + " to " + x.POListDetail.Details_PONo;
                _objDetails_PONo.flag = 0;
                db.Notifications.Add(_objDetails_PONo);
                objPODetaildb.Details_PONo = x.POListDetail.Details_PONo;
            }
            if (objPODetaildb.Details_POIssueDate != x.POListDetail.Details_POIssueDate)
            {
                Notification _objDetails_POIssueDate = new Notification();
                _objDetails_POIssueDate.uuid = Guid.NewGuid();
                _objDetails_POIssueDate.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_POIssueDate.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_POIssueDate.createDate = DateTime.Now;
                _objDetails_POIssueDate.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_POIssueDate.content = objPODetaildb.Details_POIssueDate == null ? "change PO Issue date to " + x.POListDetail.Details_POIssueDate :
                    "change PO Issue date from " + objPODetaildb.Details_POIssueDate + " to " + x.POListDetail.Details_POIssueDate;
                _objDetails_POIssueDate.flag = 0;
                db.Notifications.Add(_objDetails_POIssueDate);
                objPODetaildb.Details_POIssueDate = x.POListDetail.Details_POIssueDate;
            }
            if (objPODetaildb.Details_projectDescription != x.POListDetail.Details_projectDescription)
            {
                Notification _objDetails_projectDescription = new Notification();
                _objDetails_projectDescription.uuid = Guid.NewGuid();
                _objDetails_projectDescription.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_projectDescription.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_projectDescription.createDate = DateTime.Now;
                _objDetails_projectDescription.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_projectDescription.content = objPODetaildb.Details_projectDescription == null ? "change project description selection to " + x.POListDetail.Details_projectDescription :
                    "change project description selection from " + objPODetaildb.Details_projectDescription + " to " + x.POListDetail.Details_projectDescription;
                _objDetails_projectDescription.flag = 0;
                db.Notifications.Add(_objDetails_projectDescription);
                objPODetaildb.Details_projectDescription = x.POListDetail.Details_projectDescription;
            }
            if (objPODetaildb.Details_quantity != x.POListDetail.Details_quantity)
            {
                Notification _objDetails_quantity = new Notification();
                _objDetails_quantity.uuid = Guid.NewGuid();
                _objDetails_quantity.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_quantity.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_quantity.createDate = DateTime.Now;
                _objDetails_quantity.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_quantity.content = objPODetaildb.Details_quantity == null ? "change quantity to " + x.POListDetail.Details_quantity :
                    "change quantity from " + objPODetaildb.Details_quantity + " to " + x.POListDetail.Details_quantity;
                _objDetails_quantity.flag = 0;
                db.Notifications.Add(_objDetails_quantity);
                objPODetaildb.Details_quantity = x.POListDetail.Details_quantity;
            }
            if (objPODetaildb.Details_pricePerUnit != x.POListDetail.Details_pricePerUnit)
            {
                Notification _objDetails_pricePerUnit = new Notification();
                _objDetails_pricePerUnit.uuid = Guid.NewGuid();
                _objDetails_pricePerUnit.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_pricePerUnit.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_pricePerUnit.createDate = DateTime.Now;
                _objDetails_pricePerUnit.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_pricePerUnit.content = objPODetaildb.Details_pricePerUnit == null ? "change quantity to " + x.POListDetail.Details_pricePerUnit :
                    "change quantity from " + objPODetaildb.Details_pricePerUnit + " to " + x.POListDetail.Details_pricePerUnit;
                _objDetails_pricePerUnit.flag = 0;
                db.Notifications.Add(_objDetails_pricePerUnit);
                objPODetaildb.Details_pricePerUnit = x.POListDetail.Details_pricePerUnit;
            }
            if (objPODetaildb.Details_totalPrice != x.POListDetail.Details_totalPrice)
            {
                Notification _objDetails_totalPrice = new Notification();
                _objDetails_totalPrice.uuid = Guid.NewGuid();
                _objDetails_totalPrice.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_totalPrice.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_totalPrice.createDate = DateTime.Now;
                _objDetails_totalPrice.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_totalPrice.content = objPODetaildb.Details_totalPrice == null ? "change total price to " + x.POListDetail.Details_totalPrice :
                    "change total price from " + objPODetaildb.Details_totalPrice + " to " + x.POListDetail.Details_totalPrice;
                _objDetails_totalPrice.flag = 0;
                db.Notifications.Add(_objDetails_totalPrice);
                objPODetaildb.Details_totalPrice = x.POListDetail.Details_totalPrice;
            }
            if (objPODetaildb.Details_totalGST != x.POListDetail.Details_totalGST)
            {
                Notification _objDetails_totalGST = new Notification();
                _objDetails_totalGST.uuid = Guid.NewGuid();
                _objDetails_totalGST.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_totalGST.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_totalGST.createDate = DateTime.Now;
                _objDetails_totalGST.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_totalGST.flag = 0;
                _objDetails_totalGST.content = objPODetaildb.Details_totalGST == null ? "change the total GST value to " + x.POListDetail.Details_totalGST :
                    "change the total GST value from " + objPODetaildb.Details_totalGST + " to " + x.POListDetail.Details_totalGST;
                db.Notifications.Add(_objDetails_totalGST);
                objPODetaildb.Details_totalGST = x.POListDetail.Details_totalGST;
            }
            if (objPODetaildb.Details_deliveryDate != x.POListDetail.Details_deliveryDate)
            {
                Notification _objDetails_deliveryDate = new Notification();
                _objDetails_deliveryDate.uuid = Guid.NewGuid();
                _objDetails_deliveryDate.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_deliveryDate.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_deliveryDate.createDate = DateTime.Now;
                _objDetails_deliveryDate.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_deliveryDate.flag = 0;
                _objDetails_deliveryDate.content = objPODetaildb.Details_deliveryDate == null ? "change the delivery date to " + x.POListDetail.Details_deliveryDate :
                    "change the delivery date from " + objPODetaildb.Details_deliveryDate + " to " + x.POListDetail.Details_deliveryDate;
                db.Notifications.Add(_objDetails_deliveryDate);
                objPODetaildb.Details_deliveryDate = x.POListDetail.Details_deliveryDate;
            }
            if (objPODetaildb.Details_materialNo != x.POListDetail.Details_materialNo)
            {
                Notification _objDetails_materialNo = new Notification();
                _objDetails_materialNo.uuid = Guid.NewGuid();
                _objDetails_materialNo.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_materialNo.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_materialNo.createDate = DateTime.Now;
                _objDetails_materialNo.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_materialNo.flag = 0;
                _objDetails_materialNo.content = objPODetaildb.Details_materialNo == null ? "change material no. to " + x.POListDetail.Details_materialNo :
                    "change material no. from " + objPODetaildb.Details_materialNo + " to " + x.POListDetail.Details_materialNo;
                db.Notifications.Add(_objDetails_materialNo);
                objPODetaildb.Details_materialNo = x.POListDetail.Details_materialNo;
            }
            if (objPODetaildb.Details_region != x.POListDetail.Details_region)
            {
                Notification _objDetails_region = new Notification();
                _objDetails_region.uuid = Guid.NewGuid();
                _objDetails_region.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_region.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_region.createDate = DateTime.Now;
                _objDetails_region.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_region.flag = 0;
                _objDetails_region.content = objPODetaildb.Details_region == null ? "change the region to " + x.POListDetail.Details_region :
                    "change the region from " + objPODetaildb.Details_region + " to " + x.POListDetail.Details_region;
                db.Notifications.Add(_objDetails_region);
                objPODetaildb.Details_region = x.POListDetail.Details_region;
            }
            if (objPODetaildb.Details_trackingNo != x.POListDetail.Details_trackingNo)
            {
                Notification _objDetails_trackingNo = new Notification();
                _objDetails_trackingNo.uuid = Guid.NewGuid();
                _objDetails_trackingNo.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objDetails_trackingNo.POId = Int32.Parse(Session["POId"].ToString());
                _objDetails_trackingNo.createDate = DateTime.Now;
                _objDetails_trackingNo.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objDetails_trackingNo.content = objPODetaildb.Details_trackingNo == null ? "change tracking no. to " + x.POListDetail.Details_trackingNo :
                    "change tracking no. from " + objPODetaildb.Details_trackingNo + " to " + x.POListDetail.Details_trackingNo;
                _objDetails_trackingNo.flag = 0;
                db.Notifications.Add(_objDetails_trackingNo);
                objPODetaildb.Details_trackingNo = x.POListDetail.Details_trackingNo;
            }
            if (objPODetaildb.Status != x.POListDetail.Status)
            {
                Notification _objStatus = new Notification();
                _objStatus.uuid = Guid.NewGuid();
                _objStatus.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                _objStatus.POId = Int32.Parse(Session["POId"].ToString());
                _objStatus.createDate = DateTime.Now;
                _objStatus.fromUserId = Int32.Parse(Session["UserId"].ToString());
                _objStatus.content = objPODetaildb.Status == null ? "change status to " + x.POListDetail.Status :
                    "change status from " + objPODetaildb.Status + " to " + x.POListDetail.Status;
                _objStatus.flag = 0;
                db.Notifications.Add(_objStatus);
                objPODetaildb.Status = x.POListDetail.Status;
            }

            FileUpload fileUploadModel = new FileUpload();
            DateTime createDate = DateTime.Now;
            string filename1 = x.file.FileName.Replace("\\", ",");
            string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            string fullPath = x.file.FileName;
            string extension = filename.Split('.')[1].ToString();
            byte[] uploadFile = new byte[x.file.InputStream.Length];

            fileUploadModel.uuid = Guid.NewGuid();
            fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
            fileUploadModel.POId = PoId;
            fileUploadModel.uploadUserId = userid;
            fileUploadModel.uploadDate = createDate;
            fileUploadModel.FullPath = fullPath;
            fileUploadModel.FileName = filename;
            fileUploadModel.description = x.FileDescription;
            fileUploadModel.Extension = extension;
            fileUploadModel.archivedFlag = false;
            fileUploadModel.PODetailInfo = true;

            x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
            fileUploadModel.File = uploadFile;
            db.FileUploads.Add(fileUploadModel);

            Notification _objSendMessage = new Notification();
            _objSendMessage.uuid = Guid.NewGuid();
            _objSendMessage.opportunityId = Int32.Parse(Session["ContractId"].ToString());
            _objSendMessage.POId = Int32.Parse(Session["POId"].ToString());
            _objSendMessage.createDate = DateTime.Now;
            _objSendMessage.fromUserId = Int32.Parse(Session["UserId"].ToString());
            _objSendMessage.content = "uploaded " + filename;
            _objSendMessage.flag = 0;
            db.Notifications.Add(_objSendMessage);

            int ContractId = Int32.Parse(Session["ContractId"].ToString());
            x.objCanEdit = (from m in db.Users_Opportunity
                            where m.opportunityId == ContractId && m.userId == userid
                            select new EditPermission()
                            {
                                canEdits = m.canEdit
                            }).FirstOrDefault();
            try
            {
                db.SaveChanges();
                x.RoleId = Session["RoleId"].ToString();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return Json(new { success = true });
        }
        #endregion