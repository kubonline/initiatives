﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Net.Mail;
using System.Data;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;

namespace Initiatives.Controllers
{
    public class DOListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();

        #region Create DO
        [HttpPost]
        public JsonResult NewDO(string doid)
        {
            Session["DOId"] = doid;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("CreateDO", "DOList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult CreateDO(string id)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (id != null && User.Identity.IsAuthenticated)
                {
                    Session["DOId"] = id;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int contractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.OppContId = contractId;
                model.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, model.OppContId, model.CompanyId, "DO");
                model.DOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.PartNo_Items on n.partNo equals o.partNo into p
                                          from q in p.DefaultIfEmpty()
                                          where n.POId == 0
                                          select new DOItemsTable()
                                          {
                                              ItemsId = n.itemsId,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                              DeliveredQuantity = n.deliveredQuantity,
                                              OutstandingQuantity = n.outstandingQuantity
                                          }).ToList();
                var MaterialNoQuery = model.DOItemListObject.ToList()
                    .Select(c => new SelectListItem { Text = c.MaterialNo, Value = c.ItemsId.ToString() }).ToList();
                ViewBag.MaterialNoList = MaterialNoQuery;
                var CustBranchQuery = db.CustBranches.Select(c => new { BranchId = c.branchId, branchAddress = c.branchAddress })
                    .OrderBy(c => c.BranchId);
                ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpGet]
        public ActionResult GetPOList()
        {
            MainModel model = new MainModel
            {
                UserId = Int32.Parse(Session["UserId"].ToString())
            };
            int contractId = Int32.Parse(Session["ContractId"].ToString());
            var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
            model.RoleId = getRole.Role.roleId.Trim();
            model.OppContId = contractId;
            model.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
            model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, model.OppContId, model.CompanyId, "DO");

            return PartialView("POTable", model);
        }

        [HttpPost]
        public ActionResult GetMaterialNo(MainModel MM)
        {
            List<DOItemsTable> newDOITemList = new List<DOItemsTable>();
            foreach (var value in MM.POListObject)
            {
                newDOITemList = newDOITemList.Concat(from m in db.PurchaseOrders
                                                     join n in db.PO_Item on m.POId equals n.POId
                                                     join o in db.PartNo_Items on n.partNo equals o.partNo
                                                     //join o in db.DO_Items on n.itemsId equals o.itemsId into p
                                                     //from q in p.DefaultIfEmpty()
                                                     where n.POId == value.POId && n.outstandingQuantity > 0
                                                     select new DOItemsTable()
                                                     {
                                                         POId = m.POId,
                                                         ItemsId = n.itemsId,
                                                         PONo = m.Details_PONo,
                                                         MaterialNo = o.materialNo,
                                                         Description = o.description,
                                                         DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                                         DeliveredQuantity = n.deliveredQuantity,
                                                         DeliveryQuantity = n.deliveryQuantity,
                                                         OutstandingQuantity = n.outstandingQuantity
                                                     }).ToList();
            }
            MM.DOItemListObject = newDOITemList;

            return PartialView("DOItemsTable", MM);
        }

        [HttpPost]
        public ActionResult CreateDO(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (Session["ContractId"] == null)
                {
                    Session["ContractId"] = x.OppContId;
                }
                //if (x.file == null)
                //{
                //    ModelState.AddModelError("file", "Please input the attachment before proceed.");
                //}
                var CustBranchQuery = db.CustBranches.Select(c => new { BranchId = c.branchId, branchAddress = c.branchAddress })
                    .OrderBy(c => c.BranchId);

                bool haveQuantity = false; string ErrorMessage = "";
                foreach (var value in x.DOItemListObject)
                {
                    if (value.DeliveryQuantity < 1)
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot be 0 or less in DO Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                haveDONo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreateDO", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    if (value.DeliveryQuantity > value.OutstandingQuantity)
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot exceed Outstanding Quantity in DO Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                haveDONo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreateDO", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                }
                if (!ModelState.IsValid)
                {
                    int DeliveryQuantity = 0;
                    foreach (var value in ModelState)
                    {
                        if (value.Key.Contains("DeliveryQuantity"))
                        {
                            DeliveryQuantity = Int32.Parse(value.Value.Value.AttemptedValue);
                            if (DeliveryQuantity < 1)
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot be 0 or less in DO Item";
                            }
                        }
                        if (value.Key.Contains("OutstandingQuantity"))
                        {
                            if (DeliveryQuantity > Int32.Parse(value.Value.Value.AttemptedValue))
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create DO failed! Reason: Delivery Quantity cannot exceed Outstanding Quantity in DO Item";
                            }
                        }
                    }
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            haveDONo = false,
                            haveQuantity,
                            ErrorMessage,
                            exception = false,
                            view = this.RenderPartialView("CreateDO", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                var getDONo = db.DeliveryOrders.SingleOrDefault(y => y.DONo == x.DODetailObject.DONo);
                if (getDONo != null)
                {
                    ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            haveDONo = true,
                            haveQuantity,
                            exception = false,
                            view = this.RenderPartialView("CreateDO", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                DeliveryOrder _objCreateDO = new DeliveryOrder
                {
                    uuid = Guid.NewGuid(),
                    PreparedById = Int32.Parse(Session["UserId"].ToString()),
                    CompleteDate = DateTime.Now,
                    DONo = x.DODetailObject.DONo,
                    BranchId = x.DODetailObject.BranchId,
                    DOIssueDate = x.DODetailObject.DOIssueDate,
                    TelNo = x.DODetailObject.TelNo,
                    TrackingNo = x.DODetailObject.TrackingNo,
                    ContactPerson = x.DODetailObject.ContactPerson,
                    Update = true
                };
                db.DeliveryOrders.Add(_objCreateDO);
                db.SaveChanges();

                foreach (var value in x.DOItemListObject)
                {
                    PO_Item _objPOItem = db.PO_Item.First(m => m.itemsId == value.ItemsId);
                    _objPOItem.deliveredQuantity = value.DeliveryQuantity + _objPOItem.deliveredQuantity;
                    _objPOItem.outstandingQuantity = value.OutstandingQuantity - value.DeliveryQuantity;
                    _objPOItem.deliveryQuantity = 0;
                    db.SaveChanges();

                    DO_Items _objDOItem = new DO_Items
                    {
                        uuid = Guid.NewGuid(),
                        DOId = _objCreateDO.DOId,
                        itemsId = value.ItemsId
                    };
                    var checkItems = (from m in db.DO_Items
                                      where m.itemsId == value.ItemsId
                                      select new
                                      {
                                          DOId = m.DOId,
                                          DeliveredQuantity = m.deliveryQuantity
                                      }).OrderByDescending(m => m.DOId).FirstOrDefault();
                    if (checkItems == null)
                    {
                        _objDOItem.deliveryQuantity = value.DeliveryQuantity;
                        _objDOItem.deliveredQuantity = 0;
                        _objDOItem.outstandingQuantity = value.OutstandingQuantity - value.DeliveryQuantity;
                    }
                    else
                    {
                        _objDOItem.deliveryQuantity = value.DeliveryQuantity;
                        _objDOItem.deliveredQuantity = checkItems.DeliveredQuantity;
                        _objDOItem.outstandingQuantity = _objPOItem.outstandingQuantity;
                    }

                    db.DO_Items.Add(_objDOItem);
                    db.SaveChanges();
                }

                foreach (var value in x.POListObject)
                {
                    int TotalOutstandingQuantity = 0; int TotalDeliveredQuantity = 0;
                    var _objCreatePOItemList = (from m in db.PurchaseOrders
                                                join n in db.PO_Item on m.POId equals n.POId
                                                where m.POId == value.POId
                                                select new POItemsTable()
                                                {
                                                    OutstandingQuantity = n.outstandingQuantity,
                                                    DeliveredQuantity = n.deliveredQuantity,
                                                }).ToList();

                    foreach (var values in _objCreatePOItemList)
                    {
                        TotalOutstandingQuantity = TotalOutstandingQuantity + values.OutstandingQuantity;
                        TotalDeliveredQuantity = TotalDeliveredQuantity + values.DeliveredQuantity;
                    }
                    PurchaseOrder _objPO = db.PurchaseOrders.First(m => m.POId == value.POId);
                    if (TotalOutstandingQuantity == 0)
                    {
                        _objPO.StatusId = "POS008";
                    }
                    else if (TotalOutstandingQuantity > 0)
                    {
                        _objPO.StatusId = "POS007";
                    }
                    db.SaveChanges();
                }
                int DOId = _objCreateDO.DOId;
                int cId = Int32.Parse(Session["ContractId"].ToString());
                int fromUserID = _objCreateDO.PreparedById;
                var myEmail = db.Users.SingleOrDefault(y => y.userId == fromUserID);

                if (x.file != null)
                {
                    FileUpload fileUploadModel = new FileUpload();
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = x.OppContId;
                    fileUploadModel.DOId = _objCreateDO.DOId;
                    fileUploadModel.uploadUserId = Int32.Parse(Session["UserId"].ToString());
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.DODetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);
                    db.SaveChanges();

                    foreach (var value in x.DOItemListObject)
                    {
                        POs_FileUpload fileUploadModel2 = new POs_FileUpload
                        {
                            uuid = Guid.NewGuid(),
                            fileEntryId = fileUploadModel.fileEntryId,
                            POId = value.POId
                        };
                        db.POs_FileUpload.Add(fileUploadModel2);
                        db.SaveChanges();
                    }

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        DOId = DOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                    db.SaveChanges();
                }

                foreach (var value in x.DOItemListObject)
                {
                    Notification _objSendLog = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        POId = value.POId,
                        DOId = _objCreateDO.DOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        flag = 0
                    };

                    var _objCreatePOItem = (from m in db.PurchaseOrders
                                            join n in db.PO_Item on m.POId equals n.POId
                                            join o in db.DO_Items on n.itemsId equals o.itemsId
                                            join p in db.DeliveryOrders on o.DOId equals p.DOId
                                            where m.POId == value.POId && p.DOId == DOId
                                            select new POItemsTable()
                                            {
                                                PONo = m.Details_PONo,
                                                DONo = p.DONo
                                            }).FirstOrDefault();
                    //PO_Item _objPOItem = db.PO_Item.First(m => m.itemsId == value.ItemsId);
                    _objSendLog.content = "create delivery order no. " + _objCreatePOItem.DONo + " for PO No. " + _objCreatePOItem.PONo;
                    db.Notifications.Add(_objSendLog);
                }

                if (x.DODetailObject.SendEmail == true)
                {
                    try
                    {
                        List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
                        NotiMemberList = DBQueryController.getNotiMemberList(cId);

                        Notification _objSendDOMessage = new Notification
                        {
                            uuid = Guid.NewGuid(),
                            opportunityId = x.OppContId,
                            DOId = DOId,
                            createDate = DateTime.Now,
                            fromUserId = Int32.Parse(Session["UserId"].ToString()),
                            content = "Send email(s) updates for new Delivery Order",
                            QNAtype = "Message",
                            flag = 0
                        };
                        db.Notifications.Add(_objSendDOMessage);
                        db.SaveChanges();

                        foreach (var model in NotiMemberList)
                        {
                            var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
                            var DOInfo = (from m in db.DeliveryOrders
                                          join n in db.DO_Items on m.DOId equals n.DOId
                                          join o in db.PO_Item on n.itemsId equals o.itemsId
                                          join p in db.PurchaseOrders on o.POId equals p.POId
                                          join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                          join r in db.Users on m.PreparedById equals r.userId
                                          join s in db.Customers on q.custId equals s.custId
                                          where m.DOId == DOId
                                          select new MailModel()
                                          {
                                              CustName = s.name + " (" + s.abbreviation + ")",
                                              ContractName = q.name,
                                              Description = q.description
                                          }).FirstOrDefault();
                            x.RoleId = Session["RoleId"].ToString();
                            x.UserId = Int32.Parse(Session["UserId"].ToString());
                            x.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                            x.DODetailObject = DBQueryController.getDOList("DODetails", x.UserId, "R02", x.OppContId, x.CompanyId, DOId).FirstOrDefault();

                            string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/DOEmailTemplate.cshtml"));
                            DOInfo.Content = myEmail.userName + " has register new Delivery Order. Please refer the details as below: ";
                            DOInfo.FromName = myEmail.emailAddress;
                            DOInfo.DOFlow = "DO Details";
                            DOInfo.DONo = x.DODetailObject.DONo;
                            DOInfo.PONo = x.DODetailObject.PONo;
                            DOInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/DOList/ViewDO") + "?cId=" + cId + "&DOId=" + DOId);
                            var result =
                                    Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "DODetailsTemplateKey", null, DOInfo);

                            MailMessage mail = new MailMessage
                            {
                                From = new MailAddress("info@kubtel.com")
                            };
                            mail.To.Add(new MailAddress(UserEmail.emailAddress));
                            mail.Subject = "OCM: Updates on new " + DOInfo.DONo + " for PO Number " + DOInfo.PONo;
                            mail.Body = result;
                            mail.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient
                            {
                                Host = "outlook.office365.com",
                                Port = 587,
                                EnableSsl = true,
                                Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                            };
                            smtp.Send(mail);

                            NotiGroup _objSendToUserId = new NotiGroup
                            {
                                uuid = Guid.NewGuid(),
                                chatId = _objSendDOMessage.chatId,
                                toUserId = model.Id
                            };
                            db.NotiGroups.Add(_objSendToUserId);
                            db.SaveChanges();
                        }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        Exception raise = dbEx;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                string message = string.Format("{0}:{1}",
                                    validationErrors.Entry.Entity.ToString(),
                                    validationError.ErrorMessage);
                                // raise a new exception nesting
                                // the current instance as InnerException
                                raise = new InvalidOperationException(message, raise);
                            }
                        }
                        throw raise;
                    }
                } else
                {
                    Notification _objSendDOMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        DOId = DOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "Create new delivery order without sending emails",
                        QNAtype = "Message",
                        flag = 0
                    };
                    db.Notifications.Add(_objSendDOMessage);
                    db.SaveChanges();
                }
                
                x.OppContId = cId;
                ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        haveDONo = false,
                        exception = false,
                        view = this.RenderPartialView("CreateDO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult CreateAllDO()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                int custId = Int32.Parse(Session["CompanyId"].ToString());
                model.UserName = Session["Username"].ToString();
                List<ContractListTable> OppoTypeQuery = new List<ContractListTable>();
                if (model.RoleId == "R02")
                {
                    OppoTypeQuery = db.Opportunities.Select(c => new ContractListTable() { Id = c.opportunityId, ContractName = c.contractNo + " - " + c.name, SupplierId = c.supplierId })
                        .Where(c => c.SupplierId == custId).OrderBy(c => c.ContractName).ToList();
                }
                else
                {
                    OppoTypeQuery = (from m in db.Opportunities
                                     join n in db.Users_Opportunity on m.opportunityId equals n.opportunityId
                                     where m.supplierId == custId && n.userId == model.UserId
                                     select new ContractListTable()
                                     {
                                         Id = m.opportunityId,
                                         ContractName = m.contractNo + " - " + m.name
                                     }).OrderBy(c => c.ContractName).ToList().ToList();
                }

                ViewBag.ContNoList = new SelectList(OppoTypeQuery.AsEnumerable(), "Id", "ContractName");

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult AllDOList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                //model.DOListObject = DBQueryController.getDOList(model.UserId, model.RoleId, "", model.CompanyId);

                return PartialView("AllDOList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region DO Tabs
        [HttpPost]
        public JsonResult DOTabs(string cId, string DOId)
        {
            if (cId != null)
            {
                Session["ContractId"] = cId;
            }
            Session["DOId"] = DOId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewDO", "DOList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }
        public ActionResult ViewDO(string cId, string DOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (cId != null && DOId != null && User.Identity.IsAuthenticated)
                {
                    Session["ContractId"] = cId;
                    Session["DOId"] = DOId;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int DOID = Int32.Parse(Session["DOId"].ToString()); int ContractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.DOId = DOID;
                model.OppContId = ContractId;
                model.UserName = Session["Username"].ToString();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult DODetails(int DOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel DODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    DOId = Int32.Parse(Session["DOId"].ToString()),
                    DODetailObject = (from m in db.DeliveryOrders
                                      join n in db.Users on m.PreparedById equals n.userId into s
                                      from x in s.DefaultIfEmpty()
                                      where m.DOId == DOId
                                      select new DOListTable()
                                      {
                                          DONo = m.DONo,
                                          DOIssueDate = m.DOIssueDate,
                                          DOCompleteDate = m.CompleteDate,
                                          TrackingNo = m.TrackingNo,
                                          ContactPerson = m.ContactPerson,
                                          TelNo = m.TelNo,
                                          BranchId = m.BranchId,
                                          DOUpdate = m.Update
                                      }).FirstOrDefault(),
                    DOItemListObject = (from m in db.PurchaseOrders
                                        join n in db.PO_Item on m.POId equals n.POId
                                        join o in db.DO_Items on n.itemsId equals o.itemsId
                                        join p in db.DeliveryOrders on o.DOId equals p.DOId
                                        join q in db.PartNo_Items on n.partNo equals q.partNo
                                        where p.DOId == DOId
                                        select new DOItemsTable()
                                        {
                                            PONo = m.Details_PONo,
                                            MaterialNo = q.materialNo,
                                            Description = q.description,
                                            DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                            DeliveryQuantity = o.deliveryQuantity,
                                            DeliveredQuantity = o.deliveredQuantity,
                                            OutstandingQuantity = o.outstandingQuantity
                                        }).ToList()
                };
                Session["Details_Update"] = DODetailList.DODetailObject.DOUpdate;
                DODetailList.RoleId = Session["RoleId"].ToString();
                DODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == DODetailList.OppContId && m.userId == DODetailList.UserId
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();
                var CustBranchQuery = db.CustBranches.Select(c => new { BranchId = c.branchId, branchAddress = c.branchAddress })
                    .OrderBy(c => c.BranchId);
                ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress", DODetailList.DODetailObject.BranchId);

                return PartialView(DODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult DODetails(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                //if (bool.Parse(Session["Details_Update"].ToString()) == false && x.file == null)
                //{
                //    ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
                //}
                var CustBranchQuery = db.CustBranches.Select(c => new { BranchId = c.branchId, branchAddress = c.branchAddress })
                    .OrderBy(c => c.BranchId);
                if (!ModelState.IsValid)
                {
                    x.DOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.DO_Items on n.itemsId equals o.itemsId
                                          join p in db.DeliveryOrders on o.DOId equals p.DOId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.DOId == x.DOId
                                          select new DOItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              DeliveryQuantity = o.deliveryQuantity,
                                              DeliveredQuantity = o.deliveredQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();
                    ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            exception = false,
                            view = this.RenderPartialView("DODetails", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                int DoId = x.DOId;
                int userid = Int32.Parse(Session["UserId"].ToString());
                DeliveryOrder objDODetaildb = db.DeliveryOrders.First(m => m.DOId == DoId);
                var objDODetail = (from m in db.DeliveryOrders
                                   join n in db.CustBranches on m.BranchId equals n.branchId
                                   where m.DOId == DoId
                                   select new DOFormModels.DOFormList()
                                   {
                                       Address = n.branchAddress
                                   }).FirstOrDefault();
                var DOBranchInfo = db.CustBranches.First(m => m.branchId == x.DODetailObject.BranchId);

                if (objDODetaildb.DONo != x.DODetailObject.DONo)
                {
                    Notification _objDetails_DONo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change DO Number from " + objDODetaildb.DONo + " to " + x.DODetailObject.DONo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_DONo);
                    objDODetaildb.DONo = x.DODetailObject.DONo;
                }
                if (objDODetaildb.BranchId != x.DODetailObject.BranchId)
                {
                    Notification _objDetails_BranchId = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objDODetail.Address == null ? "change DO Branch Address to " + DOBranchInfo.branchAddress : 
                        "change DO Branch Address from " + objDODetail.Address + " to " + DOBranchInfo.branchAddress,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_BranchId);
                    objDODetaildb.BranchId = x.DODetailObject.BranchId;
                }
                if (objDODetaildb.DOIssueDate != x.DODetailObject.DOIssueDate)
                {
                    Notification _objDetails_DOIssueDate = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change DO Issue date from " + objDODetaildb.DOIssueDate + " to " + x.DODetailObject.DOIssueDate,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_DOIssueDate);
                    objDODetaildb.DOIssueDate = x.DODetailObject.DOIssueDate;
                }
                if (objDODetaildb.TelNo != x.DODetailObject.TelNo)
                {
                    Notification _objDetails_TelNo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objDODetaildb.TelNo == null ? "change Telephone No. to " + x.DODetailObject.TelNo :
                        "change Telephone No. from " + objDODetaildb.TelNo + " to " + x.DODetailObject.TelNo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_TelNo);
                    objDODetaildb.TelNo = x.DODetailObject.TelNo;
                }
                if (objDODetaildb.TrackingNo != x.DODetailObject.TrackingNo)
                {
                    Notification _objDetails_TrackingNo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objDODetaildb.TrackingNo == null ? "change Tracking No. to " + x.DODetailObject.TrackingNo :
                        "change Tracking No. from " + objDODetaildb.TrackingNo + " to " + x.DODetailObject.TrackingNo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_TrackingNo);
                    objDODetaildb.TrackingNo = x.DODetailObject.TrackingNo;
                }
                if (objDODetaildb.ContactPerson != x.DODetailObject.ContactPerson)
                {
                    Notification _objDetails_ContactPerson = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objDODetaildb.ContactPerson == null ? "change Contact Person to " + x.DODetailObject.ContactPerson :
                        "change ContactPerson from " + objDODetaildb.ContactPerson + " to " + x.DODetailObject.ContactPerson,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_ContactPerson);
                    objDODetaildb.ContactPerson = x.DODetailObject.ContactPerson;
                }

                if (x.file != null)
                {
                    FileUpload fileUploadModel = new FileUpload();
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                    fileUploadModel.DOId = DoId;
                    fileUploadModel.uploadUserId = userid;
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.DODetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        DOId = Int32.Parse(Session["DOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                }

                int ContractId = Int32.Parse(Session["ContractId"].ToString());
                x.objCanEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == ContractId && m.userId == userid
                                select new EditPermission()
                                {
                                    canEdits = m.canEdit
                                }).FirstOrDefault();
                try
                {
                    db.SaveChanges();
                    x.RoleId = Session["RoleId"].ToString();
                    //x.POTrackingTable = DBQueryController.getPOTracking(PoId);
                    x.DOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.DO_Items on n.itemsId equals o.itemsId
                                          join p in db.DeliveryOrders on o.DOId equals p.DOId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.DOId == DoId
                                          select new DOItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              DeliveryQuantity = o.deliveryQuantity,
                                              DeliveredQuantity = o.deliveredQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();
                    ViewBag.CustBranchList = new SelectList(CustBranchQuery.AsEnumerable(), "BranchId", "branchAddress");
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        exception = false,
                        view = this.RenderPartialView("DODetails", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        #region DocManagement
        [Authorize]
        public ActionResult DODocManagement(int DOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel();
                int userid = Int32.Parse(Session["UserId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                model.DODocListObject = (from m in db.FileUploads
                                         join n in db.Users on m.uploadUserId equals n.userId
                                         where m.DOId == DOId && m.archivedFlag == false
                                         select new FilePODetailInfo()
                                         {
                                             opportunityId = m.opportunityId,
                                             fileEntryId = m.fileEntryId,
                                             uploadUserName = n.firstName + " " + n.lastName,
                                             uploadDate = m.uploadDate,
                                             FileName = m.FileName,
                                             File = m.File,
                                             Description = m.description
                                         }).ToList();
                model.FileArchiveModel = (from m in db.FileUploads
                                          join n in db.Users on m.uploadUserId equals n.userId
                                          where m.DOId == DOId && m.archivedFlag == true
                                          select new FilePODetailInfo()
                                          {
                                              opportunityId = m.opportunityId,
                                              fileEntryId = m.fileEntryId,
                                              uploadUserName = n.firstName + " " + n.lastName,
                                              uploadDate = m.uploadDate,
                                              FileName = m.FileName,
                                              File = m.File,
                                              Description = m.description
                                          }).ToList();
                return PartialView(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public FileContentResult FileDownload(int id)
        {
            byte[] fileData;

            FileUpload fileRecord = db.FileUploads.Find(id);

            string fileName;
            string filename1 = fileRecord.FileName.Replace("\\", ",");
            string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            fileData = (byte[])fileRecord.File.ToArray();
            fileName = filename2;

            return File(fileData, "text", fileName);
        }
        public JsonResult FileArchive(string selectedId)
        {
            int id = Int32.Parse(selectedId);

            FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
            if (itemToArchive != null)
            {
                itemToArchive.archivedFlag = true;
                db.SaveChanges();
            }

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region Updates
        [Authorize]
        public ActionResult DOUpdates(int DOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int Uid = Int32.Parse(Session["UserId"].ToString());
                int cId = Int32.Parse(Session["ContractId"].ToString());
                MainModel QNAModelList = new MainModel
                {
                    MessageListModel = DBQueryController.getMessageList(cId, 0, 0, 0, DOId, Uid),
                    UserId = Uid
                };

                var NotiMemberList = (from m in db.Users
                                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                      from x in gy.DefaultIfEmpty()
                                      where m.status == true && x.opportunityId == cId
                                      select new NotiMemberList()
                                      {
                                          Id = m.userId,
                                          Name = m.firstName + " " + m.lastName
                                      }).ToList().OrderBy(c => c.Name);

                ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                return PartialView(QNAModelList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public virtual ActionResult DODocUpload(MainModel fd)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                FileUpload fileUploadModel = new FileUpload();
                int userId = fd.UserId;
                DateTime createDate = DateTime.Now;
                string filename1 = fd.file.FileName.Replace("\\", ",");
                string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                string fullPath = fd.file.FileName;
                string extension = Path.GetExtension(fullPath);
                byte[] uploadFile = new byte[fd.file.InputStream.Length];

                fileUploadModel.uuid = Guid.NewGuid();
                fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                fileUploadModel.DOId = fd.DOId;
                fileUploadModel.uploadUserId = userId;
                fileUploadModel.uploadDate = createDate;
                fileUploadModel.FullPath = fullPath;
                fileUploadModel.FileName = filename;
                fileUploadModel.description = fd.FileDescription;
                fileUploadModel.Extension = extension;
                fileUploadModel.archivedFlag = false;
                fileUploadModel.notPO = false;
                fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                fileUploadModel.File = uploadFile;
                db.FileUploads.Add(fileUploadModel);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    DOId = Int32.Parse(Session["DOId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "uploaded " + filename,
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);

                db.SaveChanges();

                //return PartialView(db.FileUploads.ToList());
                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult DOSendMessage(string UserId, int DOId, string Content, List<int> selectedUserId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int fromUserID = Int32.Parse(UserId);
                int cId = Int32.Parse(Session["ContractId"].ToString());
                var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = cId,
                    DOId = DOId,
                    createDate = DateTime.Now,
                    fromUserId = fromUserID,
                    content = Content,
                    QNAtype = "Message",
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();

                foreach (var model in selectedUserId)
                {
                    var UserEmail = db.Users.SingleOrDefault(z => z.userId == model);
                    var DOInfo = (from m in db.DeliveryOrders
                                  join n in db.DO_Items on m.DOId equals n.DOId
                                  join o in db.PO_Item on n.itemsId equals o.itemsId
                                  join p in db.PurchaseOrders on o.POId equals p.POId
                                  join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                  join r in db.Users on m.PreparedById equals r.userId
                                  join s in db.Customers on q.custId equals s.custId
                                  where m.DOId == DOId
                                  select new MailModel()
                                  {
                                      CustName = s.name + " (" + s.abbreviation + ")",
                                      CustAbbreviation = s.abbreviation,
                                      ContractName = q.name,
                                      PONo = p.Details_PONo,
                                      DONo = m.DONo,
                                      Description = q.description
                                  }).FirstOrDefault();

                    string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/DOEmailTemplate.cshtml"));
                    DOInfo.Content = Content;
                    DOInfo.FromName = myEmail.emailAddress;
                    DOInfo.DOFlow = "DO Details";
                    DOInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority,
                        Url.Content("~/DOList/ViewDO") + "?cId=" + cId + "&DOId=" + DOId + "#DOUpdates-1");
                    var result = Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "DOTemplateKey", null, DOInfo);

                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress("info@kubtel.com")
                    };
                    //mail.From = new MailAddress("info@kubtel.com");
                    mail.To.Add(new MailAddress(UserEmail.emailAddress));
                    //mail.To.Add(new MailAddress("info@kubtel.com"));
                    mail.Subject = "OCM: Updates on " + DOInfo.ContractName + " for " + DOInfo.CustAbbreviation;
                    mail.Body = result;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient
                    {
                        //smtp.Host = "pops.kub.com";
                        Host = "outlook.office365.com",
                        //smtp.Host = "smtp.gmail.com";
                        Port = 587,
                        //smtp.Port = 25;
                        EnableSsl = true,
                        Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                    };
                    //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
                    smtp.Send(mail);

                    NotiGroup _objSendToUserId = new NotiGroup
                    {
                        uuid = Guid.NewGuid(),
                        chatId = _objSendMessage.chatId,
                        toUserId = model
                    };
                    db.NotiGroups.Add(_objSendToUserId);
                }
                db.SaveChanges();

                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion
        #endregion
        public ActionResult DOForm()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int DOId = Int32.Parse(Session["DOId"].ToString());
                MainModel model = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                var DODetailsObj = DBQueryController.getDOList("DODetails", model.UserId, "R02", model.OppContId, model.CompanyId, DOId).FirstOrDefault();
                var DOContractInfo = (from m in db.Opportunities
                                      where m.opportunityId == model.OppContId
                                      select new DOFormModels.DOFormList()
                                      {
                                          ContractNo = m.contractNo,
                                          ContractName = m.name
                                      }).FirstOrDefault();
                var DOBranchInfo = (from m in db.DeliveryOrders
                                    join n in db.CustBranches on m.BranchId equals n.branchId
                                    join o in db.Customers on n.custId equals o.custId
                                    where m.DOId == DOId
                                    select new DOFormModels.DOFormList()
                                    {
                                        Address = n.branchAddress,
                                        Region = n.branchRegion,
                                        ExchangeName = o.abbreviation
                                    }).FirstOrDefault();
                var DODetails = new DOFormModels()
                {                    
                    IssueDate = DODetailsObj.DOIssueDate.Value.ToString("dd/MM/yyyy"),
                    TelephoneNo = DODetailsObj.TelNo,
                    Region = DOBranchInfo.Region,
                    TrackingNo = DODetailsObj.TrackingNo,
                    ContractNo = DOContractInfo.ContractNo,
                    PONo = DODetailsObj.PONo,
                    ContractName = DOContractInfo.ContractName,
                    ContactPerson = DODetailsObj.ContactPerson,
                    DONo = DODetailsObj.DONo,
                    DOFormListObj = (from m in db.PurchaseOrders
                                     join n in db.PO_Item on m.POId equals n.POId
                                     join o in db.DO_Items on n.itemsId equals o.itemsId
                                     join p in db.DeliveryOrders on o.DOId equals p.DOId
                                     join q in db.PartNo_Items on n.partNo equals q.partNo
                                     where p.DOId == DOId
                                     select new DOFormModels.DOFormList()
                                     {
                                         MaterialNo = q.materialNo,
                                         Description = q.description,
                                         DemandQty = n.deliveredQuantity + n.outstandingQuantity,
                                         DeliveryQty = o.deliveryQuantity,
                                         DeliveredQty = o.deliveredQuantity,
                                         OutstandingQty = o.outstandingQuantity
                                     }).ToList()
                };
                if (DOBranchInfo.Address.Substring(0, 1) == "-")
                {
                    DODetails.ToCust = DOBranchInfo.Address;
                }
                else if (DOBranchInfo.Address.Substring(0, 2) == "TM")
                {
                    DODetails.ToCust = DOBranchInfo.Address;
                }
                else {
                    DODetails.ToCust = DOBranchInfo.ExchangeName + ", " + DOBranchInfo.Address + ", " + DOBranchInfo.Region;
                };

                return new RazorPDF.PdfActionResult("DOForm", DODetails);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
    }
}
