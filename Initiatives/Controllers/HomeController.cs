﻿using Initiatives.Models;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
//using DotNetOpenAuth.AspNet;
//using Microsoft.Web.WebPages.OAuth;
using System.Text;

namespace Initiatives.Controllers
{
    public static class MvcHelpers
    {
        public static string RenderPartialView(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
    public class JsonExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.ExceptionHandled = true;
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        // obviously here you could include whatever information you want about the exception
                        // for example if you have some custom exceptions you could test
                        // the type of the actual exception and extract additional data
                        // For the sake of simplicity let's suppose that we want to
                        // send only the exception message to the client
                        errorMessage = filterContext.Exception.Message
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
    public class HomeController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();
        public ActionResult Index()
        {
            return View();
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        [HttpPost]
        public ActionResult Index(MainModel model)
        {
            try
            {
                //SHA256 sha256 = SHA256Managed.Create();
                //byte[] bytes = Encoding.UTF8.GetBytes(model.LoginObject.Password);
                //byte[] hash = sha256.ComputeHash(bytes);
                string hashed = BCrypt.HashPassword(model.LoginObject.Password, BCrypt.GenerateSalt(12));
                bool matches = BCrypt.CheckPassword(model.LoginObject.Password, hashed);

                var CheckUserId = (from m in db.Users
                               join n in db.Users_Roles on m.userId equals n.userId
                               join o in db.Roles on n.roleId equals o.roleId
                               where m.userName == model.UserName 
                               && m.password == model.LoginObject.Password
                                   select new MainModel()
                               {
                                   UserId = m.userId,
                                   CompanyId = m.companyId,
                                   RoleId = n.roleId,
                                   Role = o.name,
                                   Status = m.status,
                                   FullName = m.firstName + " " + m.lastName
                               }).FirstOrDefault();

                if (CheckUserId != null && CheckUserId.Status)
                {
                    Session["UserId"] = CheckUserId.UserId;
                    Session["Username"] = model.UserName;
                    Session["Role"] = CheckUserId.Role;
                    Session["RoleId"] = CheckUserId.RoleId.Trim();
                    Session["FullName"] = CheckUserId.FullName;
                    Session["CompanyId"] = CheckUserId.CompanyId;
                    FormsAuthentication.SetAuthCookie(model.UserName, true);
                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("Dashboard", "Home");

                    return Json(new { success = true, url = redirectUrl });
                }
                else if (CheckUserId != null && !CheckUserId.Status)
                {
                    ModelState.AddModelError("UserName", "User is not registered here anymore");
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            view = this.RenderPartialView("Index", model)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    ModelState.AddModelError("UserName", "Wrong username or password. Please check again.");
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            view = this.RenderPartialView("Index", model)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int userid = Int32.Parse(Session["UserId"].ToString());

                User objUserDetaildb = db.Users.First(m => m.userId == userid);
                objUserDetaildb.loginDate = DateTime.Now;
                db.SaveChanges();

                MainModel x = new MainModel();
                x.RoleId = Session["RoleId"].ToString();

                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");

                return View("Dashboard", x);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult Dashboard(MainModel x)
        {
            var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
            //int userid = Int32.Parse(Session["UserId"].ToString());
            //User objUserDetaildb = db.Users.First(m => m.userId == userid);
            //objUserDetaildb.loginLatLng = lat + "," + lng;
            //objUserDetaildb.loginAddress = address;
            //db.SaveChanges();
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", x.ActivityDetail.CustId);
                    return new JsonResult
                    {                       
                        Data = new
                        {
                            success = false,
                            view = this.RenderPartialView("Dashboard", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                Activity _objActivity = new Activity
                {
                    uuid = Guid.NewGuid(),

                };
                db.Activities.Add(_objActivity);
                db.SaveChanges();

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "create new activity",
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();

                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        view = this.RenderPartialView("Dashboard", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [Authorize]
        public ActionResult ViewProfile()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int userid = Int32.Parse(Session["UserId"].ToString());
                UserList UserDetailList = new UserList();
                UserDetailList = (from m in db.Users
                                  join n in db.Users_Roles on m.userId equals n.userId
                                  where m.userId == userid
                                  select new UserList()
                                  {
                                      UserName = m.userName,
                                      EmailAddress = m.emailAddress,
                                      FirstName = m.firstName,
                                      LastName = m.lastName,
                                      JobTitle = m.jobTitle,
                                      OneLevelSuperiorId = m.oneLevelSuperiorId,
                                      RoleId = n.roleId,
                                      Status = m.status,
                                      TelephoneNo = m.telephoneNo,
                                      Address = m.address,
                                      ExtensionNo = m.extensionNo
                                  }).FirstOrDefault();
                if (UserDetailList.RoleId.Trim() == "R01" || UserDetailList.RoleId.Trim() == "R03") { UserDetailList.ReadOnly = true; } else { UserDetailList.ReadOnly = false; }

                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var UserRole = db.Roles.Select(c => new { c.roleId, c.name }).OrderBy(c => c.name);

                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.Role = new SelectList(UserRole.AsEnumerable(), "roleId", "name");

                return PartialView(UserDetailList);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult ViewProfile(UserList UserListDetail)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("ViewProfile", UserListDetail);
                }
                int userid = Int32.Parse(Session["UserId"].ToString());
                User objUserDetaildb = db.Users.First(m => m.userId == userid);
                Users_Roles objUserRole = db.Users_Roles.First(m => m.userId == userid);
                objUserDetaildb.modifiedByUserId = userid;
                objUserDetaildb.modifiedDate = DateTime.Now;
                objUserDetaildb.userName = UserListDetail.UserName;
                objUserDetaildb.emailAddress = UserListDetail.EmailAddress;
                objUserDetaildb.firstName = UserListDetail.FirstName;
                objUserDetaildb.lastName = UserListDetail.LastName;
                objUserDetaildb.jobTitle = UserListDetail.JobTitle;
                objUserDetaildb.telephoneNo = UserListDetail.TelephoneNo;
                objUserDetaildb.address = UserListDetail.Address;
                objUserDetaildb.extensionNo = UserListDetail.ExtensionNo;
                if (UserListDetail.RoleId != null)
                {
                    objUserRole.roleId = UserListDetail.RoleId;
                }
                if (UserListDetail.OneLevelSuperiorId != null)
                {
                    objUserDetaildb.oneLevelSuperiorId = UserListDetail.OneLevelSuperiorId;
                }
                //objOppoDetaildb.createByUserId = OppoListDetail.AccManagerId;
                try
                {
                    db.SaveChanges();
                    return Redirect("Dashboard");
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    UserListDetail.SaveSuccess = false;
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }           
        }

        [Authorize]
        public ActionResult UserList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel MM = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserListObject = (from m in db.Users
                                      //join n in db.Users on m.oneLevelSuperiorId equals n.userId into o
                                      from n in db.Users.Where(x => x.userId == m.oneLevelSuperiorId).DefaultIfEmpty()
                                      from o in db.Users_Roles.Where(x => x.userId == m.userId).DefaultIfEmpty()
                                      from p in db.Roles.Where(x => x.roleId == o.roleId).DefaultIfEmpty()
                                      where m.status == true
                                      orderby m.userName ascending
                                      select new UserListModel()
                                      {
                                          userId = m.userId,
                                          UserName = m.userName,
                                          Email = m.emailAddress,
                                          FirstName = m.firstName,
                                          LastName = m.lastName,
                                          JobTitle = m.jobTitle,
                                          SuperiorName = n.firstName + " " + n.lastName,
                                          Role = p.name,
                                          TelephoneNo = m.telephoneNo,
                                          Address = m.address,
                                          ExtensionNo = m.extensionNo
                                      }).ToList()
                };

                return View("UserList", MM);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public JsonResult UserList(string uId)
        {
            Session["uId"] = uId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewUser", "Home", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult ViewUser()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int uid = Int32.Parse(Session["uId"].ToString());
                UserList UserDetailList = new UserList();
                UserDetailList = (from m in db.Users
                                  join n in db.Users_Roles on m.userId equals n.userId
                                  join o in db.Roles on n.roleId equals o.roleId
                                  where m.userId == uid
                                  select new UserList()
                                  {
                                      UserName = m.userName,
                                      EmailAddress = m.emailAddress,
                                      FirstName = m.firstName,
                                      LastName = m.lastName,
                                      JobTitle = m.jobTitle,
                                      OneLevelSuperiorId = m.oneLevelSuperiorId,
                                      RoleId = n.roleId,
                                      Role = o.name,
                                      Status = m.status,
                                      TelephoneNo = m.telephoneNo,
                                      Address = m.address,
                                      ExtensionNo = m.extensionNo
                                  }).FirstOrDefault();
                //UserDetailList.RoleId = Session["RoleId"].ToString();
                if (Session["RoleId"].ToString().Trim() == "R02" || Session["RoleId"].ToString().Trim() == "R03" || Session["RoleId"].ToString().Trim() == "R04")
                {
                    UserDetailList.ReadOnly = false;
                }
                else { UserDetailList.ReadOnly = true; }

                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var UserRole = db.Roles.Select(c => new { c.roleId, c.name }).OrderBy(c => c.name);

                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.Role = new SelectList(UserRole.AsEnumerable(), "roleId", "name");
                UserDetailList.UserLoginRole = Session["RoleId"].ToString().Trim();

                return PartialView(UserDetailList);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult ViewUser(UserList UserListDetail)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("Viewuser", UserListDetail);
                }
                int userid = Int32.Parse(Session["UserId"].ToString());
                int uid = Int32.Parse(Session["uId"].ToString());
                User objUserDetaildb = db.Users.First(m => m.userId == uid);
                Users_Roles objUserRole = db.Users_Roles.First(m => m.userId == uid);
                objUserDetaildb.modifiedByUserId = userid;
                objUserDetaildb.modifiedDate = DateTime.Now;
                objUserDetaildb.userName = UserListDetail.UserName;
                objUserDetaildb.emailAddress = UserListDetail.EmailAddress;
                objUserDetaildb.firstName = UserListDetail.FirstName;
                objUserDetaildb.lastName = UserListDetail.LastName;
                objUserDetaildb.jobTitle = UserListDetail.JobTitle;
                objUserDetaildb.oneLevelSuperiorId = UserListDetail.OneLevelSuperiorId;
                objUserDetaildb.telephoneNo = UserListDetail.TelephoneNo;
                objUserDetaildb.address = UserListDetail.Address;
                objUserDetaildb.extensionNo = UserListDetail.ExtensionNo;
                objUserDetaildb.status = UserListDetail.Status;
                objUserRole.roleId = UserListDetail.RoleId;
                //objOppoDetaildb.createByUserId = OppoListDetail.AccManagerId;
                try
                {
                    db.SaveChanges();
                    return Redirect("UserList");
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    UserListDetail.SaveSuccess = false;
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }            
        }

        [Authorize]
        public ActionResult NewUser()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var UserRole = db.Roles
                    .Select(c => new { c.roleId, c.name })
                    .OrderBy(c => c.name);
                ViewBag.Managerlist = new SelectList(UserQuery, "AccmanagerId", "AccManagerName");
                ViewBag.Role = new SelectList(UserRole.AsEnumerable(), "roleId", "name");

                return PartialView();
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult NewUser(UserList UserDetailList)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("NewUser", UserDetailList);
                }
                User _objCreateUser = new User
                {
                    uuid = Guid.NewGuid(),
                    createDate = DateTime.Now,
                    userName = UserDetailList.UserName,
                    password = "welcome123$",
                    createByUserId = Int32.Parse(Session["UserId"].ToString()),
                    companyId = 81,
                    emailAddress = UserDetailList.EmailAddress,
                    firstName = UserDetailList.FirstName,
                    lastName = UserDetailList.LastName,
                    jobTitle = UserDetailList.JobTitle,
                    oneLevelSuperiorId = UserDetailList.OneLevelSuperiorId,
                    telephoneNo = UserDetailList.TelephoneNo,
                    address = UserDetailList.Address,
                    extensionNo = UserDetailList.ExtensionNo,
                    passwordReset = false,
                    lockout = false,
                    status = true
                };
                db.Users.Add(_objCreateUser);
                db.SaveChanges();

                Users_Roles _objCreateUserRole = new Users_Roles
                {
                    uuid = Guid.NewGuid(),
                    userId = _objCreateUser.userId,
                    roleId = UserDetailList.RoleId
                };
                db.Users_Roles.Add(_objCreateUserRole);
                db.SaveChanges();

                //return Redirect("Main");
                return Redirect("UserList");
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }            
        }

        [Authorize]
        public ActionResult CustomerList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel MM = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    CustomerListObject = db.Customers.Select(m => new CustomerListModel()
                    {
                        custId = m.custId,
                        Name = m.name,
                        Abbreviation = m.abbreviation,
                        CustRegNo = m.custRegNo,
                        Type = m.type,
                        HomeURL = m.homeURL,
                        Address = m.address,
                        TelephoneNo = m.telephoneNo
                    }).OrderBy(c => c.Abbreviation).ToList()
                };

                return View("CustomerList", MM);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public JsonResult CustomerList(string cId)
        {
            Session["cId"] = cId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewCustomer", "Home", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult ViewCustomer()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int customerid = Int32.Parse(Session["cId"].ToString());
                CustomerList CustDetailList = new CustomerList();
                CustDetailList = (from m in db.Customers
                                  where m.custId == customerid
                                  select new CustomerList()
                                  {
                                      Name = m.name,
                                      Abbreviation = m.abbreviation,
                                      RegistrationNo = m.custRegNo,
                                      CustomerType = m.type,
                                      CustomerURL = m.homeURL,
                                      Address = m.address,
                                      TelephoneNo = m.telephoneNo
                                  }).FirstOrDefault();
                string RoleId = Session["RoleId"].ToString();
                if (RoleId == "R02" || RoleId == "R03" || RoleId == "R04") {
                    CustDetailList.ReadOnly = false; 
                }
                else { CustDetailList.ReadOnly = true; }

                return PartialView(CustDetailList);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult ViewCustomer(CustomerList CustListDetail)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("Viewuser", CustListDetail);
                }
                int userid = Int32.Parse(Session["UserId"].ToString());
                int cid = Int32.Parse(Session["cId"].ToString());
                Customer objCustDetaildb = db.Customers.First(m => m.custId == cid);
                objCustDetaildb.modifiedByUserId = userid;
                objCustDetaildb.modifiedDate = DateTime.Now;
                objCustDetaildb.name = CustListDetail.Name;
                objCustDetaildb.abbreviation = CustListDetail.Abbreviation;
                objCustDetaildb.custRegNo = CustListDetail.RegistrationNo;
                objCustDetaildb.type = CustListDetail.CustomerType;
                objCustDetaildb.homeURL = CustListDetail.CustomerURL;
                objCustDetaildb.address = CustListDetail.Address;
                objCustDetaildb.telephoneNo = CustListDetail.TelephoneNo;

                try
                {
                    db.SaveChanges();
                    return Redirect("CustomerList");
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    CustListDetail.SaveSuccess = false;
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }           
        }

        [Authorize]
        public ActionResult NewCustomer()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                return PartialView();
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult NewCustomer(CustomerList CustDetailList)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("NewCustomer", CustDetailList);
                }
                Customer _objCreateCustomer = new Customer
                {
                    uuid = Guid.NewGuid(),
                    createDate = DateTime.Now,
                    createByUserId = Int32.Parse(Session["UserId"].ToString()),
                    name = CustDetailList.Name,
                    abbreviation = CustDetailList.Abbreviation,
                    custRegNo = CustDetailList.RegistrationNo,
                    type = CustDetailList.CustomerType,
                    homeURL = CustDetailList.CustomerURL,
                    address = CustDetailList.Address,
                    telephoneNo = CustDetailList.TelephoneNo
                };
                db.Customers.Add(_objCreateCustomer);
                db.SaveChanges();

                return Redirect("CustomerList");
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }           
        }

        [Authorize]
        public ActionResult PartNoList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel MM = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    PartNoListObject = db.PartNo_Items.Select(m => new PartNoListModel()
                    {
                        partNo = m.partNo,
                        MaterialNo = m.materialNo,
                        Description = m.description,
                        UoM = m.UoM
                    }).OrderBy(c => c.MaterialNo).ToList()
                };

                return View("PartNoList", MM);
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [Authorize]
        public ActionResult ViewPartNo()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ViewPartNo(PartNoList PartNoDetailList)
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult NewPartNo()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult NewPartNo(PartNoList PartNoDetailList)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return PartialView("NewPartNo", PartNoDetailList);
                }
                PartNo_Items _objCreatePartNo = new PartNo_Items
                {
                    uuid = Guid.NewGuid(),
                    createDate = DateTime.Now,
                    createByUserId = Int32.Parse(Session["UserId"].ToString()),
                    materialNo = PartNoDetailList.MaterialNo,
                    description = PartNoDetailList.Description,
                    UoM = PartNoDetailList.UoM,
                    inQuantity = 0,
                    outQuantity = 0,
                    availableBalance = 0,
                    balanceReserved = 0
                };
                db.PartNo_Items.Add(_objCreatePartNo);
                db.SaveChanges();

                return Redirect("PartNoList");
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [Authorize]
        public ActionResult NewPassword()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                return PartialView();
            }
            else
            {
                return Redirect(string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
            }
        }

        [HttpPost]
        public ActionResult NewPassword(UserList UserList)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("NewPassword", UserList);
            }
            Customer _objCreateCustomer = new Customer
            {
                uuid = Guid.NewGuid(),
                createDate = DateTime.Now,
                createByUserId = Int32.Parse(Session["UserId"].ToString())
            };
            //_objCreateCustomer.name = CustDetailList.Name;
            //_objCreateCustomer.abbreviation = CustDetailList.Abbreviation;
            //_objCreateCustomer.custRegNo = CustDetailList.RegistrationNo;
            //_objCreateCustomer.type = CustDetailList.CustomerType;
            //_objCreateCustomer.homeURL = CustDetailList.CustomerURL;
            //_objCreateCustomer.address = CustDetailList.Address;
            //_objCreateCustomer.telephoneNo = CustDetailList.TelephoneNo;
            db.Customers.Add(_objCreateCustomer);
            db.SaveChanges();

            return Redirect("Dashboard");
        }
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect("Index");
        }
    }
}
