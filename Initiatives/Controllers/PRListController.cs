﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Net.Mail;
using System.Data;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;

namespace Initiatives.Controllers
{
    public class PRListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();

        #region Create PR
        [HttpPost]
        public JsonResult NewPR(string prid)
        {
            Session["PRId"] = prid;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("CreatePR", "PRList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult CreatePR(string id)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (id != null && User.Identity.IsAuthenticated)
                {
                    Session["PRId"] = id;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int contractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.OppContId = contractId;
                model.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, model.OppContId, model.CompanyId, "PR");
                model.PRItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.PartNo_Items on n.partNo equals o.partNo
                                          join p in db.PR_Items on n.itemsId equals p.itemsId into q
                                          from r in q.DefaultIfEmpty()
                                          where r.PRId == 0
                                          select new PRItemsTable()
                                          {
                                              ItemsId = n.itemsId,
                                              MaterialNo = o.materialNo,
                                              Description = o.description,
                                              //RequiredDate = q.requiredDate,
                                              DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                              //RequestedQuantity = q.requestedQuantity,
                                              UnitPrice = n.unitPrice
                                              //TotalAmount = n.totalPrice
                                          }).ToList();

                //model.PRDetailObject = (from m in db.PurchaseRequisitions
                //                        where m.PRId == 0
                //                        select new PRListTable()
                //                        {
                //                            TotalPrice = 0,
                //                            BudgetedAmount = 0,
                //                            UtilizedToDate = 0,
                //                            AmountRequired = 0,
                //                            BudgetBalance = 0
                //                        }).FirstOrDefault();
                var MaterialNoQuery = model.PRItemListObject.ToList()
                    .Select(c => new SelectListItem { Text = c.MaterialNo, Value = c.ItemsId.ToString() }).ToList();
                ViewBag.MaterialNoList = MaterialNoQuery;
                var CustomersQuery = db.Customers.Select(c => new { VendorId = c.custId, ReqId = c.custId, custName = c.name + " (" + c.abbreviation + ")" })
                    .OrderBy(c => c.custName);
                ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName");
                ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName");
                var UserQuery = db.Users.Select(c => new { ReqId = c.userId, FullName = c.firstName + " " + c.lastName }).OrderBy(c => c.FullName);
                ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName");

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult GetMaterialNoPR(MainModel MM)
        {
            List<PRItemsTable> newPRITemList = new List<PRItemsTable>();
            foreach (var value in MM.POListObject)
            {
                newPRITemList = newPRITemList.Concat(from m in db.PurchaseOrders
                                                     join n in db.PO_Item on m.POId equals n.POId
                                                     join o in db.PartNo_Items on n.partNo equals o.partNo
                                                     join p in db.PR_Items on n.itemsId equals p.itemsId into q                                                     
                                                     from r in q.DefaultIfEmpty()
                                                     where m.POId == value.POId && n.outstandingQuantity > 0
                                                     select new PRItemsTable()
                                                     {
                                                         POId = m.POId,
                                                         ItemsId = n.itemsId,
                                                         PONo = m.Details_PONo,
                                                         MaterialNo = o.materialNo,
                                                         Description = o.description,
                                                         DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                                         //RequestedQuantity = n.requestedQuantity.Value,
                                                         RequiredDate = DateTime.Now,
                                                         OutstandingQuantity = r.PRId == null ? n.outstandingQuantity : r.outstandingQuantity,
                                                         //UnitPrice = n.unitPrice
                                                         //TotalAmount = n.totalPrice
                                                     }).ToList();
            }
            MM.PRItemListObject = newPRITemList;

            return PartialView("PRItemsTable", MM);
        }

        [HttpPost]
        public ActionResult CreatePR(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (Session["ContractId"] == null)
                {
                    Session["ContractId"] = x.OppContId;
                }
                //if (x.file == null)
                //{
                //    ModelState.AddModelError("file", "Please input the attachment before proceed.");
                //}
                var CustomersQuery = db.Customers.Select(c => new { VendorId = c.custId, ReqId = c.custId, custName = c.name + " (" + c.abbreviation + ")" })
                        .OrderBy(c => c.custName);
                var UserQuery = db.Users.Select(c => new { ReqId = c.userId, FullName = c.firstName + " " + c.lastName }).OrderBy(c => c.FullName);

                bool haveQuantity = false; string ErrorMessage = "";
                foreach (var value in x.PRItemListObject)
                {
                    if (value.RequestedQuantity < 1)
                    {
                        ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                        ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                        ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName", x.PRDetailObject.ReqId);
                        haveQuantity = true;
                        ErrorMessage = "Create PR failed! Reason: Request Quantity cannot be 0 or less in PR Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                havePRNo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreatePR", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    if (value.RequestedQuantity > value.OutstandingQuantity)
                    {
                        ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                        ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                        ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName", x.PRDetailObject.ReqId);
                        haveQuantity = true;
                        ErrorMessage = "Create PR failed! Reason: Request Quantity cannot exceed Outstanding Quantity in PR Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                havePRNo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreatePR", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                }
                if (!ModelState.IsValid)
                {
                    int RequestedQuantity = 0;
                    foreach (var value in ModelState)
                    {
                        if (value.Key.Contains("RequestedQuantity"))
                        {
                            RequestedQuantity = Int32.Parse(value.Value.Value.AttemptedValue);
                            if (RequestedQuantity < 1)
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create PR failed! Reason: Request Quantity cannot be 0 or less in DO Item";
                            }
                        }
                        if (value.Key.Contains("RequestedQuantity"))
                        {
                            if (RequestedQuantity > Int32.Parse(value.Value.Value.AttemptedValue))
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create PR failed! Reason: Request Quantity cannot exceed Outstanding Quantity in IO Item";
                            }
                        }
                    }

                    ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                    ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                    ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName", x.PRDetailObject.ReqId);

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            havePRNo = false,
                            haveQuantity,
                            ErrorMessage,
                            exception = false,
                            view = this.RenderPartialView("CreatePR", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                var getPRNo = db.PurchaseRequisitions.SingleOrDefault(y => y.PRNo == x.PRDetailObject.PRNo);
                if (getPRNo != null)
                {
                    ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                    ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                    ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName", x.PRDetailObject.ReqId);

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            havePRNo = true,
                            haveQuantity,
                            exception = false,
                            view = this.RenderPartialView("CreatePR", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                PurchaseRequisition _objCreatePR = new PurchaseRequisition
                {
                    uuid = Guid.NewGuid(),
                    //PRId = x.PRDetailObject.PRId,
                    PRNo = x.PRDetailObject.PRNo,
                    PRIssueDate = x.PRDetailObject.PRIssueDate,
                    VendorId = x.PRDetailObject.VendorId,
                    VendorPIC = x.PRDetailObject.VendorPIC,
                    VendorContNo = x.PRDetailObject.VendorContNo,
                    VendorEmail = x.PRDetailObject.VendorEmail,
                    ReqCompId = x.PRDetailObject.ReqCompId,
                    ReqId = x.PRDetailObject.ReqId,
                    TotalPrice = x.PRDetailObject.TotalPrice,
                    PurchaseType = x.PRDetailObject.PurchaseType,
                    BudgetDescription = x.PRDetailObject.BudgetDescription,
                    BudgetedAmount = x.PRDetailObject.BudgetedAmount,
                    UtilizedToDate = x.PRDetailObject.UtilizedToDate,
                    AmountRequired = x.PRDetailObject.AmountRequired,
                    BudgetBalance = x.PRDetailObject.BudgetBalance,
                    Remarks = x.PRDetailObject.Remarks,
                    Justification = x.PRDetailObject.Justification,
                    //TotalGST = x.PRDetailObject.TotalGST,
                    PreparedById = Int32.Parse(Session["UserId"].ToString()),
                    CompleteDate = DateTime.Now,
                    Update = true
                };
                db.PurchaseRequisitions.Add(_objCreatePR);
                db.SaveChanges();

                int PRId = _objCreatePR.PRId;
                int cId = Int32.Parse(Session["ContractId"].ToString());
                int fromUserID = _objCreatePR.PreparedById;
                var myEmail = db.Users.SingleOrDefault(y => y.userId == fromUserID);

                FileUpload fileUploadModel = new FileUpload();
                if (x.file != null)
                {
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = x.OppContId;
                    fileUploadModel.PRId = _objCreatePR.PRId;
                    fileUploadModel.uploadUserId = Int32.Parse(Session["UserId"].ToString());
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.PRDetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        PRId = PRId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                    db.SaveChanges();
                }

                foreach (var value in x.PRItemListObject)
                {
                    PR_Items _objPRItem = new PR_Items
                    {
                        uuid = Guid.NewGuid(),
                        PRId = _objCreatePR.PRId,
                        itemsId = value.ItemsId,
                        requiredDate = value.RequiredDate,
                        remark = value.Remark,
                        requestedQuantity = value.RequestedQuantity,
                        outstandingQuantity = value.OutstandingQuantity - value.RequestedQuantity,
                        totalPrice = value.TotalAmount,
                        //gst = (value.RequestedQuantity * value.UnitPrice) * 6 / 100,
                        amount = value.TotalAmount/* + ((value.RequestedQuantity * value.UnitPrice) * 6 / 100)*/
                    };
                    db.PR_Items.Add(_objPRItem);
                    db.SaveChanges();
                }

                foreach (var value in x.POListObject)
                {
                    Notification _objSendLog = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        POId = value.POId,
                        PRId = _objCreatePR.PRId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        flag = 0
                    };

                    var _objCreatePRItem = (from m in db.PurchaseOrders
                                            join n in db.PO_Item on m.POId equals n.POId
                                            join o in db.PR_Items on n.itemsId equals o.itemsId
                                            join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                            where m.POId == value.POId && p.PRId == PRId
                                            select new POItemsTable()
                                            {
                                                PONo = m.Details_PONo,
                                                PRNo = p.PRNo
                                            }).FirstOrDefault();

                    _objSendLog.content = "create purchase requisition no. " + _objCreatePRItem.PRNo + " for PO No. " + _objCreatePRItem.PONo;
                    db.Notifications.Add(_objSendLog);

                    int TotalOutstandingQuantity = 0;
                    var _objCreatePRItemList = (from m in db.PurchaseOrders
                                                join n in db.PO_Item on m.POId equals n.POId
                                                join o in db.PR_Items on n.itemsId equals o.itemsId
                                                join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                                where m.POId == value.POId
                                                select new POItemsTable()
                                                {
                                                    OutstandingQuantity = o.outstandingQuantity,
                                                    RequestedQuantity = o.requestedQuantity
                                                }).ToList();

                    foreach (var values in _objCreatePRItemList)
                    {
                        TotalOutstandingQuantity = TotalOutstandingQuantity + values.OutstandingQuantity;
                    }
                    PurchaseOrder _objPO = db.PurchaseOrders.First(m => m.POId == value.POId);
                    if (TotalOutstandingQuantity == 0)
                    {
                        _objPO.PRInfo_Update = true;
                    }
                    if (x.file != null)
                    {
                        POs_FileUpload fileUploadModel2 = new POs_FileUpload
                        {
                            uuid = Guid.NewGuid(),
                            fileEntryId = fileUploadModel.fileEntryId,
                            POId = value.POId
                        };
                        db.POs_FileUpload.Add(fileUploadModel2);
                    }
                    db.SaveChanges();
                }

                try
                {
                    List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
                    NotiMemberList = DBQueryController.getNotiMemberList(cId);

                    Notification _objSendPRMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        PRId = PRId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "Send email(s) updates for new Purchase Requisition",
                        QNAtype = "Message",
                        flag = 0
                    };
                    db.Notifications.Add(_objSendPRMessage);
                    db.SaveChanges();

                    foreach (var model in NotiMemberList)
                    {
                        var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
                        var PRInfo = (from m in db.PurchaseRequisitions
                                      join n in db.PR_Items on m.PRId equals n.PRId
                                      join o in db.PO_Item on n.itemsId equals o.itemsId
                                      join p in db.PurchaseOrders on o.POId equals p.POId
                                      join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                      join r in db.Users on m.PreparedById equals r.userId
                                      join s in db.Customers on q.custId equals s.custId
                                      where m.PRId == PRId
                                      select new MailModel()
                                      {
                                          CustName = s.name + " (" + s.abbreviation + ")",
                                          ContractName = q.name,
                                          Description = q.description
                                      }).FirstOrDefault();
                        x.RoleId = Session["RoleId"].ToString();
                        x.UserId = Int32.Parse(Session["UserId"].ToString());
                        x.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                        x.PRDetailObject = DBQueryController.getPRList("PRDetails", x.UserId, "R02", x.OppContId, x.CompanyId, PRId).FirstOrDefault();

                        string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/PREmailTemplate.cshtml"));
                        PRInfo.Content = myEmail.userName + " has register new Purchase Requisition. Please refer the details as below: ";
                        PRInfo.FromName = myEmail.emailAddress;
                        PRInfo.PRFlow = "PR Details";
                        PRInfo.PRNo = x.PRDetailObject.PRNo;
                        PRInfo.PONo = x.PRDetailObject.PONo;
                        PRInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/PRList/ViewPR") + "?cId=" + cId + "&PRId=" + PRId);
                        var result =
                                Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PRDetailsTemplateKey", null, PRInfo);

                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress("info@kubtel.com")
                        };
                        mail.To.Add(new MailAddress(UserEmail.emailAddress));
                        mail.Subject = "OCM: Updates on new " + PRInfo.PRNo + " for PO Number " + PRInfo.PONo;
                        mail.Body = result;
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient
                        {
                            Host = "outlook.office365.com",
                            Port = 587,
                            EnableSsl = true,
                            Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                        };
                        smtp.Send(mail);

                        NotiGroup _objSendToUserId = new NotiGroup
                        {
                            uuid = Guid.NewGuid(),
                            chatId = _objSendPRMessage.chatId,
                            toUserId = model.Id
                        };
                        db.NotiGroups.Add(_objSendToUserId);
                        db.SaveChanges();
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }

                x.OppContId = cId;
                ViewBag.VendorCompany = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                ViewBag.RequestorCompany = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                ViewBag.RequestorName = new SelectList(UserQuery, "ReqId", "FullName", x.PRDetailObject.ReqId);

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        havePRNo = false,
                        exception = false,
                        view = this.RenderPartialView("CreatePR", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region PR Tabs
        [HttpPost]
        public JsonResult PRTabs(string cId, string PRId)
        {
            if (cId != null)
            {
                Session["ContractId"] = cId;
            }
            Session["PRId"] = PRId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewPR", "PRList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }
        public ActionResult ViewPR(string cId, string PRId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (cId != null && PRId != null && User.Identity.IsAuthenticated)
                {
                    Session["ContractId"] = cId;
                    Session["PRId"] = PRId;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int PRID = Int32.Parse(Session["PRId"].ToString()); int ContractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.PRId = PRID;
                model.OppContId = ContractId;
                model.UserName = Session["Username"].ToString();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult PRDetails(int PRId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel PRDetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    PRId = Int32.Parse(Session["PRId"].ToString()),
                    PRDetailObject = (from m in db.PurchaseRequisitions
                                      join n in db.Users on m.PreparedById equals n.userId into s
                                      //join o in db.POStatus on m.StatusId equals o.StatusId
                                      from x in s.DefaultIfEmpty()
                                      where m.PRId == PRId
                                      select new PRListTable()
                                      {
                                          PRNo = m.PRNo,
                                          PRIssueDate = m.PRIssueDate,
                                          VendorId = m.VendorId,
                                          VendorPIC = m.VendorPIC,
                                          VendorContNo = m.VendorContNo,
                                          VendorEmail = m.VendorEmail,
                                          ReqCompId = m.ReqCompId,
                                          ReqId = m.ReqId,
                                          TotalPrice = m.TotalPrice,
                                          PurchaseType = m.PurchaseType,
                                          BudgetDescription = m.BudgetDescription,
                                          BudgetedAmount = m.BudgetedAmount,
                                          UtilizedToDate = m.UtilizedToDate,
                                          AmountRequired = m.AmountRequired,
                                          BudgetBalance = m.BudgetBalance,
                                          Remarks = m.Remarks,
                                          Justification = m.Justification,
                                          PRCompleteDate = m.CompleteDate,
                                          PRUpdate = m.Update
                                      }).FirstOrDefault(),
                    PRItemListObject = (from m in db.PurchaseOrders
                                        join n in db.PO_Item on m.POId equals n.POId
                                        join o in db.PR_Items on n.itemsId equals o.itemsId
                                        join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                        join q in db.PartNo_Items on n.partNo equals q.partNo
                                        where p.PRId == PRId
                                        select new PRItemsTable()
                                        {
                                            PONo = m.Details_PONo,
                                            MaterialNo = q.materialNo,
                                            Description = q.description,
                                            Remark = o.remark,
                                            RequiredDate = o.requiredDate,
                                            DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                            RequestedQuantity = o.requestedQuantity,
                                            OutstandingQuantity = o.outstandingQuantity,
                                            TotalAmount = o.totalPrice
                                        }).ToList()
                };
                var CustomersQuery = db.Customers.Select(c => new { VendorId = c.custId, ReqId = c.custId, custName = c.name + " (" + c.abbreviation + ")" })
                    .OrderBy(c => c.custName);
                var UserQuery = db.Users.Select(c => new { c.userId, FullName = c.firstName + " " + c.lastName }).OrderBy(c => c.FullName);
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                ViewBag.Vendor = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", PRDetailList.PRDetailObject.VendorId);
                ViewBag.ReqComp = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", PRDetailList.PRDetailObject.ReqId);
                ViewBag.RequestorName = new SelectList(UserQuery, "userId", "FullName", PRDetailList.PRDetailObject.ReqId);
                Session["Details_Update"] = PRDetailList.PRDetailObject.PRUpdate;
                PRDetailList.RoleId = Session["RoleId"].ToString();
                PRDetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PRDetailList.OppContId && m.userId == PRDetailList.UserId
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(PRDetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult PRDetails(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (bool.Parse(Session["Details_Update"].ToString()) == false && x.file == null)
                {
                    ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
                }

                var CustomersQuery = db.Customers.Select(c => new { VendorId = c.custId, ReqId = c.custId, custName = c.name + " (" + c.abbreviation + ")" })
                        .OrderBy(c => c.custName);
                var UserQuery2 = db.Users.Select(c => new { ReqId = c.userId, FullName = c.firstName + " " + c.lastName }).OrderBy(c => c.FullName);

                if (!ModelState.IsValid)
                {
                    x.PRItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.PR_Items on n.itemsId equals o.itemsId
                                          join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.PRId == x.PRId
                                          select new PRItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              RequiredDate = o.requiredDate,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              RequestedQuantity = o.requestedQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();

                    ViewBag.Vendor = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                    ViewBag.ReqComp = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                    ViewBag.RequestorName = new SelectList(UserQuery2, "ReqId", "FullName", x.PRDetailObject.ReqId);

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            exception = false,
                            view = this.RenderPartialView("PRDetails", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                int PRId = x.PRId;
                int userid = Int32.Parse(Session["UserId"].ToString());
                PurchaseRequisition objPRDetaildb = db.PurchaseRequisitions.First(m => m.PRId == PRId);
                var objPRDetail = (from m in db.PurchaseRequisitions
                                   join n in db.Customers on m.VendorId equals n.custId
                                   where m.PRId == PRId
                                   select new PRFormModels.PRFormList()
                                   {
                                       CompanyName = n.name
                                   }).FirstOrDefault();
                var objPRDetail2 = (from m in db.PurchaseRequisitions
                                    join n in db.Customers on m.ReqCompId equals n.custId
                                    where m.PRId == PRId
                                    select new PRFormModels.PRFormList()
                                    {
                                        CompanyName = n.name
                                    }).FirstOrDefault();
                var objPRDetail3 = (from m in db.PurchaseRequisitions
                                    join n in db.Users on m.ReqId equals n.userId
                                    where m.PRId == PRId
                                    select new PRFormModels.PRFormList()
                                    {
                                        FullName = n.firstName + " " + n.lastName
                                    }).FirstOrDefault();
                var PRVendorInfo = db.Customers.First(m => m.custId == x.PRDetailObject.VendorId);
                var PRRequestInfo = db.Customers.First(m => m.custId == x.PRDetailObject.ReqCompId);
                var UserQuery = (from m in db.Users
                                 where m.userId == x.PRDetailObject.ReqId
                                 select new PRFormModels.PRFormList()
                                 {
                                     FullName = m.firstName + " " + m.lastName
                                 }).FirstOrDefault();

                if (objPRDetaildb.PRNo != x.PRDetailObject.PRNo)
                {
                    Notification _objDetails_PRNo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change PR Number from " + objPRDetaildb.PRNo + " to " + x.PRDetailObject.PRNo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_PRNo);
                    objPRDetaildb.PRNo = x.PRDetailObject.PRNo;
                }
                if (objPRDetaildb.PRIssueDate != x.PRDetailObject.PRIssueDate)
                {
                    Notification _objDetails_PRIssueDate = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change PR Issue date from " + objPRDetaildb.PRIssueDate + " to " + x.PRDetailObject.PRIssueDate,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_PRIssueDate);
                    objPRDetaildb.PRIssueDate = x.PRDetailObject.PRIssueDate;
                }
                if (objPRDetaildb.VendorId != x.PRDetailObject.VendorId)
                {
                    Notification _objDetails_VendorId = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.VendorId == null ? "change PR Vendor Company to " + PRVendorInfo.name :
                        "change PR Vendor Company from " + objPRDetail.CompanyName + " to " + PRVendorInfo.name,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_VendorId);
                    objPRDetaildb.VendorId = x.PRDetailObject.VendorId;
                }
                if (objPRDetaildb.VendorPIC != x.PRDetailObject.VendorPIC)
                {
                    Notification _objDetails_VendorPIC = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.VendorPIC == null ? "change Vendor PIC to " + x.PRDetailObject.VendorPIC :
                        "change Vendor PIC from " + objPRDetaildb.VendorPIC + " to " + x.PRDetailObject.VendorPIC,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_VendorPIC);
                    objPRDetaildb.VendorPIC = x.PRDetailObject.VendorPIC;
                }
                if (objPRDetaildb.VendorContNo != x.PRDetailObject.VendorContNo)
                {
                    Notification _objDetails_VendorContNo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.VendorContNo == null ? "change Vendor Contact No. to " + x.PRDetailObject.VendorContNo :
                        "change Vendor Contact No. from " + objPRDetaildb.VendorContNo + " to " + x.PRDetailObject.VendorContNo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_VendorContNo);
                    objPRDetaildb.VendorContNo = x.PRDetailObject.VendorContNo;
                }
                if (objPRDetaildb.VendorEmail != x.PRDetailObject.VendorEmail)
                {
                    Notification _objDetails_VendorEmail = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.VendorEmail == null ? "change Vendor Email to " + x.PRDetailObject.VendorEmail :
                        "change Vendor Email from " + objPRDetaildb.VendorEmail + " to " + x.PRDetailObject.VendorEmail,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_VendorEmail);
                    objPRDetaildb.VendorEmail = x.PRDetailObject.VendorEmail;
                }
                if (objPRDetaildb.ReqCompId != x.PRDetailObject.ReqCompId)
                {
                    Notification _objDetails_ReqCompId = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.ReqCompId == null ? "change PR Requestor Company to " + PRRequestInfo.name :
                        "change PR Requestor Company from " + objPRDetail2.CompanyName + " to " + PRRequestInfo.name,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_ReqCompId);
                    objPRDetaildb.ReqCompId = x.PRDetailObject.ReqCompId;
                }
                if (objPRDetaildb.ReqId != x.PRDetailObject.ReqId)
                {
                    Notification _objDetails_ReqId = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.ReqId == null ? "change PR Requestor Name to " + UserQuery.FullName :
                        "change PR Requestor Name from " + objPRDetail3.FullName + " to " + UserQuery.FullName,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_ReqId);
                    objPRDetaildb.ReqId = x.PRDetailObject.ReqId;
                }
                if (objPRDetaildb.TotalPrice != x.PRDetailObject.TotalPrice)
                {
                    Notification _objDetails_TotalPrice = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.TotalPrice == 0 ? "change Total Price to " + x.PRDetailObject.TotalPrice :
                        "change Total Price from " + objPRDetaildb.TotalPrice + " to " + x.PRDetailObject.TotalPrice,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_TotalPrice);
                    objPRDetaildb.TotalPrice = x.PRDetailObject.TotalPrice;
                }
                if (objPRDetaildb.PurchaseType != x.PRDetailObject.PurchaseType)
                {
                    Notification _objDetails_PurchaseType = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.PurchaseType == null ? "change Purchase Type to " + x.PRDetailObject.PurchaseType :
                        "change Purchase Type from " + objPRDetaildb.PurchaseType + " to " + x.PRDetailObject.PurchaseType,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_PurchaseType);
                    objPRDetaildb.PurchaseType = x.PRDetailObject.PurchaseType;
                }
                if (objPRDetaildb.BudgetDescription != x.PRDetailObject.BudgetDescription)
                {
                    Notification _objDetails_BudgetDescription = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.BudgetDescription == null ? "change Budget Description to " + x.PRDetailObject.BudgetDescription :
                        "change Budget Description from " + objPRDetaildb.BudgetDescription + " to " + x.PRDetailObject.BudgetDescription,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_BudgetDescription);
                    objPRDetaildb.BudgetDescription = x.PRDetailObject.BudgetDescription;
                }
                if (objPRDetaildb.BudgetedAmount != x.PRDetailObject.BudgetedAmount)
                {
                    Notification _objDetails_BudgetedAmount = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.BudgetedAmount == 0 ? "change Budgeted Amount to " + x.PRDetailObject.BudgetedAmount :
                        "change Budgeted Amount from " + objPRDetaildb.BudgetedAmount + " to " + x.PRDetailObject.BudgetedAmount,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_BudgetedAmount);
                    objPRDetaildb.BudgetedAmount = x.PRDetailObject.BudgetedAmount;
                }
                if (objPRDetaildb.UtilizedToDate != x.PRDetailObject.UtilizedToDate)
                {
                    Notification _objDetails_UtilizedToDate = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.UtilizedToDate == 0 ? "change Utilized To Date to " + x.PRDetailObject.UtilizedToDate :
                        "change Utilized To Date from " + objPRDetaildb.UtilizedToDate + " to " + x.PRDetailObject.UtilizedToDate,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_UtilizedToDate);
                    objPRDetaildb.UtilizedToDate = x.PRDetailObject.UtilizedToDate;
                }
                if (objPRDetaildb.AmountRequired != x.PRDetailObject.AmountRequired)
                {
                    Notification _objDetails_AmountRequired = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.AmountRequired == 0 ? "change Amount Required to " + x.PRDetailObject.AmountRequired :
                        "change Amount Required from " + objPRDetaildb.AmountRequired + " to " + x.PRDetailObject.AmountRequired,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_AmountRequired);
                    objPRDetaildb.AmountRequired = x.PRDetailObject.AmountRequired;
                }
                if (objPRDetaildb.BudgetBalance != x.PRDetailObject.BudgetBalance)
                {
                    Notification _objDetails_BudgetBalance = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.BudgetBalance == 0 ? "change Budget Balance to " + x.PRDetailObject.BudgetBalance :
                        "change Budget Balance from " + objPRDetaildb.BudgetBalance + " to " + x.PRDetailObject.BudgetBalance,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_BudgetBalance);
                    objPRDetaildb.BudgetBalance = x.PRDetailObject.BudgetBalance;
                }
                if (objPRDetaildb.Remarks != x.PRDetailObject.Remarks)
                {
                    Notification _objDetails_Remarks = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.Remarks == null ? "change Remarks to " + x.PRDetailObject.Remarks :
                        "change Remarks from " + objPRDetaildb.Remarks + " to " + x.PRDetailObject.Remarks,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_Remarks);
                    objPRDetaildb.Remarks = x.PRDetailObject.Remarks;
                }
                if (objPRDetaildb.Justification != x.PRDetailObject.Justification)
                {
                    Notification _objDetails_Justification = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = objPRDetaildb.Justification == null ? "change Justification to " + x.PRDetailObject.Justification :
                        "change Justification from " + objPRDetaildb.Justification + " to " + x.PRDetailObject.Justification,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_Justification);
                    objPRDetaildb.Justification = x.PRDetailObject.Justification;
                }
                if (x.file != null)
                {
                    FileUpload fileUploadModel = new FileUpload();
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                    fileUploadModel.PRId = PRId;
                    fileUploadModel.uploadUserId = userid;
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.PRDetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        PRId = Int32.Parse(Session["PRId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                }

                int ContractId = Int32.Parse(Session["ContractId"].ToString());
                x.objCanEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == ContractId && m.userId == userid
                                select new EditPermission()
                                {
                                    canEdits = m.canEdit
                                }).FirstOrDefault();

                try
                {
                    db.SaveChanges();
                    x.RoleId = Session["RoleId"].ToString();
                    //x.POTrackingTable = DBQueryController.getPOTracking(PoId);
                    x.PRItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.PR_Items on n.itemsId equals o.itemsId
                                          join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.PRId == PRId
                                          select new PRItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              RequiredDate = o.requiredDate,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              RequestedQuantity = o.requestedQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();
                   
                    ViewBag.Vendor = new SelectList(CustomersQuery.AsEnumerable(), "VendorId", "custName", x.PRDetailObject.VendorId);
                    ViewBag.ReqComp = new SelectList(CustomersQuery.AsEnumerable(), "ReqId", "custName", x.PRDetailObject.ReqId);
                    ViewBag.RequestorName = new SelectList(UserQuery2, "ReqId", "FullName", x.PRDetailObject.ReqId);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        exception = false,
                        view = this.RenderPartialView("PRDetails", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        #region DocManagement
        [Authorize]
        public ActionResult PRDocManagement(int PRId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel();
                int userid = Int32.Parse(Session["UserId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                model.PRDocListObject = (from m in db.FileUploads
                                         join n in db.Users on m.uploadUserId equals n.userId
                                         where m.PRId == PRId && m.archivedFlag == false
                                         select new FilePODetailInfo()
                                         {
                                             opportunityId = m.opportunityId,
                                             fileEntryId = m.fileEntryId,
                                             uploadUserName = n.firstName + " " + n.lastName,
                                             uploadDate = m.uploadDate,
                                             FileName = m.FileName,
                                             File = m.File,
                                             Description = m.description
                                         }).ToList();
                model.FileArchiveModel = (from m in db.FileUploads
                                          join n in db.Users on m.uploadUserId equals n.userId
                                          where m.PRId == PRId && m.archivedFlag == true
                                          select new FilePODetailInfo()
                                          {
                                              opportunityId = m.opportunityId,
                                              fileEntryId = m.fileEntryId,
                                              uploadUserName = n.firstName + " " + n.lastName,
                                              uploadDate = m.uploadDate,
                                              FileName = m.FileName,
                                              File = m.File,
                                              Description = m.description
                                          }).ToList();
                return PartialView(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public FileContentResult FileDownload(int id)
        {
            byte[] fileData;

            FileUpload fileRecord = db.FileUploads.Find(id);

            string fileName;
            string filename1 = fileRecord.FileName.Replace("\\", ",");
            string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            fileData = (byte[])fileRecord.File.ToArray();
            fileName = filename2;

            return File(fileData, "text", fileName);
        }
        public JsonResult FileArchive(string selectedId)
        {
            int id = Int32.Parse(selectedId);

            FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
            if (itemToArchive != null)
            {
                itemToArchive.archivedFlag = true;
                db.SaveChanges();
            }

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region Updates
        [Authorize]
        public ActionResult PRUpdates(int PRId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int Uid = Int32.Parse(Session["UserId"].ToString());
                int cId = Int32.Parse(Session["ContractId"].ToString());
                MainModel QNAModelList = new MainModel
                {
                    MessageListModel = DBQueryController.getMessageList(cId, 0, 0, PRId, 0, Uid),
                    UserId = Uid
                };

                var NotiMemberList = (from m in db.Users
                                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                      from x in gy.DefaultIfEmpty()
                                      where m.status == true && x.opportunityId == cId
                                      select new NotiMemberList()
                                      {
                                          Id = m.userId,
                                          Name = m.firstName + " " + m.lastName
                                      }).ToList().OrderBy(c => c.Name);

                ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                return PartialView(QNAModelList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public virtual ActionResult PRDocUpload(MainModel fd)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                FileUpload fileUploadModel = new FileUpload();
                int userId = fd.UserId;
                DateTime createDate = DateTime.Now;
                string filename1 = fd.file.FileName.Replace("\\", ",");
                string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                string fullPath = fd.file.FileName;
                string extension = Path.GetExtension(fullPath);
                byte[] uploadFile = new byte[fd.file.InputStream.Length];

                fileUploadModel.uuid = Guid.NewGuid();
                fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                fileUploadModel.PRId = fd.PRId;
                fileUploadModel.uploadUserId = userId;
                fileUploadModel.uploadDate = createDate;
                fileUploadModel.FullPath = fullPath;
                fileUploadModel.FileName = filename;
                fileUploadModel.description = fd.FileDescription;
                fileUploadModel.Extension = extension;
                fileUploadModel.archivedFlag = false;
                fileUploadModel.notPO = false;
                fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                fileUploadModel.File = uploadFile;
                db.FileUploads.Add(fileUploadModel);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    PRId = Int32.Parse(Session["PRId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "uploaded " + filename,
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);

                db.SaveChanges();

                //return PartialView(db.FileUploads.ToList());
                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult PRSendMessage(string UserId, int PRId, string Content, List<int> selectedUserId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int fromUserID = Int32.Parse(UserId);
                int cId = Int32.Parse(Session["ContractId"].ToString());
                var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = cId,
                    PRId = PRId,
                    createDate = DateTime.Now,
                    fromUserId = fromUserID,
                    content = Content,
                    QNAtype = "Message",
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();

                foreach (var model in selectedUserId)
                {
                    var UserEmail = db.Users.SingleOrDefault(z => z.userId == model);
                    var PRInfo = (from m in db.PurchaseRequisitions
                                  join n in db.PR_Items on m.PRId equals n.PRId
                                  join o in db.PO_Item on n.itemsId equals o.itemsId
                                  join p in db.PurchaseOrders on o.POId equals p.POId
                                  join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                  join r in db.Users on m.PreparedById equals r.userId
                                  join s in db.Customers on q.custId equals s.custId
                                  where m.PRId == PRId
                                  select new MailModel()
                                  {
                                      CustName = s.name + " (" + s.abbreviation + ")",
                                      CustAbbreviation = s.abbreviation,
                                      ContractName = q.name,
                                      Description = q.description,
                                      PONo = p.Details_PONo,
                                      PRNo = m.PRNo
                                  }).FirstOrDefault();

                    string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/PREmailTemplate.cshtml"));
                    PRInfo.Content = Content;
                    PRInfo.FromName = myEmail.emailAddress;
                    PRInfo.PRFlow = "PR Details";
                    PRInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority,
                        Url.Content("~/PRList/ViewPR") + "?cId=" + cId + "&PRId=" + PRId + "#PRUpdates-1");
                    var result =
                            Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PRTemplateKey", null, PRId);

                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress("info@kubtel.com")
                    };
                    //mail.From = new MailAddress("info@kubtel.com");
                    mail.To.Add(new MailAddress(UserEmail.emailAddress));
                    //mail.To.Add(new MailAddress("info@kubtel.com"));
                    mail.Subject = "OCM: Updates on " + PRInfo.ContractName + " for " + PRInfo.CustAbbreviation;
                    mail.Body = result;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient
                    {
                        //smtp.Host = "pops.kub.com";
                        Host = "outlook.office365.com",
                        //smtp.Host = "smtp.gmail.com";
                        Port = 587,
                        //smtp.Port = 25;
                        EnableSsl = true,
                        Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                    };
                    //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
                    smtp.Send(mail);

                    NotiGroup _objSendToUserId = new NotiGroup
                    {
                        uuid = Guid.NewGuid(),
                        chatId = _objSendMessage.chatId,
                        toUserId = model
                    };
                    db.NotiGroups.Add(_objSendToUserId);
                }
                db.SaveChanges();

                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion
        #endregion

        public ActionResult PRForm()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int PRId = Int32.Parse(Session["PRId"].ToString());
                MainModel model = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                var PRDetailsObj = DBQueryController.getPRList("PRDetails", model.UserId, "R02", model.OppContId, model.CompanyId, PRId).FirstOrDefault();
                var PRVendorInfo = (from m in db.PurchaseRequisitions
                                    join n in db.Customers on m.VendorId equals n.custId
                                    where m.PRId == PRId
                                    select new PRFormModels.PRFormList()
                                    {
                                        CompanyName = n.name
                                    }).FirstOrDefault();
                var PRRequestInfo = (from m in db.PurchaseRequisitions
                                     join n in db.Customers on m.ReqCompId equals n.custId
                                     where m.PRId == PRId
                                     select new PRFormModels.PRFormList()
                                     {
                                         CompanyName = n.name
                                     }).FirstOrDefault();
                var PRUserInfo = (from m in db.PurchaseRequisitions
                                  join n in db.Users on m.ReqId equals n.userId
                                  join o in db.DepartmentDivisons on n.depDivId equals o.depDivId
                                  where m.PRId == PRId
                                  select new PRFormModels.PRFormList()
                                  {
                                      FullName = n.firstName + " " + n.lastName,
                                      Designation = n.jobTitle,
                                      DivDept = o.depDivName
                                  }).FirstOrDefault();
                var PRDetails = new PRFormModels()
                {
                    PRNo = PRDetailsObj.PRNo,
                    PRIssueDate = PRDetailsObj.PRIssueDate.Value.ToString("dd/MM/yyyy"),
                    PurchaseType = PRDetailsObj.PurchaseType,
                    Justification = PRDetailsObj.Justification,
                    Vendor = PRVendorInfo.CompanyName,
                    ContactPIC = PRDetailsObj.VendorPIC,
                    ContactNo = PRDetailsObj.VendorContNo,
                    Email = PRDetailsObj.VendorEmail,
                    ReqCompany = PRRequestInfo.CompanyName,
                    ReqName = PRUserInfo.FullName,
                    Designation = PRUserInfo.Designation,
                    DivDept = PRUserInfo.DivDept,
                    TotalPrice = PRDetailsObj.TotalPrice.ToString(),
                    BudgetDescription = PRDetailsObj.BudgetDescription,
                    BudgetedAmount = PRDetailsObj.BudgetedAmount.ToString(),
                    UtilizedToDate = PRDetailsObj.UtilizedToDate.ToString(),
                    AmountRequired = PRDetailsObj.AmountRequired.ToString(),
                    BudgetBalance = PRDetailsObj.BudgetBalance.ToString(),
                    Remarks = PRDetailsObj.Remarks,

                    PRFormListObj = (from m in db.PurchaseOrders
                                     join n in db.PO_Item on m.POId equals n.POId
                                     join o in db.PR_Items on n.itemsId equals o.itemsId
                                     join p in db.PurchaseRequisitions on o.PRId equals p.PRId
                                     join q in db.PartNo_Items on n.partNo equals q.partNo
                                     where p.PRId == PRId
                                     select new PRFormModels.PRFormList()
                                     {
                                         DateRequired = o.requiredDate,
                                         Description = q.description,
                                         Remarks = o.remark,
                                         PartNo = q.materialNo,
                                         PORefNo = m.Details_PONo,
                                         Quantity = o.requestedQuantity,
                                         UM = q.UoM,
                                         UnitPrice = PRDetailsObj.TotalPrice / o.requestedQuantity,
                                         Total = PRDetailsObj.TotalPrice,
                                         //GST = o.gst,
                                         Amount = PRDetailsObj.TotalPrice
                                     }).ToList(),
                };

                return new RazorPDF.PdfActionResult("PRForm", PRDetails);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
    }
}
