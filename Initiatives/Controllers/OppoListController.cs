﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Net.Mail;
using System.Data;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;

namespace Initiatives.Controllers
{
    public class OppoListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();
        // GET: /PertinentInfo/

        #region Create Opportunity
        [Authorize]
        public ActionResult CreateOpportunity()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");
                ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status");
                ViewBag.LOBApproval = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                            }, "ApprovalId", "LOBApproval");
                ViewBag.WinningChances = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = "S001      "},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = "S002      "},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = "S003      "},
                                            }, "WinningChancesId", "WinningChances");

                return PartialView();
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult CreateOpportunity(MainModel MM)
        {
            if (!ModelState.IsValid)
            {
                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");
                ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status");

                return PartialView("CreateOpportunity", MM);
            }
            Opportunity _objCreateOpportunity = new Opportunity
            {
                uuid = Guid.NewGuid(),
                custId = MM.OppoListDetail.CustId,
                typeId = MM.OppoListDetail.OppoTypeId,
                name = MM.OppoListDetail.OpportunityName,
                description = MM.OppoListDetail.Description,
                assignToId = MM.OppoListDetail.AccManagerId,
                approvalId = MM.OppoListDetail.ApprovalId,
                chanceId = MM.OppoListDetail.WinningChancesId,
                catId = MM.OppoListDetail.CatId,
                value = MM.OppoListDetail.OpportunityValue,
                targetGP = MM.OppoListDetail.TargetGP,
                majorRisk = MM.OppoListDetail.MajorProjectRisk,
                similarity = MM.OppoListDetail.SimilarProject,
                approvingAuthority = MM.OppoListDetail.ApprovingAuthority,
                proposalDueDate = MM.OppoListDetail.Submission,
                targetAward = MM.OppoListDetail.TargetAward,
                statusId = "OS001",
                createByUserId = Int32.Parse(Session["UserId"].ToString()),
                active = true
            };
            db.Opportunities.Add(_objCreateOpportunity);
            db.SaveChanges();

            Users_Opportunity _objUsersOpportunity = new Users_Opportunity
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreateOpportunity.opportunityId,
                userId = Int32.Parse(Session["UserId"].ToString()),
                canView = true,
                canEdit = true,
                assignTo = true,
                remark = "Opportunity Creator"
            };
            db.Users_Opportunity.Add(_objUsersOpportunity);

            if (_objCreateOpportunity.createByUserId != _objCreateOpportunity.assignToId)
            {
                Users_Opportunity _objUserAssignedOpportunity = new Users_Opportunity
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = _objCreateOpportunity.opportunityId,
                    userId = _objCreateOpportunity.assignToId,
                    canView = true,
                    canEdit = true,
                    assignTo = true,
                    remark = "Team Leader"
                };
                db.Users_Opportunity.Add(_objUserAssignedOpportunity);
            }

            //OpportunityTechProvider _objTechProvider = new OpportunityTechProvider()
            //{
            //    uuid = Guid.NewGuid(),
            //    opportunityId = _objCreateOpportunity.opportunityId,
                
            //};
            //db.OpportunityTechProviders.Add(_objTechProvider);

            //OpportunityCompetitor _objOpportunityCompetitor = new OpportunityCompetitor()
            //{
            //    uuid = Guid.NewGuid(),
            //    opportunityId = _objCreateOpportunity.opportunityId
            //};

            RiskReview _objRiskReview = new RiskReview
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreateOpportunity.opportunityId
            };
            db.RiskReviews.Add(_objRiskReview);

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreateOpportunity.opportunityId,
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "create " + _objCreateOpportunity.name + " opportunity",
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);

            db.SaveChanges();

            return RedirectToAction("OpportunityList", "OppoList");
        }
        #endregion

        #region Opportunity List
        [Authorize]
        public ActionResult OpportunityList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                    MainModel model = new MainModel
                    {
                        RoleId = Session["RoleId"].ToString(),
                        UserName = Session["Username"].ToString()
                    };
                    model.OppoListObject = DBQueryController.getOpportunityInfo(model.UserName, model.RoleId);

                    return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region Opportunity Tabs
        [Authorize]
        public ActionResult ViewInfo(string OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (OppContId != null && User.Identity.IsAuthenticated)
                {
                    Session["OppoId"] = OppContId;
                }
                MainModel model = new MainModel();
                int userID = Int32.Parse(Session["UserId"].ToString()); 
                int opportunityId = Int32.Parse(Session["OppoId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                model.OppContId = opportunityId;
                model.UserName = Session["Username"].ToString();
                model.OppoListDetail = (from m in db.Opportunities
                                        where m.opportunityId == opportunityId
                                        select new OppoDetailList()
                                        {
                                            StatusId = m.statusId
                                        }).FirstOrDefault();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        #region Pertinent Info
        [Authorize]
        public ActionResult PertinentInfo(string OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int OpportunityId = Int32.Parse(OppContId); int UserId = Int32.Parse(Session["UserId"].ToString());
                MainModel MM = new MainModel
                {
                    OppContId = OpportunityId,
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    OppoListDetail = (from m in db.Opportunities
                                      join n in db.Users on m.createByUserId equals n.userId into s
                                      join o in db.Customers on m.custId equals o.custId into t
                                      join p in db.OpportunityCategories on m.catId equals p.catId into u
                                      join q in db.OpportunityStatus on m.statusId equals q.statusId into v
                                      join r in db.OpportunityTypes on m.typeId equals r.typeId into w
                                      from x in s.DefaultIfEmpty()
                                      from y in t.DefaultIfEmpty()
                                      from z in u.DefaultIfEmpty()
                                      from aa in v.DefaultIfEmpty()
                                      from ab in w.DefaultIfEmpty()
                                      where m.opportunityId == OpportunityId
                                      select new OppoDetailList()
                                      {
                                          CustId = m.custId,
                                          CustName = (y == null ? String.Empty : y.name + " (" + y.abbreviation + ")"),
                                          OppoTypeId = m.typeId,
                                          OpportunityName = m.name,
                                          Description = m.description,
                                          AccManagerId = m.assignToId,
                                          AccManagerName = (x == null ? String.Empty : x.firstName + " " + x.lastName),
                                          ApprovalId = m.approvalId,
                                          WinningChancesId = m.chanceId,
                                          OppoType = (ab == null ? String.Empty : ab.type + " (" + y.abbreviation + ")"),
                                          CatId = m.catId,
                                          OppoCat = (z == null ? String.Empty : z.category),                                          
                                          OpportunityValue = m.value.Value,
                                          TargetGP = m.targetGP,
                                          MajorProjectRisk = m.majorRisk,
                                          SimilarProject = m.similarity,
                                          ApprovingAuthority = m.approvingAuthority,
                                          //ContractNo = m.contractNo,
                                          Submission = m.proposalDueDate.Value,
                                          TargetAward = m.targetAward,
                                          StatusId = m.statusId,
                                          Status = (aa == null ? String.Empty : aa.status),
                                          CauseOfLost = m.causeOfLost
                                          //BudgetaryRevenue = m.budgetaryRevenue.Value
                                      }).FirstOrDefault(),
                    RoleId = Session["RoleId"].ToString(),
                    objCanEdit = (from m in db.Users_Opportunity
                                  where m.opportunityId == OpportunityId && m.userId == UserId
                                  select new EditPermission()
                                  {
                                      canEdits = m.canEdit
                                  }).FirstOrDefault(),
                    //MessageListModel = DBQueryController.getMessageList(OpportunityId, 0, 0, 0, 0, userid)
                };

                //var NotiMemberList = (from m in db.Users
                //                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                //                      from x in gy.DefaultIfEmpty()
                //                      where m.status == true && x.opportunityId == OpportunityId
                //                      select new NotiMemberList()
                //                      {
                //                          Id = m.userId,
                //                          Name = m.firstName + " " + m.lastName
                //                      }).ToList().OrderBy(c => c.Name);

                //ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);

                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", MM.OppoListDetail.CustId);
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName", MM.OppoListDetail.AccManagerId);
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type", MM.OppoListDetail.OppoTypeId);
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category", MM.OppoListDetail.CatId);
                ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status", MM.OppoListDetail.StatusId);
                ViewBag.LOBApproval = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                            }, "ApprovalId", "LOBApproval", MM.OppoListDetail.ApprovalId);
                ViewBag.WinningChances = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = "S001      "},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = "S002      "},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = "S003      "},
                                            }, "WinningChancesId", "WinningChances", MM.OppoListDetail.WinningChancesId);
                //return View("PertinentInfo",OppoDetailList);
                //return JavaScript("windows.location = '" + Url.Action("PertinentInfo", "MainTabs") + "'");
                return PartialView(MM);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult PertinentInfo(MainModel x)
        {
            int caseId = x.OppContId;
            int userid = x.UserId;
            var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
            var UserQuery = (from m in db.Users
                             join n in db.Users_Roles on m.userId equals n.userId into o
                             from p in o.DefaultIfEmpty()
                             where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                             select new OppoDetailList()
                             {
                                 AccManagerId = p.userId,
                                 AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                             }).OrderBy(c => c.AccManagerName).AsEnumerable();
            var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
            var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
            var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);

            if (!ModelState.IsValid)
            {
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", x.OppoListDetail.CustId);
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName", x.OppoListDetail.AccManagerId);
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type", x.OppoListDetail.OppoTypeId);
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category", x.OppoListDetail.CatId);
                ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status", x.OppoListDetail.StatusId);
                ViewBag.LOBApproval = new SelectList(
                                                new List<OppoDetailList>
                                                {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                                }, "ApprovalId", "LOBApproval", x.OppoListDetail.ApprovalId);
                ViewBag.WinningChances = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = "S001      "},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = "S002      "},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = "S003      "},
                                            }, "WinningChancesId", "WinningChances", x.OppoListDetail.WinningChancesId);
                x.objCanEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == x.OppContId && m.userId == userid
                                select new EditPermission()
                                {
                                    canEdits = m.canEdit
                                }).FirstOrDefault();

                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        view = this.RenderPartialView("PertinentInfo", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            Opportunity objOppoDetaildb = db.Opportunities.First(m => m.opportunityId == caseId);

            if (objOppoDetaildb.custId != x.OppoListDetail.CustId)
            {
                Notification _objCustomerChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                Customer _objCustomerdb = db.Customers.First(m => m.custId == objOppoDetaildb.custId);
                Customer objCustomerdb = db.Customers.First(m => m.custId == x.OppoListDetail.CustId);
                _objCustomerChange.content = "change customer selection from " + _objCustomerdb.name + " to " + objCustomerdb.name;
                _objCustomerChange.flag = 0;
                db.Notifications.Add(_objCustomerChange);
                objOppoDetaildb.custId = x.OppoListDetail.CustId;
            }
            if (objOppoDetaildb.name != x.OppoListDetail.OpportunityName)
            {
                Notification _objNameChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change opportunity name from " + objOppoDetaildb.name + " to " + x.OppoListDetail.OpportunityName,
                    flag = 0
                };
                db.Notifications.Add(_objNameChange);
                objOppoDetaildb.name = x.OppoListDetail.OpportunityName;
            }
            if (objOppoDetaildb.description != x.OppoListDetail.Description)
            {
                Notification _objDescChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change the opportunity description to " + x.OppoListDetail.Description
                };
                ;
                _objDescChange.flag = 0;
                db.Notifications.Add(_objDescChange);
                objOppoDetaildb.description = x.OppoListDetail.Description;
            }
            if (objOppoDetaildb.assignToId != x.OppoListDetail.AccManagerId)
            {
                Notification _objAssignToChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                User _objAssignTodb = db.Users.First(m => m.userId == objOppoDetaildb.assignToId);
                User objAssignTodb = db.Users.First(m => m.userId == x.OppoListDetail.AccManagerId);
                _objAssignToChange.content = "change opportunity assigned to selection from " + _objAssignTodb.firstName + " "
                + _objAssignTodb.lastName + " to " + objAssignTodb.firstName + " " + objAssignTodb.lastName;
                _objAssignToChange.flag = 0;
                db.Notifications.Add(_objAssignToChange);
                objOppoDetaildb.assignToId = x.OppoListDetail.AccManagerId;
            }
            if (objOppoDetaildb.approvalId != x.OppoListDetail.ApprovalId)
            {
                string approvingLOB = "";
                if (objOppoDetaildb.approvalId == true)
                {
                    approvingLOB = "Yes";
                } else if (objOppoDetaildb.approvalId == false)
                {
                    approvingLOB = "No";
                }
                if (x.OppoListDetail.ApprovalId == true)
                {
                    x.OppoListDetail.ApprovalbyLOB = "Yes";
                }
                else if (objOppoDetaildb.approvalId == false)
                {
                    x.OppoListDetail.ApprovalbyLOB = "No";
                }
                Notification _objapprovalIdChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.approvalId == null ? "change the approval by head of LOB to " + x.OppoListDetail.ApprovalbyLOB : 
                    "change the approval by head of LOB from " + approvingLOB + " to " + x.OppoListDetail.ApprovalbyLOB
                };
                ;
                db.Notifications.Add(_objapprovalIdChange);
                objOppoDetaildb.approvalId = x.OppoListDetail.ApprovalId;
            }
            if (objOppoDetaildb.chanceId != x.OppoListDetail.WinningChancesId)
            {
                Opportunity _objOppoTypedb = db.Opportunities.First(m => m.chanceId == objOppoDetaildb.chanceId);
                CompetitorStrength objOppoTypedb = db.CompetitorStrengths.First(m => m.strengthId == x.OppoListDetail.WinningChancesId);

                Notification _objwinningChanceIdChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = _objOppoTypedb.CompetitorStrength.strengthLevel == null ? "change the opportunity description to " + objOppoTypedb.strengthLevel : 
                    "change the winning chances from " + _objOppoTypedb.CompetitorStrength.strengthLevel + " to " + objOppoTypedb.strengthLevel
                };
                ;
                db.Notifications.Add(_objwinningChanceIdChange);
                objOppoDetaildb.chanceId = x.OppoListDetail.WinningChancesId;
            }
            if (objOppoDetaildb.typeId != x.OppoListDetail.OppoTypeId)
            {
                Notification _objTypeChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                OpportunityType _objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == objOppoDetaildb.typeId);
                OpportunityType objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == x.OppoListDetail.OppoTypeId);
                _objTypeChange.content = "change opportunity type selection from " + _objOppoTypedb.type + " to " + objOppoTypedb.type;
                _objTypeChange.flag = 0;
                db.Notifications.Add(_objTypeChange);
                objOppoDetaildb.typeId = x.OppoListDetail.OppoTypeId;
            }
            if (objOppoDetaildb.catId != x.OppoListDetail.CatId)
            {
                Notification _objCatChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                OpportunityCategory _objOppoCatdb = db.OpportunityCategories.First(m => m.catId == objOppoDetaildb.catId);
                OpportunityCategory objOppoCatdb = db.OpportunityCategories.First(m => m.catId == x.OppoListDetail.CatId);
                _objCatChange.content = "change opportunity category selection from " + _objOppoCatdb.category + " to " + objOppoCatdb.category;
                _objCatChange.flag = 0;
                db.Notifications.Add(_objCatChange);
                objOppoDetaildb.catId = x.OppoListDetail.CatId;
            }                      
            if (objOppoDetaildb.value != x.OppoListDetail.OpportunityValue)
            {
                Notification _objValueChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = "change the opportunity value from " + objOppoDetaildb.value + " to " + x.OppoListDetail.OpportunityValue
                };
                db.Notifications.Add(_objValueChange);
                objOppoDetaildb.value = x.OppoListDetail.OpportunityValue;
            }
            if (objOppoDetaildb.targetGP != x.OppoListDetail.TargetGP)
            {
                Notification _objtargetGPChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.value == null ? "change the target GP to " + x.OppoListDetail.TargetGP : 
                    "change the target GP from " + objOppoDetaildb.targetGP + " to " + x.OppoListDetail.TargetGP
                };
                db.Notifications.Add(_objtargetGPChange);
                objOppoDetaildb.targetGP = x.OppoListDetail.TargetGP;
            }
            if (objOppoDetaildb.majorRisk != x.OppoListDetail.MajorProjectRisk)
            {
                Notification _objMajorProjectRiskChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.majorRisk == null ? "change the major project risk to " + x.OppoListDetail.MajorProjectRisk :
                    "change the major project risk from " + objOppoDetaildb.majorRisk + " to " + x.OppoListDetail.MajorProjectRisk
                };
                db.Notifications.Add(_objMajorProjectRiskChange);
                objOppoDetaildb.majorRisk = x.OppoListDetail.MajorProjectRisk;
            }
            if (objOppoDetaildb.similarity != x.OppoListDetail.SimilarProject)
            {
                Notification _objSimilarProjectChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.similarity == null ? "change the similar project previously done by KUBTel to " + x.OppoListDetail.SimilarProject : 
                    "change the similar project previously done by KUBTel from " + objOppoDetaildb.similarity + " to " + x.OppoListDetail.SimilarProject
                };
                db.Notifications.Add(_objSimilarProjectChange);
                objOppoDetaildb.similarity = x.OppoListDetail.SimilarProject;
            }
            if (objOppoDetaildb.approvingAuthority != x.OppoListDetail.ApprovingAuthority)
            {
                Notification _objtargetGPChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0, 
                    content = objOppoDetaildb.approvingAuthority == null ? "change the Approving Authority to Enter Business Potential to " + x.OppoListDetail.ApprovingAuthority :
                    "change the Approving Authority to Enter Business Potential from " + objOppoDetaildb.approvingAuthority + " to " + x.OppoListDetail.ApprovingAuthority
                };
                db.Notifications.Add(_objtargetGPChange);
                objOppoDetaildb.approvingAuthority = x.OppoListDetail.ApprovingAuthority;
            }
            if (objOppoDetaildb.contractNo != x.OppoListDetail.ContractNo)
            {
                Notification _objcontractNoChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.contractNo == null ? "change the opportunity no. to " + x.OppoListDetail.ContractNo : 
                    "change the opportunity no. from " + objOppoDetaildb.contractNo + " to " + x.OppoListDetail.ContractNo
                };
                db.Notifications.Add(_objcontractNoChange);
                objOppoDetaildb.contractNo = x.OppoListDetail.ContractNo;
            }
            if (objOppoDetaildb.proposalDueDate != x.OppoListDetail.Submission)
            {
                Notification _objSubmissionDateChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.proposalDueDate == null ? "change the opportunity submission date to " + x.OppoListDetail.Submission : 
                    "change the opportunity submission date from " + objOppoDetaildb.proposalDueDate + " to " + x.OppoListDetail.Submission
                };
                db.Notifications.Add(_objSubmissionDateChange);
                objOppoDetaildb.proposalDueDate = x.OppoListDetail.Submission;
            }
            if (objOppoDetaildb.targetAward != x.OppoListDetail.TargetAward)
            {
                Notification _objTargetAwardChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = objOppoDetaildb.targetAward == null ? "change the opportunity target award to " + x.OppoListDetail.TargetAward : 
                    "change the opportunity target award from " + objOppoDetaildb.targetAward + " to " + x.OppoListDetail.TargetAward
                };
                db.Notifications.Add(_objTargetAwardChange);
                objOppoDetaildb.targetAward = x.OppoListDetail.TargetAward;
            }
            //if (objOppoDetaildb.statusId != x.OppoListDetail.StatusId)
            //{
            //    Notification _obStatusChange = new Notification
            //    {
            //        uuid = Guid.NewGuid(),
            //        opportunityId = Int32.Parse(Session["OppoId"].ToString()),
            //        createDate = DateTime.Now,
            //        fromUserId = Int32.Parse(Session["UserId"].ToString())
            //    };
            //    OpportunityStatu _objStatusdb = db.OpportunityStatus.First(m => m.statusId == objOppoDetaildb.statusId);
            //    _obStatusChange.content = "change opportunity status selection from " + _objStatusdb.status + " to " + objStatusdb.status;
            //    _obStatusChange.flag = 0;
            //    db.Notifications.Add(_obStatusChange);
            //    objOppoDetaildb.statusId = x.OppoListDetail.StatusId;
            //}
            x.objCanEdit = (from m in db.Users_Opportunity
                            where m.opportunityId == x.OppContId && m.userId == userid
                                         select new EditPermission()
                                         {
                                             canEdits = m.canEdit
                                         }).FirstOrDefault();
            db.SaveChanges();     
            
            ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName", x.OppoListDetail.CustId);
            ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName", x.OppoListDetail.AccManagerId);
            ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type", x.OppoListDetail.OppoTypeId);
            ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category", x.OppoListDetail.CatId);
            ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status", x.OppoListDetail.StatusId);
            ViewBag.LOBApproval = new SelectList(
                                            new List<OppoDetailList>
                                            {
                                                new OppoDetailList {LOBApproval = "Yes", ApprovalId = true},
                                                new OppoDetailList {LOBApproval = "No", ApprovalId = false},
                                            }, "ApprovalId", "LOBApproval", x.OppoListDetail.ApprovalId);
            ViewBag.WinningChances = new SelectList(
                                        new List<OppoDetailList>
                                        {
                                                new OppoDetailList {WinningChances = "High", WinningChancesId = "S001      "},
                                                new OppoDetailList {WinningChances = "Medium", WinningChancesId = "S002      "},
                                                new OppoDetailList {WinningChances = "Low", WinningChancesId = "S003      "},
                                        }, "WinningChancesId", "WinningChances", x.OppoListDetail.WinningChancesId);

            return new JsonResult
            {
                Data = new
                {
                    success = true,
                    view = this.RenderPartialView("PertinentInfo", x)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

        #region Team Member

        [Authorize]
        public ActionResult TeamMember(string OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int OpportunityId = Int32.Parse(OppContId);
                int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel TeamMemberDetail = new MainModel
                {
                    TeamMemberList = (from m in db.Users_Opportunity
                                      join n in db.Users on m.userId equals n.userId
                                      //into o from x in o.DefaultIfEmpty()
                                      where m.opportunityId == OpportunityId && n.status == true
                                      orderby m.assignTo descending
                                      select new TeamMember()
                                      {
                                          OpportunityId = m.opportunityId,
                                          UserId = m.userId,
                                          UserName = m.User.firstName + " " + m.User.lastName,
                                          Remark = m.remark,
                                          canView = m.canView,
                                          canEdit = m.canEdit
                                      }).OrderBy(c => c.UserName).ToList(),
                    RoleId = Session["RoleId"].ToString(),
                    ViewEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == OpportunityId && m.userId == userid
                                select new TeamPermission()
                                {
                                    canView = m.canView,
                                    canEdit = m.canEdit
                                }).FirstOrDefault()
                };

                OppoDetailList objOppoDetail = new OppoDetailList();

                var itemIds = TeamMemberDetail.TeamMemberList.Select(x => x.UserId).ToArray();
                var UserQuery = db.Users
                    .Select(c => new { c.userId, FullName = c.firstName + " " + c.lastName, c.status })
                    .Where(c => c.status == true)
                    .Where(c => !itemIds.Contains(c.userId))
                    .OrderBy(c => c.FullName);

                ViewBag.TeamMemberList = new SelectList(UserQuery.AsEnumerable(), "userId", "FullName", Convert.ToString(objOppoDetail.AccManagerId));

                return PartialView(TeamMemberDetail);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public ActionResult TeamPermission(MainModel MM)
        {
            try
            {
                if (Session["OppoId"] != null)
                {
                    MM.OppContId = Int32.Parse(Session["OppoId"].ToString());
                } else if (Session["ContractId"] != null)
                {
                    MM.OppContId = Int32.Parse(Session["ContractId"].ToString());
                }
                
                MM.UserId = Int32.Parse(Session["UserId"].ToString());
                MM.RoleId = Session["RoleId"].ToString();
                var TeamMemberList = (from m in db.Users_Opportunity
                                      join n in db.Users on m.userId equals n.userId
                                      where m.opportunityId == MM.OppContId && n.status == true
                                      orderby n.firstName
                                      select m).ToList();               
                int i = 0;
                TeamMemberList.ForEach(field =>
                {
                    field.remark = MM.TeamMemberList[i].Remark;
                    field.canView = MM.TeamMemberList[i].canView;
                    field.canEdit = MM.TeamMemberList[i].canEdit;
                    i++;
                });
                db.SaveChanges();

                MM.TeamMemberList = (from m in db.Users_Opportunity
                                     join n in db.Users on m.userId equals n.userId
                                     //into o from x in o.DefaultIfEmpty()
                                     where m.opportunityId == MM.OppContId && n.status == true
                                     orderby m.assignTo descending
                                     select new TeamMember()
                                     {
                                         OpportunityId = m.opportunityId,
                                         UserId = m.userId,
                                         UserName = m.User.firstName + " " + m.User.lastName,
                                         Remark = m.remark,
                                         canView = m.canView,
                                         canEdit = m.canEdit
                                     }).OrderBy(c => c.UserName).ToList();

                MM.ViewEdit = (from m in db.Users_Opportunity
                                   where m.opportunityId == MM.OppContId && m.userId == MM.UserId
                                   select new TeamPermission()
                                   {
                                       canView = m.canView,
                                       canEdit = m.canEdit
                                   }).FirstOrDefault();

                OppoDetailList objOppoDetail = new OppoDetailList();

                var itemIds = MM.TeamMemberList.Select(x => x.UserId).ToArray();
                var UserQuery = db.Users
                    .Select(c => new { c.userId, FullName = c.firstName + " " + c.lastName, c.status })
                    .Where(c => c.status == true)
                    .Where(c => !itemIds.Contains(c.userId))
                    .OrderBy(c => c.FullName);

                ViewBag.TeamMemberList = new SelectList(UserQuery.AsEnumerable(), "userId", "FullName", Convert.ToString(objOppoDetail.AccManagerId));

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        view = this.RenderPartialView("TeamMember", MM)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        view = this.RenderPartialView("TeamMember", MM)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }          
        }
        public JsonResult AddTeamMember(string UserId, string OppContId)
        {

            int OpportunityId = Int32.Parse(OppContId);
            int UserID = Int32.Parse(UserId);
            Users_Opportunity _objAddMember = new Users_Opportunity
            {
                uuid = Guid.NewGuid(),
                opportunityId = OpportunityId,
                userId = UserID,
                canView = true,
                canEdit = false
            };
            db.Users_Opportunity.Add(_objAddMember);

            //var SuperiorId = (from m in db.Users
            //                  where m.userId == UserID
            //                  select m).FirstOrDefault();
            //if (SuperiorId.oneLevelSuperiorId != null)
            //{
            //    var checkSuperior = (from m in db.Users_Opportunity
            //                         where m.userId == SuperiorId.oneLevelSuperiorId && m.opportunityId == OpportunityId
            //                         select m).FirstOrDefault();
            //    if (checkSuperior == null)
            //    {
            //        //string _objAddSuperior = "_objAddMember" +i;
            //        Users_Opportunity _objAddSuperior = new Users_Opportunity
            //        {
            //            uuid = Guid.NewGuid(),
            //            opportunityId = OpportunityId,
            //            userId = SuperiorId.oneLevelSuperiorId.Value,
            //            canView = true,
            //            canEdit = false
            //        };
            //        db.Users_Opportunity.Add(_objAddSuperior);
            //    }
            //}
            db.SaveChanges();
            //var NotiMemberList = MemberList.Select(c => new { c.userId, c.userName }).ToList();

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region Doc Management

        [Authorize]
        public ActionResult DocManagement(string OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel();
                int userid = Int32.Parse(Session["UserId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
            int oId = Int32.Parse(OppContId);
            model.ContDocListObject = (from m in db.FileUploads
                                     join n in db.Users on m.uploadUserId equals n.userId
                                     where m.opportunityId == oId && m.archivedFlag == false
                                        select new FilePODetailInfo()
                                     {
                                         opportunityId = m.opportunityId,
                                         fileEntryId = m.fileEntryId,
                                         uploadUserName = n.firstName + " " + n.lastName,
                                         uploadDate = m.uploadDate,
                                         FileName = m.FileName,
                                         File = m.File,
                                         Description = m.description
                                     }).ToList();
            model.FileArchiveModel = (from m in db.FileUploads
                                      join n in db.Users on m.uploadUserId equals n.userId
                                      where m.opportunityId == oId && m.archivedFlag == true
                                      select new FilePODetailInfo()
                                      {
                                          opportunityId = m.opportunityId,
                                          fileEntryId = m.fileEntryId,
                                          uploadUserName = n.firstName + " " + n.lastName,
                                          uploadDate = m.uploadDate,
                                          FileName = m.FileName,
                                          File = m.File,
                                          Description = m.description
                                      }).ToList();
            //model.UploadDelete = (from m in db.Users_Opportunity
            //                       where m.opportunityId == oppoId && m.userId == userid
            //                       select new Permission()
            //                       {
            //                           canUpload = m.canUpload,
            //                           canDelete = m.canDelete,
            //                       }).FirstOrDefault();
            return PartialView(model);
        }
        else
            {
                return Redirect("~/Home/Index");
            }
        }
        //public JsonResult Deletes(string selectedId)
        //{
        //    int fileId = Int32.Parse(selectedId);
        //    var itemToRemove = db.FileUploads.SingleOrDefault(x => x.fileEntryId == fileId); //returns a single item.

        //    if (itemToRemove != null)
        //    {
        //        db.FileUploads.Remove(itemToRemove);
        //        db.SaveChanges();
        //    }

        //    return Json(new { Result = String.Format("Test berjaya!") });
        //}
        public FileContentResult FileDownload(int id)
        {
            byte[] fileData;

            FileUpload fileRecord = db.FileUploads.Find(id);

            string fileName;
            string filename1 = fileRecord.FileName.Replace("\\", ",");
            string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            fileData = (byte[])fileRecord.File.ToArray();
            fileName = filename2;

            return File(fileData, "text", fileName);
        }
        public JsonResult FileArchive(string selectedId)
        {
            int id = Int32.Parse(selectedId);

            FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
            if (itemToArchive != null)
            {
                itemToArchive.archivedFlag = true;
                db.SaveChanges();
            }

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region Q & A
        [Authorize]
        public ActionResult QuestionAnswer(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int Uid = Int32.Parse(Session["UserId"].ToString());

                MainModel QNAModelList = new MainModel
                {
                    MessageListModel = DBQueryController.getMessageList(OppContId, 0, 0, 0, 0, Uid),
                    UserId = Uid
                };

                var NotiMemberList = (from m in db.Users
                                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                      from x in gy.DefaultIfEmpty()
                                      where m.status == true && x.opportunityId == OppContId
                                      select new NotiMemberList()
                                      {
                                          Id = m.userId,
                                          Name = m.firstName + " " + m.lastName
                                      }).ToList().OrderBy(c => c.Name);

                ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                return PartialView(QNAModelList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public virtual ActionResult DocUpload(MainModel fd)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(db.FileUploads.ToList());
            //}

            FileUpload fileUploadModel = new FileUpload();
            int userId = fd.UserId;
            DateTime createDate = DateTime.Now;
            string filename1 = fd.file.FileName.Replace("\\", ",");
            string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            string fullPath = fd.file.FileName;
            string extension = Path.GetExtension(fullPath);
            byte[] uploadFile = new byte[fd.file.InputStream.Length];

            fileUploadModel.uuid = Guid.NewGuid();
            fileUploadModel.opportunityId = fd.OppContId;
            fileUploadModel.uploadUserId = userId;
            fileUploadModel.uploadDate = createDate;
            fileUploadModel.FullPath = fullPath;
            fileUploadModel.FileName = filename;
            fileUploadModel.description = fd.FileDescription;
            fileUploadModel.Extension = extension;
            fileUploadModel.archivedFlag = false;
            fileUploadModel.notPO = false;
            fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
            fileUploadModel.File = uploadFile;
            db.FileUploads.Add(fileUploadModel);

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = Int32.Parse(Session["OppoId"].ToString()),
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "uploaded " + filename,
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);

            db.SaveChanges();

            //return PartialView(db.FileUploads.ToList());
            return Json(new { Result = String.Format("Test berjaya!") });
        }
        public JsonResult OppoSendMessage(string UserId, string OppContId, string Content, List<int> selectedUserId)
        {
            int OpportunityId = Int32.Parse(OppContId);
            int fromUserID = Int32.Parse(UserId);
            var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = OpportunityId,
                POId = null,
                createDate = DateTime.Now,
                fromUserId = fromUserID,
                content = Content,
                QNAtype = "Message",
                latlng = myEmail.loginLatLng,
                senderLocation = myEmail.loginAddress,
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);
            db.SaveChanges();

            foreach (var model in selectedUserId)
            {
                //toUserId = Int32.Parse(((string[])(selectedUserId))[i].Split(',')[0]);
                var UserEmail = db.Users.SingleOrDefault(x => x.userId == model);
                var OppoInfo = (from m in db.Opportunities
                                join n in db.Users on m.createByUserId equals n.userId
                                join o in db.Customers on m.custId equals o.custId
                                join p in db.OpportunityTypes on m.typeId equals p.typeId
                                where m.opportunityId == OpportunityId
                                select new MailModel()
                                {
                                    CustName = o.name + " (" + o.abbreviation + ")",
                                    CustAbbreviation = o.abbreviation,
                                    OpportunityName = m.name,
                                    Description = m.description,
                                    OpportunityType = p.type,
                                    AssignTo = n.firstName + " " + n.lastName
                                }).FirstOrDefault();

                string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/EmailTemplate.cshtml"));
                OppoInfo.Content = Content;
                OppoInfo.FromName = myEmail.emailAddress;
                OppoInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/OppoList/ViewInfo") + "?id=" + OppContId);
                var result =
                        Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "OppoTemplateKey", null, OppoInfo);

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("info@kubtel.com")
                };
                //mail.From = new MailAddress("info@kubtel.com");
                mail.To.Add(new MailAddress(UserEmail.emailAddress));
                //mail.To.Add(new MailAddress("info@kubtel.com"));
                mail.Subject = "OCM: Updates on " + OppoInfo.OpportunityName + " for " + OppoInfo.CustAbbreviation;
                mail.Body = result;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient
                {
                    //smtp.Host = "pops.kub.com";
                    Host = "outlook.office365.com",
                    Port = 587,
                    //smtp.Port = 25;
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                };
                //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
                smtp.Send(mail);

                NotiGroup _objSendToUserId = new NotiGroup
                {
                    uuid = Guid.NewGuid(),
                    chatId = _objSendMessage.chatId,
                    toUserId = model
                };

                db.NotiGroups.Add(_objSendToUserId);
            }
            db.SaveChanges();

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        [Authorize]
        public ActionResult ActivityReport(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel MM = new MainModel
                {
                    OppContId = OppContId,
                    RoleId = Session["RoleId"].ToString()
                };
                return PartialView(MM);
            }
            else
            {
                return Redirect("~/Home/Index");
            }            
        }

        [Authorize]
        public ActionResult InitiationReport(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel MM = new MainModel
                {
                    OppContId = OppContId,
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    RoleId = Session["RoleId"].ToString(),
                    TechProviderDetail = (from m in db.OpportunityTechProviders
                                        where m.opportunityId == OppContId
                                        select new TechProviderList()
                                        {
                                            TechName = m.techProviderName,
                                            TechProduct = m.techProviderProduct,
                                            TechSolution = m.techProviderSolution,
                                            appointmentId = m.appointmentId,
                                            appointmentName = m.Appointment.appointmentName
                                        }).ToList(),
                    CompetitorDetail = (from m in db.OpportunityCompetitors
                                      where m.opportunityId == OppContId
                                      select new CompetitorList()
                                      {
                                          CompetitorName = m.competitorName,
                                          CompetitorProduct = m.competitorProduct,
                                          CompetitorSolution = m.competitorSolution,
                                          CompStrengthId = m.strengthId,
                                          CompStrength = m.CompetitorStrength.strengthLevel
                                      }).ToList(),
                    SWOTAnalysisDetail = (from m in db.OpportunitySWOTs
                                          where m.opportunityId == OppContId
                                          select new SWOTAnalysisList()
                                          {
                                              Strength = m.strength,
                                              Weakness = m.weakness,
                                              Opportunity = m.opportunity,
                                              Threat = m.threat
                                          }).DefaultIfEmpty().FirstOrDefault()
                };
                               
                MM.objCanEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == MM.OppContId && m.userId == MM.UserId
                                 select new EditPermission()
                                {
                                    canEdits = m.canEdit
                                }).FirstOrDefault();
                var AppointmentQuery = db.Appointments.Select(c => new { c.appointmentId, c.appointmentName }).OrderBy(c => c.appointmentId);
                var StrengthQuery = db.CompetitorStrengths.Select(c => new { c.strengthId, c.strengthLevel }).OrderBy(c => c.strengthId);
                int i = 1; int j = 1;
                foreach (var value in MM.TechProviderDetail)
                {
                    ViewData["Appointmentlist" + i] = new SelectList(AppointmentQuery.AsEnumerable(), "appointmentId", "appointmentName", value.appointmentId);
                    i++;
                }
                foreach (var value in MM.CompetitorDetail)
                {
                    ViewData["CompStrengthList" + j] = new SelectList(StrengthQuery.AsEnumerable(), "strengthId", "strengthLevel", value.CompStrengthId);
                    j++;
                }

                return PartialView(MM);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult CreateInitiationReport(MainModel MM)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (Session["OppoId"] == null)
                {
                    Session["OppoId"] = MM.OppContId;
                }
                if (!ModelState.IsValid || (MM.TechProviderDetail == null && MM.CompetitorDetail == null && MM.SWOTAnalysisDetail.Strength == null))
                {
                    MM.TechProviderDetail = (from m in db.OpportunityTechProviders
                                             where m.opportunityId == MM.OppContId
                                             select new TechProviderList()
                                             {
                                                 TechName = m.techProviderName,
                                                 TechProduct = m.techProviderProduct,
                                                 TechSolution = m.techProviderSolution,
                                                 appointmentId = m.appointmentId,
                                                 appointmentName = m.Appointment.appointmentName
                                             }).ToList();
                    MM.CompetitorDetail = (from m in db.OpportunityCompetitors
                                           where m.opportunityId == MM.OppContId
                                           select new CompetitorList()
                                           {
                                               CompetitorName = m.competitorName,
                                               CompetitorProduct = m.competitorProduct,
                                               CompetitorSolution = m.competitorSolution,
                                               CompStrengthId = m.strengthId,
                                               CompStrength = m.CompetitorStrength.strengthLevel
                                           }).ToList();
                    MM.SWOTAnalysisDetail = (from m in db.OpportunitySWOTs
                                             where m.opportunityId == MM.OppContId
                                             select new SWOTAnalysisList()
                                             {
                                                 Strength = m.strength,
                                                 Weakness = m.weakness,
                                                 Opportunity = m.opportunity,
                                                 Threat = m.threat
                                             }).DefaultIfEmpty().FirstOrDefault();
                    MM.objCanEdit = (from m in db.Users_Opportunity
                                     where m.opportunityId == MM.OppContId && m.userId == MM.UserId
                                     select new EditPermission()
                                     {
                                         canEdits = m.canEdit
                                     }).FirstOrDefault();

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            exception = false,
                            view = this.RenderPartialView("InitiationReport", MM)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }

                if (MM.TechProviderDetail != null)
                {
                    foreach (var value in MM.TechProviderDetail)
                    {
                        OpportunityTechProvider _objOpportunityTechProvider = new OpportunityTechProvider
                        {

                            uuid = Guid.NewGuid(),
                            opportunityId = MM.OppContId,
                            techProviderName = value.TechName,
                            techProviderProduct = value.TechProduct,
                            techProviderSolution = value.TechSolution,
                            appointmentId = value.appointmentId
                        };
                        db.OpportunityTechProviders.Add(_objOpportunityTechProvider);
                        db.SaveChanges();
                    }
                }
                
                if (MM.CompetitorDetail != null)
                {
                    foreach (var value in MM.CompetitorDetail)
                    {
                        OpportunityCompetitor _objOpportunityCompetitor = new OpportunityCompetitor
                        {
                            uuid = Guid.NewGuid(),
                            opportunityId = MM.OppContId,
                            competitorName = value.CompetitorName,
                            competitorProduct = value.CompetitorProduct,
                            competitorSolution = value.CompetitorSolution,
                            strengthId = value.CompStrengthId
                        };
                        db.OpportunityCompetitors.Add(_objOpportunityCompetitor);
                        db.SaveChanges();
                    }
                }
                
                if (MM.SWOTAnalysisDetail.Strength != null)
                {
                    OpportunitySWOT _objOpportunitySWOT = new OpportunitySWOT
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = MM.OppContId,
                        strength = MM.SWOTAnalysisDetail.Strength,
                        weakness = MM.SWOTAnalysisDetail.Weakness,
                        opportunity = MM.SWOTAnalysisDetail.Opportunity,
                        threat = MM.SWOTAnalysisDetail.Threat
                    };
                    db.OpportunitySWOTs.Add(_objOpportunitySWOT);
                    db.SaveChanges();
                }
                
                Notification _objCreateAssessment = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = MM.OppContId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "Create assessment report",
                    QNAtype = "Message",
                    flag = 0
                };
                db.Notifications.Add(_objCreateAssessment);
                db.SaveChanges();

                var AppointmentQuery = db.Appointments.Select(c => new { c.appointmentId, c.appointmentName }).OrderBy(c => c.appointmentId);
                var StrengthQuery = db.CompetitorStrengths.Select(c => new { c.strengthId, c.strengthLevel }).OrderBy(c => c.strengthId);
                int i = 1; int j = 1;
                foreach (var value in MM.TechProviderDetail)
                {
                    ViewData["Appointmentlist" + i] = new SelectList(AppointmentQuery.AsEnumerable(), "appointmentId", "appointmentName", value.appointmentId);
                    i++;
                }
                foreach (var value in MM.CompetitorDetail)
                {
                    ViewData["CompStrengthList" + j] = new SelectList(StrengthQuery.AsEnumerable(), "strengthId", "strengthLevel", value.CompStrengthId);
                    j++;
                }

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        exception = false,
                        view = this.RenderPartialView("InitiationReport", MM)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult InitiationReport(MainModel MM)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                OpportunityCompetitor objOpportunityCompetitor = db.OpportunityCompetitors.FirstOrDefault(m => m.opportunityId == MM.OppContId);
                OpportunityTechProvider objOpportunityTechProvider = db.OpportunityTechProviders.FirstOrDefault(m => m.opportunityId == MM.OppContId);
                OpportunitySWOT objOpportunitySWOT = db.OpportunitySWOTs.FirstOrDefault(m => m.opportunityId == MM.OppContId);

                var AppointmentQuery = db.Appointments.Select(c => new { c.appointmentId, c.appointmentName }).OrderBy(c => c.appointmentId);
                var StrengthQuery = db.CompetitorStrengths.Select(c => new { c.strengthId, c.strengthLevel }).OrderBy(c => c.strengthId);
                int i = 1; int j = 1;
                foreach (var value in MM.TechProviderDetail)
                {
                    ViewData["Appointmentlist" + i] = new SelectList(AppointmentQuery.AsEnumerable(), "appointmentId", "appointmentName", value.appointmentId);
                    i++;
                }
                foreach (var value in MM.CompetitorDetail)
                {
                    ViewData["CompStrengthList" + j] = new SelectList(StrengthQuery.AsEnumerable(), "strengthId", "strengthLevel", value.CompStrengthId);
                    j++;
                }

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        view = this.RenderPartialView("InitiationReport", MM)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public ActionResult ProjectBG(int OppContId)
        {
            return PartialView();
        }

        #region Risk Review
        [Authorize]
        public ActionResult RiskReview(string OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int OpportunityId = Int32.Parse(OppContId); int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel RiskReviewList = new MainModel
                {
                    OppContId = OpportunityId,
                    CreateByUserId = userid
                };
                RiskReviewList.RiskReviewDetail = (from m in db.RiskReviews
                              join n in db.Users on m.modifyById equals n.userId into s
                              join o in db.Customers on m.secB_Q9CustId equals o.custId into t
                              from x in s.DefaultIfEmpty()
                              from y in t.DefaultIfEmpty()
                              where m.opportunityId == OpportunityId
                              select new RiskReviewList()
                              {
                                  SecA_Q1 = m.secA_Q1,
                                  SecA_Q1Remark = m.secA_Q1Remark,
                                  SecA_Q1CeoComment = m.secA_Q1CeoComment,
                                  SecA_Q2 = m.secA_Q2,
                                  SecA_Q2Remark = m.secA_Q2Remark,
                                  SecA_Q2CeoComment = m.secA_Q2CeoComment,
                                  SecA_Q3 = m.secA_Q3,
                                  SecA_Q3Remark = m.secA_Q3Remark,
                                  SecA_Q3CeoComment = m.secA_Q3CeoComment,
                                  SecA_Q4 = m.secA_Q4,
                                  SecA_Q4Remark = m.secA_Q4Remark,
                                  SecA_Q4CeoComment = m.secA_Q4CeoComment,
                                  SecA_Q5 = m.secA_Q5,
                                  SecA_Q5Remark = m.secA_Q5Remark,
                                  SecA_Q5CeoComment = m.secA_Q5CeoComment,
                                  SecA_Q6 = m.secA_Q6,
                                  SecA_Q6Remark = m.secA_Q6Remark,
                                  SecA_Q6CeoComment = m.secA_Q6CeoComment,
                                  SecA_Q7 = m.secA_Q7,
                                  SecA_Q7Remark = m.secA_Q7Remark,
                                  SecA_Q7CeoComment = m.secA_Q7CeoComment,
                                  SecA_Q8 = m.secA_Q8,
                                  SecA_Q8Remark = m.secA_Q8Remark,
                                  SecA_Q8CeoComment = m.secA_Q8CeoComment,
                                  SecA_Q9 = m.secA_Q9,
                                  SecA_Q9Remark = m.secA_Q9Remark,
                                  SecA_Q9CeoComment = m.secA_Q9CeoComment,
                                  SecA_Q10 = m.secA_Q10,
                                  SecA_Q10Remark = m.secA_Q10Remark,
                                  SecA_Q10CeoComment = m.secA_Q10CeoComment,
                                  SecA_Q11 = m.secA_Q11,
                                  SecA_Q11Remark = m.secA_Q11Remark,
                                  SecA_Q11CeoComment = m.secA_Q11CeoComment,
                                  SecA_Q12 = m.secA_Q12,
                                  SecA_Q12Remark = m.secA_Q12Remark,
                                  SecA_Q12CeoComment = m.secA_Q12CeoComment,
                                  SecA_Q13 = m.secA_Q13,
                                  SecA_Q13Remark = m.secA_Q13Remark,
                                  SecA_Q13CeoComment = m.secA_Q13CeoComment,
                                  SecA_Q14 = m.secA_Q14,
                                  SecA_Q14Remark = m.secA_Q14Remark,
                                  SecA_Q14CeoComment = m.secA_Q14CeoComment,
                                  SecA_Q15 = m.secA_Q15,
                                  SecA_Q15Remark = m.secA_Q15Remark,
                                  SecA_Q15CeoComment = m.secA_Q15CeoComment,
                                  SecA_Q16 = m.secA_Q16,
                                  SecA_Q16Remark = m.secA_Q16Remark,
                                  SecA_Q16CeoComment = m.secA_Q16CeoComment,
                                  SecA_Q17 = m.secA_Q17,
                                  SecA_Q17Remark = m.secA_Q17Remark,
                                  SecA_Q17CeoComment = m.secA_Q17CeoComment,
                                  SecA_Q18 = m.secA_Q18,
                                  SecA_Q18Remark = m.secA_Q18Remark,
                                  SecA_Q18CeoComment = m.secA_Q18CeoComment,
                                  SecA_Q19 = m.secA_Q19,
                                  SecA_Q19Remark = m.secA_Q19Remark,
                                  SecA_Q19CeoComment = m.secA_Q19CeoComment,
                                  SecA_Q20 = m.secA_Q20,
                                  SecA_Q20Remark = m.secA_Q20Remark,
                                  SecA_Q20CeoComment = m.secA_Q20CeoComment,
                                  SecA_Q21 = m.secA_Q21,
                                  SecA_Q21Remark = m.secA_Q21Remark,
                                  SecA_Q21CeoComment = m.secA_Q21CeoComment,
                                  SecA_Q22 = m.secA_Q22,
                                  SecA_Q22Remark = m.secA_Q22Remark,
                                  SecA_Q22CeoComment = m.secA_Q22CeoComment,
                                  SecA_Q23 = m.secA_Q23,
                                  SecA_Q23Remark = m.secA_Q23Remark,
                                  SecA_Q23CeoComment = m.secA_Q23CeoComment,
                                  SecB_Q1 = m.secB_Q1,
                                  SecB_Q1Remark = m.secB_Q1Remark,
                                  SecB_Q1CeoComment = m.secB_Q1CeoComment,
                                  SecB_Q2 = m.secB_Q2,
                                  SecB_Q2Remark = m.secB_Q2Remark,
                                  SecB_Q2CeoComment = m.secB_Q2CeoComment,
                                  SecB_Q3 = m.secB_Q3,
                                  SecB_Q3Remark = m.secB_Q3Remark,
                                  SecB_Q3CeoComment = m.secB_Q3CeoComment,
                                  SecB_Q4 = m.secB_Q4,
                                  SecB_Q4Remark = m.secB_Q4Remark,
                                  SecB_Q4CeoComment = m.secB_Q4CeoComment,
                                  SecB_Q5 = m.secB_Q5,
                                  SecB_Q5Remark = m.secB_Q5Remark,
                                  SecB_Q5CeoComment = m.secB_Q5CeoComment,
                                  SecB_Q6 = m.secB_Q6,
                                  SecB_Q6Remark = m.secB_Q6Remark,
                                  SecB_Q6CeoComment = m.secB_Q6CeoComment,
                                  SecB_Q7 = m.secB_Q7,
                                  SecB_Q7Remark = m.secB_Q7Remark,
                                  SecB_Q7CeoComment = m.secB_Q7CeoComment,
                                  SecB_Q8 = m.secB_Q8,
                                  SecB_Q8Remark = m.secB_Q8Remark,
                                  SecB_Q8CeoComment = m.secB_Q8CeoComment,
                                  SecB_Q9CustId = m.secB_Q9CustId,
                                  SecB_Q9CustName = (y == null ? String.Empty : y.name + " (" + y.abbreviation + ")"),
                                  SecB_Q9StaffId = m.secB_Q9StaffId,
                                  SecB_Q9StaffName = (x == null ? String.Empty : x.firstName + " " + x.lastName),
                              }).FirstOrDefault();
            RiskReviewList.RiskReviewDetail.Role = Session["Role"].ToString();
            RiskReviewList.RoleId = Session["RoleId"].ToString();
            RiskReviewList.objCanEdit = (from m in db.Users_Opportunity
                                         where m.opportunityId == OpportunityId && m.userId == userid
                                         select new EditPermission()
                                         {
                                             canEdits = m.canEdit
                                         }).FirstOrDefault();

            var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
            var UserQuery = (from m in db.Users
                             join n in db.Users_Roles on m.userId equals n.userId into o
                             from p in o.DefaultIfEmpty()
                             where m.status == true
                             select new RiskReviewList()
                             {
                                 SecB_Q9StaffId = p.userId,
                                 SecB_Q9StaffName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                             }).OrderBy(c => c.SecB_Q9StaffName).AsEnumerable();
            ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
            ViewBag.Stafflist = new SelectList(UserQuery, "SecB_Q9StaffId", "SecB_Q9StaffName");

            return PartialView(RiskReviewList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult RiskReview(MainModel x)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("RiskReview", x);
            }
            int caseId = x.OppContId;
            int userid = x.CreateByUserId;
            x.RiskReviewDetail.Role = Session["Role"].ToString();

            RiskReview objRiskReviewdb = db.RiskReviews.First(m => m.opportunityId == caseId);            
            objRiskReviewdb.modifyById = userid;
            objRiskReviewdb.modifyDate = DateTime.Now;
            objRiskReviewdb.secA_Q1 = x.RiskReviewDetail.SecA_Q1;
            objRiskReviewdb.secA_Q1Remark = x.RiskReviewDetail.SecA_Q1Remark;
            objRiskReviewdb.secA_Q1CeoComment = x.RiskReviewDetail.SecA_Q1CeoComment;
            objRiskReviewdb.secA_Q2 = x.RiskReviewDetail.SecA_Q2;
            objRiskReviewdb.secA_Q2Remark = x.RiskReviewDetail.SecA_Q2Remark;
            objRiskReviewdb.secA_Q2CeoComment = x.RiskReviewDetail.SecA_Q2CeoComment;
            objRiskReviewdb.secA_Q3 = x.RiskReviewDetail.SecA_Q3;
            objRiskReviewdb.secA_Q3Remark = x.RiskReviewDetail.SecA_Q3Remark;
            objRiskReviewdb.secA_Q3CeoComment = x.RiskReviewDetail.SecA_Q3CeoComment;
            objRiskReviewdb.secA_Q4 = x.RiskReviewDetail.SecA_Q4;
            objRiskReviewdb.secA_Q4Remark = x.RiskReviewDetail.SecA_Q4Remark;
            objRiskReviewdb.secA_Q4CeoComment = x.RiskReviewDetail.SecA_Q4CeoComment;
            objRiskReviewdb.secA_Q5 = x.RiskReviewDetail.SecA_Q5;
            objRiskReviewdb.secA_Q5Remark = x.RiskReviewDetail.SecA_Q5Remark;
            objRiskReviewdb.secA_Q5CeoComment = x.RiskReviewDetail.SecA_Q5CeoComment;
            objRiskReviewdb.secA_Q6 = x.RiskReviewDetail.SecA_Q6;
            objRiskReviewdb.secA_Q6Remark = x.RiskReviewDetail.SecA_Q6Remark;
            objRiskReviewdb.secA_Q6CeoComment = x.RiskReviewDetail.SecA_Q6CeoComment;
            objRiskReviewdb.secA_Q7 = x.RiskReviewDetail.SecA_Q7;
            objRiskReviewdb.secA_Q7Remark = x.RiskReviewDetail.SecA_Q7Remark;
            objRiskReviewdb.secA_Q7CeoComment = x.RiskReviewDetail.SecA_Q7CeoComment;
            objRiskReviewdb.secA_Q8 = x.RiskReviewDetail.SecA_Q8;
            objRiskReviewdb.secA_Q8Remark = x.RiskReviewDetail.SecA_Q8Remark;
            objRiskReviewdb.secA_Q8CeoComment = x.RiskReviewDetail.SecA_Q8CeoComment;
            objRiskReviewdb.secA_Q9 = x.RiskReviewDetail.SecA_Q9;
            objRiskReviewdb.secA_Q9Remark = x.RiskReviewDetail.SecA_Q9Remark;
            objRiskReviewdb.secA_Q9CeoComment = x.RiskReviewDetail.SecA_Q9CeoComment;
            objRiskReviewdb.secA_Q10 = x.RiskReviewDetail.SecA_Q10;
            objRiskReviewdb.secA_Q10Remark = x.RiskReviewDetail.SecA_Q10Remark;
            objRiskReviewdb.secA_Q10CeoComment = x.RiskReviewDetail.SecA_Q10CeoComment;
            objRiskReviewdb.secA_Q11 = x.RiskReviewDetail.SecA_Q11;
            objRiskReviewdb.secA_Q11Remark = x.RiskReviewDetail.SecA_Q11Remark;
            objRiskReviewdb.secA_Q11CeoComment = x.RiskReviewDetail.SecA_Q11CeoComment;
            objRiskReviewdb.secA_Q12 = x.RiskReviewDetail.SecA_Q12;
            objRiskReviewdb.secA_Q12Remark = x.RiskReviewDetail.SecA_Q12Remark;
            objRiskReviewdb.secA_Q12CeoComment = x.RiskReviewDetail.SecA_Q12CeoComment;
            objRiskReviewdb.secA_Q13 = x.RiskReviewDetail.SecA_Q13;
            objRiskReviewdb.secA_Q13Remark = x.RiskReviewDetail.SecA_Q13Remark;
            objRiskReviewdb.secA_Q13CeoComment = x.RiskReviewDetail.SecA_Q13CeoComment;
            objRiskReviewdb.secA_Q14 = x.RiskReviewDetail.SecA_Q14;
            objRiskReviewdb.secA_Q14Remark = x.RiskReviewDetail.SecA_Q14Remark;
            objRiskReviewdb.secA_Q14CeoComment = x.RiskReviewDetail.SecA_Q14CeoComment;
            objRiskReviewdb.secA_Q15 = x.RiskReviewDetail.SecA_Q15;
            objRiskReviewdb.secA_Q15Remark = x.RiskReviewDetail.SecA_Q15Remark;
            objRiskReviewdb.secA_Q15CeoComment = x.RiskReviewDetail.SecA_Q15CeoComment;
            objRiskReviewdb.secA_Q16 = x.RiskReviewDetail.SecA_Q16;
            objRiskReviewdb.secA_Q16Remark = x.RiskReviewDetail.SecA_Q16Remark;
            objRiskReviewdb.secA_Q16CeoComment = x.RiskReviewDetail.SecA_Q16CeoComment;
            objRiskReviewdb.secA_Q17 = x.RiskReviewDetail.SecA_Q17;
            objRiskReviewdb.secA_Q17Remark = x.RiskReviewDetail.SecA_Q17Remark;
            objRiskReviewdb.secA_Q17CeoComment = x.RiskReviewDetail.SecA_Q17CeoComment;
            objRiskReviewdb.secA_Q18 = x.RiskReviewDetail.SecA_Q18;
            objRiskReviewdb.secA_Q18Remark = x.RiskReviewDetail.SecA_Q18Remark;
            objRiskReviewdb.secA_Q18CeoComment = x.RiskReviewDetail.SecA_Q18CeoComment;
            objRiskReviewdb.secA_Q19 = x.RiskReviewDetail.SecA_Q19;
            objRiskReviewdb.secA_Q19Remark = x.RiskReviewDetail.SecA_Q19Remark;
            objRiskReviewdb.secA_Q19CeoComment = x.RiskReviewDetail.SecA_Q19CeoComment;
            objRiskReviewdb.secA_Q20 = x.RiskReviewDetail.SecA_Q20;
            objRiskReviewdb.secA_Q20Remark = x.RiskReviewDetail.SecA_Q20Remark;
            objRiskReviewdb.secA_Q20CeoComment = x.RiskReviewDetail.SecA_Q20CeoComment;
            objRiskReviewdb.secA_Q21 = x.RiskReviewDetail.SecA_Q21;
            objRiskReviewdb.secA_Q21Remark = x.RiskReviewDetail.SecA_Q21Remark;
            objRiskReviewdb.secA_Q21CeoComment = x.RiskReviewDetail.SecA_Q21CeoComment;
            objRiskReviewdb.secA_Q22 = x.RiskReviewDetail.SecA_Q22;
            objRiskReviewdb.secA_Q22Remark = x.RiskReviewDetail.SecA_Q22Remark;
            objRiskReviewdb.secA_Q22CeoComment = x.RiskReviewDetail.SecA_Q22CeoComment;
            objRiskReviewdb.secA_Q23 = x.RiskReviewDetail.SecA_Q23;
            objRiskReviewdb.secA_Q23Remark = x.RiskReviewDetail.SecA_Q23Remark;
            objRiskReviewdb.secA_Q23CeoComment = x.RiskReviewDetail.SecA_Q23CeoComment;
            objRiskReviewdb.secB_Q1 = x.RiskReviewDetail.SecB_Q1;
            objRiskReviewdb.secB_Q1Remark = x.RiskReviewDetail.SecB_Q1Remark;
            objRiskReviewdb.secB_Q1CeoComment = x.RiskReviewDetail.SecB_Q1CeoComment;
            objRiskReviewdb.secB_Q2 = x.RiskReviewDetail.SecB_Q2;
            objRiskReviewdb.secB_Q2Remark = x.RiskReviewDetail.SecB_Q2Remark;
            objRiskReviewdb.secB_Q2CeoComment = x.RiskReviewDetail.SecB_Q2CeoComment;
            objRiskReviewdb.secB_Q3 = x.RiskReviewDetail.SecB_Q3;
            objRiskReviewdb.secB_Q3Remark = x.RiskReviewDetail.SecB_Q3Remark;
            objRiskReviewdb.secB_Q3CeoComment = x.RiskReviewDetail.SecB_Q3CeoComment;
            objRiskReviewdb.secB_Q4 = x.RiskReviewDetail.SecB_Q4;
            objRiskReviewdb.secB_Q4Remark = x.RiskReviewDetail.SecB_Q4Remark;
            objRiskReviewdb.secB_Q4CeoComment = x.RiskReviewDetail.SecB_Q4CeoComment;
            objRiskReviewdb.secB_Q5 = x.RiskReviewDetail.SecB_Q5;
            objRiskReviewdb.secB_Q5Remark = x.RiskReviewDetail.SecB_Q5Remark;
            objRiskReviewdb.secB_Q5CeoComment = x.RiskReviewDetail.SecB_Q5CeoComment;
            objRiskReviewdb.secB_Q6 = x.RiskReviewDetail.SecB_Q6;
            objRiskReviewdb.secB_Q6Remark = x.RiskReviewDetail.SecB_Q6Remark;
            objRiskReviewdb.secB_Q6CeoComment = x.RiskReviewDetail.SecB_Q6CeoComment;
            objRiskReviewdb.secB_Q7 = x.RiskReviewDetail.SecB_Q7;
            objRiskReviewdb.secB_Q7Remark = x.RiskReviewDetail.SecB_Q7Remark;
            objRiskReviewdb.secB_Q7CeoComment = x.RiskReviewDetail.SecB_Q7CeoComment;
            objRiskReviewdb.secB_Q8 = x.RiskReviewDetail.SecB_Q8;
            objRiskReviewdb.secB_Q8Remark = x.RiskReviewDetail.SecB_Q8Remark;
            objRiskReviewdb.secB_Q8CeoComment = x.RiskReviewDetail.SecB_Q8CeoComment;
            objRiskReviewdb.secB_Q9Networking = x.RiskReviewDetail.SecB_Q9Networking;
            objRiskReviewdb.secB_Q9StaffId = x.RiskReviewDetail.SecB_Q9StaffId;
            objRiskReviewdb.secB_Q9CustId = x.RiskReviewDetail.SecB_Q9CustId;
            db.SaveChanges();

            //OppoDetailListModel OppoDetailList = new OppoDetailListModel();
            x.objCanEdit = (from m in db.Users_Opportunity
                            where m.opportunityId == x.OppContId && m.userId == userid
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();
            try
            {
                x.SaveSuccess = true;
                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                Opportunity objOppoDetaildb = db.Opportunities.First(m => m.opportunityId == caseId);
                _objSendMessage.content = "updates " + objOppoDetaildb.name + " risk review";
                _objSendMessage.flag = 0;
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                x.SaveSuccess = false;
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

            var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
            var UserQuery = (from m in db.Users
                             join n in db.Users_Roles on m.userId equals n.userId into o
                             from p in o.DefaultIfEmpty()
                             where m.status == true
                             select new RiskReviewList()
                             {
                                 SecB_Q9StaffId = p.userId,
                                 SecB_Q9StaffName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                             }).OrderBy(c => c.SecB_Q9StaffName).AsEnumerable();
            ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
            ViewBag.Stafflist = new SelectList(UserQuery, "SecB_Q9StaffId", "SecB_Q9StaffName");

            return PartialView(x);
        }
        #endregion
        #endregion                    
    }
}
