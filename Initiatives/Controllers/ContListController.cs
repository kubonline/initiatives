﻿using System;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Data;

namespace Initiatives.Controllers
{
    public class ContListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();

        #region Create Contract
        [Authorize]
        public ActionResult CreateContract()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");

                return PartialView();
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult CreateContract(MainModel MM)
        {
            if (MM.OppoListDetail.StartDate == null)
            {
                ModelState.AddModelError("OppoListDetail.StartDate", "Please insert Start Date before proceed.");
            }
            if (MM.OppoListDetail.EndDate == null)
            {
                ModelState.AddModelError("OppoListDetail.EndDate", "Please insert Start Date before proceed.");
            }            
            if (!ModelState.IsValid)
            {
                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                var StatusQuery = db.OpportunityStatus.Select(c => new { c.statusId, c.status }).OrderBy(c => c.statusId);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");
                ViewBag.StatusList = new SelectList(StatusQuery.AsEnumerable(), "statusId", "status");

                return PartialView("CreateContract", MM);
            }
            Opportunity _objCreateOpportunity = new Opportunity
            {
                uuid = Guid.NewGuid(),
                supplierId = Int32.Parse(Session["CompanyId"].ToString()),
                custId = MM.OppoListDetail.CustId,
                typeId = MM.OppoListDetail.OppoTypeId,
                catId = MM.OppoListDetail.CatId,
                name = MM.OppoListDetail.OpportunityName,
                description = MM.OppoListDetail.Description,
                assignToId = MM.OppoListDetail.AccManagerId,
                value = MM.OppoListDetail.OpportunityValue,
                //budgetaryRevenue = MM.OppoListDetail.BudgetaryRevenue,
                statusId = "OS007",
                contractNo = MM.OppoListDetail.ContractNo,
                startDate = MM.OppoListDetail.StartDate,
                endDate = MM.OppoListDetail.EndDate,
                createByUserId = Int32.Parse(Session["UserId"].ToString()),
                active = true
            };
            db.Opportunities.Add(_objCreateOpportunity);
            db.SaveChanges();

            Users_Opportunity _objUsersOpportunity = new Users_Opportunity
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreateOpportunity.opportunityId,
                userId = Int32.Parse(Session["UserId"].ToString()),
                canView = true,
                canEdit = true,
                assignTo = true,
                remark = "Contract Creator"
            };
            db.Users_Opportunity.Add(_objUsersOpportunity);

            if (_objCreateOpportunity.createByUserId != _objCreateOpportunity.assignToId)
            {
                Users_Opportunity _objUserAssignedOpportunity = new Users_Opportunity
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = _objCreateOpportunity.opportunityId,
                    userId = _objCreateOpportunity.assignToId,
                    canView = true,
                    canEdit = true,
                    assignTo = true,
                    remark = "Team Leader"
                };
                db.Users_Opportunity.Add(_objUserAssignedOpportunity);
            }

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreateOpportunity.opportunityId,
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "create " + _objCreateOpportunity.name + " contract",
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);

            db.SaveChanges();

            return RedirectToAction("ContractList", "ContList");
        }
        #endregion

        #region Contract Listing
        [Authorize]
        public ActionResult ContractList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserName = Session["Username"].ToString(),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.ContListObject = DBQueryController.getContractInfo(model.CompanyId, model.UserName, model.RoleId);

                return View("ContractList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region Contract Tabs
        [HttpPost]
        public JsonResult ViewContractTabs(string cid)
        {
            Session["ContractId"] = cid;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewContract", "ContList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult ViewContract(string id)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (id != null && User.Identity.IsAuthenticated)
                {
                    Session["ContractId"] = id;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int contractId = Int32.Parse(Session["ContractId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                model.OppContId = contractId;
                model.UserName = Session["Username"].ToString();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult POList(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, OppContId, model.CompanyId,"");

                return PartialView("POList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        #region PO Expected
        [Authorize]
        public ActionResult POExpected(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int userId = Int32.Parse(Session["UserId"].ToString());
                MainModel POExpDetail = new MainModel();
                Opportunity _objOpportunitydb = db.Opportunities.First(m => m.opportunityId == OppContId);
                POExpDetail.POExpectedListObject = DBQueryController.getPOExpected(_objOpportunitydb.startDate.Value, _objOpportunitydb.endDate.Value, OppContId);
                POExpDetail.RoleId = Session["RoleId"].ToString();
                POExpDetail.objCanEdit = (from m in db.Users_Opportunity
                                          where m.opportunityId == OppContId && m.userId == userId
                                                        select new EditPermission()
                                                        {
                                                            canEdits = m.canEdit
                                                        }).FirstOrDefault();
                //POExpDetail.POExpectedObj.objCanEdit = test.objCanEdit;
                return PartialView(POExpDetail);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult POExpected(MainModel MM)
        {
            try
            {
                int caseId = MM.OppContId;
                int userid = MM.UserId;
                string selectedYear = MM.POExpectedObj.SelectedYear;
            if (!ModelState.IsValid || selectedYear == null)
            {
                MM.objCanEdit = (from m in db.Users_Opportunity
                                                             where m.opportunityId == caseId && m.userId == userid
                                                             select new EditPermission()
                                                             {
                                                                 canEdits = m.canEdit
                                                             }).FirstOrDefault();
                //UserModel dsStaffResult = new UserModel();
                Opportunity _objOpportunitydb = db.Opportunities.First(m => m.opportunityId == caseId);
                MM.POExpectedListObject = DBQueryController.getPOExpected(_objOpportunitydb.startDate.Value, _objOpportunitydb.endDate.Value, caseId);

                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        exception = false,
                        view = this.RenderPartialView("POExpected", MM)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            //POExpectedList.ExpectedPO = Math.Round(Convert.ToDecimal(expectedPO), 2);
            var obPOExpecteddb = (from m in db.POExpecteds
                                  where m.opportunityId == caseId && m.yearMonth == selectedYear
                                  select new POExpectedField()
                                         {
                                             SelectedYear = m.yearMonth,
                                             ExpectedPO = m.expectedAmount.Value,
                                         }).SingleOrDefault();
           
                if (obPOExpecteddb == null)
                {
                    POExpected _objAddPOExpected = new POExpected
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = caseId,
                        yearMonth = selectedYear,
                        expectedAmount = MM.POExpectedObj.ExpectedPO,
                        createdById = Int32.Parse(Session["UserId"].ToString()),
                        createdDate = DateTime.Now
                    };
                    db.POExpecteds.Add(_objAddPOExpected);

                    Notification _objPOExpectedChange = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = caseId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "update PO expected for year " + selectedYear + " and its amount to "
                        + MM.POExpectedObj.ExpectedPO,
                        flag = 0
                    };
                    db.Notifications.Add(_objPOExpectedChange);
                    db.SaveChanges();

                    Opportunity _objOpportunitydb = db.Opportunities.First(m => m.opportunityId == caseId);
                    MM.POExpectedListObject = DBQueryController.getPOExpected(_objOpportunitydb.startDate.Value, _objOpportunitydb.endDate.Value, caseId);
                    MM.objCanEdit = (from m in db.Users_Opportunity
                                                                 where m.opportunityId == caseId && m.userId == userid
                                                                 select new EditPermission()
                                                                 {
                                                                     canEdits = m.canEdit
                                                                 }).FirstOrDefault();
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = true,
                            exception = false,
                            view = this.RenderPartialView("POExpected", MM)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            exception = false,
                            view = this.RenderPartialView("POExpected", MM)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            //catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            catch
            {
                //Exception raise = dbEx;
                //foreach (var validationErrors in dbEx.EntityValidationErrors)
                //{
                //    foreach (var validationError in validationErrors.ValidationErrors)
                //    {
                //        string message = string.Format("{0}:{1}",
                //            validationErrors.Entry.Entity.ToString(),
                //            validationError.ErrorMessage);
                //        raise = new InvalidOperationException(message, raise);
                //    }
                //}
                //throw raise;
                return Json(new { success = false, exception = true });
            }
        }
        #endregion

        #region PertinentInfo
        [Authorize]
        public ActionResult PertinentInfo(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel ContractDetailList = new MainModel
                {
                    OppContId = OppContId,
                    CreateByUserId = userid,
                    OppoListDetail = (from m in db.Opportunities
                                      join n in db.Users on m.createByUserId equals n.userId into s
                                      join o in db.Customers on m.custId equals o.custId into t
                                      join p in db.OpportunityCategories on m.catId equals p.catId into u
                                      join q in db.OpportunityStatus on m.statusId equals q.statusId into v
                                      join r in db.OpportunityTypes on m.typeId equals r.typeId into w
                                      from x in s.DefaultIfEmpty()
                                      from y in t.DefaultIfEmpty()
                                      from z in u.DefaultIfEmpty()
                                      from aa in v.DefaultIfEmpty()
                                      from ab in w.DefaultIfEmpty()
                                      where m.opportunityId == OppContId
                                      select new OppoDetailList()
                                      {
                                          CustId = m.custId,
                                          CustName = (y == null ? String.Empty : y.name + " (" + y.abbreviation + ")"),
                                          OppoTypeId = m.typeId,
                                          OppoType = (ab == null ? String.Empty : ab.type + " (" + y.abbreviation + ")"),
                                          CatId = m.catId,
                                          OppoCat = (z == null ? String.Empty : z.category),
                                          OpportunityName = m.name,
                                          Description = m.description,
                                          AccManagerId = m.assignToId,
                                          AccManagerName = (x == null ? String.Empty : x.firstName + " " + x.lastName),
                                          OpportunityValue = m.value.Value,
                                          Submission = m.proposalDueDate.Value,
                                          TargetAward = m.targetAward,
                                          StatusId = m.statusId,
                                          Status = (aa == null ? String.Empty : aa.status),
                                          ContractNo = m.contractNo,
                                          StartDate = m.startDate,
                                          EndDate = m.endDate,
                                          //BudgetaryRevenue = m.budgetaryRevenue.Value
                                      }).FirstOrDefault(),
                    RoleId = Session["RoleId"].ToString(),
                    objCanEdit = (from m in db.Users_Opportunity
                                  where m.opportunityId == OppContId && m.userId == userid
                                  select new EditPermission()
                                  {
                                      canEdits = m.canEdit
                                  }).FirstOrDefault()
                };

                var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
                var UserQuery = (from m in db.Users
                                 join n in db.Users_Roles on m.userId equals n.userId into o
                                 from p in o.DefaultIfEmpty()
                                 where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                                 select new OppoDetailList()
                                 {
                                     AccManagerId = p.userId,
                                     AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                                 }).OrderBy(c => c.AccManagerName).AsEnumerable();
                var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
                var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
                ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
                ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
                ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
                ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");

                return PartialView(ContractDetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult PertinentInfo(MainModel x)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("PertinentInfo", x);
            }
            int caseId = x.OppContId;
            int userid = Int32.Parse(Session["UserId"].ToString());
            Opportunity obContractDetaildb = db.Opportunities.First(m => m.opportunityId == caseId);

            if (obContractDetaildb.custId != x.OppoListDetail.CustId)
            {
                Notification _objCustomerChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                Customer _objCustomerdb = db.Customers.First(m => m.custId == obContractDetaildb.custId);
                Customer objCustomerdb = db.Customers.First(m => m.custId == x.OppoListDetail.CustId);
                _objCustomerChange.content = "change customer selection from " + _objCustomerdb.name + " to " + objCustomerdb.name;
                _objCustomerChange.flag = 0;
                db.Notifications.Add(_objCustomerChange);
                obContractDetaildb.custId = x.OppoListDetail.CustId;
            }
            if (obContractDetaildb.typeId != x.OppoListDetail.OppoTypeId)
            {
                Notification _objTypeChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                OpportunityType _objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == obContractDetaildb.typeId);
                OpportunityType objOppoTypedb = db.OpportunityTypes.First(m => m.typeId == x.OppoListDetail.OppoTypeId);
                _objTypeChange.content = "change contract type selection from " + _objOppoTypedb.type + " to " + objOppoTypedb.type;
                _objTypeChange.flag = 0;
                db.Notifications.Add(_objTypeChange);
                obContractDetaildb.typeId = x.OppoListDetail.OppoTypeId;
            }
            if (obContractDetaildb.catId != x.OppoListDetail.CatId)
            {
                Notification _objCatChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                OpportunityCategory _objOppoCatdb = db.OpportunityCategories.First(m => m.catId == obContractDetaildb.catId);
                OpportunityCategory objOppoCatdb = db.OpportunityCategories.First(m => m.catId == x.OppoListDetail.CatId);
                _objCatChange.content = "change contract category selection from " + _objOppoCatdb.category + " to " + objOppoCatdb.category;
                _objCatChange.flag = 0;
                db.Notifications.Add(_objCatChange);
                obContractDetaildb.catId = x.OppoListDetail.CatId;
            }
            if (obContractDetaildb.name != x.OppoListDetail.OpportunityName)
            {
                Notification _objNameChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change contract name from " + obContractDetaildb.name + " to " + x.OppoListDetail.OpportunityName,
                    flag = 0
                };
                db.Notifications.Add(_objNameChange);
                obContractDetaildb.name = x.OppoListDetail.OpportunityName;
            }
            if (obContractDetaildb.description != x.OppoListDetail.Description)
            {
                Notification _objDescChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change the contract description to " + x.OppoListDetail.Description,
                    flag = 0
                };
                db.Notifications.Add(_objDescChange);
                obContractDetaildb.description = x.OppoListDetail.Description;
            }
            if (obContractDetaildb.assignToId != x.OppoListDetail.AccManagerId)
            {
                Notification _objAssignToChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString())
                };
                User _objAssignTodb = db.Users.First(m => m.userId == obContractDetaildb.assignToId);
                User objAssignTodb = db.Users.First(m => m.userId == x.OppoListDetail.AccManagerId);
                _objAssignToChange.content = "change contract assigned to selection from " + _objAssignTodb.firstName + " "
                + _objAssignTodb.lastName + " to " + objAssignTodb.firstName + " " + objAssignTodb.lastName;
                _objAssignToChange.flag = 0;
                db.Notifications.Add(_objAssignToChange);
                obContractDetaildb.assignToId = x.OppoListDetail.AccManagerId;
            }
            if (obContractDetaildb.value != x.OppoListDetail.OpportunityValue)
            {
                Notification _objValueChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = "change the contract value from " + obContractDetaildb.value + " to " + x.OppoListDetail.OpportunityValue
                };
                db.Notifications.Add(_objValueChange);
                obContractDetaildb.value = x.OppoListDetail.OpportunityValue;
            }
            if (obContractDetaildb.contractNo != x.OppoListDetail.ContractNo)
            {
                Notification _objValueChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = obContractDetaildb.contractNo == null ? "change the opportunity no. to " + x.OppoListDetail.ContractNo : 
                    "change the contract number from " + obContractDetaildb.contractNo + " to " + x.OppoListDetail.ContractNo
                };
                db.Notifications.Add(_objValueChange);
                obContractDetaildb.contractNo = x.OppoListDetail.ContractNo;
            }
            if (obContractDetaildb.startDate != x.OppoListDetail.StartDate)
            {
                Notification _objSubmissionDateChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = "change the contract start date from " + obContractDetaildb.startDate + " to " + x.OppoListDetail.StartDate
                };
                db.Notifications.Add(_objSubmissionDateChange);
                obContractDetaildb.startDate = x.OppoListDetail.StartDate;
            }
            if (obContractDetaildb.endDate != x.OppoListDetail.EndDate)
            {
                Notification _objSubmissionDateChange = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = caseId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    flag = 0,
                    content = "change the contract end date from " + obContractDetaildb.endDate + " to " + x.OppoListDetail.EndDate
                };
                db.Notifications.Add(_objSubmissionDateChange);
                obContractDetaildb.endDate = x.OppoListDetail.EndDate;
            }
            //obContractDetaildb.budgetaryRevenue = x.OppoListDetail.BudgetaryRevenue;
            obContractDetaildb.proposalDueDate = x.OppoListDetail.Submission;
            db.SaveChanges();

            x.objCanEdit = (from m in db.Users_Opportunity
                            where m.opportunityId == x.OppContId && m.userId == userid
                            select new EditPermission()
                            {
                                canEdits = m.canEdit
                            }).FirstOrDefault();
            try
            {
                db.SaveChanges();
                x.SaveSuccess = true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                x.SaveSuccess = false;
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            var CustomersQuery = db.Customers.Select(c => new { c.custId, custName = c.name + " (" + c.abbreviation + ")" }).OrderBy(c => c.custName);
            var UserQuery = (from m in db.Users
                             join n in db.Users_Roles on m.userId equals n.userId into o
                             from p in o.DefaultIfEmpty()
                             where m.status == true && (p.roleId.Trim() == "R03" || p.roleId.Trim() == "R09" || p.roleId.Trim() == "R10")
                             select new OppoDetailList()
                             {
                                 AccManagerId = p.userId,
                                 AccManagerName = (m == null ? String.Empty : m.firstName + " " + m.lastName),
                             }).OrderBy(c => c.AccManagerName).AsEnumerable();
            var OppoTypeQuery = db.OpportunityTypes.Select(c => new { c.typeId, c.type }).OrderBy(c => c.typeId);
            var CategoryQuery = db.OpportunityCategories.Select(c => new { c.catId, c.category }).OrderBy(c => c.catId);
            ViewBag.Customerlist = new SelectList(CustomersQuery.AsEnumerable(), "custId", "custName");
            ViewBag.Managerlist = new SelectList(UserQuery, "AccManagerId", "AccManagerName");
            ViewBag.OppoTypeList = new SelectList(OppoTypeQuery.AsEnumerable(), "typeId", "type");
            ViewBag.CategoryList = new SelectList(CategoryQuery.AsEnumerable(), "catId", "category");

            return PartialView(x);
        }
        #endregion

        #region PO Reports
        [Authorize]
        public ActionResult POReport(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.POReportListObject = DBQueryController.getPOReportList(OppContId, 123);

                return PartialView("POReport", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region Team Member
        [Authorize]
        public ActionResult TeamMember(int OppContId)
        {
            if (User.Identity.IsAuthenticated)
            {
                int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel TeamMemberDetail = new MainModel
                {
                    TeamMemberList = (from m in db.Users_Opportunity
                                      join n in db.Users on m.userId equals n.userId
                                      //into o from x in o.DefaultIfEmpty()
                                      where m.opportunityId == OppContId && n.status == true
                                      orderby m.assignTo descending
                                      select new TeamMember()
                                      {
                                          ContractId = m.opportunityId,
                                          UserId = m.userId,
                                          UserName = m.User.firstName + " " + m.User.lastName,
                                          Remark = m.remark,
                                          canView = m.canView,
                                          canEdit = m.canEdit
                                      }).OrderBy(c => c.UserName).ToList(),
                    RoleId = Session["RoleId"].ToString(),
                    ViewEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == OppContId && m.userId == userid
                                select new TeamPermission()
                                {
                                    canView = m.canView,
                                    canEdit = m.canEdit
                                }).FirstOrDefault()
                };

                OppoDetailList objOppoDetail = new OppoDetailList();

                var itemIds = TeamMemberDetail.TeamMemberList.Select(x => x.UserId).ToArray();
                var UserQuery = db.Users
                    .Select(c => new { c.userId, FullName = c.firstName + " " + c.lastName, c.status })
                    .Where(c => c.status == true)
                    .Where(c => !itemIds.Contains(c.userId))
                    .OrderBy(c => c.FullName);

                ViewBag.TeamMemberList = new SelectList(UserQuery.AsEnumerable(), "userId", "FullName", Convert.ToString(objOppoDetail.AccManagerId));
                ViewBag.TeamMemberRemoveList = new SelectList(TeamMemberDetail.TeamMemberList.AsEnumerable(), "UserId", "UserName", Convert.ToString(objOppoDetail.AccManagerId));
                return PartialView(TeamMemberDetail);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public JsonResult TeamPermission(string[][] permissions, string OppContId)
        {
            int ContractId = Int32.Parse(OppContId);
            var TeamMemberList = (from m in db.Users_Opportunity
                                  join n in db.Users on m.userId equals n.userId
                                  where m.opportunityId == ContractId
                                  orderby n.firstName
                                  select m).ToList();
            int i = 0;
            TeamMemberList.ForEach(field =>
            {
                field.remark = permissions[i][2];
                field.canView = Convert.ToBoolean(permissions[i][3]);
                field.canEdit = Convert.ToBoolean(permissions[i][4]);
                i++;
            });

            db.SaveChanges();

            return Json(new { Result = String.Format("Test berjaya!") });
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddTeamMember(string UserId, string OppContId)
        {
            if (User.Identity.IsAuthenticated)
            {
                int ContractId = Int32.Parse(OppContId);
                int UserID = Int32.Parse(UserId);
                Users_Opportunity _objAddMember = new Users_Opportunity
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = ContractId,
                    userId = UserID,
                    canView = true,
                    canEdit = false
                };
                db.Users_Opportunity.Add(_objAddMember);

                //var SuperiorId = (from m in db.Users
                //                  where m.userId == UserID
                //                  select m).FirstOrDefault();
                //if (SuperiorId.oneLevelSuperiorId != null)
                //{
                //    var checkSuperior = (from m in db.Users_Opportunity
                //                         where m.userId == SuperiorId.oneLevelSuperiorId && m.opportunityId == ContractId
                //                         select m).FirstOrDefault();
                //    if (checkSuperior == null)
                //    {
                //        //string _objAddSuperior = "_objAddMember" +i;
                //        Users_Opportunity _objAddSuperior = new Users_Opportunity
                //        {
                //            uuid = Guid.NewGuid(),
                //            opportunityId = ContractId,
                //            userId = SuperiorId.oneLevelSuperiorId.Value,
                //            canView = true,
                //            canEdit = false
                //        };
                //        db.Users_Opportunity.Add(_objAddSuperior);
                //    }
                //}
                db.SaveChanges();

                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult RemoveTeamMember(string UserId, string OppContId)
        {
            if (User.Identity.IsAuthenticated)
            {
                int ContractId = Int32.Parse(OppContId);
                int UserID = Int32.Parse(UserId);
                var RemoveTeamMember = db.Users_Opportunity.Where(x => x.userId == UserID && x.opportunityId == ContractId).ToList();
                foreach (var item in RemoveTeamMember)
                {
                    db.Users_Opportunity.Remove(item);
                    db.SaveChanges();
                }

                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion        
        public ActionResult TNIList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                //model.TNIListObject = DBQueryController.getPOList(model.UserName, model.RoleId, Session["ContractId"].ToString(), model.CompanyId);

                return PartialView("TNIList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public ActionResult IOList(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.IOListObject = DBQueryController.getIOList("IOList", model.UserId, model.RoleId, OppContId, model.CompanyId, 0);

                return PartialView("IOList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public ActionResult PRList(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.PRListObject = DBQueryController.getPRList("PRList", model.UserId, model.RoleId, OppContId, model.CompanyId, 0);

                return PartialView("PRList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public ActionResult DeliveryList(int OppContId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.DOListObject = DBQueryController.getDOList("DOList", model.UserId, model.RoleId, OppContId, model.CompanyId, 0);

                return PartialView("DeliveryList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion
        //#region DocManagement
        //[Authorize]
        //public ActionResult DocManagement(string OppContId)
        //{
        //    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
        //    {
        //        MainModel model = new MainModel();
        //        int userid = Int32.Parse(Session["UserId"].ToString());
        //        model.RoleId = Session["RoleId"].ToString();
        //        int cId = Int32.Parse(OppContId);
        //        model.ContDocListObject = (from m in db.FileUploads
        //                                   join n in db.Users on m.uploadUserId equals n.userId
        //                                   where m.opportunityId == cId && m.archivedFlag == false && m.notPO == true
        //                                   select new FilePODetailInfo()
        //                                   {
        //                                       opportunityId = m.opportunityId,
        //                                       fileEntryId = m.fileEntryId,
        //                                       uploadUserName = n.firstName + " " + n.lastName,
        //                                       uploadDate = m.uploadDate,
        //                                       FileName = m.FileName,
        //                                       File = m.File,
        //                                       Description = m.description
        //                                   }).ToList();
        //        model.FileArchiveModel = (from m in db.FileUploads
        //                                  join n in db.Users on m.uploadUserId equals n.userId
        //                                  where m.opportunityId == cId && m.archivedFlag == true
        //                                  select new FilePODetailInfo()
        //                                  {
        //                                      opportunityId = m.opportunityId,
        //                                      fileEntryId = m.fileEntryId,
        //                                      uploadUserName = n.firstName + " " + n.lastName,
        //                                      uploadDate = m.uploadDate,
        //                                      FileName = m.FileName,
        //                                      File = m.File,
        //                                      Description = m.description
        //                                  }).ToList();

        //        return PartialView(model);
        //    }
        //    else
        //    {
        //        return Redirect("~/Home/Index");
        //    }
        //}
        //public FileContentResult FileDownload(int id)
        //{
        //    byte[] fileData;

        //    FileUpload fileRecord = db.FileUploads.Find(id);

        //    string fileName;
        //    string filename1 = fileRecord.FileName.Replace("\\", ",");
        //    string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
        //    fileData = (byte[])fileRecord.File.ToArray();
        //    fileName = filename2;

        //    return File(fileData, "text", fileName);
        //}
        //public JsonResult FileArchive(string selectedId)
        //{
        //    int id = Int32.Parse(selectedId);

        //    FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
        //    if (itemToArchive != null)
        //    {
        //        itemToArchive.archivedFlag = true;
        //        db.SaveChanges();
        //    }

        //    return Json(new { Result = String.Format("Test berjaya!") });
        //}
        //#endregion

        //#region QNA
        //[Authorize]
        //public ActionResult QuestionAnswer(string OppContId)
        //{
        //    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
        //    {
        //        int ContractId = Int32.Parse(OppContId);
        //        int Uid = Int32.Parse(Session["UserId"].ToString());

        //        MainModel QNAModelList = new MainModel();
        //        QNAModelList.MessageListModel = DBQueryController.getMessageList(ContractId, 0, Uid);
        //        QNAModelList.UserId = Uid;

        //        var NotiMemberList = (from m in db.Users
        //                              join n in db.Users_Opportunity on m.userId equals n.userId into gy
        //                              from x in gy.DefaultIfEmpty()
        //                              where m.status == true && x.opportunityId == ContractId
        //                              select new NotiMemberList()
        //                              {
        //                                  Id = m.userId,
        //                                  Name = m.firstName + " " + m.lastName
        //                              }).ToList().OrderBy(c => c.Name);

        //        ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

        //        return PartialView(QNAModelList);
        //    }
        //    else
        //    {
        //        return Redirect("~/Home/Index");
        //    }
        //}
        //public virtual ActionResult DocUpload(MainModel fd)
        //{
        //    FileUpload fileUploadModel = new FileUpload();
        //    int userId = fd.UserId;
        //    DateTime createDate = DateTime.Now;
        //    string filename1 = fd.file.FileName.Replace("\\", ",");
        //    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
        //    string fullPath = fd.file.FileName;
        //    string extension = filename.Split('.')[1].ToString();
        //    byte[] uploadFile = new byte[fd.file.InputStream.Length];

        //    fileUploadModel.uuid = Guid.NewGuid();
        //    fileUploadModel.opportunityId = fd.OppContId;
        //    fileUploadModel.uploadUserId = userId;
        //    fileUploadModel.uploadDate = createDate;
        //    fileUploadModel.FullPath = fullPath;
        //    fileUploadModel.FileName = filename;
        //    fileUploadModel.description = fd.FileDescription;
        //    fileUploadModel.Extension = extension;
        //    fileUploadModel.archivedFlag = false;
        //    fileUploadModel.notPO = true;

        //    fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
        //    fileUploadModel.File = uploadFile;
        //    db.FileUploads.Add(fileUploadModel);

        //    Notification _objSendMessage = new Notification();
        //    _objSendMessage.uuid = Guid.NewGuid();
        //    _objSendMessage.opportunityId = Int32.Parse(Session["ContractId"].ToString());
        //    _objSendMessage.createDate = DateTime.Now;
        //    _objSendMessage.fromUserId = Int32.Parse(Session["UserId"].ToString());
        //    _objSendMessage.content = "uploaded " + filename;
        //    _objSendMessage.flag = 0;
        //    db.Notifications.Add(_objSendMessage);

        //    db.SaveChanges();

        //    //return PartialView(db.FileUploads.ToList());
        //    return Json(new { Result = String.Format("Test berjaya!") });
        //}

        //[Authorize]
        //public JsonResult SendMessage(string UserId, string OppContId, string Content, List<int> selectedUserId)
        //{
        //    int contractId = Int32.Parse(OppContId);
        //    int fromUserID = Int32.Parse(UserId);
        //    var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

        //    Notification _objSendMessage = new Notification();
        //    _objSendMessage.uuid = Guid.NewGuid();
        //    _objSendMessage.opportunityId = contractId;
        //    _objSendMessage.POId = null;
        //    _objSendMessage.createDate = DateTime.Now;
        //    _objSendMessage.fromUserId = fromUserID;
        //    _objSendMessage.content = Content;
        //    _objSendMessage.QNAtype = "Message";
        //    _objSendMessage.flag = 0;
        //    db.Notifications.Add(_objSendMessage);
        //    db.SaveChanges();

        //    foreach (var model in selectedUserId)
        //    {
        //        //toUserId = Int32.Parse(((string[])(selectedUserId))[i].Split(',')[0]);
        //        var UserEmail = db.Users.SingleOrDefault(x => x.userId == model);
        //        var ContractInfo = (from m in db.Opportunities
        //                            join n in db.Users on m.createByUserId equals n.userId
        //                            join o in db.Customers on m.custId equals o.custId
        //                            join p in db.OpportunityTypes on m.typeId equals p.typeId
        //                            where m.opportunityId == contractId
        //                            select new MailModel()
        //                            {
        //                                CustName = o.name + " (" + o.abbreviation + ")",
        //                                CustAbbreviation = o.abbreviation,
        //                                ContractName = m.name,
        //                                Description = m.name,
        //                                ContractType = p.type,
        //                                AssignTo = n.firstName + " " + n.lastName
        //                            }).FirstOrDefault();

        //        string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/ContractEmailTemplate.cshtml"));
        //        ContractInfo.Content = Content;
        //        ContractInfo.FromName = myEmail.emailAddress;
        //        ContractInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/ContList/ViewContract") + "?id=" + contractId);
        //        var result =
        //                Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "ContractTemplateKey", null, ContractInfo);

        //        MailMessage mail = new MailMessage();
        //        mail.From = new MailAddress("info@kubtel.com");
        //        //mail.From = new MailAddress("info@kubtel.com");
        //        mail.To.Add(new MailAddress(UserEmail.emailAddress));
        //        //mail.To.Add(new MailAddress("info@kubtel.com"));
        //        mail.Subject = "OCM: Updates on " + ContractInfo.ContractName + " for " + ContractInfo.CustAbbreviation;
        //        mail.Body = result;
        //        mail.IsBodyHtml = true;
        //        SmtpClient smtp = new SmtpClient();
        //        smtp.Host = "outlook.office365.com";
        //        //smtp.Host = "smtp.gmail.com";
        //        smtp.Port = 587;
        //        //smtp.Port = 25;
        //        smtp.EnableSsl = true;
        //        smtp.Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$");
        //        //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
        //        smtp.Send(mail);

        //        NotiGroup _objSendToUserId = new NotiGroup();
        //        _objSendToUserId.uuid = Guid.NewGuid();
        //        _objSendToUserId.chatId = _objSendMessage.chatId;
        //        _objSendToUserId.toUserId = model;
        //        db.NotiGroups.Add(_objSendToUserId);
        //    }
        //    db.SaveChanges();

        //    return Json(new { Result = String.Format("Test berjaya!") });
        //}
        //#endregion
    }
}
