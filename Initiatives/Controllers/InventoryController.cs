﻿using Initiatives.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Initiatives.Controllers
{
    public class ValidateAjaxAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
                return;

            var modelState = filterContext.Controller.ViewData.ModelState;
            var InvAddNewReq = filterContext.ActionParameters.Values.FirstOrDefault() as InventoryDetailInfo;
            var getValidationAddNewStock = filterContext.HttpContext.Session["AddNewStock"].ToString();
            var getValidationAddNewReq = filterContext.HttpContext.Session["AddNewReq"].ToString();

            if (InvAddNewReq != null && getValidationAddNewStock == "True")
            {
                modelState.Remove("");
            }

            if (InvAddNewReq != null && getValidationAddNewReq == "True")
            {
                modelState.Remove("Supplier");
                if (InvAddNewReq.RequestQuantity > InvAddNewReq.AvailableQuantity)
                {
                    modelState.AddModelError("RequestQuantity", "Request quantity cannot exceed available quantity.");
                }
            }

            if (!modelState.IsValid)
            {
                var errorModel =
                        from x in modelState.Keys
                        where modelState[x].Errors.Count > 0
                        select new
                        {
                            key = x,
                            errors = modelState[x].Errors.
                                                          Select(y => y.ErrorMessage).
                                                          ToArray()
                        };
                filterContext.Result = new JsonResult()
                {
                    Data = new
                    {
                        success = false,
                        exception = false,
                        data = errorModel
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                //filterContext.HttpContext.Response.StatusCode = 200;
            }
        }
    }
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/
        private OpportunityManagementEntities db = new OpportunityManagementEntities();

        [Authorize]
        public ActionResult Index()
        {
            MainModel model = new MainModel
            {
                InventoryListTable = (from m in db.PartNo_Items
                                          //join n in db.InventoryOuts on m.partNo equals n.partNo into p
                                          //join o in db.InventoryIns on m.partNo equals o.partNo into q
                                      select new InventoryDetailInfo()
                                      {
                                          PartNo = m.partNo,
                                          MaterialCode = m.materialNo,
                                          MaterialDesc = m.description,
                                          AvailableBalance = m.availableBalance,
                                          BalanceReserved = m.balanceReserved
                                      }).ToList()
            };

            return View(model);
        }

        [Authorize]
        public ActionResult AddNewStock()
        {
            var MaterialNoQuery = db.PartNo_Items.Select(c => new { PartNo = c.partNo, MaterialCode = c.materialNo })
                    .OrderBy(c => c.MaterialCode);
            var ItemTypeQuery = db.InvItemTypes.Select(c => new { ItemTypeId = c.itemTypeId, ItemType = c.itemType })
                    .OrderBy(c => c.ItemType);
            ViewBag.MaterialNolist = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialCode" /*,model.DODetailObject.BranchId*/);
            ViewBag.ItemTypelist = new SelectList(ItemTypeQuery.AsEnumerable(), "ItemTypeId", "ItemType" /*model.DODetailObject.BranchId*/);
            Session["AddNewStock"] = "True"; Session["AddNewReq"] = "False";

            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAjax]
        public ActionResult AddNewStock(InventoryDetailInfo model)
        {
            try
            {
                InventoryIn _objInventoryIn = new InventoryIn
                {
                    uuid = Guid.NewGuid(),
                    partNo = model.PartNo,
                    inDate = model.ReceivedDate,
                    inQuantity = model.QuantityReceived,
                    UOM = model.UOM,
                    unitPrice = model.UnitPrice,
                    itemTypeId = model.ItemTypeId,
                    supplier = model.Supplier,
                    purchasePONo = model.PurchasePONo
                };
                db.InventoryIns.Add(_objInventoryIn);
                db.SaveChanges();

                int inQuantity = 0; int outQuantity = 0;
                var inQuantityQuery = db.InventoryIns.Where(x => x.partNo == model.PartNo).ToList();
                var outQuantityQuery = db.InventoryOuts.Where(x => x.partNo == model.PartNo).ToList();

                if (inQuantityQuery != null)
                {
                    foreach (var item in inQuantityQuery)
                    {
                        inQuantity += item.inQuantity;
                    }
                }

                if (outQuantityQuery != null)
                {
                    foreach (var item in outQuantityQuery)
                    {
                        outQuantity += item.outQuantity;
                    }
                }

                PartNo_Items updateinQuantity = db.PartNo_Items.Where(x => x.partNo == model.PartNo).First();
                updateinQuantity.inQuantity = inQuantity;
                updateinQuantity.availableBalance = inQuantity - outQuantity;
                db.SaveChanges();

                return Json(new { success = true });
            }
            catch(DbEntityValidationException e)
            {
                StringWriter ExMessage = new StringWriter();
                foreach (var eve in e.EntityValidationErrors)
                {
                    ExMessage.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ExMessage.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                ExMessage.Close();
                return Json(new { success = false, exception = true, message = ExMessage.ToString() });
            }            
        }

        [Authorize]
        public ActionResult AddNewReq()
        {
            var MaterialNoQuery = db.PartNo_Items.Select(c => new { PartNo = c.partNo, MaterialCode = c.materialNo, AvailableBalance = c.availableBalance }).Where(c => c.AvailableBalance != 0)
                    .OrderBy(c => c.MaterialCode);
            var DONoQuery = db.DeliveryOrders.Select(c => new { DOId = c.DOId, DONo = c.DONo })
                    .OrderBy(c => c.DONo);
            var StatusQuery = db.InvStatus.Select(c => new { StatusId = c.statusId, Status = c.status })
                    .OrderBy(c => c.Status);
            var ProjectQuery = db.Opportunities.Select(c => new { ProjectId = c.opportunityId, Project = c.name }).OrderBy(c => c.Project);
            var PONoQuery = db.PurchaseOrders.Select(c => new { POId = c.POId, PONo = c.Details_PONo }).OrderBy(c => c.PONo);
            ViewBag.MaterialNolist = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialCode" /*,model.DODetailObject.BranchId*/);
            ViewBag.Project = new SelectList(ProjectQuery.AsEnumerable(), "ProjectId", "Project");
            ViewBag.ProjectPONo = new SelectList(PONoQuery.AsEnumerable(), "POId", "PONo");
            ViewBag.DeliveryOrderNo = new SelectList(DONoQuery.AsEnumerable(), "DOId", "DONo" /*,model.DODetailObject.BranchId*/);
            ViewBag.Status = new SelectList(StatusQuery.AsEnumerable(), "StatusId", "Status" /*,model.DODetailObject.BranchId*/);
            Session["AddNewReq"] = "True"; Session["AddNewStock"] = "False";

            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAjax]
        public ActionResult AddNewReq(InventoryDetailInfo model)
        {
            try
            {
                InventoryOut _objInventoryOut = new InventoryOut
                {
                    uuid = Guid.NewGuid(),
                    partNo = model.PartNo,
                    outDate = model.OutDate,
                    outQuantity = model.RequestQuantity,
                    POId = model.POId,
                    requestById = Int32.Parse(Session["UserId"].ToString()),
                    DOId = model.DOId,
                    statusId = model.StatusId,
                    remark = model.Remark
                };
                db.InventoryOuts.Add(_objInventoryOut);
                db.SaveChanges();

                PartNo_Items updateItems = db.PartNo_Items.Where(x => x.partNo == model.PartNo).First();
                updateItems.outQuantity = updateItems.outQuantity + model.RequestQuantity;
                db.SaveChanges();

                updateItems.availableBalance = updateItems.inQuantity - updateItems.outQuantity;
                db.SaveChanges();

                if (model.StatusId == 1)
                {
                    int balanceReservedQuantity = 0;
                    var balanceReservedQuery = db.InventoryOuts.Where(x => x.partNo == model.PartNo && x.statusId == 1).ToList();
                    if (balanceReservedQuery != null)
                    {
                        foreach (var item in balanceReservedQuery)
                        {
                            balanceReservedQuantity += item.outQuantity;
                        }
                    }
                    updateItems.balanceReserved = balanceReservedQuantity;
                    db.SaveChanges();
                }
               
                return Json(new { success = true });
            }
            catch (DbEntityValidationException e)
            {
                StringWriter ExMessage = new StringWriter();
                foreach (var eve in e.EntityValidationErrors)
                {
                    ExMessage.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ExMessage.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                ExMessage.Close();
                return Json(new { success = false, exception = true, message = ExMessage.ToString() });
            }
        }

        public JsonResult GetPartNoInfo(int PartNo)
        {
            var MaterialNoInfo = db.PartNo_Items.Select(m => new InventoryDetailInfo()
            {
                PartNo = m.partNo,
                MaterialDesc = m.description,
                AvailableQuantity = m.availableBalance //+ m.balanceReserved
                //BudgetedAmount = m.budgetedAmount,
                //UtilizedToDate = m.utilizedToDate,
                //BudgetBalance = m.budgetBalance
            }).Where(m => m.PartNo == PartNo).FirstOrDefault();

            return Json(new { desc = MaterialNoInfo.MaterialDesc, availableQty = MaterialNoInfo.AvailableQuantity }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectInfo(int ProjectId)
        {
            string html = "<select class='form-control-static' data-val-required='The ProjectPONo field is required.' id='ProjectPONo'>";

            if (ProjectId != 0)
            {
                var PONoQuery = (from m in db.Opportunities
                                 join n in db.PurchaseOrders on m.opportunityId equals n.opportunityId
                                 where m.opportunityId == ProjectId
                                 select new
                                 {
                                     POId = n.POId,
                                     PONo = n.Details_PONo
                                 }).ToList();
                foreach (var item in PONoQuery)
                {
                    html += "<option value='" + item.POId + "' >" + item.PONo + "</option>";
                }
            }
            html += "</select>";

            return Json(new { selectListPONo = html, }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckInvInfo(int PartNo)
        {
            var checkInvInfo = (from m in db.PartNo_Items
                                join n in db.InventoryIns on m.partNo equals n.partNo
                                join o in db.InventoryOuts on m.partNo equals o.partNo
                                where m.partNo == PartNo
                                select new InventoryDetailInfo()
                                {
                                    PartNo = m.partNo
                                }).FirstOrDefault();

            if (checkInvInfo != null)
            {
                Session["PartNo"] = PartNo;
                return Json(new { success = true });
            } else
            {
                return Json(new { success = false });
            }                        
        }

        public ActionResult InventoryInfo()
        {
            int PartNo = Int32.Parse(Session["PartNo"].ToString());
            MainModel InvInfo = new MainModel();
            InvInfo.InvInListTable = (from m in db.PartNo_Items
                       join n in db.InventoryIns on m.partNo equals n.partNo into p
                       from r in p.DefaultIfEmpty()
                       join t in db.InvItemTypes on r.itemTypeId equals t.itemTypeId                                              
                       where m.partNo == PartNo
                       select new InventoryDetailInfo()
                       {
                           PartNo = m.partNo,
                           MaterialCode = m.materialNo,
                           MaterialDesc = m.description,
                           ReceivedDate = r.inDate,
                           QuantityReceived = r.inQuantity,
                           UOM = r.UOM,
                           UnitPrice = r.unitPrice,
                           ItemType = t.itemType,
                           Supplier = r.supplier,
                           PurchasePONo = r.purchasePONo                                                     
                       }).ToList();

            InvInfo.InvOutListTable = (from m in db.PartNo_Items
                                       join n in db.InventoryOuts on m.partNo equals n.partNo into o
                                       from p in o.DefaultIfEmpty()
                                       join u in db.Users on p.requestById equals u.userId
                                       join v in db.InvStatus on p.statusId equals v.statusId
                                       join w in db.PurchaseOrders on p.POId equals w.POId
                                       join x in db.Opportunities on w.opportunityId equals x.opportunityId
                                       join y in db.DeliveryOrders on p.DOId equals y.DOId
                                       where m.partNo == PartNo
                                       select new InventoryDetailInfo()
                                       {
                                           Project = x.name,
                                           ProjectPONo = w.Details_PONo,
                                           RequestBy = u.firstName + " " + u.lastName,
                                           OutDate = p.outDate,
                                           DeliveryOrderNo = y.DONo,
                                           Status = v.status,
                                           Remark = p.remark,
                                           RequestQuantity = p.outQuantity
                                           //AvailableQuantity = m.availableBalance,
                                       }).ToList();

            return View(InvInfo);
        }
    }
}
