﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Net.Mail;
using System.Data;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;
using RazorPDF;

namespace Initiatives.Controllers
{
    public class IOListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();

        #region Create IO
        [HttpPost]
        public JsonResult NewIO(string ioid)
        {
            Session["IOId"] = ioid;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("CreateIO", "IOList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult CreateIO(string id)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (id != null && User.Identity.IsAuthenticated)
                {
                    Session["IOId"] = id;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int contractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.OppContId = contractId;
                model.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, model.OppContId, model.CompanyId, "IO");
                model.IOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.PartNo_Items on n.partNo equals o.partNo
                                          join p in db.IO_Items on n.itemsId equals p.itemsId into q
                                          from r in q.DefaultIfEmpty()
                                          where r.IOId == 0
                                          select new IOItemsTable()
                                          {
                                              ItemsId = n.itemsId,
                                              MaterialNo = o.materialNo,
                                              Description = o.description,
                                              DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                              RequestedQuantity = r.requestedQuantity,
                                              OutstandingQuantity = r.outstandingQuantity
                                          }).ToList();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult GetMaterialNoIO(MainModel MM)
        {
            List<IOItemsTable> newIOITemList = new List<IOItemsTable>();
            foreach (var value in MM.POListObject)
            {
                newIOITemList = newIOITemList.Concat(from m in db.PurchaseOrders
                                                     join n in db.PO_Item on m.POId equals n.POId
                                                     join o in db.PartNo_Items on n.partNo equals o.partNo
                                                     join p in db.IO_Items on n.itemsId equals p.itemsId into q
                                                     from r in q.DefaultIfEmpty()
                                                     where m.POId == value.POId && n.outstandingQuantity > 0
                                                     select new IOItemsTable()
                                                     {
                                                         POId = m.POId,
                                                         ItemsId = n.itemsId,
                                                         PONo = m.Details_PONo,
                                                         MaterialNo = o.materialNo,
                                                         Description = o.description,
                                                         DemandQuantity = n.outstandingQuantity + n.deliveredQuantity,
                                                         //RequestedQuantity = n.requestedQuantity.Value,
                                                         OutstandingQuantity = r.IOId == null ? n.outstandingQuantity : r.outstandingQuantity
                                                     }).ToList();
            }
            MM.IOItemListObject = newIOITemList;

            return PartialView("IOItemsTable", MM);
        }

        [HttpPost]
        public ActionResult CreateIO(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (Session["ContractId"] == null)
                {
                    Session["ContractId"] = x.OppContId;
                }
                //if (x.file == null)
                //{
                //    ModelState.AddModelError("file", "Please input the attachment before proceed.");
                //}
                bool haveQuantity = false; string ErrorMessage = "";
                foreach (var value in x.IOItemListObject)
                {
                    if (value.RequestedQuantity < 1)
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create IO failed! Reason: Request Quantity cannot be 0 or less in IO Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                haveIONo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreateIO", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                    if (value.RequestedQuantity > value.OutstandingQuantity)
                    {
                        haveQuantity = true;
                        ErrorMessage = "Create IO failed! Reason: Request Quantity cannot exceed Outstanding Quantity in IO Item";
                        return new JsonResult
                        {
                            Data = new
                            {
                                success = false,
                                havePONo = false,
                                haveIONo = false,
                                haveQuantity,
                                ErrorMessage,
                                exception = false,
                                view = this.RenderPartialView("CreateIO", x)
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                }
                if (!ModelState.IsValid)
                {
                    int DeliveryQuantity = 0;
                    foreach (var value in ModelState)
                    {
                        if (value.Key.Contains("RequestQuantity"))
                        {
                            DeliveryQuantity = Int32.Parse(value.Value.Value.AttemptedValue);
                            if (DeliveryQuantity < 1)
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create IO failed! Reason: Request Quantity cannot be 0 or less in DO Item";
                            }
                        }
                        if (value.Key.Contains("OutstandingQuantity"))
                        {
                            if (DeliveryQuantity > Int32.Parse(value.Value.Value.AttemptedValue))
                            {
                                haveQuantity = true;
                                ErrorMessage = "Create IO failed! Reason: Request Quantity cannot exceed Outstanding Quantity in IO Item";
                            }
                        }
                    }
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            haveIONo = false,
                            haveQuantity,
                            ErrorMessage,
                            exception = false,
                            view = this.RenderPartialView("CreateIO", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                var getIONo = db.InternalOrders.SingleOrDefault(y => y.IONo == x.IODetailObject.IONo);
                if (getIONo != null)
                {
                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            havePONo = false,
                            haveIONo = true,
                            haveQuantity,
                            exception = false,
                            view = this.RenderPartialView("CreateIO", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                InternalOrder _objCreateIO = new InternalOrder
                {
                    uuid = Guid.NewGuid(),
                    PreparedById = Int32.Parse(Session["UserId"].ToString()),
                    CompleteDate = DateTime.Now,
                    IONo = x.IODetailObject.IONo,
                    //DeliveryDate = x.DODetailObject.DeliveryDate,
                    IOIssueDate = x.IODetailObject.IOIssueDate,
                    //DeliveredDate = x.DODetailObject.DeliveredDate,
                    Update = true
                };
                db.InternalOrders.Add(_objCreateIO);
                db.SaveChanges();

                int IOId = _objCreateIO.IOId;
                int cId = Int32.Parse(Session["ContractId"].ToString());
                int fromUserID = _objCreateIO.PreparedById;
                var myEmail = db.Users.SingleOrDefault(y => y.userId == fromUserID);
                FileUpload fileUploadModel = new FileUpload();

                if (x.file != null)
                {
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = x.OppContId;
                    fileUploadModel.IOId = _objCreateIO.IOId;
                    fileUploadModel.uploadUserId = Int32.Parse(Session["UserId"].ToString());
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.IODetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        IOId = IOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                    db.SaveChanges();
                }

                foreach (var value in x.IOItemListObject)
                {
                    IO_Items _objIOItem = new IO_Items
                    {
                        uuid = Guid.NewGuid(),
                        IOId = _objCreateIO.IOId,
                        itemsId = value.ItemsId,
                        requestedQuantity = value.RequestedQuantity,
                        outstandingQuantity = value.OutstandingQuantity - value.RequestedQuantity
                    };

                    db.IO_Items.Add(_objIOItem);
                    db.SaveChanges();
                }

                foreach (var value in x.POListObject)
                {
                    Notification _objSendLog = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        POId = value.POId,
                        IOId = _objCreateIO.IOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        flag = 0
                    };

                    var _objCreateIOItem = (from m in db.PurchaseOrders
                                            join n in db.PO_Item on m.POId equals n.POId
                                            join o in db.IO_Items on n.itemsId equals o.itemsId
                                            join p in db.InternalOrders on o.IOId equals p.IOId
                                            where m.POId == value.POId && p.IOId == IOId
                                            select new POItemsTable()
                                            {
                                                PONo = m.Details_PONo,
                                                IONo = p.IONo
                                            }).FirstOrDefault();

                    _objSendLog.content = "create internal order no. " + _objCreateIOItem.IONo + " for PO No. " + _objCreateIOItem.PONo;
                    db.Notifications.Add(_objSendLog);

                    int TotalOutstandingQuantity = 0;
                    var _objCreateIOItemList = (from m in db.PurchaseOrders
                                                join n in db.PO_Item on m.POId equals n.POId
                                                join o in db.IO_Items on n.itemsId equals o.itemsId
                                                where m.POId == value.POId
                                                select new POItemsTable()
                                                {
                                                    OutstandingQuantity = o.outstandingQuantity,
                                                    RequestedQuantity = o.requestedQuantity
                                                }).ToList();

                    foreach (var values in _objCreateIOItemList)
                    {
                        TotalOutstandingQuantity = TotalOutstandingQuantity + values.OutstandingQuantity;
                    }
                    PurchaseOrder _objPO = db.PurchaseOrders.First(m => m.POId == value.POId);
                    if (TotalOutstandingQuantity == 0)
                    {
                        _objPO.IOInfo_Update = true;
                    }

                    if (x.file != null)
                    {
                        POs_FileUpload fileUploadModel2 = new POs_FileUpload
                        {
                            uuid = Guid.NewGuid(),
                            fileEntryId = fileUploadModel.fileEntryId,
                            POId = value.POId
                        };
                        db.POs_FileUpload.Add(fileUploadModel2);
                    }
                    db.SaveChanges();
                }

                try
                {
                    List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
                    NotiMemberList = DBQueryController.getNotiMemberList(cId);

                    Notification _objSendIOMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = x.OppContId,
                        IOId = IOId,
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "Send email(s) updates for new Internal Order",
                        QNAtype = "Message",
                        flag = 0
                    };
                    db.Notifications.Add(_objSendIOMessage);
                    db.SaveChanges();

                    foreach (var model in NotiMemberList)
                    {
                        var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
                        var IOInfo = (from m in db.InternalOrders
                                      join n in db.IO_Items on m.IOId equals n.IOId
                                      join o in db.PO_Item on n.itemsId equals o.itemsId
                                      join p in db.PurchaseOrders on o.POId equals p.POId
                                      join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                      join r in db.Users on m.PreparedById equals r.userId
                                      join s in db.Customers on q.custId equals s.custId
                                      where m.IOId == IOId
                                      select new MailModel()
                                      {
                                          CustName = s.name + " (" + s.abbreviation + ")",
                                          ContractName = q.name,
                                          Description = q.description
                                      }).FirstOrDefault();
                        x.RoleId = Session["RoleId"].ToString();
                        x.UserId = Int32.Parse(Session["UserId"].ToString());
                        x.CompanyId = Int32.Parse(Session["CompanyId"].ToString());
                        x.IODetailObject = DBQueryController.getIOList("IODetails", x.UserId, "R02", x.OppContId, x.CompanyId, IOId).FirstOrDefault();

                        string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/IOEmailTemplate.cshtml"));
                        IOInfo.Content = myEmail.userName + " has register new Internal Order. Please refer the details as below: ";
                        IOInfo.FromName = myEmail.emailAddress;
                        IOInfo.IOFlow = "IO Details";
                        IOInfo.IONo = x.IODetailObject.IONo;
                        IOInfo.PONo = x.IODetailObject.PONo;
                        IOInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/IOList/ViewIO") + "?cId=" + cId + "&IOId=" + IOId);
                        var result =
                                Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "IODetailsTemplateKey", null, IOInfo);

                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress("info@kubtel.com")
                        };
                        mail.To.Add(new MailAddress(UserEmail.emailAddress));
                        mail.Subject = "OCM: Updates on new " + IOInfo.IONo + " for PO Number " + IOInfo.PONo;
                        mail.Body = result;
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient
                        {
                            Host = "outlook.office365.com",
                            Port = 587,
                            EnableSsl = true,
                            Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                        };
                        smtp.Send(mail);

                        NotiGroup _objSendToUserId = new NotiGroup
                        {
                            uuid = Guid.NewGuid(),
                            chatId = _objSendIOMessage.chatId,
                            toUserId = model.Id
                        };
                        db.NotiGroups.Add(_objSendToUserId);
                        db.SaveChanges();
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }

                x.OppContId = cId;

                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        havePRNo = false,
                        exception = false,
                        view = this.RenderPartialView("CreateIO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region IO Tabs
        [HttpPost]
        public JsonResult IOTabs(string cId, string IOId)
        {
            if (cId != null)
            {
                Session["ContractId"] = cId;
            }
            Session["IOId"] = IOId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewIO", "IOList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }
        public ActionResult ViewIO(string cId, string IOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (cId != null && IOId != null && User.Identity.IsAuthenticated)
                {
                    Session["ContractId"] = cId;
                    Session["IOId"] = IOId;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int IOID = Int32.Parse(Session["IOId"].ToString()); int ContractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.IOId = IOID;
                model.OppContId = ContractId;
                model.UserName = Session["Username"].ToString();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult IODetails(int IOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel IODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    IOId = Int32.Parse(Session["IOId"].ToString()),
                    IODetailObject = (from m in db.InternalOrders
                                      join n in db.Users on m.PreparedById equals n.userId into s
                                      //join o in db.POStatus on m.StatusId equals o.StatusId
                                      from x in s.DefaultIfEmpty()
                                      where m.IOId == IOId
                                      select new IOListTable()
                                      {
                                          IONo = m.IONo,
                                          IOIssueDate = m.IOIssueDate,
                                          IOCompleteDate = m.CompleteDate,
                                          IOUpdate = m.Update
                                      }).FirstOrDefault(),
                    IOItemListObject = (from m in db.PurchaseOrders
                                        join n in db.PO_Item on m.POId equals n.POId
                                        join o in db.IO_Items on n.itemsId equals o.itemsId
                                        join p in db.InternalOrders on o.IOId equals p.IOId
                                        join q in db.PartNo_Items on n.partNo equals q.partNo
                                        where p.IOId == IOId
                                        select new IOItemsTable()
                                        {
                                            PONo = m.Details_PONo,
                                            MaterialNo = q.materialNo,
                                            Description = q.description,
                                            DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                            RequestedQuantity = o.requestedQuantity
                                        }).ToList()
                };
                Session["Details_Update"] = IODetailList.IODetailObject.IOUpdate;
                IODetailList.RoleId = Session["RoleId"].ToString();
                IODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == IODetailList.OppContId && m.userId == IODetailList.UserId
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(IODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult IODetails(MainModel x)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (!ModelState.IsValid)
                {
                    x.IOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.IO_Items on n.itemsId equals o.itemsId
                                          join p in db.InternalOrders on o.IOId equals p.IOId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.IOId == x.IOId
                                          select new IOItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              RequestedQuantity = o.requestedQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();

                    return new JsonResult
                    {
                        Data = new
                        {
                            success = false,
                            exception = false,
                            view = this.RenderPartialView("IODetails", x)
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                int IoId = x.IOId;
                int userid = Int32.Parse(Session["UserId"].ToString());
                InternalOrder objIODetaildb = db.InternalOrders.First(m => m.IOId == IoId);

                if (objIODetaildb.IONo != x.IODetailObject.IONo)
                {
                    Notification _objDetails_IONo = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        IOId = Int32.Parse(Session["IOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change IO Number from " + objIODetaildb.IONo + " to " + x.IODetailObject.IONo,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_IONo);
                    objIODetaildb.IONo = x.IODetailObject.IONo;
                }
                if (objIODetaildb.IOIssueDate != x.IODetailObject.IOIssueDate)
                {
                    Notification _objDetails_IOIssueDate = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        IOId = Int32.Parse(Session["IOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "change IO Issue date from " + objIODetaildb.IOIssueDate + " to " + x.IODetailObject.IOIssueDate,
                        flag = 0
                    };
                    db.Notifications.Add(_objDetails_IOIssueDate);
                    objIODetaildb.IOIssueDate = x.IODetailObject.IOIssueDate;
                }

                if (x.file != null)
                {
                    FileUpload fileUploadModel = new FileUpload();
                    DateTime createDate = DateTime.Now;
                    string filename1 = x.file.FileName.Replace("\\", ",");
                    string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                    string fullPath = x.file.FileName;
                    string extension = Path.GetExtension(fullPath);
                    byte[] uploadFile = new byte[x.file.InputStream.Length];

                    fileUploadModel.uuid = Guid.NewGuid();
                    fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                    fileUploadModel.IOId = IoId;
                    fileUploadModel.uploadUserId = userid;
                    fileUploadModel.uploadDate = createDate;
                    fileUploadModel.FullPath = fullPath;
                    fileUploadModel.FileName = filename;
                    fileUploadModel.description = x.FileDescription;
                    fileUploadModel.Extension = extension;
                    fileUploadModel.archivedFlag = false;
                    fileUploadModel.IODetailInfo = true;
                    fileUploadModel.notPO = false;
                    x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    fileUploadModel.File = uploadFile;
                    db.FileUploads.Add(fileUploadModel);

                    Notification _objSendMessage = new Notification
                    {
                        uuid = Guid.NewGuid(),
                        opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                        IOId = Int32.Parse(Session["IOId"].ToString()),
                        createDate = DateTime.Now,
                        fromUserId = Int32.Parse(Session["UserId"].ToString()),
                        content = "uploaded " + filename,
                        flag = 0
                    };
                    db.Notifications.Add(_objSendMessage);
                }

                int ContractId = Int32.Parse(Session["ContractId"].ToString());
                x.objCanEdit = (from m in db.Users_Opportunity
                                where m.opportunityId == ContractId && m.userId == userid
                                select new EditPermission()
                                {
                                    canEdits = m.canEdit
                                }).FirstOrDefault();
                try
                {
                    db.SaveChanges();
                    x.RoleId = Session["RoleId"].ToString();
                    //x.POTrackingTable = DBQueryController.getPOTracking(PoId);
                    x.IOItemListObject = (from m in db.PurchaseOrders
                                          join n in db.PO_Item on m.POId equals n.POId
                                          join o in db.IO_Items on n.itemsId equals o.itemsId
                                          join p in db.InternalOrders on o.IOId equals p.IOId
                                          join q in db.PartNo_Items on n.partNo equals q.partNo
                                          where p.IOId == IoId
                                          select new IOItemsTable()
                                          {
                                              PONo = m.Details_PONo,
                                              MaterialNo = q.materialNo,
                                              Description = q.description,
                                              DemandQuantity = n.deliveredQuantity + n.outstandingQuantity,
                                              RequestedQuantity = o.requestedQuantity,
                                              OutstandingQuantity = o.outstandingQuantity
                                          }).ToList();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                return new JsonResult
                {
                    Data = new
                    {
                        success = true,
                        exception = false,
                        view = this.RenderPartialView("IODetails", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Redirect("~/Home/Index");
            }
            //if (bool.Parse(Session["Details_Update"].ToString()) == false && x.file == null)
            //{
            //    ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
            //}
        }

        #region DocManagement
        [Authorize]
        public ActionResult IODocManagement(int IOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel();
                int userid = Int32.Parse(Session["UserId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                model.IODocListObject = (from m in db.FileUploads
                                         join n in db.Users on m.uploadUserId equals n.userId
                                         where m.IOId == IOId && m.archivedFlag == false
                                         select new FilePODetailInfo()
                                         {
                                             opportunityId = m.opportunityId,
                                             fileEntryId = m.fileEntryId,
                                             uploadUserName = n.firstName + " " + n.lastName,
                                             uploadDate = m.uploadDate,
                                             FileName = m.FileName,
                                             File = m.File,
                                             Description = m.description
                                         }).ToList();
                model.FileArchiveModel = (from m in db.FileUploads
                                          join n in db.Users on m.uploadUserId equals n.userId
                                          where m.IOId == IOId && m.archivedFlag == true
                                          select new FilePODetailInfo()
                                          {
                                              opportunityId = m.opportunityId,
                                              fileEntryId = m.fileEntryId,
                                              uploadUserName = n.firstName + " " + n.lastName,
                                              uploadDate = m.uploadDate,
                                              FileName = m.FileName,
                                              File = m.File,
                                              Description = m.description
                                          }).ToList();
                return PartialView(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public FileContentResult FileDownload(int id)
        {
            byte[] fileData;

            FileUpload fileRecord = db.FileUploads.Find(id);

            string fileName;
            string filename1 = fileRecord.FileName.Replace("\\", ",");
            string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            fileData = (byte[])fileRecord.File.ToArray();
            fileName = filename2;

            return File(fileData, "text", fileName);
        }
        public JsonResult FileArchive(string selectedId)
        {
            int id = Int32.Parse(selectedId);

            FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
            if (itemToArchive != null)
            {
                itemToArchive.archivedFlag = true;
                db.SaveChanges();
            }

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region Updates
        [Authorize]
        public ActionResult IOUpdates(int IOId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int Uid = Int32.Parse(Session["UserId"].ToString());
                int cId = Int32.Parse(Session["ContractId"].ToString());
                MainModel QNAModelList = new MainModel
                {
                    MessageListModel = DBQueryController.getMessageList(cId, 0, IOId, 0, 0, Uid),
                    UserId = Uid
                };

                var NotiMemberList = (from m in db.Users
                                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                      from x in gy.DefaultIfEmpty()
                                      where m.status == true && x.opportunityId == cId
                                      select new NotiMemberList()
                                      {
                                          Id = m.userId,
                                          Name = m.firstName + " " + m.lastName
                                      }).ToList().OrderBy(c => c.Name);

                ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                return PartialView(QNAModelList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public virtual ActionResult IODocUpload(MainModel fd)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                FileUpload fileUploadModel = new FileUpload();
                int userId = fd.UserId;
                DateTime createDate = DateTime.Now;
                string filename1 = fd.file.FileName.Replace("\\", ",");
                string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                string fullPath = fd.file.FileName;
                string extension = Path.GetExtension(fullPath);
                byte[] uploadFile = new byte[fd.file.InputStream.Length];

                fileUploadModel.uuid = Guid.NewGuid();
                fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                fileUploadModel.IOId = fd.IOId;
                fileUploadModel.uploadUserId = userId;
                fileUploadModel.uploadDate = createDate;
                fileUploadModel.FullPath = fullPath;
                fileUploadModel.FileName = filename;
                fileUploadModel.description = fd.FileDescription;
                fileUploadModel.Extension = extension;
                fileUploadModel.archivedFlag = false;
                fileUploadModel.notPO = false;
                fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                fileUploadModel.File = uploadFile;
                db.FileUploads.Add(fileUploadModel);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    IOId = Int32.Parse(Session["IOId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "uploaded " + filename,
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);

                db.SaveChanges();

                //return PartialView(db.FileUploads.ToList());
                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [Authorize]
        public ActionResult IOSendMessage(string UserId, int IOId, string Content, List<int> selectedUserId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int fromUserID = Int32.Parse(UserId);
                int cId = Int32.Parse(Session["ContractId"].ToString());
                var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = cId,
                    IOId = IOId,
                    createDate = DateTime.Now,
                    fromUserId = fromUserID,
                    content = Content,
                    QNAtype = "Message",
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();

                foreach (var model in selectedUserId)
                {
                    var UserEmail = db.Users.SingleOrDefault(z => z.userId == model);
                    var IOInfo = (from m in db.InternalOrders
                                  join n in db.IO_Items on m.IOId equals n.IOId
                                  join o in db.PO_Item on n.itemsId equals o.itemsId
                                  join p in db.PurchaseOrders on o.POId equals p.POId
                                  join q in db.Opportunities on p.opportunityId equals q.opportunityId
                                  join r in db.Users on m.PreparedById equals r.userId
                                  join s in db.Customers on q.custId equals s.custId
                                  where m.IOId == IOId
                                  select new MailModel()
                                  {
                                      CustName = s.name + " (" + s.abbreviation + ")",
                                      CustAbbreviation = s.abbreviation,
                                      ContractName = q.name,
                                      IONo = m.IONo,
                                      PONo = p.Details_PONo,
                                      Description = q.description
                                  }).FirstOrDefault();

                    string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/IOEmailTemplate.cshtml"));
                    IOInfo.Content = Content;
                    IOInfo.FromName = myEmail.emailAddress;
                    IOInfo.IOFlow = "IO Details";
                    IOInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority,
                        Url.Content("~/IOList/ViewIO") + "?cId=" + cId + "&IOId=" + IOId + "#IOUpdates-1");
                    var result = Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "IOTemplateKey", null, IOInfo);

                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress("info@kubtel.com")
                    };
                    mail.To.Add(new MailAddress(UserEmail.emailAddress));
                    //mail.To.Add(new MailAddress("info@kubtel.com"));
                    mail.Subject = "OCM: Updates on " + IOInfo.ContractName + " for " + IOInfo.CustAbbreviation;
                    mail.Body = result;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient
                    {
                        //smtp.Host = "pops.kub.com";
                        Host = "outlook.office365.com",
                        //smtp.Host = "smtp.gmail.com";
                        Port = 587,
                        //smtp.Port = 25;
                        EnableSsl = true,
                        //Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                        Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                    };
                    smtp.Send(mail);

                    NotiGroup _objSendToUserId = new NotiGroup
                    {
                        uuid = Guid.NewGuid(),
                        chatId = _objSendMessage.chatId,
                        toUserId = model
                    };
                    db.NotiGroups.Add(_objSendToUserId);
                }
                db.SaveChanges();

                return Json(new { Result = String.Format("Test berjaya!") });
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion
        #endregion
        public ActionResult IOForm()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int IOId = Int32.Parse(Session["IOId"].ToString());
                MainModel model = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                var IODetailsObj = DBQueryController.getIOList("IODetails", model.UserId, model.RoleId, model.OppContId, model.CompanyId, IOId).FirstOrDefault();
                var IOContractInfo = (from m in db.Opportunities
                                      join n in db.PurchaseOrders on m.opportunityId equals n.opportunityId
                                      where m.opportunityId == model.OppContId
                                      select new IOFormModels.IOFormList()
                                      {
                                          Justification = m.description,
                                          ContractNo = m.contractNo,
                                          ExchangeName = m.Customer.name
                                      }).FirstOrDefault();

                var IODetails = new IOFormModels()
                {
                    PONo = IODetailsObj.PONo,
                    RefNo = IODetailsObj.IONo,
                    IssueDate = IODetailsObj.IOIssueDate.Value.ToString("dd/MM/yyyy"),
                    ToDept = "LOGISTIC DEPARTMENT",
                    FromDept = "PROJECT DELIVERY DEPARTMENT",
                    RequiredDate = "",
                    ExchangeName = IOContractInfo.ExchangeName,
                    ContractNo = IOContractInfo.ContractNo,
                    Justification = IOContractInfo.Justification,
                    IOFormListObj = (from m in db.PurchaseOrders
                                     join n in db.PO_Item on m.POId equals n.POId
                                     join o in db.IO_Items on n.itemsId equals o.itemsId
                                     join p in db.InternalOrders on o.IOId equals p.IOId
                                     join q in db.PartNo_Items on n.partNo equals q.partNo
                                     where p.IOId == IOId
                                     select new IOFormModels.IOFormList()
                                     {
                                         MaterialNo = q.materialNo,
                                         ItemDescriptions = q.description,
                                         ItemQuantity = o.requestedQuantity
                                     }).ToList()
                };

                return new RazorPDF.PdfActionResult("IOForm", IODetails);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
    }
}
