﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Initiatives.Models;
using System.Net.Mail;
using System.Data;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;
using System.Configuration;

namespace Initiatives.Controllers
{
    public class POListController : Controller
    {
        private OpportunityManagementEntities db = new OpportunityManagementEntities();
        //private List<int> TeamMember;

        #region Create PO
        [HttpPost]
        public JsonResult NewPO(string poid)
        {
            Session["POId"] = poid;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("CreatePO", "POList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        public JsonResult GetMaterialNoList(string iteration)
        {
            string html = "<select class='MaterialNo input-validation-error' data-val='true' data-val-number='The field appointmentId must be a number.' " +
                "data-val-required='The appointmentId field is required.' id='MaterialNo" + iteration + "'><option value='' ></option>";

            var materialNoList = db.PartNo_Items.Select(m => new PartNoListModel()
            {
                partNo = m.partNo,
                MaterialNo = m.materialNo
            }).OrderBy(c => c.MaterialNo).ToList();

            foreach (var item in materialNoList)
            {
                html += "<option value='" + item.partNo + "' >" + item.MaterialNo + "</option>";
            }

            html += "</select>";

            return Json(new { html }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetMaterialNoInfo(int partno)
        {
            var materialNoInfo = db.PartNo_Items.Select(m => new PartNoListModel()
            {
                Description = m.description,
                UoM = m.UoM,
                partNo = m.partNo
            }).Where(m => m.partNo == partno).FirstOrDefault();

            return Json(new { materialNoInfo }, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        public ActionResult CreatePO(string id)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (id != null && User.Identity.IsAuthenticated)
                {
                    Session["POId"] = id;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int contractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.OppContId = contractId;
                model.UserName = Session["Username"].ToString();
                //var MaterialNoQuery = model.PartNoListObject
                //    .Select(c => new SelectListItem { Text = c.MaterialNo, Value = c.partNo.ToString() }).ToList();
                //ViewBag.MaterialNoList = MaterialNoQuery;
                var MaterialNoQuery = db.PartNo_Items.Select(c => new { MaterialNo = c.materialNo, PartNo = c.partNo })
                    .OrderBy(c => c.MaterialNo);
                ViewBag.MaterialNoList = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialNo","");

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult CreatePO(MainModel x)
        {
            if (Session["ContractId"] == null)
            {
                Session["ContractId"] = x.OppContId;
            }
            var MaterialNoQuery = db.PartNo_Items.Select(c => new { MaterialNo = c.materialNo, PartNo = c.partNo })
                    .OrderBy(c => c.MaterialNo);

            if (x.file == null)
            {
                ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
            }
            if (!ModelState.IsValid)
            {
                ViewBag.MaterialNoList = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialNo", "");

                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        havePONo = false,
                        exception = false,
                        view = this.RenderPartialView("CreatePO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            var getPONo = db.PurchaseOrders.SingleOrDefault(y => y.Details_PONo == x.POListDetail.Details_PONo);
            if (getPONo != null)
            {
                ViewBag.MaterialNoList = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialNo", "");

                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        havePONo = true,
                        exception = false,
                        view = this.RenderPartialView("CreatePO", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            PurchaseOrder _objCreatePO = new PurchaseOrder
            {
                uuid = Guid.NewGuid(),
                opportunityId = x.OppContId,
                createByUserId = Int32.Parse(Session["UserId"].ToString()),
                createDate = DateTime.Now,

                Details_PONo = x.POListDetail.Details_PONo,
                Details_POIssueDate = x.POListDetail.Details_POIssueDate,
                Details_projectDescription = x.POListDetail.Details_projectDescription,
                Details_totalPrice = x.POListDetail.Details_totalPrice,
                Details_totalGST = x.POListDetail.Details_totalGST,
                StatusId = "POS001",
                Details_Update = true
            };
            db.PurchaseOrders.Add(_objCreatePO);
            db.SaveChanges();

            foreach (var value in x.POItemListObject)
            {
                PO_Item _objPOItem = new PO_Item
                {
                    uuid = Guid.NewGuid(),
                    POId = _objCreatePO.POId,
                    partNo = value.PartNo,
                    //materialNo = value.MaterialNo,
                    //description = value.Description,
                    //UoM = value.UoM,
                    unitPrice = value.UnitPrice,
                    deliveryDate = value.DeliveryDate,
                    outstandingQuantity = value.Quantity,
                    totalPrice = value.TotalPrice
                };
                db.PO_Item.Add(_objPOItem);
                db.SaveChanges();
            }
            db.SaveChanges();

            int pId = _objCreatePO.POId;
            int cId = _objCreatePO.opportunityId;
            int fromUserID = _objCreatePO.createByUserId;
            var myEmail = db.Users.SingleOrDefault(y => y.userId == fromUserID);

            Notification _objSendLog = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = _objCreatePO.opportunityId,
                POId = _objCreatePO.POId,
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString())
            };
            Opportunity _objOppoNamedb = db.Opportunities.First(m => m.opportunityId == _objCreatePO.opportunityId);
            _objSendLog.content = "create " + _objOppoNamedb.name + " purchase order no. " + _objCreatePO.Details_PONo;
            _objSendLog.flag = 0;
            db.Notifications.Add(_objSendLog);
            db.SaveChanges();

            if (x.file != null)
            {
                FileUpload fileUploadModel = new FileUpload();
                DateTime createDate = DateTime.Now;
                string filename1 = x.file.FileName.Replace("\\", ",");
                string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                string fullPath = x.file.FileName;
                string extension = Path.GetExtension(fullPath);
                byte[] uploadFile = new byte[x.file.InputStream.Length];

                fileUploadModel.uuid = Guid.NewGuid();
                fileUploadModel.opportunityId = x.OppContId;
                fileUploadModel.uploadUserId = Int32.Parse(Session["UserId"].ToString());
                fileUploadModel.uploadDate = createDate;
                fileUploadModel.FullPath = fullPath;
                fileUploadModel.FileName = filename;
                fileUploadModel.description = x.FileDescription;
                fileUploadModel.Extension = extension;
                fileUploadModel.archivedFlag = false;
                fileUploadModel.PODetailInfo = true;
                fileUploadModel.notPO = false;
                x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                fileUploadModel.File = uploadFile;
                db.FileUploads.Add(fileUploadModel);
                db.SaveChanges();

                POs_FileUpload fileUploadModel2 = new POs_FileUpload
                {
                    uuid = Guid.NewGuid(),
                    fileEntryId = fileUploadModel.fileEntryId,
                    POId = _objCreatePO.POId
                };
                db.POs_FileUpload.Add(fileUploadModel2);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = x.OppContId,
                    POId = pId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "uploaded " + filename,
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
                db.SaveChanges();
            }

            if (x.POListDetail.SendEmail == true) {
                try
                {
                    //var NotiMemberList = (from m in db.Users
                    //                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                    //                      from y in gy.DefaultIfEmpty()
                    //                      where m.status == true && m.userId != 862 && y.opportunityId == _objCreatePO.opportunityId
                    //                      select new NotiMemberList()
                    //                      {
                    //                          Id = m.userId,
                    //                          Name = m.firstName + " " + m.lastName
                    //                      }).ToList().OrderBy(c => c.Name);
                    List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
                    NotiMemberList = DBQueryController.getNotiMemberList(cId);
                    Notification _objSendPOMessage = new Notification();
                    string Content = "new Purchase Order";
                    _objSendPOMessage.uuid = Guid.NewGuid();
                    _objSendPOMessage.opportunityId = x.OppContId;
                    _objSendPOMessage.POId = pId;
                    _objSendPOMessage.createDate = DateTime.Now;
                    _objSendPOMessage.fromUserId = Int32.Parse(Session["UserId"].ToString());
                    _objSendPOMessage.content = "Send email(s) updates for " + Content;
                    _objSendPOMessage.QNAtype = "Message";
                    _objSendPOMessage.flag = 0;
                    db.Notifications.Add(_objSendPOMessage);

                    foreach (var model in NotiMemberList)
                    {
                        var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
                        var POInfo = (from m in db.PurchaseOrders
                                      join n in db.Users on m.createByUserId equals n.userId
                                      join o in db.Opportunities on m.opportunityId equals o.opportunityId
                                      join p in db.Customers on o.custId equals p.custId
                                      where m.POId == pId
                                      select new MailModel()
                                      {
                                          CustName = p.name + " (" + p.abbreviation + ")",
                                          ContractName = o.name,
                                          Description = o.description,
                                          PODescription = m.Details_projectDescription,
                                          PONo = m.Details_PONo,
                                          AssignTo = n.firstName + " " + n.lastName
                                      }).FirstOrDefault();

                        string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/POEmailTemplate.cshtml"));
                        POInfo.Content = myEmail.userName + " has register new Purchase Order. Please refer the details as below: ";
                        POInfo.FromName = myEmail.emailAddress;
                        POInfo.POFlow = "PO Details";
                        POInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/POList/ViewPO") + "?cId=" + cId + "&pId=" + pId);
                        var result =
                                Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PODetailsTemplateKey", null, POInfo);

                        MailMessage mail = new MailMessage
                        {
                            From = new MailAddress("info@kubtel.com")
                        };
                        mail.To.Add(new MailAddress(UserEmail.emailAddress));
                        mail.Subject = "OCM: Updates on " + POInfo.POFlow + " for PO Number " + POInfo.PONo;
                        mail.Body = result;
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient
                        {
                            Host = "outlook.office365.com",
                            Port = 587,
                            EnableSsl = true,
                            //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
                            Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                        };
                        if (ConfigurationManager.AppSettings["SentEmail"] == "true")
                        {
                            smtp.Send(mail);
                        }

                        NotiGroup _objSendToUserId = new NotiGroup
                        {
                            uuid = Guid.NewGuid(),
                            chatId = _objSendLog.chatId,
                            toUserId = model.Id
                        };
                        db.NotiGroups.Add(_objSendToUserId);
                        db.SaveChanges();
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            } else
            {
                Notification _objSendDOMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = x.OppContId,
                    POId = _objCreatePO.POId,
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "Create new purchase order without sending emails",
                    QNAtype = "Message",
                    flag = 0
                };
                db.Notifications.Add(_objSendDOMessage);
                db.SaveChanges();
            }
            x.SaveSuccess = true;
            db.SaveChanges();
            x.OppContId = _objCreatePO.opportunityId;
            ViewBag.MaterialNoList = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialNo", "");

            return new JsonResult
            {
                Data = new
                {
                    success = true,
                    havePONo = false,
                    exception = false,
                    view = this.RenderPartialView("CreatePO", x)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize]
        public ActionResult CreateAllPO()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                //int contractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                //model.OppContId = contractId;
                int custId = Int32.Parse(Session["CompanyId"].ToString());
                model.UserName = Session["Username"].ToString(); 
                List<ContractListTable> OppoTypeQuery = new List<ContractListTable>();
                if (model.RoleId == "R02")
                {
                    OppoTypeQuery = db.Opportunities.Select(c => new ContractListTable() { Id = c.opportunityId, ContractName = c.contractNo + " - " + c.name, SupplierId = c.supplierId })
                        .Where(c => c.SupplierId == custId).OrderBy(c => c.ContractName).ToList();
                }
                else
                {
                    OppoTypeQuery = (from m in db.Opportunities
                                         join n in db.Users_Opportunity on m.opportunityId equals n.opportunityId
                                         where m.supplierId == custId && n.userId == model.UserId
                                     select new ContractListTable()
                                         {
                                             Id = m.opportunityId,
                                             ContractName = m.contractNo + " - " + m.name
                                         }).OrderBy(c => c.ContractName).ToList().ToList();
                }

                ViewBag.ContNoList = new SelectList(OppoTypeQuery.AsEnumerable(), "Id", "ContractName");

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }          
        }

        [Authorize]
        public ActionResult AllPOList()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel
                {
                    RoleId = Session["RoleId"].ToString(),
                    UserId = Int32.Parse(Session["UserId"].ToString()),
                    CompanyId = Int32.Parse(Session["CompanyId"].ToString())
                };
                model.POListObject = DBQueryController.getPOList(model.UserId, model.RoleId, 0, model.CompanyId,"");

                return PartialView("AllPOList", model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region PO Tabs
        [HttpPost]
        public JsonResult POTabs(string cId, string POId)
        {
            if (cId != null)
            {
                Session["ContractId"] = cId;
            }
            Session["POId"] = POId;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("ViewPO", "POList", new { /* no params */ });
            return Json(new { success = true, url = redirectUrl });
        }

        [Authorize]
        public ActionResult ViewPO(string cId, string pId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                if (cId != null && pId != null && User.Identity.IsAuthenticated)
                {
                    Session["ContractId"] = cId;
                    Session["POId"] = pId;
                }
                MainModel model = new MainModel
                {
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                int POId = Int32.Parse(Session["POId"].ToString()); int ContractId = Int32.Parse(Session["ContractId"].ToString());
                var getRole = db.Users_Roles.SingleOrDefault(x => x.userId == model.UserId);
                model.RoleId = getRole.Role.roleId.Trim();
                model.POId = POId;
                model.OppContId = ContractId;
                model.UserName = Session["Username"].ToString();

                return View(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        #region PODetails
        [Authorize]
        public ActionResult PODetails()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel PODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    UserId = Int32.Parse(Session["UserId"].ToString())
                };
                PODetailList.POListDetail = (from m in db.PurchaseOrders
                                             join n in db.Users on m.createByUserId equals n.userId into s
                                             join o in db.POStatus on m.StatusId equals o.StatusId
                                             from x in s.DefaultIfEmpty()
                                             where m.POId == PODetailList.POId
                                             select new PODetailList()
                                             {
                                                 Details_PONo = m.Details_PONo,
                                                 Details_POIssueDate = m.Details_POIssueDate,
                                                 Details_projectDescription = m.Details_projectDescription,
                                                 Details_Update = m.Details_Update,
                                                 Details_totalPrice = m.Details_totalPrice,
                                                 Details_totalGST = m.Details_totalGST,
                                                 Status = o.status
                                             }).FirstOrDefault();
                PODetailList.POItemListObject = (from m in db.PO_Item
                                                 join n in db.PartNo_Items on m.partNo equals n.partNo
                                                 where m.POId == PODetailList.POId
                                                 select new POItemsTable()
                                                 {
                                                     ItemsId = m.itemsId,
                                                     PartNo = n.partNo,
                                                     MaterialNo = n.materialNo,
                                                     //Description = m.description,
                                                     Description = n.description,
                                                     DeliveryDate = m.deliveryDate,
                                                     Quantity = m.outstandingQuantity + m.deliveredQuantity,
                                                     //UoM = m.UoM,
                                                     UoM = n.UoM,
                                                     UnitPrice = m.unitPrice,
                                                     TotalPrice = m.totalPrice
                                                 }).ToList();
                //Session["Details_POIssueDate"] = PODetailList.POListDetail.Details_POIssueDate;
                Session["Details_Update"] = PODetailList.POListDetail.Details_Update;
                PODetailList.RoleId = Session["RoleId"].ToString();
                PODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PODetailList.OppContId && m.userId == PODetailList.UserId
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();
                var MaterialNoQuery = db.PartNo_Items.Select(c => new { MaterialNo = c.materialNo, PartNo = c.partNo })
                    .OrderBy(c => c.MaterialNo);
                ViewBag.MaterialNoList = new SelectList(MaterialNoQuery.AsEnumerable(), "PartNo", "MaterialNo", PODetailList.POItemListObject.Select(m => m.PartNo).ToList());
                //PODetailList.POTrackingTable = DBQueryController.getPOTracking(PODetailList.POId);

                return PartialView(PODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }

        [HttpPost]
        public ActionResult PODetails(MainModel x)
        {
            //if (bool.Parse(Session["Details_Update"].ToString()) == false && x.file == null)
            //{
            //    ModelState.AddModelError("file", "Please input the attachment before proceed."); ;          
            //}
            ModelState.Remove("POItemsObject.Quantity");
            if (!ModelState.IsValid)
            {
                return PartialView("PODetails", x);
            }
            int PoId = x.POId;
            int userid = Int32.Parse(Session["UserId"].ToString());
            PurchaseOrder objPODetaildb = db.PurchaseOrders.First(m => m.POId == PoId);

            if (objPODetaildb.Details_PONo != x.POListDetail.Details_PONo)
            {
                Notification _objDetails_PONo = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = objPODetaildb.Details_PONo == null ? "change PO Number to " + x.POListDetail.Details_PONo :
                    "change PO Number from " + objPODetaildb.Details_PONo + " to " + x.POListDetail.Details_PONo,
                    flag = 0
                };
                db.Notifications.Add(_objDetails_PONo);
                objPODetaildb.Details_PONo = x.POListDetail.Details_PONo;
            }
            if (objPODetaildb.Details_POIssueDate != x.POListDetail.Details_POIssueDate)
            {
                Notification _objDetails_POIssueDate = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = objPODetaildb.Details_POIssueDate == null ? "change PO Issue date to " + x.POListDetail.Details_POIssueDate :
                    "change PO Issue date from " + objPODetaildb.Details_POIssueDate + " to " + x.POListDetail.Details_POIssueDate,
                    flag = 0
                };
                db.Notifications.Add(_objDetails_POIssueDate);
                objPODetaildb.Details_POIssueDate = x.POListDetail.Details_POIssueDate;
            }
            if (objPODetaildb.Details_projectDescription != x.POListDetail.Details_projectDescription)
            {
                Notification _objDetails_projectDescription = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = objPODetaildb.Details_projectDescription == null ? "change project description selection to " + x.POListDetail.Details_projectDescription :
                    "change project description selection from " + objPODetaildb.Details_projectDescription + " to " + x.POListDetail.Details_projectDescription,
                    flag = 0
                };
                db.Notifications.Add(_objDetails_projectDescription);
                objPODetaildb.Details_projectDescription = x.POListDetail.Details_projectDescription;
            }
            if (objPODetaildb.Details_totalPrice != x.POListDetail.Details_totalPrice)
            {
                Notification _objDetails_totalPrice = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change PO Total Price from " + objPODetaildb.Details_totalPrice + " to " + x.POListDetail.Details_totalPrice,
                    flag = 0
                };
                db.Notifications.Add(_objDetails_totalPrice);
                objPODetaildb.Details_totalPrice = x.POListDetail.Details_totalPrice;
            }
            if (objPODetaildb.Details_totalGST != x.POListDetail.Details_totalGST)
            {
                Notification _objDetails_TotalGST = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "change PO Total GST from " + objPODetaildb.Details_totalGST + " to " + x.POListDetail.Details_totalGST,
                    flag = 0
                };
                db.Notifications.Add(_objDetails_TotalGST);
                objPODetaildb.Details_totalGST = x.POListDetail.Details_totalGST;
            }

            if (x.file != null)
            {
                FileUpload fileUploadModel = new FileUpload();
                DateTime createDate = DateTime.Now;
                string filename1 = x.file.FileName.Replace("\\", ",");
                string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
                string fullPath = x.file.FileName;
                string extension = Path.GetExtension(fullPath);
                byte[] uploadFile = new byte[x.file.InputStream.Length];

                fileUploadModel.uuid = Guid.NewGuid();
                fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
                fileUploadModel.uploadUserId = userid;
                fileUploadModel.uploadDate = createDate;
                fileUploadModel.FullPath = fullPath;
                fileUploadModel.FileName = filename;
                fileUploadModel.description = x.FileDescription;
                fileUploadModel.Extension = extension;
                fileUploadModel.archivedFlag = false;
                fileUploadModel.PODetailInfo = true;
                fileUploadModel.notPO = false;
                x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                fileUploadModel.File = uploadFile;
                db.FileUploads.Add(fileUploadModel);
                db.SaveChanges();

                POs_FileUpload fileUploadModel2 = new POs_FileUpload
                {
                    uuid = Guid.NewGuid(),
                    fileEntryId = fileUploadModel.fileEntryId,
                    POId = PoId
                };
                db.POs_FileUpload.Add(fileUploadModel2);

                Notification _objSendMessage = new Notification
                {
                    uuid = Guid.NewGuid(),
                    opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = Int32.Parse(Session["POId"].ToString()),
                    createDate = DateTime.Now,
                    fromUserId = Int32.Parse(Session["UserId"].ToString()),
                    content = "uploaded " + filename,
                    flag = 0
                };
                db.Notifications.Add(_objSendMessage);
            }                     

            int ContractId = Int32.Parse(Session["ContractId"].ToString());
            x.objCanEdit = (from m in db.Users_Opportunity
                            where m.opportunityId == ContractId && m.userId == userid
                            select new EditPermission()
                            {
                                canEdits = m.canEdit
                            }).FirstOrDefault();
            try
            {
                db.SaveChanges();
                x.SaveSuccess = true;
                x.RoleId = Session["RoleId"].ToString();
                //x.POTrackingTable = DBQueryController.getPOTracking(PoId);
                x.POItemListObject = (from m in db.PO_Item
                                      join n in db.PartNo_Items on m.partNo equals n.partNo
                                      where m.POId == PoId
                                      select new POItemsTable()
                                      {
                                          ItemsId = m.itemsId,
                                          //MaterialNo = m.materialNo,
                                          MaterialNo = n.materialNo,
                                          //Description = m.description,
                                          Description = n.description,
                                          DeliveryDate = m.deliveryDate,
                                          Quantity = m.outstandingQuantity + m.deliveredQuantity,
                                          //UoM = m.UoM,
                                          UoM = n.UoM,
                                          UnitPrice = m.unitPrice,
                                          TotalPrice = m.totalPrice
                                      }).ToList();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return PartialView("PODetails", x);
        }

        [HttpPost]
        public ActionResult POItemDetails(MainModel x)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult
                {
                    Data = new
                    {
                        success = false,
                        exception = false,
                        view = this.RenderPartialView("PODetails", x)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            int PoId = Int32.Parse(Session["POId"].ToString());
            int userid = Int32.Parse(Session["UserId"].ToString());
            if (x.UpdatePO == true)
            {
                int TotalOutstandingQ = 0; int TotalDeliveredQ = 0;
                foreach (var value in x.POItemListObject)
                {
                    PO_Item _objPOItem = db.PO_Item.First(m => m.itemsId == value.ItemsId);
                    _objPOItem.partNo = value.PartNo;
                    _objPOItem.materialNo = value.MaterialNo;
                    _objPOItem.description = value.Description;
                    _objPOItem.UoM = value.UoM;
                    _objPOItem.unitPrice = value.UnitPrice;
                    _objPOItem.deliveryDate = value.DeliveryDate;
                    _objPOItem.outstandingQuantity = value.Quantity - _objPOItem.deliveredQuantity;
                    TotalOutstandingQ += _objPOItem.outstandingQuantity;
                    TotalDeliveredQ += _objPOItem.deliveredQuantity;
                    _objPOItem.totalPrice = value.TotalPrice;
                    db.SaveChanges();                                    
                }
                if (TotalOutstandingQ != 0 && TotalDeliveredQ != 0)
                {
                    PurchaseOrder _objPO = db.PurchaseOrders.First(m => m.POId == PoId);
                    _objPO.StatusId = "POS007";
                    db.SaveChanges();
                }
            } else if (x.CreatePO == true)
            {
                foreach (var value in x.POItemListObject)
                {
                    PO_Item _objPOItem = new PO_Item
                    {
                        uuid = Guid.NewGuid(),
                        POId = PoId,
                        partNo = value.PartNo,
                        materialNo = value.MaterialNo,
                        description = value.Description,
                        UoM = value.UoM,
                        unitPrice = value.UnitPrice,
                        deliveryDate = value.DeliveryDate,
                        outstandingQuantity = value.Quantity,
                        totalPrice = value.TotalPrice
                    };
                    db.PO_Item.Add(_objPOItem);
                    db.SaveChanges();
                }
            }
            
            
            return Json(new { success = true });
        }
        #endregion

        #region IOInfo
        [Authorize]
        public ActionResult IOInfo()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel PODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = POId,
                    CreateByUserId = userid,
                    POItemListObject = (from m in db.PO_Item
                                        join n in db.IO_Items on m.itemsId equals n.itemsId
                                        join o in db.InternalOrders on n.IOId equals o.IOId
                                        join p in db.PartNo_Items on m.partNo equals p.partNo
                                        where m.POId == POId
                                        select new POItemsTable()
                                        {
                                            IONo = o.IONo,
                                            MaterialNo = p.materialNo,
                                            Description = p.description,
                                            DeliveryDate = m.deliveryDate,
                                            RequestedQuantity = n.requestedQuantity,
                                            OutstandingQuantity = n.outstandingQuantity,
                                            Quantity = n.requestedQuantity + n.outstandingQuantity,
                                            UoM = p.UoM,
                                            UnitPrice = m.unitPrice,
                                            TotalPrice = m.totalPrice,
                                            IssueDate = o.IOIssueDate
                                        }).ToList(),
                    RoleId = Session["RoleId"].ToString().Trim()
                };
                //Session["DOInfo_Update"] = PODetailList.DeliveryInfoDetail.DOInfo_Update;
                PODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PODetailList.OppContId && m.userId == userid
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(PODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region PRInfo
        [Authorize]
        public ActionResult PRInfo()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel PODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = POId,
                    CreateByUserId = userid,
                    POItemListObject = (from m in db.PO_Item
                                        join n in db.PR_Items on m.itemsId equals n.itemsId
                                        join o in db.PurchaseRequisitions on n.PRId equals o.PRId
                                        join p in db.PartNo_Items on m.partNo equals p.partNo
                                        where m.POId == POId
                                        select new POItemsTable()
                                        {
                                            PRNo = o.PRNo,
                                            MaterialNo = p.materialNo,
                                            Description = p.description,
                                            DeliveryDate = m.deliveryDate,
                                            RequestedQuantity = n.requestedQuantity,
                                            OutstandingQuantity = n.outstandingQuantity,
                                            Quantity = n.requestedQuantity + n.outstandingQuantity,
                                            UoM = p.UoM,
                                            UnitPrice = m.unitPrice,
                                            TotalPrice = m.totalPrice,
                                            IssueDate = o.PRIssueDate
                                        }).ToList(),
                    RoleId = Session["RoleId"].ToString().Trim()
                };
                PODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PODetailList.OppContId && m.userId == userid
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(PODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region Delivery Info
        [Authorize]
        public ActionResult DeliveryInfo()
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
                MainModel PODetailList = new MainModel
                {
                    OppContId = Int32.Parse(Session["ContractId"].ToString()),
                    POId = POId,
                    CreateByUserId = userid,
                    POItemListObject = (from m in db.PO_Item
                                        join n in db.DO_Items on m.itemsId equals n.itemsId
                                        join o in db.DeliveryOrders on n.DOId equals o.DOId
                                        join p in db.PartNo_Items on m.partNo equals p.partNo
                                        where m.POId == POId
                                        select new POItemsTable()
                                        {
                                            DONo = o.DONo,
                                            MaterialNo = p.materialNo,
                                            Description = p.description,
                                            DeliveryDate = m.deliveryDate,
                                            DeliveredQuantity = n.deliveredQuantity,
                                            OutstandingQuantity = n.outstandingQuantity,
                                            DeliveryQuantity = n.deliveryQuantity,
                                            UoM = p.UoM,
                                            UnitPrice = m.unitPrice,
                                            TotalPrice = m.totalPrice
                                            //DeliveredDate = o.DeliveredDate
                                        }).ToList(),
                    //PODetailList.DeliveryInfoDetail = (from m in db.PurchaseOrders
                    //                                   join n in db.Users on m.createByUserId equals n.userId into s
                    //                                   from x in s.DefaultIfEmpty()
                    //                                   where m.POId == POId
                    //                                   select new DeliveryInfoList()
                    //                                   {
                    //                                       DOInfo_DocReceivedDate = m.DOInfo_DocReceivedDate,
                    //                                       DOInfo_ApprovedDate = m.DOInfo_ApprovedDate,
                    //                                       DOInfo_DONo = m.DOInfo_DONo,
                    //                                       //DOInfo_DeliveryStatus = m.DOInfo_DeliveryStatus,
                    //                                       DOInfo_EI_Name = m.DOInfo_EI_Name,
                    //                                       DOInfo_EI_Position = m.DOInfo_EI_Position,
                    //                                       DOInfo_SI_Name = m.DOInfo_SI_Name,
                    //                                       DOInfo_SI_Position = m.DOInfo_SI_Position,
                    //                                       DOInfo_Remark = m.DOInfo_Remark,
                    //                                       DOInfo_CompleteDate = m.DOInfo_CompleteDate,
                    //                                       DOInfo_Update = m.DOInfo_Update
                    //                                   }).FirstOrDefault();
                    RoleId = Session["RoleId"].ToString().Trim()
                };
                //Session["DOInfo_Update"] = PODetailList.DeliveryInfoDetail.DOInfo_Update;
                PODetailList.objCanEdit = (from m in db.Users_Opportunity
                                           where m.opportunityId == PODetailList.OppContId && m.userId == userid
                                           select new EditPermission()
                                           {
                                               canEdits = m.canEdit
                                           }).FirstOrDefault();

                return PartialView(PODetailList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        #endregion

        #region DocManagement
        [Authorize]
        public ActionResult PODocManagement(string POId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                MainModel model = new MainModel();
                int userid = Int32.Parse(Session["UserId"].ToString());
                model.RoleId = Session["RoleId"].ToString();
                int pId = Int32.Parse(POId);
                model.PODocListObject = (from m in db.FileUploads
                                         join n in db.Users on m.uploadUserId equals n.userId
                                         join o in db.POs_FileUpload on m.fileEntryId equals o.fileEntryId
                                         where o.POId == pId && m.archivedFlag == false
                                         select new FilePODetailInfo()
                                         {
                                             opportunityId = m.opportunityId,
                                             fileEntryId = m.fileEntryId,
                                             uploadUserName = n.firstName + " " + n.lastName,
                                             uploadDate = m.uploadDate,
                                             FileName = m.FileName,
                                             File = m.File,
                                             Description = m.description
                                         }).ToList();
                model.FileArchiveModel = (from m in db.FileUploads
                                          join n in db.Users on m.uploadUserId equals n.userId
                                          join o in db.POs_FileUpload on m.fileEntryId equals o.fileEntryId
                                          where o.POId == pId && m.archivedFlag == true
                                          select new FilePODetailInfo()
                                          {
                                              opportunityId = m.opportunityId,
                                              fileEntryId = m.fileEntryId,
                                              uploadUserName = n.firstName + " " + n.lastName,
                                              uploadDate = m.uploadDate,
                                              FileName = m.FileName,
                                              File = m.File,
                                              Description = m.description
                                          }).ToList();
                //model.UploadPODetailInfo = (from m in db.FileUploads
                //                            join n in db.Users on m.uploadUserId equals n.userId
                //                            where m.POId == pId && m.archivedFlag == false && m.PODetailInfo == true
                //                            select new FilePODetailInfo()
                //                            {
                //                                opportunityId = m.opportunityId,
                //                                fileEntryId = m.fileEntryId,
                //                                uploadUserName = n.firstName + " " + n.lastName,
                //                                uploadDate = m.uploadDate,
                //                                FileName = m.FileName,
                //                                File = m.File,
                //                                Description = m.description
                //                            }).ToList();
                //model.UploadPODetailInfo = (from m in db.FileUploads
                //                            join n in db.Users on m.uploadUserId equals n.userId
                //                            where m.POId == pId && m.archivedFlag == false && m.PODetailInfo == true
                //                            select new FilePODetailInfo()
                //                            {
                //                                opportunityId = m.opportunityId,
                //                                fileEntryId = m.fileEntryId,
                //                                uploadUserName = n.firstName + " " + n.lastName,
                //                                uploadDate = m.uploadDate,
                //                                FileName = m.FileName,
                //                                File = m.File,
                //                                Description = m.description
                //                            }).ToList();
                //model.UploadPOIOInfo = (from m in db.FileUploads
                //                        join n in db.Users on m.uploadUserId equals n.userId
                //                        where m.POId == pId && m.archivedFlag == false && m.POIOInfo == true
                //                        select new FilePODetailInfo()
                //                        {
                //                            opportunityId = m.opportunityId,
                //                            fileEntryId = m.fileEntryId,
                //                            uploadUserName = n.firstName + " " + n.lastName,
                //                            uploadDate = m.uploadDate,
                //                            FileName = m.FileName,
                //                            File = m.File,
                //                            Description = m.description
                //                        }).ToList();
                //model.UploadPOPRInfo = (from m in db.FileUploads
                //                        join n in db.Users on m.uploadUserId equals n.userId
                //                        where m.POId == pId && m.archivedFlag == false && m.POPRInfo == true
                //                        select new FilePODetailInfo()
                //                        {
                //                            opportunityId = m.opportunityId,
                //                            fileEntryId = m.fileEntryId,
                //                            uploadUserName = n.firstName + " " + n.lastName,
                //                            uploadDate = m.uploadDate,
                //                            FileName = m.FileName,
                //                            File = m.File,
                //                            Description = m.description
                //                        }).ToList();
                //model.UploadPOPPInfo = (from m in db.FileUploads
                //                        join n in db.Users on m.uploadUserId equals n.userId
                //                        where m.POId == pId && m.archivedFlag == false && m.POPPInfo == true
                //                        select new FilePODetailInfo()
                //                        {
                //                            opportunityId = m.opportunityId,
                //                            fileEntryId = m.fileEntryId,
                //                            uploadUserName = n.firstName + " " + n.lastName,
                //                            uploadDate = m.uploadDate,
                //                            FileName = m.FileName,
                //                            File = m.File,
                //                            Description = m.description
                //                        }).ToList();
                //model.UploadPOFPInfo = (from m in db.FileUploads
                //                        join n in db.Users on m.uploadUserId equals n.userId
                //                        where m.POId == pId && m.archivedFlag == false && m.POFPInfo == true
                //                        select new FilePODetailInfo()
                //                        {
                //                            opportunityId = m.opportunityId,
                //                            fileEntryId = m.fileEntryId,
                //                            uploadUserName = n.firstName + " " + n.lastName,
                //                            uploadDate = m.uploadDate,
                //                            FileName = m.FileName,
                //                            File = m.File,
                //                            Description = m.description
                //                        }).ToList();
                //model.UploadPOCEOOInfo = (from m in db.FileUploads
                //                        join n in db.Users on m.uploadUserId equals n.userId
                //                          where m.POId == pId && m.archivedFlag == false && m.POCEOOInfo == true
                //                        select new FilePODetailInfo()
                //                        {
                //                            opportunityId = m.opportunityId,
                //                            fileEntryId = m.fileEntryId,
                //                            uploadUserName = n.firstName + " " + n.lastName,
                //                            uploadDate = m.uploadDate,
                //                            FileName = m.FileName,
                //                            File = m.File,
                //                            Description = m.description
                //                        }).ToList();
                //model.UploadPOPO2Supplier = (from m in db.FileUploads
                //                             join n in db.Users on m.uploadUserId equals n.userId
                //                             where m.POId == pId && m.archivedFlag == false && m.POPO2Supplier == true
                //                             select new FilePODetailInfo()
                //                             {
                //                                 opportunityId = m.opportunityId,
                //                                 fileEntryId = m.fileEntryId,
                //                                 uploadUserName = n.firstName + " " + n.lastName,
                //                                 uploadDate = m.uploadDate,
                //                                 FileName = m.FileName,
                //                                 File = m.File,
                //                                 Description = m.description
                //                             }).ToList();
                //model.UploadPODeliveryInfo = (from m in db.FileUploads
                //                              join n in db.Users on m.uploadUserId equals n.userId
                //                              where m.POId == pId && m.archivedFlag == false && m.PODeliveryInfo == true
                //                              select new FilePODetailInfo()
                //                              {
                //                                  opportunityId = m.opportunityId,
                //                                  fileEntryId = m.fileEntryId,
                //                                  uploadUserName = n.firstName + " " + n.lastName,
                //                                  uploadDate = m.uploadDate,
                //                                  FileName = m.FileName,
                //                                  File = m.File,
                //                                  Description = m.description
                //                              }).ToList();
                //model.UploadPOInvFromVendor = (from m in db.FileUploads
                //                          join n in db.Users on m.uploadUserId equals n.userId
                //                          where m.POId == pId && m.archivedFlag == false && m.POInvFromSupplier == true
                //                               select new FilePODetailInfo()
                //                          {
                //                              opportunityId = m.opportunityId,
                //                              fileEntryId = m.fileEntryId,
                //                              uploadUserName = n.firstName + " " + n.lastName,
                //                              uploadDate = m.uploadDate,
                //                              FileName = m.FileName,
                //                              File = m.File,
                //                              Description = m.description
                //                          }).ToList();
                //model.UploadPOInvToCust = (from m in db.FileUploads
                //                               join n in db.Users on m.uploadUserId equals n.userId
                //                               where m.POId == pId && m.archivedFlag == false && m.POInvToCust == true
                //                           select new FilePODetailInfo()
                //                               {
                //                                   opportunityId = m.opportunityId,
                //                                   fileEntryId = m.fileEntryId,
                //                                   uploadUserName = n.firstName + " " + n.lastName,
                //                                   uploadDate = m.uploadDate,
                //                                   FileName = m.FileName,
                //                                   File = m.File,
                //                                   Description = m.description
                //                               }).ToList();               

                return PartialView(model);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public FileContentResult FileDownload(int id)
        {
            byte[] fileData;

            FileUpload fileRecord = db.FileUploads.Find(id);

            string fileName;
            string filename1 = fileRecord.FileName.Replace("\\", ",");
            string filename2 = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            fileData = (byte[])fileRecord.File.ToArray();
            fileName = filename2;

            return File(fileData, "text", fileName);
        }
        public JsonResult FileArchive(string selectedId)
        {
            int id = Int32.Parse(selectedId);

            FileUpload itemToArchive = db.FileUploads.SingleOrDefault(x => x.fileEntryId == id); //returns a single item.
            if (itemToArchive != null)
            {
                itemToArchive.archivedFlag = true;
                db.SaveChanges();
            }

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion

        #region QNA
        [Authorize]
        public ActionResult POQuestionAnswer(int POId)
        {
            if (User.Identity.IsAuthenticated && Session["UserId"] != null)
            {
                int Uid = Int32.Parse(Session["UserId"].ToString());
                int cId = Int32.Parse(Session["ContractId"].ToString());

                MainModel QNAModelList = new MainModel
                {
                    MessageListModel = DBQueryController.getMessageList(cId, POId, 0, 0, 0, Uid),
                    UserId = Uid
                };

                var NotiMemberList = (from m in db.Users
                                      join n in db.Users_Opportunity on m.userId equals n.userId into gy
                                      from x in gy.DefaultIfEmpty()
                                      where m.status == true && x.opportunityId == cId
                                      select new NotiMemberList()
                                      {
                                          Id = m.userId,
                                          Name = m.firstName + " " + m.lastName
                                      }).ToList().OrderBy(c => c.Name);

                ViewBag.NotiMemberList = new MultiSelectList(NotiMemberList, "Id", "Name");

                return PartialView(QNAModelList);
            }
            else
            {
                return Redirect("~/Home/Index");
            }
        }
        public virtual ActionResult PODocUpload(MainModel fd)
        {
            FileUpload fileUploadModel = new FileUpload();
            int userId = fd.UserId;
            DateTime createDate = DateTime.Now;
            string filename1 = fd.file.FileName.Replace("\\", ",");
            string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
            string fullPath = fd.file.FileName;
            string extension = Path.GetExtension(fullPath);
            byte[] uploadFile = new byte[fd.file.InputStream.Length];

            fileUploadModel.uuid = Guid.NewGuid();
            fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
            fileUploadModel.uploadUserId = userId;
            fileUploadModel.uploadDate = createDate;
            fileUploadModel.FullPath = fullPath;
            fileUploadModel.FileName = filename;
            fileUploadModel.description = fd.FileDescription;
            fileUploadModel.Extension = extension;
            fileUploadModel.archivedFlag = false;
            fileUploadModel.notPO = false;
            fd.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
            fileUploadModel.File = uploadFile;
            db.FileUploads.Add(fileUploadModel);
            db.SaveChanges();

            POs_FileUpload fileUploadModel2 = new POs_FileUpload
            {
                uuid = Guid.NewGuid(),
                fileEntryId = fileUploadModel.fileEntryId,
                POId = fd.POId
            };
            db.POs_FileUpload.Add(fileUploadModel2);

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
                POId = Int32.Parse(Session["POId"].ToString()),
                createDate = DateTime.Now,
                fromUserId = Int32.Parse(Session["UserId"].ToString()),
                content = "uploaded " + filename,
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);

            db.SaveChanges();

            //return PartialView(db.FileUploads.ToList());
            return Json(new { Result = String.Format("Test berjaya!") });
        }

        [Authorize]
        public JsonResult POSendMessage(string UserId, string POId, string Content, List<int> selectedUserId)
        {
            int pId = Int32.Parse(POId);
            int fromUserID = Int32.Parse(UserId);
            int cId = Int32.Parse(Session["ContractId"].ToString());
            var myEmail = db.Users.SingleOrDefault(x => x.userId == fromUserID);

            Notification _objSendMessage = new Notification
            {
                uuid = Guid.NewGuid(),
                opportunityId = cId,
                POId = pId,
                createDate = DateTime.Now,
                fromUserId = fromUserID,
                content = Content,
                QNAtype = "Message",
                flag = 0
            };
            db.Notifications.Add(_objSendMessage);
            db.SaveChanges();

            foreach (var model in selectedUserId)
            {
                //toUserId = Int32.Parse(((string[])(selectedUserId))[i].Split(',')[0]);
                var UserEmail = db.Users.SingleOrDefault(x => x.userId == model);
                var POInfo = (from m in db.PurchaseOrders
                              join n in db.Users on m.createByUserId equals n.userId
                              join o in db.Opportunities on m.opportunityId equals o.opportunityId
                              join p in db.Customers on o.custId equals p.custId
                              //join p in db.OpportunityTypes on m.typeId equals p.typeId
                              where m.POId == pId
                              select new MailModel()
                              {
                                  CustName = p.name + " (" + p.abbreviation + ")",
                                  CustAbbreviation = p.abbreviation,
                                  ContractName = o.name,
                                  Description = o.description,
                                  PODescription = m.Details_projectDescription,
                                  PONo = m.Details_PONo,
                                  //ContractType = p.type,
                                  AssignTo = n.firstName + " " + n.lastName
                              }).FirstOrDefault();

                string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/POEmailTemplate.cshtml"));
                POInfo.Content = Content;
                POInfo.FromName = myEmail.emailAddress;
                POInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, 
                    Url.Content("~/POList/ViewPO") + "?cId=" + cId + "&pId=" + pId + "#POQuestionAnswer-1");
                var result = Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "POTemplateKey", null, POInfo);

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("info@kubtel.com")
                };
                //mail.From = new MailAddress("info@kubtel.com");
                mail.To.Add(new MailAddress(UserEmail.emailAddress));
                //mail.To.Add(new MailAddress("info@kubtel.com"));
                mail.Subject = "OCM: Updates on " + POInfo.ContractName + " for " + POInfo.CustAbbreviation;
                mail.Body = result;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient
                {
                    //smtp.Host = "pops.kub.com";
                    Host = "outlook.office365.com",
                    //smtp.Host = "smtp.gmail.com";
                    Port = 587,
                    //smtp.Port = 25;
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
                };
                //smtp.Credentials = new System.Net.NetworkCredential("ocm@kubtel.com", "welcome123$");
                smtp.Send(mail);

                NotiGroup _objSendToUserId = new NotiGroup
                {
                    uuid = Guid.NewGuid(),
                    chatId = _objSendMessage.chatId,
                    toUserId = model
                };
                db.NotiGroups.Add(_objSendToUserId);
            }
            db.SaveChanges();

            return Json(new { Result = String.Format("Test berjaya!") });
        }
        #endregion
        #endregion
    }
}

//#region Procurement & Finance Phasessd
//[Authorize]
//public ActionResult ProcurementPhase()
//{
//    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
//    {
//        int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
//        MainModel PODetailList = new MainModel
//        {
//            OppContId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = POId,
//            CreateByUserId = userid,
//            PPDetail = (from m in db.PurchaseOrders
//                        join n in db.Users on m.createByUserId equals n.userId into s
//                        from x in s.DefaultIfEmpty()
//                        where m.POId == POId
//                        select new PPList()
//                        {
//                            PP_DocReceivedDate = m.PP_DocReceivedDate,
//                            PP_TypeOfDoc = m.PP_TypeOfDoc,
//                            PP_PONo = m.PP_PONo,
//                            PP_Remark = m.PP_Remark,
//                            PP_CompleteDate = m.PP_CompleteDate,
//                            PP_Update = m.PP_Update
//                        }).FirstOrDefault(),
//            RoleId = Session["RoleId"].ToString().Trim()
//        };
//        Session["PP_Update"] = PODetailList.PPDetail.PP_Update;
//        PODetailList.objCanEdit = (from m in db.Users_Opportunity
//                                   where m.opportunityId == PODetailList.OppContId && m.userId == userid
//                                   select new EditPermission()
//                                   {
//                                       canEdits = m.canEdit
//                                   }).FirstOrDefault();

//        return PartialView(PODetailList);
//    }
//    else
//    {
//        return Redirect("~/Home/Index");
//    }
//}

//[HttpPost]
//public ActionResult ProcurementPhase(MainModel x)
//{
//    if (bool.Parse(Session["PP_Update"].ToString()) == false && x.file == null)
//    {
//        ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
//    }
//    if (!ModelState.IsValid)
//    {
//        return PartialView("ProcurementPhase", x);
//    }
//    int PoId = x.POId;
//    int userid = Int32.Parse(Session["UserId"].ToString());
//    PurchaseOrder objPODetaildb = db.PurchaseOrders.First(m => m.POId == PoId);

//    if (objPODetaildb.PP_DocReceivedDate != x.PPDetail.PP_DocReceivedDate)
//    {
//        Notification _objDetails_PDInfoDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.PP_DocReceivedDate == null ? "change the Doc received date to " + x.PPDetail.PP_DocReceivedDate :
//            "change the Doc received date from " + objPODetaildb.PP_DocReceivedDate + " to " + x.PPDetail.PP_DocReceivedDate
//        };
//        db.Notifications.Add(_objDetails_PDInfoDetail);
//        objPODetaildb.PP_DocReceivedDate = x.PPDetail.PP_DocReceivedDate;
//    }
//    if (objPODetaildb.PP_TypeOfDoc != x.PPDetail.PP_TypeOfDoc)
//    {
//        Notification _objDetails_PDInfoDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.PP_TypeOfDoc == null ? "change the Type Of Doc to " + x.PPDetail.PP_TypeOfDoc :
//            "change the Type Of Doc from " + objPODetaildb.PP_TypeOfDoc + " to " + x.PPDetail.PP_TypeOfDoc
//        };
//        db.Notifications.Add(_objDetails_PDInfoDetail);
//        objPODetaildb.PP_TypeOfDoc = x.PPDetail.PP_TypeOfDoc;
//    }
//    if (objPODetaildb.PP_PONo != x.PPDetail.PP_PONo)
//    {
//        Notification _objDetails_PDInfoDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.PP_PONo == null ? "change the PO No. to " + x.PPDetail.PP_PONo :
//            "change the PO No. from " + objPODetaildb.PP_PONo + " to " + x.PPDetail.PP_PONo
//        };
//        db.Notifications.Add(_objDetails_PDInfoDetail);
//        objPODetaildb.PP_PONo = x.PPDetail.PP_PONo;
//    }
//    if (objPODetaildb.PP_Remark != x.PPDetail.PP_Remark)
//    {
//        Notification _objDetails_PDInfoDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.PP_Remark == null ? "change the remark to " + x.PPDetail.PP_Remark :
//            "change the remark from " + objPODetaildb.PP_Remark + " to " + x.PPDetail.PP_Remark
//        };
//        db.Notifications.Add(_objDetails_PDInfoDetail);
//        objPODetaildb.PP_Remark = x.PPDetail.PP_Remark;
//    }
//    objPODetaildb.PP_CompleteDate = DateTime.Now;
//    x.PPDetail.PP_CompleteDate = objPODetaildb.PP_CompleteDate;

//    if (x.file != null)
//    {
//        FileUpload fileUploadModel = new FileUpload();
//        DateTime createDate = DateTime.Now;
//        string filename1 = x.file.FileName.Replace("\\", ",");
//        string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
//        string fullPath = x.file.FileName;
//        string extension = Path.GetExtension(fullPath);
//        byte[] uploadFile = new byte[x.file.InputStream.Length];

//        fileUploadModel.uuid = Guid.NewGuid();
//        fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
//        fileUploadModel.uploadUserId = userid;
//        fileUploadModel.uploadDate = createDate;
//        fileUploadModel.FullPath = fullPath;
//        fileUploadModel.FileName = filename;
//        fileUploadModel.description = x.FileDescription;
//        fileUploadModel.Extension = extension;
//        fileUploadModel.archivedFlag = false;
//        fileUploadModel.POPPInfo = true;
//        fileUploadModel.notPO = false;
//        x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
//        fileUploadModel.File = uploadFile;
//        db.FileUploads.Add(fileUploadModel);
//        db.SaveChanges();

//        POs_FileUpload fileUploadModel2 = new POs_FileUpload
//        {
//            uuid = Guid.NewGuid(),
//            fileEntryId = fileUploadModel.fileEntryId,
//            POId = PoId
//        };
//        db.POs_FileUpload.Add(fileUploadModel2);

//        Notification _objSendMessage = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            content = "uploaded " + filename,
//            flag = 0
//        };
//        db.Notifications.Add(_objSendMessage);
//    }

//    x.objCanEdit = (from m in db.Users_Opportunity
//                    where m.opportunityId == x.OppContId && m.userId == userid
//                    select new EditPermission()
//                    {
//                        canEdits = m.canEdit
//                    }).FirstOrDefault();
//    try
//    {
//        objPODetaildb.PP_Update = true;
//        Session["PP_Update"] = objPODetaildb.PP_Update;
//        db.SaveChanges();
//        x.SaveSuccess = true;
//        x.RoleId = Session["RoleId"].ToString();

//        List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
//        NotiMemberList = DBQueryController.getNotiMemberList(x.OppContId);

//        Notification _objSendPOMessage = new Notification();
//        string Content = "Procurement Department Process Info";
//        _objSendPOMessage.uuid = Guid.NewGuid();
//        _objSendPOMessage.opportunityId = x.OppContId;
//        _objSendPOMessage.POId = x.POId;
//        _objSendPOMessage.createDate = DateTime.Now;
//        _objSendPOMessage.fromUserId = userid;
//        _objSendPOMessage.content = "Send email(s) updates for " + Content;
//        _objSendPOMessage.QNAtype = "Message";
//        _objSendPOMessage.flag = 0;
//        db.Notifications.Add(_objSendPOMessage);
//        db.SaveChanges();

//        foreach (var model in NotiMemberList)
//        {
//                var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
//                var myEmail = db.Users.SingleOrDefault(y => y.userId == userid);
//                var POInfo = (from m in db.PurchaseOrders
//                              join n in db.Users on m.createByUserId equals n.userId
//                              join o in db.Opportunities on m.opportunityId equals o.opportunityId
//                              join p in db.Customers on o.custId equals p.custId
//                              where m.POId == x.POId
//                              select new MailModel()
//                              {
//                                  CustName = p.name + " (" + p.abbreviation + ")",
//                                  ContractName = o.name,
//                                  Description = o.description,
//                                  PODescription = m.Details_projectDescription,
//                                  PONo = m.Details_PONo,
//                                  AssignTo = n.firstName + " " + n.lastName
//                              }).FirstOrDefault();

//                string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/POEmailTemplate.cshtml"));
//                POInfo.POFlow = Content;
//                POInfo.Content = myEmail.userName + " has register new " + POInfo.POFlow + ". Please refer the details as below: ";
//                POInfo.FromName = myEmail.emailAddress;
//                POInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/POList/ViewPO") + "?cId=" + x.OppContId + "&pId=" + PoId);
//                var result =
//                        Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PPInfoTemplateKey", null, POInfo);

//            MailMessage mail = new MailMessage
//            {
//                From = new MailAddress("info@kubtel.com")
//            };
//            mail.To.Add(new MailAddress(UserEmail.emailAddress));
//                mail.Subject = "OCM: Updates on " + POInfo.POFlow + " for PO Number " + POInfo.PONo;
//                mail.Body = result;
//                mail.IsBodyHtml = true;
//            SmtpClient smtp = new SmtpClient
//            {
//                Host = "outlook.office365.com",
//                Port = 587,
//                EnableSsl = true,
//                Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
//            };
//            smtp.Send(mail);

//            NotiGroup _objSendToUserId = new NotiGroup
//            {
//                uuid = Guid.NewGuid(),
//                chatId = _objSendPOMessage.chatId,
//                toUserId = model.Id
//            };
//            db.NotiGroups.Add(_objSendToUserId);
//                db.SaveChanges();
//        }
//    }
//    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
//    {
//        Exception raise = dbEx;
//        foreach (var validationErrors in dbEx.EntityValidationErrors)
//        {
//            foreach (var validationError in validationErrors.ValidationErrors)
//            {
//                string message = string.Format("{0}:{1}",
//                    validationErrors.Entry.Entity.ToString(),
//                    validationError.ErrorMessage);
//                // raise a new exception nesting
//                // the current instance as InnerException
//                raise = new InvalidOperationException(message, raise);
//            }
//        }
//        throw raise;
//    }
//    return PartialView("ProcurementPhase", x);
//}
//#endregion

//#region CEOOffice
//[Authorize]
//public ActionResult CEOOffice()
//{
//    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
//    {
//        int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
//        MainModel PODetailList = new MainModel
//        {
//            OppContId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = POId,
//            CreateByUserId = userid,
//            CEOOfficeDetail = (from m in db.PurchaseOrders
//                               join n in db.Users on m.createByUserId equals n.userId into s
//                               from x in s.DefaultIfEmpty()
//                               where m.POId == POId
//                               select new CEOOfficeList()
//                               {
//                                   CEOO_DocReceivedDate = m.CEOO_DocReceivedDate,
//                                   CEOO_TypeOfDoc = m.CEOO_TypeOfDoc,
//                                   CEOO_Remark = m.CEOO_Remark,
//                                   CEOO_CompleteDate = m.CEOO_CompleteDate,
//                                   CEOO_Update = m.CEOO_Update
//                               }).FirstOrDefault(),
//            RoleId = Session["RoleId"].ToString().Trim()
//        };
//        Session["CEOO_Update"] = PODetailList.CEOOfficeDetail.CEOO_Update;
//        PODetailList.objCanEdit = (from m in db.Users_Opportunity
//                                   where m.opportunityId == PODetailList.OppContId && m.userId == userid
//                                   select new EditPermission()
//                                   {
//                                       canEdits = m.canEdit
//                                   }).FirstOrDefault();

//        return PartialView(PODetailList);
//    }
//    else
//    {
//        return Redirect("~/Home/Index");
//    }
//}

//[HttpPost]
//public ActionResult CEOOffice(MainModel x)
//{           
//    if (bool.Parse(Session["CEOO_Update"].ToString()) == false && x.file == null)
//    {
//        ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
//    }
//    if (!ModelState.IsValid)
//    {
//        return PartialView("CEOOffice", x);
//    }
//    int PoId = x.POId;
//    int userid = Int32.Parse(Session["UserId"].ToString());
//    PurchaseOrder objPODetaildb = db.PurchaseOrders.First(m => m.POId == PoId);

//    if (objPODetaildb.CEOO_DocReceivedDate != x.CEOOfficeDetail.CEOO_DocReceivedDate)
//    {
//        Notification _objDetails_CEOOfficeDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.CEOO_DocReceivedDate == null ? "change the doc received date to " + x.CEOOfficeDetail.CEOO_DocReceivedDate :
//            "change the doc received date from " + objPODetaildb.CEOO_DocReceivedDate + " to " + x.CEOOfficeDetail.CEOO_DocReceivedDate
//        };
//        db.Notifications.Add(_objDetails_CEOOfficeDetail);
//        objPODetaildb.CEOO_DocReceivedDate = x.CEOOfficeDetail.CEOO_DocReceivedDate;
//    }
//    if (objPODetaildb.CEOO_TypeOfDoc != x.CEOOfficeDetail.CEOO_TypeOfDoc)
//    {
//        Notification _objDetails_CEOOfficeDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.CEOO_TypeOfDoc == null ? "change the type of doc to " + x.CEOOfficeDetail.CEOO_TypeOfDoc :
//            "change the type of doc from " + objPODetaildb.CEOO_TypeOfDoc + " to " + x.CEOOfficeDetail.CEOO_TypeOfDoc
//        };
//        db.Notifications.Add(_objDetails_CEOOfficeDetail);
//        objPODetaildb.CEOO_TypeOfDoc = x.CEOOfficeDetail.CEOO_TypeOfDoc;
//    }
//    if (objPODetaildb.CEOO_Remark != x.CEOOfficeDetail.CEOO_Remark)
//    {
//        Notification _objDetails_CEOOfficeDetail = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            flag = 0,
//            content = objPODetaildb.CEOO_Remark == null ? "change the remark to " + x.CEOOfficeDetail.CEOO_Remark :
//            "change the remark from " + objPODetaildb.CEOO_Remark + " to " + x.CEOOfficeDetail.CEOO_Remark
//        };
//        db.Notifications.Add(_objDetails_CEOOfficeDetail);
//        objPODetaildb.CEOO_Remark = x.CEOOfficeDetail.CEOO_Remark;
//    }
//    objPODetaildb.CEOO_CompleteDate = DateTime.Now;
//    x.CEOOfficeDetail.CEOO_CompleteDate = objPODetaildb.CEOO_CompleteDate;

//    if (x.file != null)
//    {
//        FileUpload fileUploadModel = new FileUpload();
//        DateTime createDate = DateTime.Now;
//        string filename1 = x.file.FileName.Replace("\\", ",");
//        string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
//        string fullPath = x.file.FileName;
//        string extension = Path.GetExtension(fullPath);
//        byte[] uploadFile = new byte[x.file.InputStream.Length];

//        fileUploadModel.uuid = Guid.NewGuid();
//        fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
//        fileUploadModel.uploadUserId = userid;
//        fileUploadModel.uploadDate = createDate;
//        fileUploadModel.FullPath = fullPath;
//        fileUploadModel.FileName = filename;
//        fileUploadModel.description = x.FileDescription;
//        fileUploadModel.Extension = extension;
//        fileUploadModel.archivedFlag = false;
//        fileUploadModel.POCEOOInfo = true;
//        fileUploadModel.notPO = false;
//        x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
//        fileUploadModel.File = uploadFile;
//        db.FileUploads.Add(fileUploadModel);
//        db.SaveChanges();

//        POs_FileUpload fileUploadModel2 = new POs_FileUpload
//        {
//            uuid = Guid.NewGuid(),
//            fileEntryId = fileUploadModel.fileEntryId,
//            POId = PoId
//        };
//        db.POs_FileUpload.Add(fileUploadModel2);

//        Notification _objSendMessage = new Notification
//        {
//            uuid = Guid.NewGuid(),
//            opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = Int32.Parse(Session["POId"].ToString()),
//            createDate = DateTime.Now,
//            fromUserId = Int32.Parse(Session["UserId"].ToString()),
//            content = "uploaded " + filename,
//            flag = 0
//        };
//        db.Notifications.Add(_objSendMessage);
//    }

//    x.objCanEdit = (from m in db.Users_Opportunity
//                    where m.opportunityId == x.OppContId && m.userId == userid
//                    select new EditPermission()
//                    {
//                        canEdits = m.canEdit
//                    }).FirstOrDefault();
//    try
//    {
//        objPODetaildb.CEOO_Update = true;
//        Session["CEOO_Update"] = objPODetaildb.CEOO_Update;
//        db.SaveChanges();
//        x.SaveSuccess = true;
//        x.RoleId = Session["RoleId"].ToString();

//        List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
//        NotiMemberList = DBQueryController.getNotiMemberList(x.OppContId);

//        Notification _objSendPOMessage = new Notification();
//        string Content = "CEO Office Process Info";
//        _objSendPOMessage.uuid = Guid.NewGuid();
//        _objSendPOMessage.opportunityId = x.OppContId;
//        _objSendPOMessage.POId = x.POId;
//        _objSendPOMessage.createDate = DateTime.Now;
//        _objSendPOMessage.fromUserId = userid;
//        _objSendPOMessage.content = "Send email(s) updates for " + Content;
//        _objSendPOMessage.QNAtype = "Message";
//        _objSendPOMessage.flag = 0;
//        db.Notifications.Add(_objSendPOMessage);
//        db.SaveChanges();

//        foreach (var model in NotiMemberList)
//        {
//                var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
//                var myEmail = db.Users.SingleOrDefault(y => y.userId == userid);
//                var POInfo = (from m in db.PurchaseOrders
//                              join n in db.Users on m.createByUserId equals n.userId
//                              join o in db.Opportunities on m.opportunityId equals o.opportunityId
//                              join p in db.Customers on o.custId equals p.custId
//                              where m.POId == x.POId
//                              select new MailModel()
//                              {
//                                  CustName = p.name + " (" + p.abbreviation + ")",
//                                  ContractName = o.name,
//                                  Description = o.description,
//                                  PODescription = m.Details_projectDescription,
//                                  PONo = m.Details_PONo,
//                                  AssignTo = n.firstName + " " + n.lastName
//                              }).FirstOrDefault();

//                string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/POEmailTemplate.cshtml"));
//                POInfo.POFlow = Content;
//                POInfo.Content = myEmail.userName + " has register new " + POInfo.POFlow + ". Please refer the details as below: ";
//                POInfo.FromName = myEmail.emailAddress;
//                POInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/POList/ViewPO") + "?cId=" + x.OppContId + "&pId=" + PoId);
//                var result =
//                        Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PRInfoTemplateKey", null, POInfo);

//            MailMessage mail = new MailMessage
//            {
//                From = new MailAddress("info@kubtel.com")
//            };
//            mail.To.Add(new MailAddress(UserEmail.emailAddress));
//                mail.Subject = "OCM: Updates on " + POInfo.POFlow + " for PO Number " + POInfo.PONo;
//                mail.Body = result;
//                mail.IsBodyHtml = true;
//            SmtpClient smtp = new SmtpClient
//            {
//                Host = "outlook.office365.com",
//                Port = 587,
//                EnableSsl = true,
//                Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
//            };
//            smtp.Send(mail);

//            NotiGroup _objSendToUserId = new NotiGroup
//            {
//                uuid = Guid.NewGuid(),
//                chatId = _objSendPOMessage.chatId,
//                toUserId = model.Id
//            };
//            db.NotiGroups.Add(_objSendToUserId);
//                db.SaveChanges();
//        }
//    }
//    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
//    {
//        Exception raise = dbEx;
//        foreach (var validationErrors in dbEx.EntityValidationErrors)
//        {
//            foreach (var validationError in validationErrors.ValidationErrors)
//            {
//                string message = string.Format("{0}:{1}",
//                    validationErrors.Entry.Entity.ToString(),
//                    validationError.ErrorMessage);
//                // raise a new exception nesting
//                // the current instance as InnerException
//                raise = new InvalidOperationException(message, raise);
//            }
//        }
//        throw raise;
//    }
//    return PartialView("CEOOffice", x);
//}
//#endregion

//#region PO2Supplier
//[Authorize]
//public ActionResult PO2Supplier()
//{
//    if (User.Identity.IsAuthenticated && Session["UserId"] != null)
//    {
//        int POId = Int32.Parse(Session["POId"].ToString()); int userid = Int32.Parse(Session["UserId"].ToString());
//        MainModel PODetailList = new MainModel
//        {
//            OppContId = Int32.Parse(Session["ContractId"].ToString()),
//            POId = POId,
//            CreateByUserId = userid,
//            PO2SupplierDetail = (from m in db.PurchaseOrders
//                                 join n in db.Users on m.createByUserId equals n.userId into s
//                                 from x in s.DefaultIfEmpty()
//                                 where m.POId == POId
//                                 select new PO2SupplierList()
//                                 {
//                                     VS_DocReceivedDate = m.VS_DocReceivedDate,
//                                     VS_TypeOfDoc = m.VS_TypeOfDoc,
//                                     VS_Remarks = m.VS_Remarks,
//                                     VS_POIssueDate = m.PP_CompleteDate,
//                                     VS_PONo = m.PP_PONo,
//                                     VS_POAmount = m.VS_POAmount,
//                                     VS_TotalGST = m.VS_TotalGST,
//                                     VS_AwardDate = m.VS_AwardDate,
//                                     VS_CompanyName = m.VS_CompanyName,
//                                     VS_ContactPerson = m.VS_ContactPerson,
//                                     VS_TelephoneNo = m.VS_TelephoneNo,
//                                     VS_CompleteDate = m.VS_CompleteDate,
//                                     VS_Update = m.VS_Update
//                                 }).FirstOrDefault(),
//            RoleId = Session["RoleId"].ToString().Trim()
//        };
//        Session["VS_Update"] = PODetailList.PO2SupplierDetail.VS_Update;
//        PODetailList.objCanEdit = (from m in db.Users_Opportunity
//                                   where m.opportunityId == PODetailList.OppContId && m.userId == userid
//                                   select new EditPermission()
//                                   {
//                                       canEdits = m.canEdit
//                                   }).FirstOrDefault();

//        return PartialView(PODetailList);
//    }
//    else
//    {
//        return Redirect("~/Home/Index");
//    }
//}

//[HttpPost]
//public ActionResult PO2Supplier(MainModel x)
//{
//    if (User.Identity.IsAuthenticated)
//    {               
//        if (bool.Parse(Session["VS_Update"].ToString()) == false && x.file == null)
//        {
//            ModelState.AddModelError("file", "Please input the attachment before proceed."); ;
//        }
//        if (!ModelState.IsValid)
//        {
//            return PartialView("PO2Supplier", x);
//        }
//        int PoId = x.POId;
//        int userid = Int32.Parse(Session["UserId"].ToString());
//        PurchaseOrder objPODetaildb = db.PurchaseOrders.First(m => m.POId == PoId);

//        if (objPODetaildb.VS_DocReceivedDate != x.PO2SupplierDetail.VS_DocReceivedDate)
//        {
//            Notification _objPO2Supplier_IssueDate = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_DocReceivedDate == null ? "change Doc Received Date to " + x.PO2SupplierDetail.VS_DocReceivedDate :
//                "change Doc Received Date Date from " + objPODetaildb.VS_DocReceivedDate + " to " + x.PO2SupplierDetail.VS_DocReceivedDate,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_IssueDate);
//            objPODetaildb.VS_DocReceivedDate = x.PO2SupplierDetail.VS_DocReceivedDate;
//        }
//        if (objPODetaildb.VS_TypeOfDoc != x.PO2SupplierDetail.VS_TypeOfDoc)
//        {
//            Notification _objPO2Supplier_IssueDate = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_TypeOfDoc == null ? "change Type of Doc to " + x.PO2SupplierDetail.VS_TypeOfDoc :
//                "change Type of Doc from " + objPODetaildb.VS_TypeOfDoc + " to " + x.PO2SupplierDetail.VS_TypeOfDoc,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_IssueDate);
//            objPODetaildb.VS_TypeOfDoc = x.PO2SupplierDetail.VS_TypeOfDoc;
//        }
//        if (objPODetaildb.VS_Remarks != x.PO2SupplierDetail.VS_Remarks)
//        {
//            Notification _objPO2Supplier_Remarks = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_Remarks == null ? "change remarks to " + x.PO2SupplierDetail.VS_Remarks :
//                "change remarks from " + objPODetaildb.VS_Remarks + " to " + x.PO2SupplierDetail.VS_Remarks,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_Remarks);
//            objPODetaildb.VS_Remarks = x.PO2SupplierDetail.VS_Remarks;
//        }
//        if (objPODetaildb.VS_AwardDate != x.PO2SupplierDetail.VS_AwardDate)
//        {
//            Notification _objPO2Supplier_PONo = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_AwardDate == null ? "change Award Date to " + x.PO2SupplierDetail.VS_AwardDate :
//                "change Award Date from " + objPODetaildb.VS_AwardDate + " to " + x.PO2SupplierDetail.VS_AwardDate,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_PONo);
//            objPODetaildb.VS_AwardDate = x.PO2SupplierDetail.VS_AwardDate;
//        }
//        if (objPODetaildb.VS_POAmount != x.PO2SupplierDetail.VS_POAmount)
//        {
//            Notification _objPO2Supplier_POAmount = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_POAmount == null ? "change PO Amount from " + x.PO2SupplierDetail.VS_POAmount :
//                "change PO Amount from " + objPODetaildb.VS_POAmount + " to " + x.PO2SupplierDetail.VS_POAmount,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_POAmount);
//            objPODetaildb.VS_POAmount = x.PO2SupplierDetail.VS_POAmount;
//        }
//        if (objPODetaildb.VS_TotalGST != x.PO2SupplierDetail.VS_TotalGST)
//        {
//            Notification _objPO2Supplier_TotalGST = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_TotalGST == null ? "change Total GST to " + x.PO2SupplierDetail.VS_TotalGST :
//                "change Total GST from " + objPODetaildb.VS_TotalGST + " to " + x.PO2SupplierDetail.VS_TotalGST,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_TotalGST);
//            objPODetaildb.VS_TotalGST = x.PO2SupplierDetail.VS_TotalGST;
//        }
//        if (objPODetaildb.VS_CompanyName != x.PO2SupplierDetail.VS_CompanyName)
//        {
//            Notification _objPO2Supplier_IssueDate = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_CompanyName == null ? "change Company Name to " + x.PO2SupplierDetail.VS_CompanyName :
//                "change Company Name from " + objPODetaildb.VS_CompanyName + " to " + x.PO2SupplierDetail.VS_CompanyName,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_IssueDate);
//            objPODetaildb.VS_CompanyName = x.PO2SupplierDetail.VS_CompanyName;
//        }
//        if (objPODetaildb.VS_ContactPerson != x.PO2SupplierDetail.VS_ContactPerson)
//        {
//            Notification _objPO2Supplier_IssueDate = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_ContactPerson == null ? "change Contact Person to " + x.PO2SupplierDetail.VS_ContactPerson :
//                "change Contact Person from " + objPODetaildb.VS_ContactPerson + " to " + x.PO2SupplierDetail.VS_ContactPerson,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_IssueDate);
//            objPODetaildb.VS_ContactPerson = x.PO2SupplierDetail.VS_ContactPerson;
//        }
//        if (objPODetaildb.VS_TelephoneNo != x.PO2SupplierDetail.VS_TelephoneNo)
//        {
//            Notification _objPO2Supplier_IssueDate = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = objPODetaildb.VS_TelephoneNo == null ? "change Telephone No. to " + x.PO2SupplierDetail.VS_TelephoneNo :
//                "change Telephone No. from " + objPODetaildb.VS_TelephoneNo + " to " + x.PO2SupplierDetail.VS_TelephoneNo,
//                flag = 0
//            };
//            db.Notifications.Add(_objPO2Supplier_IssueDate);
//            objPODetaildb.VS_TelephoneNo = x.PO2SupplierDetail.VS_TelephoneNo;
//        }
//        objPODetaildb.VS_CompleteDate = DateTime.Now;
//        x.PO2SupplierDetail.VS_CompleteDate = objPODetaildb.VS_CompleteDate;

//        if (x.file != null)
//        {
//            FileUpload fileUploadModel = new FileUpload();
//            DateTime createDate = DateTime.Now;
//            string filename1 = x.file.FileName.Replace("\\", ",");
//            string filename = filename1.Split(',')[filename1.Split(',').Length - 1].ToString();
//            string fullPath = x.file.FileName;
//            string extension = Path.GetExtension(fullPath);
//            byte[] uploadFile = new byte[x.file.InputStream.Length];

//            fileUploadModel.uuid = Guid.NewGuid();
//            fileUploadModel.opportunityId = Int32.Parse(Session["ContractId"].ToString());
//            fileUploadModel.uploadUserId = userid;
//            fileUploadModel.uploadDate = createDate;
//            fileUploadModel.FullPath = fullPath;
//            fileUploadModel.FileName = filename;
//            fileUploadModel.description = x.FileDescription;
//            fileUploadModel.Extension = extension;
//            fileUploadModel.archivedFlag = false;
//            fileUploadModel.POPO2Supplier = true;
//            fileUploadModel.notPO = false;
//            x.file.InputStream.Read(uploadFile, 0, uploadFile.Length);
//            fileUploadModel.File = uploadFile;
//            db.FileUploads.Add(fileUploadModel);
//            db.SaveChanges();

//            POs_FileUpload fileUploadModel2 = new POs_FileUpload
//            {
//                uuid = Guid.NewGuid(),
//                fileEntryId = fileUploadModel.fileEntryId,
//                POId = PoId
//            };
//            db.POs_FileUpload.Add(fileUploadModel2);

//            Notification _objSendMessage = new Notification
//            {
//                uuid = Guid.NewGuid(),
//                opportunityId = Int32.Parse(Session["ContractId"].ToString()),
//                POId = Int32.Parse(Session["POId"].ToString()),
//                createDate = DateTime.Now,
//                fromUserId = Int32.Parse(Session["UserId"].ToString()),
//                content = "uploaded " + filename,
//                flag = 0
//            };
//            db.Notifications.Add(_objSendMessage);
//        }              

//        int ContractId = Int32.Parse(Session["ContractId"].ToString());
//        x.objCanEdit = (from m in db.Users_Opportunity
//                        where m.opportunityId == ContractId && m.userId == userid
//                        select new EditPermission()
//                        {
//                            canEdits = m.canEdit
//                        }).FirstOrDefault();
//        try
//        {
//            objPODetaildb.VS_Update = true;
//            Session["VS_Update"] = objPODetaildb.VS_Update;
//            db.SaveChanges();
//            x.SaveSuccess = true;
//            x.RoleId = Session["RoleId"].ToString();

//            //if (Session["Details_POIssueDate"] != null)
//            //{
//            //    DateTime StartDate = DateTime.Parse(Session["Details_POIssueDate"].ToString());
//            //    DateTime EndDate = x.PO2SupplierDetail.VS_CompleteDate.Value;
//            //    if ((EndDate - StartDate).TotalDays > 15)
//            //    {
//            //        objPODetaildb.Status = "Red Alert";
//            //    }
//            //    else if ((EndDate - StartDate).TotalDays <= 15 && ((EndDate - StartDate).TotalDays > 10))
//            //    {
//            //        objPODetaildb.Status = "Yellow Alert";
//            //    }
//            //}

//            List<NotiMemberList> NotiMemberList = new List<NotiMemberList>();
//            NotiMemberList = DBQueryController.getNotiMemberList(x.OppContId);

//            Notification _objSendPOMessage = new Notification();
//            string Content = "Vendor/Supplier Process Info";
//            _objSendPOMessage.uuid = Guid.NewGuid();
//            _objSendPOMessage.opportunityId = x.OppContId;
//            _objSendPOMessage.POId = x.POId;
//            _objSendPOMessage.createDate = DateTime.Now;
//            _objSendPOMessage.fromUserId = userid;
//            _objSendPOMessage.content = "Send email(s) updates for " + Content;
//            _objSendPOMessage.QNAtype = "Message";
//            _objSendPOMessage.flag = 0;
//            db.Notifications.Add(_objSendPOMessage);
//            db.SaveChanges();

//            foreach (var model in NotiMemberList)
//            {
//                //if (model.Id == 2)
//                //{
//                    var UserEmail = db.Users.SingleOrDefault(z => z.userId == model.Id);
//                    var myEmail = db.Users.SingleOrDefault(y => y.userId == userid);
//                    var POInfo = (from m in db.PurchaseOrders
//                                  join n in db.Users on m.createByUserId equals n.userId
//                                  join o in db.Opportunities on m.opportunityId equals o.opportunityId
//                                  join p in db.Customers on o.custId equals p.custId
//                                  where m.POId == x.POId
//                                  select new MailModel()
//                                  {
//                                      CustName = p.name + " (" + p.abbreviation + ")",
//                                      ContractName = o.name,
//                                      Description = o.description,
//                                      PODescription = m.Details_projectDescription,
//                                      PONo = m.Details_PONo,
//                                      AssignTo = n.firstName + " " + n.lastName
//                                  }).FirstOrDefault();

//                    string templateFile = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Shared/POEmailTemplate.cshtml"));
//                    POInfo.POFlow = Content;
//                    POInfo.Content = myEmail.userName + " has register new " + POInfo.POFlow + ". Please refer the details as below: ";
//                    POInfo.FromName = myEmail.emailAddress;
//                    POInfo.BackLink = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/POList/ViewPO") + "?cId=" + x.OppContId + "&pId=" + PoId);
//                    var result =
//                            Engine.Razor.RunCompile(new LoadedTemplateSource(templateFile), "PRInfoTemplateKey", null, POInfo);

//                MailMessage mail = new MailMessage
//                {
//                    From = new MailAddress("info@kubtel.com")
//                };
//                mail.To.Add(new MailAddress(UserEmail.emailAddress));
//                    mail.Subject = "OCM: Updates on " + POInfo.POFlow + " for PO Number " + POInfo.PONo;
//                    mail.Body = result;
//                    mail.IsBodyHtml = true;
//                SmtpClient smtp = new SmtpClient
//                {
//                    Host = "outlook.office365.com",
//                    Port = 587,
//                    EnableSsl = true,
//                    Credentials = new System.Net.NetworkCredential("info@kubtel.com", "welcome123$")
//                };
//                smtp.Send(mail);

//                NotiGroup _objSendToUserId = new NotiGroup
//                {
//                    uuid = Guid.NewGuid(),
//                    chatId = _objSendPOMessage.chatId,
//                    toUserId = model.Id
//                };
//                db.NotiGroups.Add(_objSendToUserId);
//                    db.SaveChanges();
//                //}
//            }
//        }
//        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
//        {
//            Exception raise = dbEx;
//            foreach (var validationErrors in dbEx.EntityValidationErrors)
//            {
//                foreach (var validationError in validationErrors.ValidationErrors)
//                {
//                    string message = string.Format("{0}:{1}",
//                        validationErrors.Entry.Entity.ToString(),
//                        validationError.ErrorMessage);
//                    // raise a new exception nesting
//                    // the current instance as InnerException
//                    raise = new InvalidOperationException(message, raise);
//                }
//            }
//            throw raise;
//        }
//        //return Json(new { success = true });
//        return PartialView("PO2Supplier", x);
//    }
//    else
//    {
//        return Redirect("~/Home/Index");
//    }
//}
//#endregion
